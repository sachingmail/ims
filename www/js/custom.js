$(function() {
       var tblList =  $('#userdetails').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
    	});
    });
    
    function deleteUser(id){
    	var confirmdel = confirm('Are You Sure');
        if(confirmdel){
        var userid = id;
    	$.ajax({
            url:"/user/delete",
            type:"post",
            data:{id:userid},
            success:function(data){
            	window.location= '/user/list';	
			
            }         
    	});
    }}
    function deleteCard(id){
    	var confirmdel = confirm('Are You Sure');
        if(confirmdel){
        var cardid = id;
    	$.ajax({
            url:"/card-type/delete",
            type:"post",
            data:{id:cardid},
            success:function(data){
            	window.location= '/card-type';	
			
            }         
    	});
    }}
    function editUser(id){
        var userid = id;
        window.location= '/user/edit/userid/'+userid;
    }
    function editProject(id){
        var proid = id;
        window.location= '/project/edit/projectid/'+proid;
    }
    function viewProject(id){
        var proid = id;
        window.location= '/project/uploadxml/projectid/'+proid;
    }
    