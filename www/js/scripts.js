/*
 * @Function Users
 * @author  Amit Kumar
 * This function used for User update, delete.
 */

function Users() {
    this.edit = function (id) {
        document.location.href = '/users/edit-user/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/users';
    };

    this.deleteUser = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this user?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/users/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/users?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Users = new Users();

function Countries() {
    this.edit = function (id) {
        document.location.href = '/country/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/country';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Country?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/country/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/country?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Countries = new Countries();

/*
 * @Function Roles
 * @author  Amit Kumar
 * This function used for manage update, delete of roles.
 */

function Roles() {
    this.edit = function (id) {
        document.location.href = '/roles/edit-role/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/roles';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this role?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/roles/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/roles?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.SavePermissions = function (totRow, roleId) {
        data = new Array();
        //push the data into the array
        for (i = 1; i <= parseInt(totRow); i++) {
            innerArray = new Array();
            var screen = $('#screen' + i).val();
            if ($("#chkview" + i).is(':checked'))
                var cView = 1;
            else
                cView = 0;

//            if ($("#chkadd" + i).is(':checked'))
//                var cAdd = 1;
//            else
//                cAdd = 0;
//
//            if ($("#chkedit" + i).is(':checked'))
//                var cEdit = 1;
//            else
//                cEdit = 0;
//
//            if ($("#chkdel" + i).is(':checked'))
//                var cDel = 1;
//            else
//                cDel = 0;

            var chkview = cView;
//            var chkadd = cAdd;
//            var chkedit = cEdit;
//            var chkdel = cDel;

            innerArray.push(screen);
            innerArray.push(chkview);
//            innerArray.push(chkadd);
//            innerArray.push(chkedit);
//            innerArray.push(chkdel);
            data.push(innerArray);

        }
        // send the data to controller and action
        $.ajax({
            type: 'POST',
            url: "/roles/add-permissions",
            data: {'data[]': data, roleId: roleId},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                document.location.href = '/roles?status=pupdated';

            }
        });

    };
}

var Roles = new Roles();

function Carriers() {
    this.edit = function (id) {
        document.location.href = '/carrier/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/carrier';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Carrier?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/carrier/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Carriers = new Carriers();


function Cards() {
    this.edit = function (id) {
        document.location.href = '/card-type/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/card-type';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Card?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/card-type/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Cards = new Cards();

function Employee() {
    this.edit = function (id) {
        document.location.href = '/employee/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/employee';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Employee?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/employee/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Employee = new Employee();

function Locations() {
    this.edit = function (id) {
        document.location.href = '/inventory-location/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/inventory-location';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Location?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/inventory-location/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Locations = new Locations();

function Currencies() {
    this.edit = function (id) {
        document.location.href = '/currency/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/currency';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Currency?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/currency/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Currencies = new Currencies();

function Units() {
    this.edit = function (id) {
        document.location.href = '/unit/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/unit';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Unit?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/unit/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Units = new Units();

function Payments() {
    this.edit = function (id) {
        document.location.href = '/payment-method/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/payment-method';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Payment Method?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/payment-method/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Payments = new Payments();

function Sales() {
    this.edit = function (id) {
        document.location.href = '/sale-reps/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/sale-reps';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Sales Representative?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/sales-reps/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Sales = new Sales();

function Terms() {
    this.edit = function (id) {
        document.location.href = '/payment-terms/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/payment-terms';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Payment Term?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/payment-terms/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Terms = new Terms();

function Products() {
    this.edit = function (id) {
        document.location.href = '/product-type/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/product-type';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Product Type?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/product-type/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Products = new Products();

function Pricing() {
    this.edit = function (id) {
        document.location.href = '/pricing-currency/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/pricing-currency';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Pricing Currency?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/pricing-currency/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.MakeDefaultPricingCurrency = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to make this Pricing Currency as Default?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Default Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/pricing-currency/make-default-pricing-currency",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/pricing-currency?status=default';
                                    SalesOrder.getPricingUnitPrice();

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Pricing = new Pricing();

function State() {
    this.edit = function (id) {
        document.location.href = '/state/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/state';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this State?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/state/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/state?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var State = new State();

function City() {
    this.edit = function (id) {
        document.location.href = '/city/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/city';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this City?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/city/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/city?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

}
;
var City = new City();

function Customers() {
    this.Add = function () {
        document.location.href = '/customer/add';
    };
    this.Edit = function (id) {
        document.location.href = '/customer/add/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/customer';
    };
    this.ChangeAddress = function () {
        var add_type = $("#address_type").val();
        if(add_type=='Bussiness Address'){
           $("#address").show();
            $("#address1").hide();
            $("#address2").hide();
        }else if(add_type=='Billing Address'){
            $("#address").hide();
            $("#address1").show();
            $("#address2").hide();
        }else if(add_type=='Shipping Address'){
            $("#address").hide();
            $("#address1").hide();
            $("#address2").show();
        }
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this customer?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/customer/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/customer?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.DeactivateCustomer = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Deactivate this Customer?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                url: "/customer/deactivate-customer",
                                data: {id: id},
                                success: function (data) {
                                    document.location.href = '/customer/add/id/' + id;
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });

    };
    this.ActivateCustomer = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Activate this Customer?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                url: "/customer/activate-customer",
                                data: {id: id},
                                success: function (data) {
                                    document.location.href = '/customer/add/id/' + id;
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
;
var Customers = new Customers();

function Receiving() {
    this.edit = function (id) {
        document.location.href = '/receiving-address/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/receiving-address';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Receiving Address?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/receiving-address/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/receiving-address?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

}
;
var Receiving = new Receiving();

function Taxing() {
    this.edit = function (id) {
        document.location.href = '/taxing-scheme/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/taxing-scheme';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Taxing Scheme?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/taxing-sxheme/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.Calculation = function () {

    };

}
;
var Taxing = new Taxing();

function Vendors() {
    this.Add = function () {
        document.location.href = '/vendor/add';
    };
    this.Edit = function (id) {
        document.location.href = '/vendor/add/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/vendor';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this vendor?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/vendor/delete/id/'+id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.DeactivateVendor = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to Deactivate this Vendor?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                             $('#dialog').dialog('close');
                             document.location.href = "/vendor/deactivate-vendor/id/" + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });

    };
    this.ActivateVendor = function (id) {
        
        $("#dialog").remove();
        data = "Are you sure you want to Activate this Vendor?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                             document.location.href = "/vendor/activate-vendor/id/" + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.deleteProduct = function (id, vendor_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/vendor/delete-product",
                                data: {id: id, vendor_id: vendor_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                    $("#vendor_product_id").val('');
                                    $("#product_code").val('');
                                    $("#vendor_pcode").val('');
                                    $("#product_cost").val('');

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.AddProduct = function () {
        var id = $("#id").val();
        var vendor_product_id = $("#vendor_product_id").val();
        var product_code = $("#product_code").val();
        var vendor_pcode = $("#vendor_pcode").val();
        var product_cost = $("#product_cost").val();
        var pattern = /^\d+\.?\d{0,2}$/;
        var pattern_code = /^[a-zA-Z0-9- ]*$/;
        var errorCode = 0;
        if (product_code == '') {
            $("#perror").html('Please enter valid product Name');
            errorCode = 1;
        }
        else {
            $("#vpcerror").html('');
        }
        if (vendor_pcode == '') {
            $("#vpcerror").html('Please enter valid product code');
            errorCode = 1;
        } else {
            if (pattern_code.test(vendor_pcode)) {
                $("#vpcerror").html('');
            }
            else {
                $("#vpcerror").html('No Special Characters Allowed');
                errorCode = 1;
            }
        }
        if (product_cost == '') {
            $("#cerror").html('Please enter product cost');
            errorCode = 1;
        } else {
            if (pattern.test(product_cost)) {
                $("#cerror").html('');
            }
            else {
                $("#cerror").html('Cost should be in digits and can have upto two decimal places');
                errorCode = 1;
            }

        }
        if (errorCode == 1) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "/vendor/add-product",
            data: {id: id, vendor_product_id: vendor_product_id, product_code: product_code, vendor_pcode: vendor_pcode, product_cost: product_cost},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                if (data == "product-exist") {
                    $("#vendor_pcode").val('');
                    $("#perror").html('Product Is Already Present For this Vendor. Please Select another Product');
                    return false;
                }
                else if (data == "code-exist") {
                    $("#vendor_pcode").val('');
                    $("#vpcerror").html('Product Code Is Present For this Vendor. Please Enter another Product Code');
                    return false;
                }
                else {
                    $('#product-list').fadeTo(500, .5, function () {
                        $(this).html(data).fadeTo(500, 1);
                    });
                    $("#vendor_product_id").val('');
                    $("#product_code").val('');
                    $("#vendor_pcode").val('');
                    $("#product_cost").val('');
                }
            }
        });

    };

    this.EditProduct = function (id) {
        var vendor_id = $("#id").val();
        $.ajax({
            url: "/vendor/edit-product",
            data: {id: id, vendor_id: vendor_id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#perror").html('');
                $("#vpcerror").html('');
                $("#cerror").html('');
                $("#vendor_product_id").val(data.id);
                $("#product_code").val(data.product_name);
                $("#vendor_pcode").val(data.vendor_product_code);
                $("#product_cost").val(data.cost);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };
    this.SendPaymentLink = function() {
        var vendor_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/vendor/send-payment-link",
            data: {vendor_id: vendor_id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data) {
                $(document).ajaxStop($.unblockUI);
                $("#dialog").remove();
                $("#container").append("<div id='dialog'></div");
                document.getElementById('dialog').title = 'Vendor Batch Payment';
                $("#dialog").html("<p>" + data + "</p>");
                $("#dialog").dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 650,
                    width: 1000,
                    buttons:
                            {
                                Cancel: function ()
                                {
                                    $('#dialog').dialog('close');

                                }
                            },
                    modal: true
                });
            }
        });
    }
    
    this.addVendorPayment = function() {
        var id = $("#id").val();
        var payment_amount  = $("#vendor_payment_amount").val();
        var currency  = $("#currency_id").val();
        var payment_method  = $("#method_id").val();
        var date_paid = $("#date_paid").val();
        var reference  = $("#reference").val();
        var memo = $("#memo").val();
        var errorCode = 0;
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        var decimal_pattern = /^\d+\.?\d{0,2}$/;
        
        if(payment_amount == '') {
            $("#amount_error").html('Please enter Payment Amount');
            errorCode = 1;
        }
        else {
            if(payment_amount < 0 ||  payment_amount == 0) {
                $("#amount_error").html('Payment Amount Should be Greater than 0');
                errorCode = 1;
            }
            else {
                if(decimal_pattern.test(payment_amount)) {
                    $("#amount_error").html('');
                }
                else {
                    $("#amount_error").html('Payment Amount Should be a numerical value and can have upto two decimal Places');
                    errorCode = 1;
                }
            }
        }
        if(currency == '') {
            $("#currency_error").html('Please Select a Currency');
            errorCode = 1;
        }
        else {
            $("#currency_error").html('');
        }
        
        if(payment_method == '') {
            $("#method_error").html('Please select a payment method');
            errorCode = 1;
        }
        else {
            $("#method_error").html('');
        }
        
        if(date_paid !== '') {
            if(date_pattern.test(date_paid)) {
                $("#date_error").html('');
            }
            else {
                $("#date_error").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if(errorCode == 1) {
            return false;
        }
        
        $.ajax({
            url:"/vendor/insert-send-payment-link",
            type:"POST",
            data:{id:id,payment_amount:payment_amount,currency:currency,payment_method:payment_method,date_paid:date_paid,reference:reference,memo:memo},
            success: function(data) {
                $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
            }
        });
    }
    this.PayBalance = function() {
        var vendor_id = $("#id").val();
        data = "Are you sure you want to pay order balance?";
        $("#container").append("<div id='dialog1'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/vendor/pay-balance",
                                data: {vendor_id:vendor_id},
                                beforeSend: function ()
                                {
                                },
                                success: function (data)
                                {

                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                            $(this).dialog('close');
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
}
;

var Vendors = new Vendors();

function Category() {
    this.edit = function (id) {
        document.location.href = '/product-category/edit/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/product-category';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Product Category?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/product-category/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

}
;

var Category = new Category();

function Inventoryproduct() {
    this.Add = function () {
        document.location.href = '/product/add';
    };
    this.Edit = function (id) {
        document.location.href = '/product/add/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/product';
    };
    this.FileUpload = function () {
        var id = $("#file_upload").val();
        $("#product_div").append('');
        $("#picture_div").append('<img src=' + id + '>');
        // alert(id);
    };
    this.CostCalculation = function() {
        $(".pricingAll").each(function (i) {
            if ($(this).val() !== null) {
                var flag = '0';
                var b = "markup_div" + i;
                var cost = $("#costAll").val();
                var pricing = $(this).val();
                if (cost == '') {
                    $("#" + b).hide("fast");
                }
                else {
                    $(".col-md-4").show("fast");
                    $.ajax({
                        type: "POST",
                        url: "/product/calculation",
                        data: {flag: flag, cost: cost, i: i, pricing: pricing},
                        success: function (data) {
                            data = JSON.parse(data);
                            var a = "markup" + i;
                            $("#" + a).val(data.markup);
                        }
                    });
                }
            }
        });
    }
//    this.CheckProduct = function () {
//        var product_name = $("#product_name").val();
//        $.ajax({
//            url: "/product/check-product-name-present",
//            data: {product_name: product_name},
//            success: function (data) {
//                if (data == 'Product Present') {
//                    $("#product_name").val('');
//                    $("#producterror").html('Product Name already Present');
//                }
//                else {
//                    $("#producterror").html('');
//                }
//            }
//        });
//    };
    this.CancelledCheck = function () {
        var is_active = $("#is_active").val();
        if (is_active == "0") {
            $("#frmuser :input").attr("disabled", true);
//           $("a").css("cursor","arrow").click(false);
            $("#frmorder").removeAttr("href");
            $("#activate_button").removeAttr('disabled');
            $("#back_to_list").removeAttr('disabled');
//            $("#back_to_quote").removeAttr('disabled');
//            $("#back_to_order").removeAttr('disabled');
        }
    }
    this.ValidateCost = function () {
        var product = $("#product_name").val();
        var product_type = $("#product_type").val();
        var product_category = $("#product_category").val();
        var browse = $("#imgInp").val();
        var pricing = $("#pricing0").val();
        var cost = $("#costAll").val();
        var errorCode = 0;
        var extension = browse.split('.').pop().toUpperCase();
        if(browse) {
            if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG"){
                $("#pic_error").html('Only Image File is Allowed. No other File will be uploaded.Allowed extensions are png,jpg,jpeg,gif.');
                errorCode =1;
            }
            else {
                 $("#pic_error").html('');
            }
        }
        if (product == '') {
            $("#producterror").html('');
            $("#producterror").html('Please enter Product Name');
            errorCode = 1;
        } else {
            $("#producterror").html('');
        }
        // return false;
        if (product_type == '') {
            $("#product_type_error").html('Please select Product type');
            errorCode = 1;
        } else {
            $("#product_type_error").html('');
        }
        if (product_category == '') {
            $("#product_category_error").html('Please select Product Category');
            errorCode = 1;
        } else {
            $("#product_category_error").html('');
        }
        if (pricing == '') {
            $("#pricing_error").html('Please enter Price');
            errorCode = 1;
        } else {
            $("#pricing_error").html('');
        }
        if (cost == '') {
            $("#cost_error").html('Please enter Cost');
            errorCode = 1;
        } else {
            $("#cost_error").html('');
        }
        if (errorCode == 1) {
            return false;
        }
    }
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/product/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/product?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.deleteVendor = function (id, product_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this vendor?";
        $("#product_vendor").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/product/delete-vendor",
                                data: {id: id, product_id: product_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#vendor-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
//    this.ImageCheck = function(field_name, allowed_ext){
//        obj1= document.req_form;
//        var temp_field= 'obj1.'+field_name+'.value';
//        field_value=eval(temp_field);
//        if(field_value!=""){
//            var file_ext= (field_value.substring((field_value.lastIndexOf('.')+1)).toLowerCase());
//            ext=allowed_ext.split(',');
//            var allow=0;
//            for ( var i=0; i < ext.length; i++) {
//                    if(ext[i]==file_ext){
//                        allow=1;
//                    }
//            }
//            if(!allow){
//                alert('Invalid File format. Please upload file in '+allowed_ext+' format');        
//                return false;
//            }
//        }
//    return false;
//} 
    this.AddVendors = function () {
        var id = $("#id").val();

        var vendor_product_id = $("#vendor_product_id").val();
        var vendor_name = $("#vendor_id").val();
        var vendor_pcode = $("#vendor_pcode").val();
        var product_cost = $("#product_cost").val();
        var decimal_pattern = /^\d+\.?\d{0,2}$/;
        var errorCode = 0;
        if (vendor_name == '') {
            $("#perror").html('Please enter valid vendor Name');
            errorCode = 1;
        } else {
            $("#perror").html('');
            //errorCode = 0;
        }
        if (vendor_pcode == '') {
            $("#vpcerror").html('Please enter valid product code');
            errorCode = 1;
        } else {
            $("#vpcerror").html('');
            //errorCode = 0;
        }
        if (product_cost == '') {
            $("#cerror").html('Please enter Product Cost');
            errorCode = 1;
        } else {
            if (decimal_pattern.test(product_cost)) {
                $("#cerror").html('');
            }
            else {
                $("#cerror").html('Cost should be in digits and can have upto two decimal places');
                errorCode = 1;
            }

            //errorCode = 0;
        }
        if (errorCode == 1) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: "/product/add-vendor",
            data: {id: id, vendor_product_id: vendor_product_id, vendor_name: vendor_name, vendor_pcode: vendor_pcode, product_cost: product_cost},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $('#vendor-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#vendor_product_id").val('');
                $("#vendor_id").val('');
                $("#vendor_pcode").val('');
                $("#product_cost").val('');

            }
        });

    };
    
    this.editVendor = function (id) {
        var product_id = $("#id").val();
        $.ajax({
            url: "/product/edit-vendor",
            data: {id: id, product_id: product_id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#vendor_product_id").val(data.id);
                $("#vendor_id").val(data.name);
                $("#vendor_pcode").val(data.vendor_product_code);
                $("#product_cost").val(data.cost);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.AddComponent = function () {
        var id = $("#id").val();
        var bom_id = $("#bom_id").val();
        var name = $("#item_id").val();
        var quantity = $("#quantity").val();
        var product_cost = $("#cost").val();
        var pattern = /^\d+$/;
        var decimal_pattern = /^\d+\.?\d{0,2}$/;

        var name_errorCode = 0;
        var quantity_errorCode = 0;
        var product_errorCode = 0;
        if (name == '') {
            $("#comperror").html('Please enter valid component name');
            name_errorCode = 1;
        } else {
            $("#comperror").html('');
            name_errorCode = 0;
        }

        if (quantity == '') {
            $("#qerror").html('Please enter valid quantity');
            quantity_errorCode = 1;
        }
        else {
            if (pattern.test(quantity)) {
                $("#qerror").html('');
                quantity_errorCode = 0;
            }
            else {
                $("#qerror").html('Quantity should be in digits');
                quantity_errorCode = 1;
            }
        }
        if (product_cost == '') {
            $("#costerror").html('Please enter valid cost');
            product_errorCode = 1;
        } else {
            if (decimal_pattern.test(product_cost)) {
                $("#costerror").html('');
                product_errorCode = 0;
            }
            else {
                $("#costerror").html('Cost should be in digits and can have upto two decimal places');
                product_errorCode = 1;
            }
        }
        if (name_errorCode == 1 || quantity_errorCode == 1 || product_errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/product/add-component",
            data: {id: id, bom_id: bom_id, product_name: name, quantity: quantity, cost: product_cost},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $('#bill-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#bom_id").val('');
                $("#item_id").val('');
                $("#quantity").val('');
                $("#cost").val('');

            }
        });

    };

    this.editComponent = function (id) {
        var product_id = $("#id").val();
        $.ajax({
            url: "/product/edit-component",
            data: {id: id, product_id: product_id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#bom_id").val(data.id);
                $("#item_id").val(data.product_name);
                $("#quantity").val(data.quantity);
                $("#cost").val(data.cost);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.deleteComponent = function (id, product_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this component?";
        $("#product_vendor").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/product/delete-component",
                                data: {id: id, product_id: product_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#bill-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };


    this.AddInvenotryLocation = function () {
        var id = $("#id").val();
        var location_product_id = $("#location_product_id").val();
        var location_id = $("#location_id").val();
        var location_quantity = $("#location_quantity").val();
        var location_errorCode = 0;
        var quantity_errorCode = 0;
        var pattern = /^[0-9]+$/;
        if (location_id == '') {
            $("#ilerror").html('Please enter Inventory location name');
            location_errorCode = 1;
        } else {
            $("#ilerror").html('');
            location_errorCode = 0;
        }
        if (location_quantity == '') {
            $("#ilqerror").html('Please enter quantity');
            quantity_errorCode = 1;
        } else {

            if (pattern.test(location_quantity)) {
                $("#ilqerror").html('');
                quantity_errorCode = 0;
            }
            else {
                $("#ilqerror").html('Quantity should be in digits');
                quantity_errorCode = 1;
            }

        }



        if (location_errorCode === 1 || quantity_errorCode === 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/product/add-inventory-location",
            data: {id: id, location_product_id: location_product_id, location_id: location_id, location_quantity: location_quantity},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $('#location-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#location_product_id").val('');
                $("#location_id").val('');
                $("#location").val('');
                $("#location_quantity").val('');
                // $("#cost").val('');

            }
        });

    };

    this.editInventoryLocation = function (id) {
        $.ajax({
            url: "/product/edit-inventory-location",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {


                $("#location_product_id").val(data.id);
                $("#location_id").val(data.location_name);
                $("#location_quantity").val(data.quantity);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.deleteInventoryLocation = function (id, product_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Location?";
        $("#product_vendor").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/product/delete-inventory-location",
                                data: {id: id, product_id: product_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#location-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.currencyPopup = function () {
        id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/pricing-currency/add-new-pricing-currency-popup",
            data: {},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data) {
                $(document).ajaxStop($.unblockUI);
                $("#dialog").remove();
                $("#container").append("<div id='dialog'></div");
                document.getElementById('dialog').title = ' Pricing/Currency';
                $("#dialog").html("<p>" + data + "</p>");
                $("#dialog").dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 410,
                    width: 450,
                    buttons:
                            {
                                "Save and Close": function ()
                                {
                                    var pricing_scheme = $("#pricing_scheme").val();
                                    var currency_name = $("#currency_name").val();
                                    $.ajax({
                                        type: 'POST',
                                        url: "/pricing-currency/insert-new-pricing-currency-popup",
                                        data: {product_id: id, pricing_scheme: pricing_scheme, currency_name: currency_name},
                                        success: function (data) {
                                            $("#cost_div").html('');
                                            $("#cost_div").html(data);
                                            $('#dialog').dialog('close');
                                            // document.location.href = '/product/add?status=added';
                                        }
                                    });

                                },
                                Cancel: function ()
                                {
                                    $('#dialog').dialog('close');

                                }
                            },
                    modal: true
                });
            }
        });
    };

    this.DeletecurrencyPopup = function (id) {
        var product_id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to delete this Pricing Currency?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/product/delete-currency-popup",
                                data: {product_id: product_id, id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $("#cost_div").html('');
                                    $("#cost_div").html(data);
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);


                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.DeactivateProduct = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Deactivate this Product?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                url: "/product/deactivate-product",
                                data: {id: id},
                                success: function (data) {
                                    $('#dialog').dialog('close');
                                    document.location.href = '/product/add/id/' + id;
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });

    };
    this.ActivateProduct = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Activate this Product?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                url: "/product/activate-product",
                                data: {id: id},
                                success: function (data) {
                                    $('#dialog').dialog('close');
                                    document.location.href = '/product/add/id/' + id;
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

}
;

var Inventoryproduct = new Inventoryproduct();

function Inventory() {
    this.EditAdjustStock = function (id) {
        document.location.href = '/inventory/update-adjust-stock/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/inventory/adjust-stock';
    };
    this.EditReorderStock = function (id) {
        document.location.href = '/inventory/update-reorder-stock/id/' + id;
    };
    this.CancelAdjustStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to cancel this record?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            document.location.href = '/inventory/cancel-adjust-stock/id/'+id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.GetProductQuantity = function() {
        var product_id = $("#item_id").val();
        $.ajax({
           dataType:"json",
           url:"/inventory/get-reorder-product-quantity",
           data:{product_id:product_id},
           success: function(data) {
               if(data.quantity == null) {
                   $("#newqty0").val('0');
               }
               else{
                   $("#newqty0").val(data.quantity);
               }
            } 
        });
    };
    this.ValidateReorder = function() {
        var product_id = $("#item_id").val();
        var suggested_quantity = $("#suggested_quantity").val();
        var vendor_id = $("#vendor_id").val();
        var errorCode = 0;
        var pattern_quantity = /^\d+/;
        if(product_id == '') {
            $("#product_error").html('Please Select a Product');
            errorCode=1;
        }
        else {
            $("#product_error").html('');
        }
        if(suggested_quantity == '') {
            $("#suggested_error").html('Please enter Quantity');
            errorCode=1;
        }
        else {
            if(pattern_quantity.test(suggested_quantity)) {
                if(suggested_quantity < 0 || suggested_quantity == 0) {
                    $("#suggested_error").html('Quantity Should be Greater than 0');
                    errorCode=1;
                }
                else {
                    $("#suggested_error").html('');
                }
            }
            else {
                $("#suggested_quantity").val('');
                $("#suggested_error").html('Quantity should be a numerical value');
                errorCode = 1;
            } 
        }
        if(vendor_id == '') {
            $("#vendor_error").html('Please Select a Vendor');
            errorCode=1;
        }
        else {
            $("#vendor_error").html('');
        }
        if(errorCode == 1) {
            return false;
        }
    };
    this.refreshQty = function () {
        var name = $("#name").val();
        var location_id = $("#location").val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/inventory/get-product-quantity",
            data: {name: name, location_id: location_id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#newqty").val(data.quantity - 1);
                $("#oldqty").val(data.quantity);
                Inventory.calcDiff();
            }
        });
    };
    this.calcDiff = function () {
        var newqty = $("#newqty").val();
        var oldqty = $("#oldqty").val();
        var diff = parseInt(newqty) - parseInt(oldqty);
        if (isNaN(diff)) {
            $("#diff").val('0');
        } else {
            $("#diff").val(diff);
        }
    };

    this.getCurrentQtyByLocation = function () {
        var name = $("#name").val();
        var location_id = $("#location").val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/inventory/get-product-quantity",
            data: {name: name, location_id: location_id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#currentqty").val(data.quantity);
            }
        });
    };
    this.getSuggestedProduct = function () {
        var product = $("#select").val();
        var status = $("#select").val();
        if (product == '') {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "/inventory/get-suggested-product",
            data: {status:status,product: product},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                if (data) {
                    //$("#list-grid").html('');
                    $("#table-list-grid").html(data);
                    $("#list-grid-div").html('');
                }
                else {
                    alert("None of the Product to Suggest");
                }

            }
        });
    };
    this.ReorderProduct = function (flag) {
        if (flag == '1') {
            var add = 1;
        }
        else {
            var add = 0;
        }
        count = 0;
        con = 0;
        data = 0;
        var result = [];
        $(".reorder_checkbox").each(function (i) {
            if ($(this).is(":checked")) {
                var id = (this).id;
                var vendor_id = $("#vendor" + id).val();
                if(vendor_id == '0') {
                    $("#vendor_error" + id).html('No Vendor is present');
                    data = 1;
                }
                else {
                    $("#vendor_error" + id).html('');
                }
                var product_id = $("#product" + id).val();
                var quantity = $("#quantity" + id).val();
                if(quantity < 0 || quantity == 0) {
                    $("#quantity_error" + id).html('Quantity should be greater than 0');
                    data =1;
                }
                else {
                    $("#quantity_error" + id).html('');
                }
                var status = $("#status" + id).val();
                if (count != 0) {
                    if (count == vendor_id) {
                        var temp_arr = [];
                        temp_arr.push(id);
                        temp_arr.push(vendor_id);
                        temp_arr.push(product_id);
                        temp_arr.push(quantity);
                        temp_arr.push(status);
                        result.push(temp_arr);
                    }
                    else {
                        alert('Reorder Product Should be of the same Vendor');
                        data = 1;
                    }
                } else {
                    if (con == 0) {
                        var temp_arr = [];
                        count = vendor_id;
                        temp_arr.push(id);
                        temp_arr.push(vendor_id);
                        temp_arr.push(product_id);
                        temp_arr.push(quantity);
                        temp_arr.push(status);
                        result.push(temp_arr);
                        con++;
                    }
                }
            }
        });
        if (data == '1') {
            return false;
        }
        else {
            $.ajax({
                dataType: "JSON",
                url: "/inventory/reorder-product",
                data: {add: add, result: result},
                success: function (data) {
                    document.location.href = '/purchase-order/add?vendor=' + data.vendor_id
                }
            });
        }
    };
    this.CancelReorderStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/inventory/cancel-reorder-stock",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/inventory/reorder-stock?status=cancelled';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.sendStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to send stock to other location?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/inventory/send-stock/id/'+id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.receiveStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to receive stock to other location?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/inventory/received-stock/id/'+id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
;

var Inventory = new Inventory();

function PurchaseOrder() {
    this.Add = function () {
        document.location.href = '/purchase-order/add';
    };
    this.Edit = function (id) {
        document.location.href = '/purchase-order/edit/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/purchase-order';
    };
    this.ValidateOrder = function () {
        var vendorId = $("#vendor_id").val();
        if (vendorId == '') {
            $("#venError").html('Please select vendor name');
            $("#vendor_id").focus();
            return false;
        } else {
            $("#venError").html('');
        }
    };

    this.CancelledCheck = function () {
        var order_status = $("#status").val();
        if (order_status == "Cancelled") {
            $("#frmorder :input").attr("disabled", true);
//           $("a").css("cursor","arrow").click(false);
            $("#frmorder").removeAttr("href");
            $("#reopen_button").removeAttr('disabled');
            $("#back_to_order").removeAttr('disabled');
        }
    }
    this.CancelPurchaseOrder = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to cancel this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            document.location.href = '/purchase-order/cancel-purchase-order/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.ReopenCancelledOrder = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to Reopen this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            document.location.href = '/purchase-order/reopen-purchase-order/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.addShipping = function () {
        $("#ship_address").show();
        $("#add_address").hide();
        $("#terms_div").show();
        $("#vorder_div").show();
        $("#ship_req").val(1);
        $("#freight_div").show();

        PurchaseOrder.calculateTax();
    }

    this.noShipping = function () {
        $("#ship_address").hide();
        $("#add_address").show();
        $("#terms_div").hide();
        $("#vorder_div").hide();
        $("#ship_req").val(0);
        $("#freight_div").hide();

        PurchaseOrder.calculateTax();


    }

    this.getvendorDetails = function () {
        var id = $("#vendor_id").val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/purchase-order/get-vendor-details",
            data: {id: id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#vendor_contact").val(data.contact_name);
                $("#phone").val(data.contact_phone);
                $("#vendor_address").val(data.address);
                $("#taxing_scheme").val(data.taxing_schemes);
                $("#carrier").val(data.carrier_id);
                $("#currency_id").val(data.currency_id);
                $("#address_type").val(data.address_type);
                $("#address").val(data.address);
                $("#address1").val(data.address1);
                $("#address2").val(data.address2);

                GlobalConfigs.refreshSelect();
                PurchaseOrder.calculateTax();

            }
        });
    };

    this.AddProduct = function () {
        var id = $("#id").val();
        var vendor_id = $("#vendor_id").val();
        var product_record_id = $("#purchase_product_id").val();
        var product_id = $("#product_code").val();
        var vendor_pcode = $("#vendor_pcode").val();
        var quantity = $("#quantity").val();
        var unit_price = $("#unit_price").val();
        var discount = $("#discount").val();
        var sub_total = $("#sub_total").val();
        var availQuantity = $("#availQuantity").val();
        var errorCode = 0;
        if (product_id == '') {
            $("#perror").html('Please select product');
            errorCode = 1;
        } else {
            $("#perror").html('');
            //errorCode =0;
        }

        if (quantity == '') {
            $("#qtyerror").html('Please enter quantity');
            errorCode = 1;
        } else {
            $("#qtyerror").html('');
            //errorCode =0;
        }
        if (quantity) {
            if (parseInt(quantity) < 1 || isNumberOnly(quantity)) {
                $("#qtyerror").html('Please enter valid quantity');
                errorCode = 1;
            } else {
                $("#qtyerror").html('');
                //errorCode =0;
            }
        }

        if (unit_price == '') {
            $("#cerror").html('Please enter cost');
            errorCode = 1;
        } else {
            $("#cerror").html('');
            //errorCode =0;
        }

        if (unit_price) {
            if (parseInt(unit_price) < 1 || isNumber1(unit_price)) {
                $("#cerror").html('Please enter valid cost');
                errorCode = 1;
            } else {
                $("#cerror").html('');
                //errorCode =0;
            }
        }
        
        if (discount > 100 || discount < 0 || isNumber1(discount)) {
            $("#diserror").html('Discount shoould be in between 0 to 100%');
            errorCode = 1;
        } else {
            $("#diserror").html('');
            // errorCode =0;
        }
        if (errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/purchase-order/add-product",
            data: {id: id,vendor_id:vendor_id, product_record_id: product_record_id, product_id: product_id, vendor_pcode: vendor_pcode, quantity: quantity, discount: discount, unit_price: unit_price, sub_total: sub_total},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $('#product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#purchase_product_id").val('');
                $("#product_code").val('');
                $("#vendor_pcode").val('');
                $("#quantity").val('');
                $("#unit_price").val('');
                $("#discount").val('');
                $("#sub_total").val('');

                GlobalConfigs.refreshSelect();
                PurchaseOrder.getSubTotal(id);
                PurchaseOrder.countTotalQty(id);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };
    this.CopyPurchaseOrder = function (purchase_id) {
        $("#dialog").remove();
        data = "Are you sure you want to copy this purchase order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/copy-purchase-order",
                                data: {purchase_id: purchase_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                    //alert(data);
                                    document.location.href = '/purchase-order/add/copy/' + data;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    }
    this.editProduct = function (id) {
        var po_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/edit-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#purchase_product_id").val(data.id);
                $("#product_code").val(data.product_id);
                $("#vendor_pcode").val(data.vendor_product_code);
                $("#quantity").val(data.quantity);
                $("#unit_price").val(data.unit_price);
                $("#discount").val(data.discount);
                $("#sub_total").val(data.sub_total);

                GlobalConfigs.refreshSelect();
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.deleteProduct = function (id, po_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/delete-product",
                                data: {id: id, po_id: po_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    PurchaseOrder.RefreshReceivedProducts(po_id);
                                    PurchaseOrder.getSubTotal(po_id);
                                    PurchaseOrder.countTotalQty(po_id);

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.RefreshReceivedProducts = function (order_id) {

       $.ajax({
            type: 'POST',
            url: "/purchase-order/refresh-receive-products",
            data: {order_id: order_id},
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $('#receive-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
                       
    };
    
    this.calcSubTotal = function () {
        var quantity = $("#quantity").val();
        var unit_price = $("#unit_price").val();
        var discount = $("#discount").val();

        var amount = quantity * unit_price;
        var disAmount = (amount * discount) / 100;
        var sub_total = amount - disAmount;
        if(isNaN(sub_total)){
            $("#sub_total").val(0);
        }else {
            $("#sub_total").val(sub_total);
        }
        

    };


    this.getProductData = function () {
        var product_id = $("#product_code").val();
        var vendor_id = $("#vendor_id").val();
        var location_id = $("#location_id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/get-product-details",
            data: {product_id: product_id, vendor_id: vendor_id, location_id: location_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#unit_price").val(data.price);
                $("#quantity").val(1);
                $("#vendor_pcode").val(data.vendor_product_code);
                $("#sub_total").val(data.price * $("#quantity").val());
                $("#availQuantity").val(data.avail_qty);
            }
        });
    };

    this.getReturnProductData = function () {
        var product_id = $("#ret_product_code").val();
        var vendor_id = $("#vendor_id").val();
        var location_id = $("#location_id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/get-product-details",
            data: {product_id: product_id, vendor_id: vendor_id, location_id: location_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#ret_unit_price").val(data.price);
                $("#ret_quantity").val(1);
                $("#ret_vendor_pcode").val(data.vendor_product_code);
                $("#ret_sub_total").val(data.price * $("#ret_quantity").val());
            }
        });
    };

    this.getReceiveProductData = function () {
        var product_id = $("#product_code").val();
        var vendor_id = $("#vendor_id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/get-product-details",
            data: {product_id: product_id, vendor_id: vendor_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#rec_quantity").val(1);
                $("#rec_vendor_pcode").val(data.vendor_product_code);
            }
        });
    };

    this.getSubTotal = function (po_id) {
        $.ajax({
            type: 'POST',
            url: "/purchase-order/get-sub-total",
            data: {po_id: po_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#sub_total_products").val(data.sub_total);
                $("#ret_net_sub_total").val(data.ret_sub_total);

                if( data.ret_sub_total > 0 ){
                    $("#ret_autofill").css("display","block");                    
                } else {
                    $("#ret_autofill").css("display","none");
                }
                
                var subTotal = $("#ret_net_sub_total").val();
                var fee = $("#ret_fee").val();

                var total = subTotal - fee;
                $("#ret_total").val(total);

                PurchaseOrder.calculateTax(po_id);

            }
        });
    };

    this.calculateTax = function (id) {
        var scheme_id = $("#taxing_scheme").val();
        var ship_req = $("#ship_req").val();
        var sub_total = $("#sub_total_products").val();
        var freight = $("#freight").val();

        $.ajax({
            type: 'POST',
            url: "/purchase-order/calc-tax",
            data: {id: id, scheme_id: scheme_id, ship_req: ship_req, sub_total: sub_total,freight:freight},
            beforeSend: function ()
            {
                // $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $('#totals_div').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
    };

    this.AutofillReceiveProducts = function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to autofill all products?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/autofill-receive-products",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#receive-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    PurchaseOrder.countTotalQty(order_id);
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.calculateBalance = function () {
        var total_amount = $("#total_amount").val();
        var paid_amount = $("#paid_amount").val();
        var balance = total_amount - paid_amount;

        $("#balance").val(balance);
    };

    this.fulfilledOrder = function () {
        var id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to fulfilled this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/fulfilled-order",
                                data: {id: id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.UnfulfilledOrder = function () {

        var id = $("#id").val();

        $("#dialog").remove();
        data = "Are you sure you want to unfulfilled this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/unfulfilled-order",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.PayOrder = function () {

        var order_id = $("#id").val();
        var balance = $("#balance").val();

        $("#dialog").remove();
        data = "Are you sure you want to pay full amount of this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/Payment-order",
                                data: {order_id: order_id, balance: balance},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + order_id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
     this.RefundPayOrder = function () {

        var order_id = $("#id").val();
        var balance = $("#balance").val();

        $("#dialog").remove();
        data = "Are you sure you want to refund amount of this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/refund-payment-order",
                                data: {order_id: order_id, balance: balance},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                     $(document).ajaxStop($.unblockUI);
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + order_id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    
    this.PayOrderPopup = function () {

        var id = $("#id").val();
        var balance = $("#balance").val();
        data = "Are you sure you want to Pay this order?";
        $("#container").append("<div id='dialog2'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog2").html("<p>" + data + "</p>");
        $("#dialog2").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                             $(this).dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/payment-order-popup",
                                data: {id: id, balance: balance},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {

                                    
                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    
                                    PurchaseOrder.countTotalQty(id);
                                    PurchaseOrder.calculateTax(id);
                                   

                                }

                            });
                           
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.IssueRefund = function () {

        var order_id = $("#id").val();
        var total_amount = $("#total_amount").val();
        data = "Are you sure you want to unpay full amount of this order?";
        $("#container").append("<div id='dialog1'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/issue-refund",
                                data: {order_id: order_id, total_amount: total_amount},
                                beforeSend: function ()
                                {
                                },
                                success: function (data)
                                {

                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                            $(this).dialog('close');
                            PurchaseOrder.calculateTax(order_id);
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.UnpayOrder = function () {

        var order_id = $("#id").val();
        var total_amount = $("#total_amount").val();
        $("#dialog").remove();
        data = "Are you sure you want to unpay full amount of this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/unpay-order",
                                data: {order_id: order_id, total_amount: total_amount},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + order_id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.AddReceiveProduct = function () {
        var id = $("#id").val();
        var product_record_id = $("#rec_product_id").val();
        var product_id = $("#rec_product_code").val();
        var vendor_pcode = $("#rec_vendor_pcode").val();
        var quantity = $("#rec_quantity").val();
        var location_id = $("#receive_location_id").val();
        var receive_date = $("#receive_date").val();
        var errorCode = 0;
        if (product_id == '') {
            $("#rperror").html('Please select product');
            errorCode = 1;
        } else {
            $("#rperror").html('');
            // errorCode =0;
        }
        
        if (quantity == '' || isNumberOnly(quantity)) {
            $("#rqtyerror").html('Please enter valid quantity');
            errorCode = 1;
        } else {
            $("#rqtyerror").html('');
            //errorCode =0;
        }
        if (location_id == '') {
            $("#rlerror").html('Please select location');
            errorCode = 1;
        } else {
            $("#rlerror").html('');
            //errorCode =0;
        }

        if (receive_date == '') {
            $("#rderror").html('Please select date');
            errorCode = 1;
        } else {
            $("#rderror").html('');
            //errorCode =0;
        }
        if (errorCode == 1) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "/purchase-order/add-receive-product",
            data: {id: id, product_record_id: product_record_id, product_id: product_id, vendor_pcode: vendor_pcode, quantity: quantity, receive_date: receive_date, location_id: location_id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                if (data == 'qtyError') {
                    $("#rqtyerror").html('Quantity can not be greater than order Quantity');
                    return false;
                }
                if (data == 'recQtyExistError') {
                    $("#rqtyerror").html('Quantity can not be greater than order Quantity');
                    return false;
                }
                $('#receive-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#rec_product_id").val('');
                $("#rec_product_code").val('');
                $("#rec_vendor_pcode").val('');
                $("#rec_quantity").val('');
                $("#receive_date").val('');

                GlobalConfigs.refreshSelect();

                PurchaseOrder.countTotalQty(id);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.countTotalQty = function (id) {
        if(id){
            $.ajax({
                type: 'POST',
                url: "/purchase-order/count-quantity",
                data: {id: id},
                dataType: "json",
                beforeSend: function ()
                {
                },
                success: function (data)
                {
                    $("#total_ordered").val(data.order_qty);
                    $("#total_received").val(data.receive_qty);
                    $("#status").val(data.order_status + ',' + data.pay_status);

                    if (data.order_status == 'Fulfilled') {
                        var btnHtml = '<button type="button" onclick="PurchaseOrder.UnfulfilledOrder();" class="btn btn-warning margn-right10px">Unfulfilled</button>';
                        $("#order-btn").html(btnHtml);
                    }
                    ;

                    if (data.order_status == 'Unfulfilled') {
                        var btnHtml = '<button type="button" onclick="PurchaseOrder.fulfilledOrder();" class="btn btn-success margn-right10px">Fulfilled</button>';
                        $("#order-btn").html(btnHtml);
                    };

                    if (data.pay_status == 'Paid') {
                        var btnHtml = '<button type="button" onclick="PurchaseOrder.UnpayOrder();" class="btn btn-warning margn-right10px">Un Pay</button>';
                        $("#pay-btn").html(btnHtml);
                    };

                    if (data.pay_status == 'Unpaid' || data.pay_status == 'Partial') {
                        var btnHtml = '<button type="button" onclick="PurchaseOrder.PayOrder();" class="btn btn-success margn-right10px">Pay</button>';
                        $("#pay-btn").html(btnHtml);
                    };
                    
                    if (data.pay_status == 'Owing') {
                        var btnHtml = '<button type="button" onclick="PurchaseOrder.RefundPayOrder();" class="btn btn-danger">Refund</button>';
                        $("#pay-btn").html(btnHtml);
                    };

                }
            });
        }

    };

    this.editReceiveProduct = function (id) {
        var po_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/edit-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#rec_product_id").val(data.id);
                $("#rec_product_code").val(data.product_id);
                $("#rec_vendor_pcode").val(data.vendor_product_code);
                $("#rec_quantity").val(data.quantity);
                $("#receive_location_id").val(data.location_id);
                $("#receive_date").val(data.transaction_date);

                PurchaseOrder.countTotalQty(id);

                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.deleteReceiveProduct = function (id, po_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/delete-receive-product",
                                data: {id: id, po_id: po_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#receive-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    PurchaseOrder.countTotalQty(po_id);
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.AddReturnProduct = function () {
        var id = $("#id").val();
        var product_record_id = $("#ret_product_id").val();
        var product_id = $("#ret_product_code").val();
        var vendor_pcode = $("#ret_vendor_pcode").val();
        var quantity = $("#ret_quantity").val();
        var unit_price = $("#ret_unit_price").val();
        var discount = $("#ret_discount").val();
        var sub_total = $("#ret_sub_total").val();
        var ret_date = $("#ret_date").val();
        var errorCode = 0;
        if (product_id == '') {
            $("#ret_perror").html('Please select product');
            errorCode = 1;
        } else {
            $("#ret_perror").html('');
            //errorCode =0;
        }

        if (quantity == '') {
            $("#ret_qtyerror").html('Please enter quantity');
            errorCode = 1;
        } else {
            $("#ret_qtyerror").html('');
            //errorCode =0;
        }

        if (quantity < 1 || isNumberOnly(quantity)) {
            $("#ret_qtyerror").html('Please enter valid quantity');
            errorCode = 1;
        } else {
            $("#ret_qtyerror").html('');
            //errorCode =0;
        }

        if (unit_price == '') {
            $("#ret_cerror").html('Please enter valid cost');
            errorCode = 1;
        } else {
            $("#ret_cerror").html('');
            //errorCode =0;
        }

        if (unit_price < 1) {
            $("#ret_cerror").html('Cost sould be greater than 0');
            errorCode = 1;
        } else {
            $("#ret_cerror").html('');
            //errorCode =0;
        }

        if (discount > 100) {
            $("#ret_diserror").html('Discount can not be greater than 100%');
            errorCode = 1;
        } else {
            $("#ret_diserror").html('');
            // errorCode =0;
        }

        if (ret_date == '') {
            $("#ret_dateerror").html('Return Date can not be blank');
            errorCode = 1;
        } else {
            $("#ret_dateerror").html('');
            // errorCode =0;
        }

        if (errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/purchase-order/add-return-product",
            data: {id: id, product_record_id: product_record_id, product_id: product_id, vendor_pcode: vendor_pcode, quantity: quantity, discount: discount, unit_price: unit_price, sub_total: sub_total, ret_date: ret_date},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $('#ret-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#ret_product_id").val('');
                $("#ret_product_code").val('');
                $("#ret_vendor_pcode").val('');
                $("#ret_quantity").val('0');
                $("#ret_unit_price").val('0.00');
                $("#ret_discount").val('0.00');
                $("#ret_sub_total").val('0.00');
                $("#ret_date").val('');

                PurchaseOrder.refreshOrderProductsGrid(id);
                PurchaseOrder.countTotalQty(id);
                PurchaseOrder.getSubTotal(id);

                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.refreshOrderProductsGrid = function (id) {
        $.ajax({
            type: 'POST',
            url: "/purchase-order/refresh-order-products",
            data: {id: id},
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $('#product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });

    };
    this.editReturnProduct = function (id) {
        var po_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/edit-return-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#ret_product_id").val(data.id);
                $("#ret_product_code").val(data.product_id);
                $("#ret_vendor_pcode").val(data.vendor_product_code);
                $("#ret_quantity").val(data.quantity);
                $("#ret_unit_price").val(data.unit_price);
                $("#ret_discount").val(data.discount);
                $("#ret_sub_total").val(data.sub_total);
                $("#ret_date").val(data.transaction_date);

                PurchaseOrder.refreshOrderProductsGrid(po_id);
                PurchaseOrder.getSubTotal(po_id);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.deleteReturnProduct = function (id, po_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/delete-return-product",
                                data: {id: id, po_id: po_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#ret-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                                                       
                                    PurchaseOrder.RefreshUnstockProducts(po_id);
                                    PurchaseOrder.getReturnSubTotal(po_id);
                                    PurchaseOrder.refreshOrderProductsGrid(po_id);
                                    PurchaseOrder.getSubTotal(po_id);
                                    
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.calcRetTotal = function () {

        var quantity = $("#ret_quantity").val();
        var unit_price = $("#ret_unit_price").val();
        var discount = $("#ret_discount").val();

        var amount = quantity * unit_price;
        var disAmount = (amount * discount) / 100;
        var sub_total = amount - disAmount;

        $("#ret_sub_total").val(sub_total);

    };

    this.getReturnSubTotal = function (po_id) {
        $.ajax({
            type: 'POST',
            url: "/purchase-order/get-return-subtotal",
            data: {po_id: po_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#ret_net_sub_total").val(data.sub_total);

                PurchaseOrder.calculateTax(po_id);

            }
        });
    };

    this.calcRetNetTotal = function () {
        var subTotal = $("#ret_net_sub_total").val();
        var fee = $("#ret_fee").val();

        var total = subTotal - fee;
        $("#ret_total").val(total);

    };
    this.AutofillReturnProducts = function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to autofill all products?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/autofill-return-products",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#unstock-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    //$("#ret_autofill").remove();
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.RefreshUnstockProducts = function (order_id) {

       $.ajax({
            type: 'POST',
            url: "/purchase-order/refresh-unstock-products",
            data: {order_id: order_id},
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $('#unstock-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
                       
    };
    
    this.PaidPopup = function () {
        var order_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/purchase-order/purchase-order-payment",
            data: {order_id: order_id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data) {
                $(document).ajaxStop($.unblockUI);
                $("#dialog").remove();
                $("#container").append("<div id='dialog'></div");
                document.getElementById('dialog').title = 'Payment details';
                $("#dialog").html("<p>" + data + "</p>");
                $("#dialog").dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 750,
                    width: 1000,
                    modal: true
                });
            }
        });
    };
    this.AddPurchaseOrderPayment = function () {
        var id = $("#sales_payment_order_id").val();
        var order_id = $("#id").val();
        var payment_method = $("#payment_method_id").val();
        var payment_ref = $("#payment_ref").val();
        var payment_date = $("#payment_date").val();
        var payment_type = $("#payment_type").val();
        var payment_remarks = $("#payment_remarks").val();
        var payment_amount = $("#payment_amount").val();
        var pattern = /^\d+\.?\d{0,2}$/;
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

        var errorCode = 0;
        if (payment_method == '') {
            $("#methoderror").html('Please select payment method');
            errorCode = 1;
        } else {
            $("#methoderror").html('');
        }
        if (payment_date == '') {
            $("#dateerror").html('Please select Date');
            errorCode = 1;
        } else {
            if (date_pattern.test(payment_date)) {
                $("#dateerror").html('');
            }
            else {
                $("#dateerror").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if (payment_amount == '') {
            $("#amounterror").html('Please enter amount');
            errorCode = 1;
        } else {
            if (pattern.test(payment_amount)) {
                $("#amounterror").html('');
            }
            else {
                $("#amounterror").html('Cost should be in digits and can have upto two decimal places');
                errorCode = 1;
            }
        }
        if (errorCode == 1) {
            return false;
        }
        $.ajax({
            url: "/purchase-order/insert-purchase-order-payment",
            type: "POST",
            data: {payment_amount: payment_amount, id: id, order_id: order_id, payment_method: payment_method, payment_ref: payment_ref, payment_date: payment_date, payment_type: payment_type, payment_remarks: payment_remarks},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
                
            },
            success: function (data) {
        
                $('#payment-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                    $("#payment_method_id").val('');
                    $("#payment_ref").val('');
                    $("#payment_remarks").val('');
                    $("#payment_amount").val('');
                });
                PurchaseOrder.calculateTax(order_id);
                PurchaseOrder.countTotalQty(order_id);
                
                $(document).ajaxStop($.unblockUI);
            }

        });
    };
    this.editPurchaseOrderPayment = function (id) {
        $.ajax({
            type: 'POST',
            url: "/purchase-order/edit-purchase-order-payments",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {

            },
            success: function (data)
            {
                $("#sales_payment_order_id").val(data.id);
                $("#payment_method_id").val(data.payment_method_id);
                $("#payment_ref").val(data.payment_ref);
                $("#payment_type").val(data.payment_type);
                $("#payment_date").val(data.payment_date);
                $("#payment_remarks").val(data.payment_remarks);
                $("#payment_amount").val(data.amount);
            }

        });
    };
    this.deletePurchaseOrderPayment = function (id, po_id) {
        data = "Are you sure you want to delete this payment?";
        $("#dialog1").remove();
        $("#container").append("<div id='dialog1'></div");
        document.getElementById('dialog1').title = 'Delete Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $("#dialog1").dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/delete-purchase-order-payments",
                                data: {id: id, po_id: po_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    
                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                        PurchaseOrder.calculateTax(po_id);
                                        PurchaseOrder.countTotalQty(po_id);
                                    });
                                    $(document).ajaxStop($.unblockUI);

                                }
                            });
                        },
                        No: function ()
                        {
                           $("#dialog1").dialog('close');
                        }
                    },
            modal: true
        });
    };
    
     this.AutofillReturn= function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to autofill all products?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/autofill-returns",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#ret-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    PurchaseOrder.getSubTotal(order_id);
                                     PurchaseOrder.calculateTax(order_id);
                                    PurchaseOrder.countTotalQty(order_id);
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
};

var PurchaseOrder = new PurchaseOrder();

function SalesQuote() {
    this.Add = function () {
        document.location.href = '/sales-quote/add';
    };
    this.Edit = function (id) {
        document.location.href = '/sales-quote/add/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/sales-quote';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Sales Quote?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-quote/delete",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-quote?status=deleted';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.CancelledCheck = function () {
        var order_status = $("#status").val();
        if (order_status == "Rejected") {
            $("#frmuser :input").attr("disabled", true);
//           $("a").css("cursor","arrow").click(false);
            $("#frmorder").removeAttr("href");
            $("#reopen_button").removeAttr('disabled');
            $("#reopen_button_1").removeAttr('disabled');
            $("#back_to_quote").removeAttr('disabled');
            $("#back_to_order").removeAttr('disabled');
        }
    }
    this.CancelQuote = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Reject this Sales Quote?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-quote/cancel-quote",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-quote/add/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.RegenerateQuote = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Regenerate this Sales Quote?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-quote/regenerate-quote",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-quote/add/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.AddShippingAddress = function () {
        $("#payment_terms_div").show('fast');
        $("#p_o_div").show('fast');
        $("#shipping_address_div").show('fast');
        $("#no_shipping_address").show('fast');
        $("#due_date_div").show('fast');
        $("#req_ship_date_div").show('fast');
        $("#freight_div").show('fast');
        $("#add_shipping_address").hide('fast');
    };
    this.NoShippingAddress = function () {
        $("#payment_terms_div").hide('fast');
        $("#p_o_div").hide('fast');
        $("#shipping_address_div").hide('fast');
        $("#no_shipping_address").hide('fast');
        $("#due_date_div").hide('fast');
        $("#req_ship_date_div").hide('fast');
        $("#freight_div").hide('fast');
        $("#add_shipping_address").show('fast');
    };

    this.AddSalesProduct = function () {
        var id = $("#id").val();
        var order_id = $("#sales_order_id").val();
        var sales_item = $("#sales_item_id").val();
        var sales_quantity = $("#sales_quantity").val();
        var sales_unit_price = $("#sales_unit_price").val();
        var sales_discount = $("#sales_discount").val();
        var sales_total = $("#sales_total").val();
        var pattern_quantity = /^\d+/;
        var pattern = /^\d+\.?\d{0,2}$/;
        var errorCode = 0;
        if (sales_item == '') {
            $("#sqitemerror").html('Please select Product');
            errorCode = 1;
        } else {
            $("#sqitemerror").html('');
        }
        if (sales_quantity == '') {
            $("#sqquantity").html('Please enter Quantity');
            errorCode = 1;
        } else {
            if (pattern_quantity.test(sales_quantity)) {
                if(sales_quantity < 0 || sales_quantity == 0) {
                    $("#sqquantity").html('Quantity Should be greater than 0');
                    errorCode = 1;
                }
                else {
                    $("#sqquantity").html('');
                }   
            }
            else {
                $("#sqquantity").html('Quantity Should be a numerical value');
                errorCode = 1;
            }
//            errorCode = 0;
        }
        if (sales_unit_price == '') {
            $("#sqprice").html('Please enter Price');
            errorCode = 1;
        } else {
            if (pattern.test(sales_unit_price)) {
                if(sales_unit_price < 0 || sales_unit_price == 0) {
                    $("#sqprice").html('Unit Price Should be greater than 0');
                    errorCode = 1;
                }
                else {
                    $("#sqprice").html('');
                } 
            }
            else {
                $("#sqprice").html('Unit Price Should be numerical value and can have upto two decimal places');
                errorCode = 1;
            }
            //errorCode = 0;
        }
        if (sales_discount != '') {
            if (pattern.test(sales_discount)) {
                if(sales_discount < 0 || sales_discount > 100) {
                    $("#sqdiscount").html('Discount should be between 0 to 100');
                    errorCode = 1;
                }
                else {
                    $("#sqdiscount").html('');
                } 
            }
            else {
                $("#sqdiscount").html('Discount Should be numerical and can have upto two decimal places');
                errorCode = 1;
            }
        }
        if (errorCode == 1) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: "/sales-quote/add-sales-quote-item",
            data: {order_id: order_id, id: id, sales_item: sales_item, sales_quantity: sales_quantity, sales_unit_price: sales_unit_price, sales_discount: sales_discount, sales_total: sales_total},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $('#product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#sales_item_id").val('');
                $("#sales_quantity").val('');
                $("#sales_unit_price").val('');
                $("#sales_discount").val('');
                $("#sales_total").val('');
                GlobalConfigs.refreshSelect();
                var id = $("#flag").val();
                $.ajax({
                    url: "/sales-quote/fetch-sub-total",
                    dataType: "json",
                    data: {id: id},
                    success: function (data) {
                        $("#sub_total_1").val(data.sub_total);
                    }
                });

            }
        });

    };
    this.ConvertSalesQuoteIntoOrder = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Convert this Sales Quote into Order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                url: "/sales-quote/convert-sales-quote-to-order",
                                dataType: "JSON",
                                data: {id: id},
                                success: function (data) {
                                    document.location.href = '/sales-order/edit/id/' + data.id;
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });

    };
    this.SubTotal = function () {
        $.ajax({
            url: "/sales-quote/fetch-sub-total",
            dataType: "json",
            data: {},
            success: function (data) {
                $("#sub_total_1").val(data.sub_total);
            }
        });
    }
    this.SalesCalculation = function () {
        var sub_total = $("#sub_total").val();
        var freight = $("#freight").val();
    }
    this.editSalesProduct = function (id) {
        var order_id = $("#order_id").val();
        $.ajax({
            url: "/sales-quote/edit-sales-quote-item",
            data: {id: id, order_id: order_id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $("#sales_order_id").val(data.id);
                $("#sales_item_id").val(data.product_id);
                $("#sales_quantity").val(data.quantity);
                $("#sales_unit_price").val(data.unit_price);
                $("#sales_discount").val(data.discount);
                $("#sales_total").val(data.sub_total);
                GlobalConfigs.refreshSelect();

            }
        });

    };
    this.deleteSalesProduct = function (id, product_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this component?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-quote/delete-sales-quote-item",
                                data: {id: id, order_id: product_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.taxingScheme = function (order_id) {
        var sub_total = $("#sub_total_1").val();
        var freight = $("#freight").val();
        var taxing_scheme = $("#taxing_scheme").val();
        var freight = $("#freight").val();
        $.ajax({
            type: "POST",
            url: "/sales-quote/fetch-taxing-scheme",
            data: {order_id: order_id, taxing_scheme: taxing_scheme, freight: freight},
            dataType: "json",
            success: function (data) {
                $("#tax1_div").show('fast');
                $("#tax2_div").show('fast');
                $("#freight_div").show('fast');
                $("#sub_total_1").val(data.sub_total);
                $("#tax1_name").val(data.tax1);
                $("#tax2_name").val(data.tax2);
                $("#total").val(data.total);
                $("#freight").val(data.freight);
            }
        });
    };
    this.customerFetch = function (id) {
        var customer_id = $("#customer").val();
        var taxing_scheme =$("#taxing_scheme").val();
        $.ajax({
            url: "/sales-quote/fetch-customer-details",
            data: {customer_id: customer_id},
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $("#contact").val(data.name);
                $("#address_type").val(data.address_type);
                $("#address").val(data.address);
                $("#address1").val(data.address1);
                $("#address2").val(data.address2);
                $("#phone_no").val(data.phone);
                $("#des").val(data.designation);
                
                if(taxing_scheme == '') {
                   $("#taxing_scheme").val(data.taxing_schemes);
                }
                $("#pricing_currency").val(data.pricing_currency_id);
                if (data.payment_terms) {
                    $("#payment_terms_id").val(data.payment_terms);
                    SalesQuote.AddShippingAddress();
                }
                $("#location").val(data.default_location_id);
                $("#sales_reps").val(data.sales_reps);
                GlobalConfigs.refreshSelect();

            }
        });
    };

    this.subTotalFetch = function () {
        var quantity = $("#sales_quantity").val();
        var discount = $("#sales_discount").val();
        var unit_price = $("#sales_unit_price").val();
        $.ajax({
            type: "POST",
            url: "/sales-quote/sub-total-calculation",
            data: {quantity: quantity, discount: discount, unit_price: unit_price},
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $("#sales_total").val(data);
            }
        });
    };
    this.PriceByProductId = function () {
        var item = $("#sales_item_id").val();
        $.ajax({
            url: "/sales-quote/fetch-unit-price-by-product-id",
            data: {item: item},
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $("#sales_unit_price").val(data.price);
            }
        });
    };
}
var SalesQuote = new SalesQuote();

function SalesOrder() {
    this.Add = function () {
        document.location.href = '/sales-order/add';
    };
    this.Edit = function (id) {
        document.location.href = '/sales-order/edit/id/' + id;
    };
    this.EditAdjustStock = function (id) {
        document.location.href = '/inventory/update-adjust-stock/id/' + id;
    };
    this.GoToList = function () {
        document.location.href = '/sales-order/';
    };
    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Sales Order?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/sales-order/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.ValidateOrder = function () {
        var customer_id = $("#customer_id").val();
        if (customer_id == '') {
            $("#cusError").html('Please select Customer name');
            $("#customer_id").focus();
            return false;
        } else {
            $("#cusError").html('');
        }
    }
    this.CancelAdjustStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to cancel this record?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/inventory/cancel-adjust-stock",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/inventory/adjust-stock?status=cancelled';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };


    this.refreshQty = function () {
        var name = $("#name").val();
        var location_id = $("#location").val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/inventory/get-product-quantity",
            data: {name: name, location_id: location_id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#newqty").val(data.quantity - 1);
                $("#oldqty").val(data.quantity);
                Inventory.calcDiff();
            }
        });
    };
    this.calcDiff = function () {
        var newqty = $("#newqty").val();
        var oldqty = $("#oldqty").val();
        var diff = parseInt(newqty) - parseInt(oldqty);
        $("#diff").val(diff);
    };

    this.getCurrentQtyByLocation = function () {
        var name = $("#name").val();
        var location_id = $("#location").val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/inventory/get-product-quantity",
            data: {name: name, location_id: location_id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $("#currentqty").val(data.quantity);
            }
        });
    };

    this.sendStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to send stock to other location?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/inventory/send-stock",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                    if (data == 'sent') {
                                        document.location.href = '/inventory/transfer-stock?status=send';
                                    } else {
                                        document.location.href = '/inventory/transfer-stock?status=error';
                                    }
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.receiveStock = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to receive stock to other location?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/inventory/received-stock",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                    if (data == 'received') {
                                        document.location.href = '/inventory/transfer-stock?status=received';
                                    } else {
                                        document.location.href = '/inventory/transfer-stock?status=error';
                                    }

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.addShipping = function () {
        $("#ship_address").show();
        $("#add_address").hide();
        $("#terms_div").show();
        $("#vorder_div").show();
        $("#ship_req").val(1);
        $("#freight_div").show();
    }

    this.noShipping = function () {
        $("#ship_address").hide();
        $("#add_address").show();
        $("#terms_div").hide();
        $("#vorder_div").hide();
        $("#ship_req").val(0);
        $("#freight_div").hide();


    }

    this.getcustomerDetails = function () {
        var id = $("#customer_id").val();
         var taxing_scheme = $("#taxing_scheme").val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: "/sales-order/get-customer-details",
            data: {id: id},
            beforeSend: function ()
            {
                //$.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                
                $("#customer_contact").val(data.contact_name);
                $("#location_id").val(data.default_location_id);
                $("#currency_id").val(data.pricing_currency_id);
                $("#phone").val(data.contact_phone);
                if(taxing_scheme == "") {
                    $("#taxing_scheme").val(data.taxing_schemes);
                }
                $("#customer_address").val(data.address);
                GlobalConfigs.refreshSelect();
            }
        });
    };

    this.AddProduct = function () {
        var id = $("#id").val();
        var hidden_quantity = $("#hidden_quantity").val();
        var quantity = $("#quantity").val();
        if (quantity > hidden_quantity) {
            $("#qtyerror").html('Avaialable Quantity is ' + hidden_quantity + '.');
            return false;
        }
        var location_id = $("#location_id").val();
        var sales_product_id = $("#sales_product_id").val();
        var product_name = $("#product_code").val();

        var unit_price = $("#unit_price").val();
        var discount = $("#discount").val();
        var sub_total = $("#sub_total").val();
        var errorCode = 0;
        if (location_id == '') {
            $("#perror").html('Please select a Location');
            errorCode = 1;
        } else {
            $("#perror").html('');
            errorCode = 0;
        }
        if (product_name == '') {
            $("#perror").html('Please select product');
            errorCode = 1;
        } else {
            $("#perror").html('');
            errorCode = 0;
        }

        if (quantity == '') {
            $("#qtyerror").html('Please enter quantity');
            errorCode = 1;
        } else {
            $("#qtyerror").html('');
            errorCode = 0;
        }
        if (unit_price == '') {
            $("#cerror").html('Please enter valid cost');
            errorCode = 1;
        } else {
            $("#cerror").html('');
            errorCode = 0;
        }

        if (discount > 100) {
            $("#diserror").html('Discount can not be greater than 100%');
            errorCode = 1;
        } else {
            $("#diserror").html('');
            errorCode = 0;
        }
        if (errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/sales-order/add-product",
            data: {id: id, sales_product_id: sales_product_id, location_id: location_id, product_name: product_name, quantity: quantity, discount: discount, unit_price: unit_price, sub_total: sub_total},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $('#product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#sales_product_id").val('');
                $("#product_code").val('');
                $("#quantity").val(0);
                $("#unit_price").val(0);
                $("#discount").val(0);
                $("#sub_total").val(0);
                GlobalConfigs.refreshSelect();
                SalesOrder.getSubTotal(id);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.editProduct = function (id) {
        var so_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/sales-order/edit-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {


                $("#sales_product_id").val(data.id);
                $("#product_code").val(data.product_id);
                $("#quantity").val(data.quantity);
                $("#unit_price").val(data.unit_price);
                $("#discount").val(data.discount);
                $("#sub_total").val(data.sub_total);
                GlobalConfigs.refreshSelect();
                SalesOrder.getSubTotal(so_id);

                $(document).ajaxStop($.unblockUI);
            }
        });

    };


    this.deleteProduct = function (id, so_id) {

        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/delete-product",
                                data: {id: id, so_id: so_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    SalesOrder.getSubTotal(so_id);

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };


    this.calcSubTotal = function () {
        var quantity = $("#quantity").val();
        var unit_price = $("#unit_price").val();
        var discount = $("#discount").val();

        var amount = quantity * unit_price;
        var disAmount = (amount * discount) / 100;
        var sub_total = amount - disAmount;

        $("#sub_total").val(sub_total);

    };

    this.getFulfillProductData = function () {
        var order_id = $("#id").val();
        var product_id = $("#rec_product_code").val();
        $.ajax({
            type: "POST",
            url: "/sales-order/get-fulfill-product-details",
            data: {order_id: order_id, product_id: product_id},
            success: function (data) {
                data = JSON.parse(data);
                $("#receive_location_id").val(data.location_id);
                $("#rec_quantity").val(data.quantity);
                GlobalConfigs.refreshSelect();
            }
        });
    }
    this.getProductData = function () {
        var location_id = $("#location_id").val();
        if (location_id == null || location_id < 1) {
            $("#perror").html("Please Select A Location for the product");
            return false;
        }
        var product_name = $("#product_code").val();
        var customer_id = $("#customer_id").val();
        $.ajax({
            type: 'POST',
            url: "/sales-order/get-product-details",
            data: {location_id: location_id, product_name: product_name, customer_id: customer_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                if (data.price == "No Quantity Available") {
                    $("#perror").html("No Product Available At This Location");
                    $("#unit_price").val('');
                    $("#hidden_quantity").val('');
                    $("#quantity").val('');
                    $("#sub_total").val(data.price * $("#quantity").val());
                }
                else {
                    $("#perror").html('');
                    $("#unit_price").val(data.price);
                    $("#hidden_quantity").val(data.quantity);
                    $("#quantity").val(1);
                    $("#sub_total").val(data.price * $("#quantity").val());
                }
            }
        });
    };

    this.getSubTotal = function (so_id) {
        $.ajax({
            type: 'POST',
            url: "/sales-order/get-sub-total",
            data: {so_id: so_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#sub_total_products").val(data.sub_total);

                SalesOrder.calculateTax();

            }
        });
    };

    this.calculateTax = function () {
        var order_id = $("#id").val();
        var scheme_id = $("#taxing_scheme").val();
        var ship_req = $("#ship_req").val();
        var sub_total = $("#sub_total_products").val();

        $.ajax({
            type: 'POST',
            url: "/sales-order/calc-tax",
            data: {order_id: order_id, scheme_id: scheme_id, ship_req: ship_req, sub_total: sub_total},
            beforeSend: function ()
            {
                // $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $('#totals_div').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });

                // $(document).ajaxStop($.unblockUI);
            }
        });
    };
    this.fulfilledOrder = function () {

        var id = $("#id").val();

        $("#dialog").remove();
        data = "Are you sure you want to fulfilled this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/fulfilled-order",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.UnfulfilledOrder = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to unfulfilled this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/unfulfilled-order",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.PayOrder = function () {

        var id = $("#id").val();
        var balance = $("#balance").val();

        $("#dialog").remove();
        data = "Are you sure you want to Pay this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/payment-order",
                                data: {id: id, balance: balance},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.PayOrderPopup = function () {

        var id = $("#id").val();
        var balance = $("#balance").val();
        data = "Are you sure you want to Pay this order?";
        $("#container").append("<div id='dialog2'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog2").html("<p>" + data + "</p>");
        $("#dialog2").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/payment-order-popup",
                                data: {id: id, balance: balance},
                                beforeSend: function ()
                                {
                                },
                                success: function (data)
                                {

                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }

                            });
                            $(this).dialog('close');
                            SalesOrder.calculateTax();
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.UnpayOrder = function () {

        var order_id = $("#id").val();
        var total_amount = $("#total_amount").val();
        $("#dialog").remove();
        data = "Are you sure you want to unpay full amount of this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/unpay-order",
                                data: {order_id: order_id, total_amount: total_amount},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + order_id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.IssueRefund = function () {

        var order_id = $("#id").val();
        var total_amount = $("#total_amount").val();
        data = "Are you sure you want to unpay full amount of this order?";
        $("#container").append("<div id='dialog1'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/issue-refund",
                                data: {order_id: order_id, total_amount: total_amount},
                                beforeSend: function ()
                                {
                                },
                                success: function (data)
                                {

                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                            $(this).dialog('close');
                            SalesOrder.calculateTax();
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.PaidPopup = function () {
        var order_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/sales-order/sales-order-payment",
            data: {order_id: order_id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data) {
                $(document).ajaxStop($.unblockUI);
                $("#dialog").remove();
                $("#container").append("<div id='dialog'></div");
                document.getElementById('dialog').title = 'Sales Order Payment';
                $("#dialog").html("<p>" + data + "</p>");
                $("#dialog").dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 750,
                    width: 1000,
                    buttons:
                            {
                                Cancel: function ()
                                {
                                    $('#dialog').dialog('close');

                                }
                            },
                    modal: true
                });
            }
        });
    };
    this.AddSalesOrderPayment = function () {
        var id = $("#sales_payment_order_id").val();
        var order_id = $("#id").val();
        var payment_method = $("#payment_method_id").val();
        var payment_ref = $("#payment_ref").val();
        var payment_date = $("#payment_date").val();
        var payment_type = $("#payment_type").val();
        var payment_remarks = $("#payment_remarks").val();
        var payment_amount = $("#payment_amount").val();
        var pattern = /^\d+\.?\d{0,2}$/;
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

        var errorCode = 0;
        if (payment_method == '') {
            $("#methoderror").html('Please select payment method');
            errorCode = 1;
        } else {
            $("#methoderror").html('');
        }
        if (payment_date == '') {
            $("#dateerror").html('Please select Date');
            errorCode = 1;
        } else {
            if (date_pattern.test(payment_date)) {
                $("#dateerror").html('');
            }
            else {
                $("#dateerror").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if (payment_amount == '') {
            $("#amounterror").html('Please enter amount');
            errorCode = 1;
        } else {
            if (pattern.test(payment_amount)) {
                $("#amounterror").html('');
            }
            else {
                $("#amounterror").html('Cost should be in digits and can have upto two decimal places');
                errorCode = 1;
            }
        }
        if (errorCode == 1) {
            return false;
        }
        $.ajax({
            url: "/sales-order/insert-sales-order-payment",
            type: "POST",
            data: {payment_amount: payment_amount, id: id, order_id: order_id, payment_method: payment_method, payment_ref: payment_ref, payment_date: payment_date, payment_type: payment_type, payment_remarks: payment_remarks},
            success: function (data) {
                $('#payment-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                    $("#payment_method_id").val('');
                    $("#payment_ref").val('');
                    $("#payment_date").val('');
                    $("#payment_type").val('');
                    $("#payment_remarks").val('');
                    $("#payment_amount").val('');
                    SalesOrder.calculateTax();
                });
            }

        });
    };
    this.editSalesOrderPayment = function (id) {
        $.ajax({
            type: 'POST',
            url: "/sales-order/edit-sales-order-payments",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {

            },
            success: function (data)
            {
                $("#sales_payment_order_id").val(data.id);
                $("#payment_method_id").val(data.payment_method_id);
                $("#payment_ref").val(data.payment_ref);
                $("#payment_type").val(data.payment_type);
                $("#payment_date").val(data.payment_date);
                $("#payment_remarks").val(data.payment_remarks);
                $("#payment_amount").val(data.amount);
                GlobalConfigs.refreshSelect();
            }

        });
    };
    this.deleteSalesOrderPayment = function (id, so_id) {
        data = "Are you sure you want to delete this payment?";
        $("#container").append("<div id='dialog1'></div");
        document.getElementById('dialog1').title = 'Delete Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/delete-sales-order-payments",
                                data: {id: id, so_id: so_id},
                                beforeSend: function ()
                                {
                                },
                                success: function (data)
                                {
                                    $("#dialog1").dialog('close');
                                    $('#payment-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                        SalesOrder.calculateTax();
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $(this).dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.PriceByProductId = function () {
        var item = $("#product_code").val();
        $.ajax({
            url: "/sales-quote/fetch-unit-price-by-product-id",
            data: {item: item},
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $("#unit_price").val(data.price);
            }
        });
    };

    this.MarkInvoived = function () {

        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Mark this order as Invoiced?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/mark-order-invoiced",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.MarkUninvoived = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to Mark this order as Uninvoiced?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/mark-order-uninvoiced",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.OrderIdCheck = function () {
        var order_id = $("#order_id").val();
        $.ajax({
            url: "/sales-order/check-order-id-present",
            data: {order_id: order_id},
            type: "POST",
            success: function (data)
            {
                if (data == "fail") {
                    $("#order_error").html('Order Id Must Be in format S0-000000');
                    $("#order_id").val('');
                    return false;
                }
                if (data == "present") {
                    $("#order_error").html('Order Id is Present');
                    $("#order_id").val('');
                    return false;
                }
                if (data == "not present") {
                    return true;
                }
            }
        });
    };
    this.AddFulfillProduct = function () {
        var id = $("#id").val();
        var product_record_id = $("#rec_product_id").val();
        var product_id = $("#rec_product_code").val();
        var quantity = $("#rec_quantity").val();
        var location_id = $("#receive_location_id").val();
        var errorCode = 0;
        if (product_id == '') {
            $("#rperror").html('Please select product');
            errorCode = 1;
        } else {
            $("#rperror").html('');
            // errorCode =0;
        }

        if (quantity == '') {
            $("#rqtyerror").html('Please enter quantity');
            errorCode = 1;
        } else {
            $("#rqtyerror").html('');
            //errorCode =0;
        }
        if (location_id == '') {
            $("#rlerror").html('Please select location');
            errorCode = 1;
        } else {
            $("#rlerror").html('');
            //errorCode =0;
        }
        if (errorCode == 1) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "/sales-order/add-fulfill-product",
            data: {id: id, product_record_id: product_record_id, product_id: product_id, quantity: quantity, location_id: location_id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                if (data == 'qtyError') {
                    $("#rqtyerror").html('Quantity can not be greater than order Quanitity');
                    return false;
                }
                if (data == 'recQtyExistError') {
                    $("#rqtyerror").html('Quantity can not be greater than order Quanitity');
                    return false;
                }
                $('#receive-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#rec_product_id").val('');
                $("#rec_product_code").val('');
                $("#rec_vendor_pcode").val('');
                $("#rec_quantity").val('');
                $("#receive_location_id").val('');
                $("#receive_date").val('');
                GlobalConfigs.refreshSelect();
                SalesOrder.countTotalQty(id);
                $(document).ajaxStop($.unblockUI);
            }
        });

    };
    this.editFulfillProduct = function (id) {
        var so_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/sales-order/edit-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $("#rec_product_id").val(data.id);
                $("#rec_product_code").val(data.product_id);
                $("#rec_quantity").val(data.quantity);
                $("#receive_location_id").val(data.location_id);
                GlobalConfigs.refreshSelect();
                SalesOrder.countTotalQty(id);


            }
        });

    };
    this.deleteFulfillProduct = function (id, so_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/delete-fulfill-product",
                                data: {id: id, so_id: so_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#receive-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    SalesOrder.countTotalQty(so_id);
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.AutofillFulfillProducts = function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to Fulfill this Order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/autofill-fulfill-products",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#receive-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    document.location.href = '/sales-order/edit/id/' + order_id;
                                    SalesOrder.countTotalQty(order_id);
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.countTotalQty = function (id) {
        $.ajax({
            type: 'POST',
            url: "/sales-order/count-quantity",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
            },
            success: function (data)
            {
                $("#total_ordered").val(data.order_qty);
                $("#total_received").val(data.receive_qty);
                $("#status").val(data.order_status + ',' + data.pay_status);

                if (data.order_status == 'Fulfilled') {
                    var btnHtml = '<button type="button" onclick="SalesOrder.UnfulfilledOrder();" class="btn btn-warning">Unfulfilled</button>';
                    $("#order-btn").html(btnHtml);
                }
                ;

                if (data.order_status == 'Unfulfilled') {
                    var btnHtml = '<button type="button" onclick="SalesOrder.fulfilledOrder();" class="btn btn-success">Fulfilled</button>';
                    $("#order-btn").html(btnHtml);
                }
                ;

                if (data.order_status == 'Paid') {
                    var btnHtml = '<button type="button" onclick="SalesOrder.UnpayOrder();" class="btn btn-warning">Un Pay</button>';
                    $("#pay-btn").html(btnHtml);
                }
                ;

                if (data.order_status == 'Unpaid' || data.order_status == 'Partial') {
                    var btnHtml = '<button type="button" onclick="SalesOrder.PayOrder();" class="btn btn-success">Pay</button>';
                    $("#pay-btn").html(btnHtml);
                }
                ;

            }
        });
    };
    this.AddReturnProduct = function () {
        var id = $("#id").val();
        if ($("#discarded").is(":checked")) {
            var discarded = "1";
        }
        else {
            var discarded = "0";
        }
        var product_record_id = $("#ret_product_id").val();
        var product_id = $("#ret_product_code").val();
        var quantity = $("#ret_quantity").val();
        var unit_price = $("#ret_unit_price").val();
        var discount = $("#ret_discount").val();
        var sub_total = $("#ret_sub_total").val();
        var ret_date = $("#ret_date").val();
        var pattern_quantity = /^\d+$/;
        var pattern = /^\d+\.?\d{0,2}$/;
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

        var errorCode = 0;
        if (product_id == '') {
            $("#ret_perror").html('Please select product');
            errorCode = 1;
        } else {
            $("#ret_perror").html('');
            //errorCode =0;
        }

        if (quantity == '') {
            $("#ret_qtyerror").html('Please enter quantity');
            errorCode = 1;
        } 
        else {
            if(pattern_quantity.test(quantity))  {
                if(quantity <= 0) {
                    $("#ret_qtyerror").html('Quantity should be greater than 0');
                    errorCode=1;
                }
                else {
                    $("#ret_qtyerror").html('');
                }
            }
            else {
                $("#ret_qtyerror").html('Quantity Should be a numerical value');
                errorCode = 1;
            }
            
            //errorCode =0;
        }
        if (unit_price == '') {
            $("#ret_cerror").html('Please enter valid cost');
            errorCode = 1;
        } 
        else {
            if(pattern.test(unit_price))  {
                if(quantity <= 0) {
                    $("#ret_cerror").html('Cost should be greater than 0');
                    errorCode=1;
                }
                else {
                    $("#ret_cerror").html('');
                }
            }
            else {
                $("#ret_cerror").html('Cost should be in digits and can be upto two decimal places.');
                errorCode =1;
            }
            
        }
        if(discount == '') {
            $("#ret_diserror").html('');
        }
        else {
            if(pattern.test(discount)) {
                if (discount > 100) {
                    $("#ret_diserror").html('Discount can not be greater than 100%');
                    errorCode = 1;
                } 
                else {
                     $("#ret_diserror").html('');
                    // errorCode =0;
                }
            }
            else {
                $("#ret_diserror").html('Discount should be in digits and can be upto two decimal places.');
            }
            
         }
        if (ret_date == '') {
            $("#ret_dateerror").html('Return Date can not be blank');
            errorCode = 1;
        } else {
            if(date_pattern.test(ret_date)) {
                $("#ret_dateerror").html('');
            }
            else {
                $("#ret_dateerror").html('Please select a valid date');
                 errorCode =1;
                
            }
            
            // errorCode =0;
        }

        if (errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/sales-order/add-return-product",
            data: {id: id, product_record_id: product_record_id, discarded: discarded, product_id: product_id, quantity: quantity, discount: discount, unit_price: unit_price, sub_total: sub_total, ret_date: ret_date},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                if (data == 'qtyError') {
                    $("#ret_qtyerror").html('Quantity can not be greater than Fulfilled Quantity');
                    return false;
                }
                if (data == 'recQtyExistError') {
                    $("#ret_qtyerror").html('Quantity can not be greater than Fulfilled Quantity');
                    return false;
                }
                $('#ret-product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#ret_product_id").val('');
                $("#ret_product_code").val('');
                $("#ret_quantity").val('0');
                $("#ret_unit_price").val('0.00');
                $("#ret_discount").val('0.00');
                $("#ret_sub_total").val('0.00');
                $("#ret_date").val('');
                SalesOrder.getReturnSubTotal(id);
                
            }
        });

    };

    this.editReturnProduct = function (id) {
        var so_id = $("#id").val();
        $.ajax({
            type: 'POST',
            url: "/sales-order/edit-return-product",
            data: {id: id},
            dataType: "json",
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $("#ret_product_id").val(data.id);
                $("#ret_product_code").val(data.product_id);
                $("#ret_quantity").val(data.quantity);
                $("#ret_unit_price").val(data.unit_price);
                $("#ret_discount").val(data.discount);
                $("#ret_sub_total").val(data.sub_total);
                $("#ret_date").val(data.transaction_date);
                if (data.item_status == "Discarded") {
                    $("#discarded").prop('checked', true);
                }
                GlobalConfigs.refreshSelect();
//                PurchaseOrder.getSubTotal(po_id);

            }
        });

    };

    this.deleteReturnProduct = function (id, so_id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/delete-return-product",
                                data: {id: id, so_id: so_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#ret-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    SalesOrder.getReturnSubTotal(so_id);

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };

    this.calcRetTotal = function () {

        var quantity = $("#ret_quantity").val();
        var unit_price = $("#ret_unit_price").val();
        var discount = $("#ret_discount").val();

        var amount = quantity * unit_price;
        var disAmount = (amount * discount) / 100;
        var sub_total = amount - disAmount;

        $("#ret_sub_total").val(sub_total);

    };

    this.getReturnSubTotal = function (so_id) {
        $.ajax({
            type: 'POST',
            url: "/sales-order/get-return-subtotal",
            data: {so_id: so_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#ret_net_sub_total").val(data.sub_total);

                SalesOrder.calcRetNetTotal();

            }
        });
    };
    this.getPricingUnitPrice = function () {
        var product_id = $("#product_code").val();
        var pricing_id = $("#currency_id").val();
        $.ajax({
            type: 'POST',
            url: "/pricing-currency/get-pricing-unit-price",
            data: {product_id: product_id, pricing_id: pricing_id},
            dataType: "json",
            beforeSend: function ()
            {
//                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $("#unit_price").val(data.price);

            }
        });
    };

    this.calcRetNetTotal = function () {
        var subTotal = $("#ret_net_sub_total").val();
        var fee = $("#ret_fee").val();

        var total = subTotal - fee;
        $("#ret_total").val(total);

    };
    this.AutofillReturnProducts = function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to autofill all products?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/autofill-return-products",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#ret-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    //$("#ret_autofill").remove();
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.AutofillRestockProducts = function () {
        var order_id = $("#id").val();
        var location_id = $("#location_id").val();

        $("#dialog").remove();
        data = "Are you sure you want to autofill all products?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/autofill-restock-products",
                                data: {order_id: order_id, location_id: location_id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);

                                    $('#unstock-product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });
                                    //$("#ret_autofill").remove();
                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.RefundPayOrder = function () {

        var order_id = $("#id").val();
        var balance = $("#balance").val();

        $("#dialog").remove();
        data = "Are you sure you want to refund amount of this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/purchase-order/refund-payment-order",
                                data: {order_id: order_id, balance: balance},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                     $(document).ajaxStop($.unblockUI);
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/purchase-order/edit/id/' + order_id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.CancelSalesOrder = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to cancel this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/cancel-sales-order",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.ReopenSalesOrder = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to Reopen this order?";
        $("#container").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/sales-order/reopen-sales-order",
                                data: {id: id},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/sales-order/edit/id/' + id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    this.CancelledCheck = function () {
        var order_status = $("#status").val();
        if (order_status == "Cancelled") {
            $("#frmorder :input").attr("disabled", true);
//           $("a").css("cursor","arrow").click(false);
            $("#frmorder").removeAttr("href");
            $("#reopen_button").removeAttr('disabled');
            $("#back_to_order").removeAttr('disabled');
        }
    }
}
;

var SalesOrder = new SalesOrder();

function Timesheet() {
    
    this.CalcTotal = function () {
         var ord = $("#ord").val();
         var ord_15 = $("#ord_15").val();
         var ord_20 = $("#ord_20").val();

         var total = parseFloat((ord=='')?'0.00':ord) + parseFloat((ord_15=='')?'0.00':ord_15)  + parseFloat((ord_20=='')?'0.00':ord_20);
         $("#total").val(total);
    };
    
    this.edit = function (id) {
        document.location.href = '/timesheet/edit/id/' + id;
    };

    this.GoToList = function () {
        document.location.href = '/timesheet';
    };

    this.delete = function (id) {
        $("#dialog").remove();
        data = "Are you sure you want to delete this Timesheet?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Delete Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog').dialog('close');
                            document.location.href = '/timesheet/delete/id/' + id;
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
}
var Timesheet = new Timesheet();


function WorkOrder() {
    this.add= function (id) {
        document.location.href = '/work-order/add';
    };

    this.edit = function (id) {
        document.location.href = '/work-order/edit/id/' + id;
    };
    
    this.GoToList = function () {
        document.location.href = '/work-order';
    };

    this.deleteProduct = function (id) {
        var orderId = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to delete this product?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/delete-product",
                                data: {id: id,orderId:orderId},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.StartOrder = function (id) {
        var orderId = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to start this work order?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Start Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/update-order-status",
                                data: {id: id,status:'Started',orderId:orderId},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    $('#product-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.FinishOrder = function (id) {
        var orderId = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to finish this work order?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/finish-order",
                                data: {id: id,orderId:orderId},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                    document.location.href = '/work-order/edit/id/' + orderId;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    
    this.IssueOrder = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to issue work order?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/issue-order",
                                data: {id: id,status:'Issued'},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                     document.location.href = '/work-order/edit/id/'+id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.UnIssueOrder = function () {
        var id = $("#id").val();
        $("#dialog").remove();
        data = "Are you sure you want to issue work order?";
        $("#list-grid_wrapper").append("<div id='dialog'></div");
        document.getElementById('dialog').title = 'Confirmation';
        $("#dialog").html("<p>" + data + "</p>");
        $("#dialog").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/unissue-order",
                                data: {id: id,status:'Issued'},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $('#dialog').dialog('close');
                                    $(document).ajaxStop($.unblockUI);
                                     document.location.href = '/work-order/edit/id/'+id;

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.AddProduct = function () {
        var id = $("#id").val();
        var product_record_id = $("#product_record_id").val();
        var product_id = $("#product_code").val();
        var quantity = $("#quantity").val();
        var errorCode = 0;
        if (product_id == '') {
            $("#perror").html('Please select product');
            errorCode = 1;
        } else {
            $("#perror").html('');
            //errorCode =0;
        }

        if (quantity == '') {
            $("#qtyerror").html('Please enter quantity');
            errorCode = 1;
        } else {
            $("#qtyerror").html('');
            //errorCode =0;
        }
        if (quantity) {
            if (parseInt(quantity) < 1 || isNumberOnly(quantity)) {
                $("#qtyerror").html('Please enter valid quantity');
                errorCode = 1;
            } else {
                $("#qtyerror").html('');
                //errorCode =0;
            }
        }

        if (errorCode == 1) {
            return false;
        }


        $.ajax({
            type: 'POST',
            url: "/work-order/add-product",
            data: {id: id,product_record_id: product_record_id, product_id: product_id,quantity: quantity,description:''},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {

                $('#product-list').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
                $("#purchase_product_id").val('');
                $("#product_code").val('');
                $("#quantity").val('');
                $(document).ajaxStop($.unblockUI);
            }
        });

    };

    this.bomPopup = function (id) {
        $.ajax({
            url: "/work-order/bom-popup",
            data: {id: id},
            beforeSend: function ()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function (data)
            {
                $(document).ajaxStop($.unblockUI);
                $("#dialog").remove();
                $("#container").append("<div id='dialog'></div");
                document.getElementById('dialog').title = 'Work Progress';
                $("#dialog").html("<p>" + data + "</p>");
                $("#dialog").dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 500,
                    width: 700
                });
                
            }
        });

    };
    
    
    this.updateBomStatus = function (id,pid,status) {
        var location = $("#location_id").val();
        var chkStock = $("#chkId-"+id).val();
        if(chkStock =='U' && status=='Started'){
            alert('Product is not in stock, please tick and create purchase');
            return false;
        }
        
        $("#dialog1").remove();
        data = "Are you sure you want to "+status+" this work order?";
        $("#bom-list").append("<div id='dialog1'></div");
        document.getElementById('dialog1').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog1').dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/update-bom-status",
                                data: {id: id,pid:pid,status:status,location:location},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                    $('#bom-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog1').dialog('close');
                        }
                    },
            modal: true
        });
    };
     this.voidBomStatus = function (id,pid) {
         var location = $("#location_id").val();
         $("#dialog1").remove();
        data = "Are you sure you want to void this product?";
        $("#bom-list").append("<div id='dialog1'></div");
        document.getElementById('dialog1').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            $('#dialog1').dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/void-bom-status",
                                data: {id: id,pid:pid,status:status,location:location},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                    $('#bom-list').fadeTo(500, .5, function () {
                                        $(this).html(data).fadeTo(500, 1);
                                    });

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog1').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    this.CreatePurchase = function (id,pid) {
        
        //foreach($('input:radio[name=setdefault]:checked','#addressGrid').val();)
        
        var sList = "";
        $('input[type=checkbox]:checked').each(function () {
            sList += $(this).val()+',';
        });
//        console.log (sList);
//        return false;     
                        
        $("#dialog1").remove();
        data = "Are you sure you want to create purchase?";
        $("#bom-list").append("<div id='dialog1'></div");
        document.getElementById('dialog1').title = 'Confirmation';
        $("#dialog1").html("<p>" + data + "</p>");
        $("#dialog1").dialog({
            bgiframe: true,
            resizable: false,
            height: 180,
            width: 400,
            buttons:
                    {
                        Yes: function ()
                        {
                            
                            $('#dialog1').dialog('close');
                            $.ajax({
                                type: 'POST',
                                url: "/work-order/create-purchase",
                                data: {pids:sList},
                                beforeSend: function ()
                                {
                                    $.blockUI({message: '<h1>Please Wait...</h1>'});
                                },
                                success: function (data)
                                {
                                    $(document).ajaxStop($.unblockUI);
                                     document.location.href = '/purchase-order/add';

                                }
                            });
                        },
                        No: function ()
                        {
                            $('#dialog1').dialog('close');
                        }
                    },
            modal: true
        });
    };
    
    
}
var WorkOrder = new WorkOrder();


function GlobalConfigs() {
    this.refreshSelect = function () {
        $(".custom-select").each(function () {
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");

            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);

        });

        $(".custom-select").change(function () {
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        });
    };
}
;

var GlobalConfigs = new GlobalConfigs();

function Dashboard() {
    this.Filter = function() {
        var lines= $("#line_select").val();
        var from= $("#date_from").val();
        var to= $("#date_to").val();
        var group = $("#group_by").val();
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        var errorCode = 0;
        if(lines == '') {
            $("#lines_error").html('Please Select an Option From Lines');
            errorCode = 1;
        }
        else {
            $("#lines_error").html('');
        }
        if(group == '') {
            $("#group_error").html("This field can't be empty, Please Select an Option From Group");
            errorCode = 1;
        }
        else {
            $("#group_error").html('');
        }
        if(from == '') {
            $("#from_error").html('');    
        }
        else {
            if(date_pattern.test(from)) {
                $("#from_error").html('');
            }
            else {
                $("#from_error").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if(to == '') {
            $("#from_error").html('');    
        }
        else {
            if(date_pattern.test(to)) {
                $("#to_error").html('');
            }
            else {
                $("#to_error").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if(errorCode == 1) {
            return false;
        }
        else {
            document.location.href = '/dashboard/dashboard-chart/line/'+lines+'/from/'+from+'/to/'+to+'/group/'+group ;
        }
        
    };
    this.TopBottom = function() {
        var top_bottom_option = $("#top_bottom_option").val();
        var top_bottom = $("#top_bottom").val();
        var top_bottom_limit = $("#top_bottom_limit").val();
        var date_from_top_bottom = $("#date_from_top_bottom").val();
        var date_to_top_bottom = $("#date_to_top_bottom").val();
        var date_pattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        var errorCode = 0;
        var pattern = /^\d+$/;
        if(top_bottom_option == '') {
            $("#top_bottom_option_error").html("This field can't be empty,Please Select from Select List");
            errorCode = 1;
        }
        else {
            $("#top_bottom_option_error").html('');
        }
        if(top_bottom == '') {
            $("#top_bottom_error").html("This field can't be empty,Please Select from Select List");
            errorCode = 1;
        }
        else {
            $("#top_bottom_error").html('');
        }
        if(date_from_top_bottom == '') {
            $("#date_from_top_bottom_error").html('');    
        }
        else {
            if(date_pattern.test(date_from_top_bottom)) {
                $("#date_from_top_bottom_error").html('');
            }
            else {
                $("#date_from_top_bottom_error").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if(date_to_top_bottom == '') {
            $("#date_to_top_bottom_error").html('');    
        }
        else {
            if(date_pattern.test(date_to_top_bottom)) {
                $("#date_to_top_bottom_error").html('');
            }
            else {
                $("#date_to_top_bottom_error").html('Please Select a Valid Date');
                errorCode = 1;
            }
        }
        if(top_bottom_limit == '') {
            $("#top_bottom_limit_error").html('');    
        }
        else {
            if(pattern.test(top_bottom_limit)) {
                $("#top_bottom_limit_error").html('');
            }
            else {
                $("#top_bottom_limit_error").html('Limit should be a numerical value');
                errorCode = 1;
            }
        }

        if(errorCode == 1) {
            return false;
        }
        $.ajax({
           type:"POST",
           url:"/dashboard/get-top-bottom-list",
           data:{top_bottom_option:top_bottom_option,top_bottom:top_bottom,top_bottom_limit:top_bottom_limit,date_from_top_bottom:date_from_top_bottom,date_to_top_bottom:date_to_top_bottom},
           success: function(data) {
               $('#top_bottom_display').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
           }
        });
    };
    this.DisplayDetails = function() {
        $.ajax({
           url:"/dashboard/display-details",
           success: function(data) {
               $("#outstanding_div").html('');
               $("#outstanding_details").hide('fast');
               $("#outstanding_summary").show('fast');
               $('#outstanding_div').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
           }
        });
    };
    this.DisplaySummary = function() {
        $.ajax({
           url:"/dashboard/display-summary",
           success: function(data) {
               $("#outstanding_div").html('');
               $("#outstanding_details").show('fast');
               $("#outstanding_summary").hide('fast');
               $('#outstanding_div').fadeTo(500, .5, function () {
                    $(this).html(data).fadeTo(500, 1);
                });
           }
        });
    };
    this.PieChart = function() {
        var lines = $("#lines").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var group = $("#group").val();
        var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'total' },
                    { name: 'date' },
                    { name: 'total_name' }
                ],
                url: '/dashboard/get-data/line/'+lines+'/from/'+from+'/to/'+to+'/group/'+group
            };
            var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
            // prepare jqxChart settings\
            var settings = {
                title: $("#line_select option:selected").text(),
                description: "",
                enableAnimations: true,
                showLegend: true,
                showBorderLine: true,
                legendLayout: { left: 50, top: 480, width: 300, height: 400, flow: 'vertical' },
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
                source: dataAdapter,
                colorScheme: 'scheme03',
                seriesGroups:
                    [
                        {
                            type: 'pie',
                            showLabels: true,
                            series:
                                [
                                    { 
                                        dataField: 'total',
                                        displayText: 'date',
                                        labelRadius: 140,
                                        initialAngle: 15,
                                        radius: 115,
                                        centerOffset: 0,
                                        formatFunction: function (value) {
                                            if (isNaN(value))
                                                return value;
                                            return parseFloat(value);
                                        },
                                    }
                                ]
                        }
                    ]
            };  
            // setup the chart
            $('#chartContainer').jqxChart(settings);
    };
    
    this.BarChart = function() {
        var lines = $("#lines").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var group = $("#group").val();
        var sampleData = {
                datatype: "json",
                datafields: [
//                    {name:'count',total:'total',date:'date',total_name:'total_name'}
                    {name: 'total'},
                    {name: 'date'},
                    {name: 'total_name'}
                ],
                url: '/dashboard/get-data/line/'+lines+'/from/'+from+'/to/'+to+'/group/'+group
            };
         var dataAdapter = new $.jqx.dataAdapter(sampleData, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + sampleData.url + '" : ' + error); } });
   
        // prepare jqxChart settings
           var settings = {
                title: $("#line_select option:selected").text(),
                borderLineWidth: 1,
                showBorderLine: true,
                enableAnimations: true,
                description: '',
                showLegend: false,
                //padding: { left: 5, top: 5, right: 10, bottom: 5 },
                //titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
                source: dataAdapter,
                xAxis:
                {
                    //description: 'Year and quarter',
                    dataField: 'date',
                    unitInterval: 1,
                    textRotationAngle: -90,
                    formatFunction: function (value, itemIndex, serie, group) {
                        return value;
                    },
                    valuesOnTicks: false
                },
                valueAxis: {
                    unitInterval: 5000,
//                    minValue: ,
//                    maxValue: 50,
                    title: { text: 'Time in minutes<br><br>' },
                    labels: { horizontalAlignment: 'right' }
                },
                colorScheme: 'scheme05',
                seriesGroups:
                [
                    {
                        type: 'column',
//                        valueAxis:
//                        {
//                            description: 'Cost in $',
//                            formatFunction: function (value) {
//                                return value + '';
//                            },
////                            maxValue: 10,
////                            minValue: -10,
//                        },
                        series:
						    [
                                {
                                    dataField: 'total',
                                    displayText: '',
                                    toolTipFormatFunction: function(value, itemIndex, serie, group, categoryValue, categoryAxis) {
                                        return '<DIV style="text-align:left";><b>Year-Quarter: </b>' + categoryValue
                                                 + '<br /><b>' + categoryValue +'</b> ' + value + ' </DIV>'
                                    },
                                    // Modify this function to return the desired colors.
                                    // jqxChart will call the function for each data point.
                                    // Sequential points that have the same color will be
                                    // grouped automatically in a line segment
                                    colorFunction: function (value, itemIndex, serie, group) {
                                        return (value < 0) ? '#CC1133' : '#55CC55';
                                    }
                                }
                            ]
                    }
                ]
            }
            // setup the chart
            $('#chartContainer_1').jqxChart(settings);
              // prepare chart data as an array
    };
    
    this.LineChart = function() {
        var lines = $("#lines").val();
        var from = $("#from").val();
        var to = $("#to").val();
        var group = $("#group").val();
         var sampleData = {
                datatype: "json",
                datafields: [
//                    {name:'count'},
                    {name: 'total'},
                    {name: 'date'},
                    {name: 'total_name'}
                ],
                url: '/dashboard/get-data/line/'+lines+'/from/'+from+'/to/'+to+'/group/'+group
            };
        var dataAdapter = new $.jqx.dataAdapter(sampleData, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + sampleData.url + '" : ' + error); } });
        var settings = {
                title: $("#line_select option:selected").text(),
                description: "",
                enableAnimations: true,
                showLegend: true,
                padding: { left: 10, top: 10, right: 15, bottom: 10 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: dataAdapter,
                colorScheme: 'scheme05',
                xAxis: {
                    unitInterval: 1,
                    dataField:'date',
                    displayText: 'Date',
                    textRotationAngle: -90,
                    tickMarks: {visible: true, interval: 500 },
                    gridLinesInterval: { visible: true, interval: 500 },
                    valuesOnTicks: false,
                    padding: { bottom: 10 }
                },
                valueAxis: {
                    unitInterval: 5000,
//                    minValue: 0,
//                    maxValue: 40000,
                    title: { text: 'Cost in $' },
                    labels: { horizontalAlignment: 'right' }
                },
                seriesGroups:
                    [
                        {
                            type: 'line',
//                            valueAxis:
//                            {
//                                visible: true,
//                                title: { text: 'Cost in($)' }
//                            },
                            series:
                            [
                                {
                                    dataField: 'total',
                                    symbolType: 'square',
                                    labels:
                                    {
                                        visible: true,
                                        backgroundColor: '#FEFEFE',
                                        backgroundOpacity: 0.2,
                                        borderColor: '#7FC4EF',
                                        borderOpacity: 0.7,
                                        padding: { left: 5, right: 5, top: 0, bottom: 0 }
                                    }
                                },
                            ]
                        }
                    ]
            };
            // setup the chart
            $('#chartContainer_2').jqxChart(settings);
        };
}
var Dashboard = new Dashboard;

$(function(){
	$('.slideArrow').click(function(){
    	if($(this).hasClass('rightMov')){
	       $(this).removeClass('rightMov'); 
        }else{
			$(this).addClass('rightMov');
		}
        
    });
});  
