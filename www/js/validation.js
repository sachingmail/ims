/*
* this script contains all the validation functions
* currentlly we have defined follwing validation function
	1-isNumber1():to validate wheather a given field value is number or not
	2-
	




 *function			:to validate number
 *@param field		:object of type html form element
*/
function isNumberOnly(elVal) {
    var isvalid =0;  
    var valid = "0123456789";
      var temp;
      for (var i=0; i<elVal.length; i++) {
        temp = elVal.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
            isvalid = 1;
         }
      }
      return isvalid;
}

function isNumber1(elVal) {
    var isvalid =0;  
    var valid = "0123456789.";
      var temp;
      for (var i=0; i<elVal.length; i++) {
        temp = elVal.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
            isvalid = 1;
         }
      }
      return isvalid;
}

function isNumberValidation(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
      var valid = "0123456789.";
      var temp;
      for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
        	//var msg = "Please enter a valid number";
			//set_cookie('err_flag','Y',null,null,null,'/');
         var msg="Please Put The Valid number";
          field.select();
          field.focus();
          return msg;
         }
      }
    }
   return false;
}
//alphanumeric
function isAlphaNumeric1(elId) {
  var field=document.getElementById(elId);
  if (field) {
    trimFld(field);
    var valid = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-.& / ( )";
    var temp;
    for (var i=0; i<field.value.length; i++) {
    	temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    
		{
			var msg = "Please do not enter special characters";
			set_cookie('err_flag','Y',null,null,null,'/');
          //alert("Please do not enter special characters");
          field.select();
          field.focus();
          return msg;
        }
      }
    }
	
  return false;
}

/* function			:to trim a field value
 *@param field		:object of type html form element
*/
function trimFld(field)    {
  if (field) {
    fieldval = field.value;
    var nReturn = 0;
    for (i=0; i<field.value.length; i++)    {
        temp = field.value.substring(i,i+1);
        if (temp != " ")    {
          if (temp == "\r") {
            if (nReturn == 0) nReturn++;
            else { i--; break; }
          }
          else if (temp == "\n") {
            if (nReturn == 1) nReturn = 0;
            else { i--; break; }
          }
          else break;
        }
    }
    field.value = fieldval.substring(i,fieldval.length);
    fieldval = field.value;
    nReturn = 0;
    for (i=field.value.length; i>0; i--)    {
        temp = field.value.substring(i-1,i);
        if (temp != " ")    {
          if (temp == "\n") {
            if (nReturn == 0) nReturn++;
            else { i++; break; }
          }
          else if (temp == "\r") {
            if (nReturn == 1) nReturn = 0;
            else { i++; break; }
          }
          else break;
        }
    }
    field.value = fieldval.substring(0,i);
  }
}

//alphabet
function isAlphabet1(elId) {
	var field=document.getElementById(elId);
  	if (field) {
    trimFld(field);
    var valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var temp;
    for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
			var msg = "Please enter only alphabets";
			displayTip(field,msg);
         // alert("Please enter only alphabets ");
          field.select();
          field.focus();
          return false;
        }
      }
    }
  return true;
}

//alphabet with space
function isAlphabet1Space(field) {
  if (field) {
    trimFld(field);
    var valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ , ";
    var temp;
    for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) != "-1")    {
			var msg = "Please enter only alphabets";
			errorDisplay(field,msg);
         // alert("Please enter only alphabets ");
          field.select();
          field.focus();
          return false;
        }
      }
    }
	errorHide(field);
  return true;
}

//alphabet with only hyphen
function isAlphabetWithH(field,bSubmit,strLabel) {
  if (field) {
    trimFld(field);
    var valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -  ";
    var temp;
    for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
			var msg = "Please enter only alphabets";
			errorDisplay(field,msg);
         // alert("Please enter only alphabets ");
          field.select();
          field.focus();
          return false;
        }
      }
    }
	errorHide(field);
  return true;
}
//digit only

function isNumberWithoutDecimal(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
    var valid = "0123456789";
    var temp;
    for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
        if (valid.indexOf(temp) == "-1")    {
			var msg = "Accepts only 0-9";
			set_cookie('err_flag','Y',null,null,null,'/');
         // alert("Please enter a valid number");
          field.select();
          field.focus();
          return msg;
        }
      }
    }
  return false;
}

function isPinCode(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
   
	    if(field.value.length>6)
	    {
	    	var msg = "Zip Code Should be less then 7 Digit";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
	    
	    if(field.value.length<4)
	    {
	    	var msg = "Zip Code Should be Greater then 3 Digit";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
    
   
    }
  return false;
}

function isMobile(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
   
	    if(field.value.length>10)
	    {
	    	var msg = "Mobile No Should be 10 Digits";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
	    
	    if(field.value.length<1)
	    {
	    	var msg = "Mobile No Should be 10 Digits";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
    
   
    }
  return false;
}

function isCountryCode(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
   
	    if(field.value.length>2)
	    {
	    	var msg = "Country Code Should be max 2 Digits";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
	    
	    if(field.value.length<1)
	    {
	    	var msg = "Country Code Should be max 2 Digits";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
    
   
    }
  return false;
}


//Number greater than 0 and less then 100

function Validate1To100(elId) {
	field=document.getElementById(elId);
	if (field) {
    trimFld(field);
   
	    if(field.value>100)
	    {
	    	var msg = "Number Should be less or equal to than 100";
			set_cookie('err_flag','Y',null,null,null,'/');
	      //alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
	    
	    if(field.value<1)
	    {
	    	var msg = "Number should be greater then or equal to 0";
			set_cookie('err_flag','Y',null,null,null,'/');
	     // alert("Please enter a valid number");
	      field.select();
	      field.focus();
	      return msg;
	    }
    
   
    }
  return false;
}

//email
function isEmailID(elId) {
	field=document.getElementById(elId);
	trimFld(field);
	emailStr = field.value;
	if(emailStr=='')
			return false;
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
	//changed now
	var valid = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-.@";
	var temp;
	for (var i=0; i<field.value.length; i++){
		temp = field.value.substring(i,i+1);
		if (valid.indexOf(temp) == "-1")    {
			var msg = "Please verify that the 'EmailId' does not have spaces or special characters.";
			set_cookie('err_flag','Y',null,null,null,'/');
			return msg;
		}
	}
	var matchArray=emailStr.match(emailPat);
	if(matchArray==null) {
		var msg = "Please verify that the 'EmailId' is correctly filled";
		set_cookie('err_flag','Y',null,null,null,'/');
		return msg  ;
	}
	var user=matchArray[1];
	var domain=matchArray[2];
  
	if (user.match(userPat)==null) {
		// user is not valid
		var msg = "Please verify that the 'Username' in 'EmailID' is valid";
		set_cookie('err_flag','Y',null,null,null,'/');
		return msg;
	}
  
	var IPArray=domain.match(ipDomainPat);
	if (IPArray!=null) {
		// this is an IP address
		for (var i=1;i<=4;i++) {
			if (IPArray[i]>255) {
				var msg = "Please verify that the 'Destination IP address' in the 'EmailID' is valid";
				set_cookie('err_flag','Y',null,null,null,'/');
				return msg;
			}
		}
    //return false;
	}
  
	var domainArray=domain.match(domainPat);
	if (domainArray==null) {
		var msg = "Please verify that the 'Domain name' in the 'EmailID' is valid";
		set_cookie('err_flag','Y',null,null,null,'/');
		return msg;
	}
  
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;
	if (domArr[domArr.length-1].length<2||domArr[domArr.length-1].length>3) {
		// the address must end in a two letter or three letter word.
		var msg = "Please verify that the 'EmailID' ends in a three-letter domain, or two letter country.";
		set_cookie('err_flag','Y',null,null,null,'/');
		return msg;
	}
  
	if (len<2) {
		var msg = "Please verify that the 'Hostname' in the 'EmailID' is valid";
		set_cookie('err_flag','Y',null,null,null,'/');
		return msg;
	}
	return false;
}
//phone
function isNumberPhone(field) {
	field=document.getElementById(field);
	trimFld(field);
	if (field) {
    trimFld(field);
     var valid = "0123456789,. -/()";
    var temp;
    for (var i=0; i<field.value.length; i++) {
        temp = field.value.substring(i,i+1);
       	if (valid.indexOf(temp) == "-1")    {
				var msg = "Please enter only numbers and use comma/hyphen/space/dot/Slash as separator";
				set_cookie('err_flag','Y',null,null,null,'/');
	         // alert("Please enter a valid number");
	          field.select();
	          field.focus();
	          return msg;
        }
      }
    }
return false;
}
function isValidAge(field){
	//alert(field);
	field=document.getElementById(field);
	trimFld(field);
	if (field) {
    trimFld(field);
     //look for "Hello"
    var patt=/^\d+([.]((\d)|(1[0-1])))?$/;
    var result=patt.test(field.value);
    if(!result && field.value.length!=0){
				var msg = "Please Enter Experience in proper format(years.months)";
				set_cookie('err_flag','Y',null,null,null,'/');
	         // alert("Please enter a valid number");
	          field.select();
	          field.focus();
	          return msg;
    	}
	}
return false;
}

function isValidDate()
{
	field=document.getElementById(field);
	dateArray=field.split();
	month=dateArray[0];
	date=dateArray[1];
	year=dateArray[2];
	monthDaysArray={1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
	
	if(month >12 || month < 1 )
		error=true;
	
	
	//if (a%400==0 || (a%4==0  &&  a%100 != 0 ) )
			
	
		
}