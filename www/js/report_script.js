/*
 * @Function Users
 * @author  Amit Kumar
 * This function used for User update, delete.
 */

function InvenrotyReport() {
      
    this.getProductCost= function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        $.ajax({
            url: "/reports/get-product-cost",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
    };
    
    this.getInventorySummary = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        $.ajax({
            url: "/reports/get-inventory-summary",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
    };
    
    this.getInventoryDetails = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var location = $("#location_id").val();
        $.ajax({
            url: "/reports/get-inventory-details",
            data: {category_id:category_id,product_name:product_name,location:location},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getInventoryHistory = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        $.ajax({
            url: "/reports/get-inventory-history",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getInventoryMovement = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var location = $("#location_id").val();
        $.ajax({
            url: "/reports/get-inventory-movement",
            data: {category_id:category_id,product_name:product_name,location:location},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getInventoryMovementSummary = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var location = $("#location_id").val();
        $.ajax({
            url: "/reports/get-inventory-movement-summary",
            data: {category_id:category_id,product_name:product_name,location:location},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getInventoryByLocation = function() {
        var location_id = $("#location_id").val();
        $.ajax({
            url: "/reports/get-inventory-by-location",
            data: {location_id:location_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                $(document).ajaxStop($.unblockUI);
            }
        });
    };
    this.getProductList = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        $.ajax({
            url: "/reports/get-product-price-list",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getVendorProductList = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var vendor_id = $("#vendor_id").val();
        $.ajax({
            url: "/reports/get-vendor-product-list",
            data: {category_id:category_id,product_name:product_name,vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getVendorProductList = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var vendor_id = $("#vendor_id").val();
        $.ajax({
            url: "/reports/get-vendor-product-list",
            data: {category_id:category_id,product_name:product_name,vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
};

var InvenrotyReport = new InvenrotyReport();

/*Purchase Order*/

function PurchaseReport() {
       
       
    this.getVendorProductList = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var vendor_id = $("#vendor_id").val();
        $.ajax({
            url: "/reports/get-vendor-product-list",
            data: {category_id:category_id,product_name:product_name,vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getVendorList = function() {
        var payment_terms = $("#terms_id").val();
        var taxing_schemes = $("#taxingScheme").val();
        var currency_id = $("#currency").val();
        $.ajax({
            url: "/reports/get-vendor-list",
            data: {payment_terms:payment_terms,taxing_schemes:taxing_schemes,currency_id:currency_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getPurchaseSummary = function() {
        var vendor_id = $("#vendor_id").val();
       
        $.ajax({
            url: "/reports/get-purchase-summary",
            data: {vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    this.getPurchaseDetails = function() {
        var vendor_id = $("#vendor_id").val();
       
        $.ajax({
            url: "/reports/get-purchase-details",
            data: {vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getPurchaseTax = function() {
        var vendor_id = $("#vendor_id").val();
        var taxing_scheme_id = $("#taxing_scheme_id").val();
       
        $.ajax({
            url: "/reports/get-purchase-tax",
            data: {vendor_id:vendor_id,taxing_scheme_id:taxing_scheme_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
};

var PurchaseReport = new PurchaseReport();


/*Purchase Order*/

function SalesReport() {
       
       
    this.getVendorProductList = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var vendor_id = $("#vendor_id").val();
        $.ajax({
            url: "/reports/get-vendor-product-list",
            data: {category_id:category_id,product_name:product_name,vendor_id:vendor_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getVendorList = function() {
        var payment_terms = $("#terms_id").val();
        var taxing_schemes = $("#taxingScheme").val();
        var currency_id = $("#currency").val();
        $.ajax({
            url: "/reports/get-vendor-list",
            data: {payment_terms:payment_terms,taxing_schemes:taxing_schemes,currency_id:currency_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getSalesSummary = function() {
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-sales-summary",
            data: {customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    this.getSalesByProductSummary = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
       
        $.ajax({
            url: "/reports/get-sales-by-product-summary",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getSalesByProductDetails = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
       
        $.ajax({
            url: "/reports/get-sales-by-product-details",
            data: {category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getSalesOrderOperational = function() {
        var pay_status = $("#pay_status").val();
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-sales-order-operational",
            data: {pay_status:pay_status,customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getSalesTax = function() {
        var taxing_scheme_id = $("#taxing_scheme_id").val();
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-sales-tax",
            data: {taxing_scheme_id:taxing_scheme_id,customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getCustomers = function() {
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-customer-list",
            data: {customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getSalesReps = function() {
        var sales_reps_id = $("#sales_reps_id").val();
       
        $.ajax({
            url: "/reports/get-sales-reps-order",
            data: {sales_reps_id:sales_reps_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getCustomerPaymentSummary = function() {
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-customer-payment-summary",
            data: {customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getCustomerPaymentDetails = function() {
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-customer-payment-details",
            data: {customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
            }
        });
    };
    
    this.getCustomerOrderHistory = function() {
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-customer-order-history",
            data: {customer_id:customer_id},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getProductCustomerReport = function() {
        var category_id = $("#category_id").val();
        var product_name = $("#product_name").val();
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-product-customer-report",
            data: {customer_id:customer_id,category_id:category_id,product_name:product_name},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getSalesOrderProfit = function() {
       var inv_status = $("#inv_status").val();
        var pay_status = $("#pay_status").val();
        var customer_id = $("#customer_id").val();
       
        $.ajax({
            url: "/reports/get-sales-order-profit",
            data: {customer_id:customer_id,inv_status:inv_status,pay_status:pay_status},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
};

var SalesReport = new SalesReport();

function TimesheetReport() {
    
    this.getTimesheetSummary = function() {
        var emp_id = $("#emp_id").val();
        var date_from = $("#date_from").val();
        var date_to = $("#date_to").val();
       
        $.ajax({
            url: "/timesheet/get-timesheet-summary",
            data: {emp_id:emp_id,date_from:date_from,date_to:date_to},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
    
    this.getTimesheetDetails = function() {
        var emp_id = $("#emp_id").val();
        var date_from = $("#date_from").val();
        var date_to = $("#date_to").val();
       
        $.ajax({
            url: "/timesheet/get-timesheet-details",
            data: {emp_id:emp_id,date_from:date_from,date_to:date_to},
            beforeSend: function()
            {
                $.blockUI({message: '<h1>Please Wait...</h1>'});
            },
            success: function(data)
            {
                 $('#tbl-rpt').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
                 $(document).ajaxStop($.unblockUI);
                
            }
        });
    };
};
var TimesheetReport = new TimesheetReport();


function Filter() {
    
    this.getProducts = function() {
        var category_id = $("#category_id").val();
        $.ajax({
            url: "/reports/get-products",
            data: {category_id:category_id},
            beforeSend: function()
            {
            },
            success: function(data)
            {
                $('#products').fadeTo(500, .5, function() {
                    $(this).html(data).fadeTo(500, 1);
                });
            }
        });
    };
};
var Filter = new Filter();
