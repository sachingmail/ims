<?php

// Disabling the application cache with headers
header("Expires: Tue, 01 Jan 1994 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/**
 * Get the library paths
 */
//$zendLibDir = $platformConfig['lib']['zend_lib_dir'];
  $zendLibDir = realpath(dirname(__FILE__) . '/../library');




/**
 * Add Zend library to the include path
 */
set_include_path($zendLibDir . PATH_SEPARATOR . get_include_path());

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library/'),
    get_include_path(),
)));


/**
 * Zend Autoloader
 */
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();



/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
        ->run();
