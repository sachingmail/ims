<?php
/**
 * Exceptions
 *
 */
class DMC_Exception {


     /**
      *  writeLogMessage
      *
      *
      * log levels can one of:-
      *
      *     EMERG   = 0;  // Emergency: system is unusable
      *     ALERT   = 1;  // Alert: action must be taken immediately
      *     CRIT    = 2;  // Critical: critical conditions
      *     ERR     = 3;  // Error: error conditions
      *     WARN    = 4;  // Warning: warning conditions
      *     NOTICE  = 5;  // Notice: normal but significant condition
      *     INFO    = 6;  // Informational: informational messages
      *     DEBUG   = 7;  // Debug: debug messages
      *
      *  Zend log level contstants:-
      *
      *      Zend_Log::EMERG
      *      Zend_Log::ALERT
      *      Zend_Log::CRIT
      *      Zend_Log::ERR
      *      Zend_Log::WARN
      *      Zend_Log::NOTICE
      *      Zend_Log::INFO
      *      Zend_Log::DEBUG
      *
      *
      *  @param  string $message
      *  @param  int    $logLevel   Zend log level - see Zend Logger
      *  @return void
      */
    public static function processError($logLevel, $message)
    {
		$appLogger = Zend_Registry::get('appLog');

        /*
         * get the file and line number backtrace info
		 */
        $bt = debug_backtrace();
        $filename   = isset($bt[0]['file']) ? $bt[0]['file'] : 'unknown';
        $linenumber = isset($bt[0]['file']) ? $bt[0]['line'] : 0;

        /*
         * write to the log file
		 */
        $appLogger->log($message, $logLevel);
		$appLogger->log('Line: '.$linenumber.' File: '.$filename, $logLevel);

        /*
         * redirect to error page
		 */
		echo $message;
		exit;
        //header('Location:/admin/error/error');
		//exit(0);

        return;
    }

}