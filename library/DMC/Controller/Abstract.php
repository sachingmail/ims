<?php

/**
 * Abstract
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class DMC_Controller_Abstract extends Zend_Controller_Action {

    protected $_messages;
    protected $_table;
    
    /**
     * function :Get form data in order for form type content
     * 
     * @param 
     * 
     * @return numbered array
     */
    public function init() {
        
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }

        $model = new Application_Model_Roles();
        //Report Validate
        $reportData = $model->getScreenData('/reports');
        $settings = $model->getScreenData('/settings');
                
        $this->view->reportAccess = $model->validateScreen($reportData['id'],'view');
        $this->view->settingsAccess = $model->validateScreen($settings['id'],'view');
        
        $this->view->menuSettingData = $model->getSettingMenu();
        
        $controllerName = $this->_request->getControllerName();

        $screenData = $model->getScreenData('/' . $controllerName);
        $this->view->screenHeading = $screenData['screen_name'];
        
         //Flash message
        $messages = $this->_helper->flashMessenger->getMessages();
        if(!empty($messages))
            $this->_helper->layout->getView()->message = $messages[0];

    }

    /**
     * function :show Error
     * 
     * @param  $error:either string or array
     * 
     * @return 
     */
    public function showError($error) {
        if (is_array($error)) {
            $this->view->errors = $error;
        } elseif ($error != '') {
            $this->view->err = $error;
        } else {
            $this->view->err = '';
        }
    }

    /**
     * function :show Message
     * 
     * @param  $msg:string
     * 
     * @return 
     */
    public function showMessage($msg) {
        if ($msg == '')
            $this->view->msg = '';
        else
            $this->view->msg = $msg;
    }

    /**
     * function :set Message Object
     * 
     * @param 
     * 
     * @return 
     */
    public function setMessages($confg) {
        $this->_messages = $confg;
    }

    /**
     * function :change an array of objects to associative array
     * 
     * @param array of object
     * 
     * @return associative array
     */
    public function object_to_array($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

}
