<?php

/**
 *
 * @author Amit kumar
 * @version 
 */
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';

/**
 * AVS_Helper_Abstract Action Helper 
 * 
 * @uses actionHelper Zend_Controller_Action_Helper
 */
class DMC_Helper_Abstract extends Zend_Controller_Action_Helper_Abstract {

    /**
     * @var Zend_Loader_PluginLoader
     */
    public $pluginLoader;
    protected $_messages;

    /**
     * Constructor: initialize plugin loader
     * 
     * @return void
     */
    public function __construct() {
        // TODO Auto-generated Constructor
        //$this->_messages=Zend_Registry::get('messages');
        $this->pluginLoader = new Zend_Loader_PluginLoader ( );
    }

    /* public function validate($model=null){
      return array();
      } */

    public function preBiz($model = null) {
        return array();
    }

    public function postBiz($model = null) {
        return array();
    }

    /**
     * Strategy pattern: call helper as broker method
     */
    public function direct() {
        // TODO Auto-generated 'direct' method
    }

}

