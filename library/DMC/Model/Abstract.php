<?php

class DMC_Model_Abstract {

    /**
     * The default table name 
     */
    protected $_db;
    protected $created;
    protected $created_by;
    protected $updated;
    protected $updated_by;
    protected $reportaccess;
    

    /**
     * Function for Constructor
     *
     * @param	
     *
     * @return	
     */
    public function __construct() {
        if (!$this->_db instanceof Zend_Db_Adapter_Pdo_Mysql) {
            $this->_db = Zend_Registry::get('db');
        }
    }

    public function getMenu() {
        try {
            $statement =$this->_db->prepare('
                SELECT
                distinct mt.id,
                mt.type,
				mt.sidebar_class
                from 
                user_menutype_m as mt
                inner join user_menu_m as a 
                    on a.menu_type_id = mt.id
                inner join user_roles_rights as b
                on b.screen_id=a.id and b.view=1
                WHERE b.role_id= :roleId and footer_menu=1
                order by mt.sequence
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $menus = array();

            if (count($resultSet)) {
                $menus = $resultSet;
            }

            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Menus');
        }
       return $menus; 
    }
    
    public function getSettingMenu() {
        try {
            $statement =$this->_db->prepare('
                select
                a.id,
                a.screen_name,
                a.url
                from user_menu_m as a
                inner join
                user_roles_rights as b
                on 
                b.screen_id = a.id and b.view=1
                where b.role_id= :roleId 
                and a.menu_type_id= 7
                order by a.sequence
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $menus = array();

            if (count($resultSet)) {
                $menus = $resultSet;
            }

            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Menus');
        }
       return $menus; 
    }
    
    public function getHomeMenu() {
        try {
            $statement =$this->_db->prepare('                 
                SELECT DISTINCT 
                  mt.id,
                  mt.type,mt.`menu_class` 
                FROM
                  user_menutype_m AS mt 
                  INNER JOIN user_menu_m AS a 
                    ON a.menu_type_id = mt.id 
                  INNER JOIN user_roles_rights AS b 
                    ON b.screen_id = a.id 
                    AND b.view = 1 
                WHERE b.role_id= :roleId AND mt.`home_menu`=1
                ORDER BY mt.home_sequence 
            ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $menus = array();

            if (count($resultSet)) {
                $menus = $resultSet;
            }

            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Menus');
        }
       return $menus; 
    }

    public function getSubMenu($type) {
        try {
        $statement = $this->_db->prepare('
                select
                a.id,
                a.screen_name,
                a.url
                from user_menu_m as a
                inner join
                user_roles_rights as b
                on 
                b.screen_id = a.id and b.view=1
                where b.role_id= :roleId 
                and a.menu_type_id= :type
                order by a.sequence
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->bindValue('type', $type);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $subMenus = array();

            if (count($resultSet)) {
                $subMenus = $resultSet;
            }
            return $subMenus;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Sub Menus');
        }
    }
    
    public function getHomeSubMenu($type) {
        try {
        $statement = $this->_db->prepare('
                select
                a.id,
                a.screen_name,
                a.url
                from user_menu_m as a
                inner join
                user_roles_rights as b
                on 
                b.screen_id = a.id and b.view=1
                where b.role_id= :roleId 
                and a.menu_type_id= :type and home_menu=1
                order by a.sequence 
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->bindValue('type', $type);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $subMenus = array();

            if (count($resultSet)) {
                $subMenus = $resultSet;
            }
            return $subMenus;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Sub Menus');
        }
    }

    public function validateAction($screenId, $action) {
        
         try {
        $statement = $this->_db->prepare('
                    select 
                    * from user_roles_rights
                    where screen_id= :screenId 
                        and role_id= :roleId and '.$action.'=1
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->bindValue('screenId', $screenId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $subMenus = array();

            if (count($resultSet)) {
                $subMenus = $resultSet;
            }
            return $subMenus;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate Action');
        }
        
    }
    
    public function validateScreen($screenId, $action) {
        
         try {
        $statement = $this->_db->prepare('
                    select 
                    '.$action.' from user_roles_rights
                    where screen_id= :screenId 
                        and role_id= :roleId and '.$action.'=1
                ');

            $statement->bindValue('roleId', Zend_Auth::getInstance()->getIdentity()->role_id);
            $statement->bindValue('screenId', $screenId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet[$action];
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate Action');
        }
        
    }

    public function getScreenData($url) {
        
         try {
        $statement = $this->_db->prepare('
                    select 
                    * from user_menu_m 
                    where url= :url
                ');

            $statement->bindValue('url', $url);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $subMenus = array();

            if (count($resultSet)) {
                $subMenus = $resultSet;
            }
            return $subMenus;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to get screen id');
        }
    }

    /**
      /**
     * Function to delete Data
     * @return	$deleteQuery:status of delete Query
     */
    public function deleteData($tableName) {
        $str = (int) $this->getId();
        //echo $str;exit;
        $where = 'id =' . $str;
        try {
            $deleteQuery = $this->_db->delete($tableName, $where);
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage() . ' - Unable to delete Data of ' . $str . ' in table: - ' . $tableName);
        }
        return $deleteQuery;
    }

    
    public function getCreated() {
        return $this->created;
    }

    public function setCreated($created) {
        $this->created = $created;
    }

    public function getCreated_by() {
        return $this->created_by;
    }

    public function setCreated_by($created_by) {
        $this->created_by = $created_by;
    }

    public function getUpdated() {
        return $this->updated;
    }

    public function setUpdated($updated) {
        $this->updated = $updated;
    }
    public function getUpdated_by() {
        return $this->updated_by;
    }

    public function setUpdated_by($updated_by) {
        $this->updated_by = $updated_by;
    }

    public function getReportaccess() {
        return $this->reportaccess;
    }

    public function setReportaccess($reportaccess) {
        $this->reportaccess = $reportaccess;
    }





}

