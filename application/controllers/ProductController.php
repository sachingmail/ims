<?php

/**
 * VendorController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class ProductController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $actionName = $this->_request->getActionName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
	
        if($userAccess == 0){
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }

        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');

        if ($status == 'success') {
            $this->view->successMsg = 'Product has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'Product has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'Product has been deleted successfully.';
        }

        $this->view->productData = Application_Model_ProductPeer::fetchAllInvetoryProducts();
    }

    /*
     * Add Action
     * this is default action to add the product in user table.
     * @return
     */

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
        
        $screenData = $this->model->getScreenData('/product/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('You are not Authorized to access this section', 'error');
            $this->_redirect('/product');
        }
        $this->view->screenHeading = $screenData['screen_name'];
        
        $model = new Application_Model_Product();
        $modelPeer = new Application_Model_ProductPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Product();

        //fetch all product type from the master tables
        $productType = Application_Model_ProductPeer::fetchAllProductType();
        $productTypeArr = array();
        $productTypeArr[''] = 'Please Select';
        foreach ($productType as $key => $val) {
            $productTypeArr[$val['id']] = $val['name'];
        }

        //fetch all product categories from the master tables
        $productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $productCategoryArr = array();
        $productCategoryArr[''] = 'Please Select';
        foreach ($productCategory as $key => $val) {
            $productCategoryArr[$val['id']] = $val['name'];
        }
		
		$this->view->pCategory = Application_Model_ProductPeer::fetchAllCategory();

        $this->view->pricingCurrency = Application_Model_ProductPeer::fetchAllPricingCurrency();
        $pricingCurrency = Application_Model_ProductPeer::fetchAllPricingCurrency();

        //fetch all units from the master tables
        $uom = Application_Model_ProductPeer::fetchAllUnits();
        $standarduomArr = array();
        $salesuomArr = array();
        $purchaseuomArr = array();
        $standarduomArr[''] = 'Please Select';
        $salesuomArr[''] = 'Please Select';
        $purchaseuomArr[''] = 'Please Select';
        foreach ($uom as $key => $val) {
            $standarduomArr[$val['id']] = $val['name'];
            $salesuomArr[$val['id']] = $val['name'];
            $purchaseuomArr[$val['id']] = $val['name'];
        }

        // fetch all inventory location from the master tables
        $location = Application_Model_ProductPeer::fetchAllInvenotryLocation();
        $locationArr = array();
        $locationArr[''] = 'Please Select';
        foreach ($location as $key => $val) {
            $locationArr[$val['id']] = $val['name'];
        }

        // fetch all vendors from the master tables
        $vendor = Application_Model_ProductPeer::fetchAllVendors();
        $vendorArr = array();
        $vendorArr[''] = 'Please Select';
        foreach ($vendor as $key => $val) {
            $vendorArr[$val['id']] = $val['name'];
        }

        //populate all elements of form 
        $this->_addForm->product_type->setMultiOptions($productTypeArr);
        $this->_addForm->product_category->setMultiOptions($productCategoryArr);
        $this->_addForm->storage_location->setMultiOptions($locationArr);
        $this->_addForm->standard_uom_id->setMultiOptions($standarduomArr);
        $this->_addForm->sales_uom_id->setMultiOptions($salesuomArr);
        $this->_addForm->purchasing_uom_id->setMultiOptions($purchaseuomArr);
        $this->_addForm->last_vendor->setMultiOptions($vendorArr);

        //populate all elements in edit mode
        if ($id) {
            $model->setId($id);
            $pricing_currency = Application_Model_ProductPeer::fetchAllPricingCurrency();
            $newArr = array();
            foreach($pricing_currency as $key=>$value) {
                $productData = $modelPeer->fetchProductCostOnPricingCurrency($id,$value['id']);
                if($productData != '') {
                     $newArr[$value['id']] = array(
                    'id' => $productData['id'],
                    'price_currency_id' => $value['id'],
                    'price' => $productData['price'],
                    'markup' => $productData['markup'],
                    'cost' => $productData['cost'],
                    'price_currency_name' => $productData['name'],
                );
                }
                else {
                    $newArr[$value['id']] = array(
                    'id' => '',
                    'price_currency_id' => $value['id'],
                    'price' => '0.00',
                    'markup' => '0.00',
                    'cost' => '0.00',
                    'price_currency_name' => $value['name'],
                    );
                }
            }   
//            var_dump($newArr);die;
            $productData = $model->fetchInvenotryProductById();
            
            $this->view->orderHistory = $modelPeer->fetchProductOrderHIstory($id);
            $this->view->is_active = $productData['is_active'];
            
            if ($productData['product_image'] != '') {
                $this->view->file_name = $productData['product_image'];
                $this->view->fileUpload = TEMPUPLOAD . $productData['product_image'];
            }
            $this->view->productCostData = $newArr;

            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'product_name' => $productData['product_name'],
                        'product_type' => $productData['product_type_id'],
                        'product_category' => $productData['product_category_id'],
                        'bar_code' => $productData['bar_code'],
                        'reorder_point' => $productData['reorder_point'],
                        'reorder_quantity' => $productData['reorder_quantity'],
                        'storage_location' => $productData['storage_location_id'],
                        'last_vendor' => $productData['last_vendor_id'],
                        'length' => $productData['length'],
                        'width' => $productData['width'],
                        'height' => $productData['height'],
                        'weight' => $productData['weight'],
                        'standard_uom_id' => $productData['standard_uom_id'],
                        'sales_uom_id' => $productData['sales_uom_id'],
                        'purchasing_uom_id' => $productData['purchasing_uom_id'],
                        'remarks' => $productData['remarks'],
                    )
            );


            $this->_addForm->setAction("/product/add/id/" . $id);
        }
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getParams())) {
                $post = $request->getPost();
                if($_FILES['files']['name'] != '') {
                    $file_name=$_FILES["files"]["name"];
                }
                
                foreach ($pricingCurrency as $k => $v) {
                    $model->setPricingCurrencyId($v['id']);
                    $model->setId($post['id']);
                    $cost_id = $modelPeer->fetchProductCostId($post['id'],$v['id']);
                    if($cost_id != '') {
                        $model->setId($post['id']);
                    }
                    else {
                        $model->setId('');
                        $model->setProductId($post['id']);
                    }
                    $model->setMarkup($post['markup' . $v['id']]);
                    $model->setPricingName($post['pricing' . $v['id']]);
                    $model->setCost($post['cost']);
                    $model->setSessionid(session_id());
                    $model->setUpdated(date('Y-m-d H:i:s'));
                    $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $model->setCreated(date('Y-m-d H:i:s'));
                    $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $model->saveProductCost();
                }
                $model->setId($post['id']);
                $model->setProductName($post['product_name']);
                $model->setProductTypeId($post['product_type']);
                $model->setProdcutCategoryId($post['product_category']);
                $model->setProductImage($file_name);
                $model->setBarcode($post['bar_code']);
                $model->setReorderPoint($post['reorder_point']);
                $model->setReorderQuantity($post['reorder_quantity']);
                $model->setStorageLocation($post['storage_location']);
                $model->setLastVendor($post['last_vendor']);
                $model->setLength($post['length']);
                $model->setWidth($post['width']);
                $model->setHeight($post['height']);
                $model->setWeight($post['weight']);
                $model->setStandardUoM($post['standard_uom_id']);
                $model->setSalesUom($post['sales_uom_id']);
                $model->setPurchasingUom($post['purchasing_uom_id']);
                $model->setRemarks($post['remarks']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->save();

                if($_FILES['files']['name'] != '') {
                    $path_info = pathinfo($_FILES["files"]["name"]);
                    $file_url = TEMPUPLOAD.$model->getId().".".$path_info['extension'];
                    move_uploaded_file($_FILES["files"]["tmp_name"], $file_url );
                    
                    $model->setProductImage($model->getId().".".$path_info['extension']);
                    $model->updateFileName();
                
                }
                
                if ($post['id'] == '') {
                    $model->setSessionid(session_id());
                    $model->updateProductId();
                    $model->updateProductIdBOM();
                    $model->updateProductIdipq();
                    $model->updateProductCost();
                    $this->_helper->flashMessenger()->addMessage('Product has been added successfully', 'success');
                    $this->_redirect('/product');
                } else {
                    $model->updateProductCost();
                     $this->_helper->flashMessenger()->addMessage('Product has been updatd successfully', 'success');
                $this->_redirect('/product');
                }
            }
        }

        $vendorsData = Application_Model_VendorPeer::fetchAllVendors();
        $vendorArr = array();
        $i = 0;
        foreach ($vendorsData as $key => $val) {
            $vendorArr[$i]['text'] = $val['name'];
            $vendorArr[$i]['value'] = $val['id'];
            $i++;
        }

        $this->view->vendorsData = json_encode($vendorArr);


        $components = Application_Model_ProductPeer::fetchAllBOMProduct();
        $componentsArr = array();
        $i = 0;
        foreach ($components as $key => $val) {
            $componentsArr[$i]['text'] = $val['product_name'];
            $componentsArr[$i]['value'] = $val['id'];
            $i++;
        }
        $this->view->componentsData = json_encode($componentsArr);

        $locations = Application_Model_ProductPeer::fetchAllInvenotryLocation();

        $locationsArr = array();
        $i = 0;
        foreach ($locations as $key => $val) {
            $locationsArr[$i]['text'] = $val['name'];
            $locationsArr[$i]['value'] = $val['id'];
            $i++;
        }
        $this->view->locationsData = json_encode($locationsArr);

        if ($id == null || $id < 1) {
            $this->view->vendorList = Application_Model_ProductPeer::fetchVendorsBySession(session_id());
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsBySession(session_id());
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationBySession(session_id());
        } else {
//            $a= Application_Model_ProductPeer::fetchInventoryLocationByProductId($id);
//            var_dump($a);die;
            $this->view->vendorList = Application_Model_ProductPeer::fetchVendorsByProductId($id);
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsByProductId($id);
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationByProductId($id);
            $this->view->moveData = Application_Model_ProductPeer::fetchAllMovements($id);
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Product();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('tra_inventory_product');
        exit;
    }

    /*
     * Add Vendor  Action
     * use to add vendor 
     * @return
     */

    public function addVendorAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_VendorPeer();
        $request = $this->getRequest();
        $post = $request->getParams();
        $product_id = $post['id'];

        $model->setProductRecordId($post['vendor_product_id']);

        //get product Id
        $vendorData = $model->fetchVendorByName($post['vendor_name']);

        $model->setId($vendorData['id']);
        $model->setProductCode($post['id']);

        $model->setVendor_product_code($post['vendor_pcode']);
        $model->setProduct_cost($post['product_cost']);
        if ($post['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();

        if ($product_id == null || $product_id < 1) {
            $this->view->vendorList = Application_Model_ProductPeer::fetchVendorsBySession(session_id());
        } else {
            $this->view->vendorList = Application_Model_ProductPeer::fetchVendorsByProductId($product_id);
        }
    }

    /*
     * delete Vendor Action
     * use to delete data from user table
     * @return
     */

    public function deleteVendorAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $product_id = $this->_request->getParam('product_id');
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_ProductPeer();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_vendors_product');
        $this->view->vendorList = $modelPeer->fetchVendorsByProductId($product_id);
    }

    /*
     * Edit Vendor Action
     * use to edit vendor
     * @return
     */

    public function editVendorAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Vendor();
        $model->setProductRecordId($id);
        $vendorProductData = $model->fetchVendorByProductId();
        echo json_encode($vendorProductData);
        exit;
    }

    /*
     * Add Component Action
     * use to add component in the user table
     * @return
     */

    public function addComponentAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Product();
        $modelPeer = new Application_Model_ProductPeer();
        $request = $this->getRequest();
        $post = $request->getParams();
        $product_id = $post['id'];
        $model->setId($post['id']);
        //get product Id
        $productData = $model->fetchComponentByName($post['product_name']);
        $model->setBom_id($post['bom_id']);
        $model->setComponent_name($productData['id']);
        $model->setQuantity($post['quantity']);
        $model->setCost($post['cost']);

        if ($post['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveComponent();

        if ($product_id == null || $product_id < 1) {
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsBySession(session_id());
        } else {
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsByProductId($product_id);
        }
    }

    /*
     * delete component Action
     * use to delete component data from user table
     * @return
     */

    public function deleteComponentAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $product_id = $this->_request->getParam('product_id');
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_ProductPeer();
        $model->setId($id);
        $model->deleteData('tra_inventory_product_bill_of_materials');
        if ($product_id == null || $product_id < 1) {
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsBySession(session_id());
        } else {
            $this->view->bomList = Application_Model_ProductPeer::fetchMaterialsByProductId($product_id);
        }
    }

    /*
     * delete Action
     * use to edit component from user table
     * @return
     */

    public function editComponentAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Product();
        $model->setBom_id($id);
        $productData = $model->fetchComponentDataById();
        echo json_encode($productData);
        exit;
    }

    /*
     * Ad Inventory Location Action
     * use to add inventory location in user table
     * @return
     */

    public function addInventoryLocationAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Product();
        $modelPeer = new Application_Model_ProductPeer();
        $request = $this->getRequest();
        $post = $request->getParams();

        $id = $post['location_product_id'];
        $product_id = $post['id'];
        $model->setId($id);
        //get product Id

        $locationData = $model->fetchInventoryLocationByName($post['location_id']);
        $model->setLocationId($locationData['id']);
        $model->setLocationQuantity($post['location_quantity']);
        $model->setProductId($product_id);
        if ($post['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveInventoryLocation();

        if ($product_id == null || $product_id < 1) {
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationBySession(session_id());
        } else {
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationByProductId($product_id);
        }
    }

    /*
     * delete inventory location Action
     * use to delete inventory location in user table
     * @return
     */

    public function deleteInventoryLocationAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $product_id = $this->_request->getParam('product_id');
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_ProductPeer();
        $model->setId($id);
        $model->deleteData('tra_inventory_product_quantity');
        if ($product_id == null || $product_id < 1) {
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationBySession(session_id());
        } else {
            $this->view->locList = Application_Model_ProductPeer::fetchInventoryLocationByProductId($product_id);
        }
    }

    /*
     * Edit Inventory location Action
     * use to edit inventory location
     * @return
     */

    public function editInventoryLocationAction() {

        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Product();
        $model->setId($id);
        $productData = $model->fetchInventoryLocationDataById();
        echo json_encode($productData);
        exit;
    }

    /*
     * Calculation Action
     * use to calculate markup
     * @return
     */

    public function calculationAction() {
        $this->_helper->layout->disableLayout();
        $flag = $this->_request->getParam('flag');
        $i = $this->_request->getParam('i');
        if ($flag == '0') {
            $cost = $this->_request->getParam('cost');
            $pricing = $this->_request->getParam('pricing');
            $net = $pricing - $cost;
            $markup = array();
            $markup['markup'] = (($net / $cost) * 100);
            $markup['i'] = $this->_request->getParam('i');
            echo json_encode($markup);
            exit;
        }
        if ($flag == '1') {
            $cost = $this->_request->getParam('cost');
            $markup = $this->_request->getParam('markup');
            $normal = ((($markup / 100) * $cost) + $cost);
            echo json_encode($normal);
            exit;
        }
    }

    /*
     * delete Currency Popup Action
     * use to delete Pricing Currency in user table
     * @return
     */

    public function deleteCurrencyPopupAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $product_id = $this->_request->getParam('product_id');
        $model = new Application_Model_Product();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_pricing_currency');
        $model->setId($product_id);
        $productCostData = $model->fetchProductCostById();
//            var_dump($productCostData);die;
            $newArr = array();
            $pricing_currency = Application_Model_ProductPeer::fetchAllPricingCurrency();
            foreach ($productCostData as $valData) {
                $newArr[$valData['price_currency_id']] = array(
                    'id' => $valData['id'],
                    'price_currency_id' => $valData['price_currency_id'],
                    'price' => $valData['price'],
                    'markup' => $valData['markup'],
                    'cost' => $valData['cost'],
                    'price_currency_name' => $valData['price_currency_name'],
                );
            }
            $this->view->productCostData = $newArr;
            $this->view->pricingCurrency = $pricing_currency;
    }
    
    private function _upload() {
            $newfile = '';
            $upload = new Zend_File_Transfer_Adapter_Http();
            $root = getenv("DOCUMENT_ROOT");
            $upload->addValidator('IsImage', false, array('jpg', 'png'))
                    ->addValidator('Size', false, array('max' => '10MB'))
                    ->setDestination($root . '/upload/products/')
                    //->addValidator('Mimetype', false, 'image/jpg')
                    ;
            $file_info = $upload->getFileInfo();
            if ($file_info['browse']['name']) {
                $date = Zend_Date::now();
                $timeStamp = gmdate("Y-m-d H-i-s", $date->getTimestamp());
                $newfile = Trim($timeStamp . '_' . $file_info['browse']['name'], " ");
                $upload->addFilter('Rename', $newfile);
                try {
                    if(!$upload->receive()) {
                        var_dump($upload->getMessages()) ;die;
                    }
                    else {
                        echo "success";die;
                    }
                    return $newfile;
                } catch (Zend_Exception $e) {
                    //throw new Exception($e->getMessage());
                    var_dump($e);die;
                }
            }
    }
    /**
     * 
     * 
     */
    public function deactivateProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Product();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->DeactivateProduct();die;
        //$this->_helper->flashMessenger()->addMessage('Product has been deactivated successfully', 'success');  die;             
//        $this->_redirect('/product');
    }
    /**
     * 
     * 
     */
    public function activateProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Product();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->ActivateProduct();die;
        
        //$this->_helper->flashMessenger()->addMessage('Product has been activated successfully', 'success');   die;            
//        $this->_redirect('/vendor');
    }
}
