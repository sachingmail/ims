<?php

/**
 * VendorController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class SalesQuoteController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');

        $this->view->sales_quote_data = Application_Model_SalesQuotePeer::fetchAllSalesQuote();
    }

    /*
     * Add Action
     * this is default action to add the product in user table.
     * @return
     */

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
        $screenData = $this->model->getScreenData('/sales-quote/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('You are not Authorized to access this section', 'error');
            $this->_redirect('/sales-quote');
        }
        
        $this->view->screenHeading = $screenData['screen_name'];
        
        $model = new Application_Model_SalesQuote();
        $modelPeer = new Application_Model_SalesQuotePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_SalesQuote();

        //Populate all value on edit mode
        if ($id) {
            $model->setId($id);
            $salesQuoteData = $model->fetchSalesQuoteByIdOrderMas();
           // print_r($salesQuoteData);die;
            if($salesQuoteData['order_date'] !== "00/00/0000") {
                $this->_addForm->populate(array('quote_date'=>$salesQuoteData['order_date']));
            }
            if($salesQuoteData['req_ship_date'] !== "00/00/0000") {
                $this->_addForm->populate(array('req_ship_date'=>$salesQuoteData['req_ship_date']));
            }
//            $quote_date = date('d/m/Y', strtotime($salesQuoteData['order_date']));
            $this->view->order_status = $salesQuoteData['order_status'];
//            $req_ship_date = date('d/m/Y', strtotime($salesQuoteData['req_ship_date']));
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'customer' => $salesQuoteData['customer_id'],
                        'des' => $salesQuoteData['designation'],
                        'address_type' => $salesQuoteData['address_type'],
                        'address' => $salesQuoteData['customer_address'],
                        'address1' => $salesQuoteData['address1'],
                        'address2' => $salesQuoteData['address2'],
                        'phone' => $salesQuoteData['customer_phone'],
                        'quote' => $salesQuoteData['order_id'],
                        'sales_reps' => $salesQuoteData['sales_rep_id'],
                        'location' => $salesQuoteData['location_id'],
                        'status' => $salesQuoteData['order_status'],
                        'payment_terms_id' => $salesQuoteData['terms_id'],
                        'p_o' => $salesQuoteData['po_ref'],
                        'shipping_address' => $salesQuoteData['shipping_address'],
                        'non_customer_cost' => $salesQuoteData['non_customer_costs'],
                        'taxing_scheme' => $salesQuoteData['taxing_scheme_id'],
                        'tax1_name' => $salesQuoteData['tax1_amount'],
                        'tax2_name' => $salesQuoteData['tax2_amount'],
                        'pricing_currency' => $salesQuoteData['pricing_currency_id'],
                        'sub_total' => $salesQuoteData['sub_total'],
                        'freight' => $salesQuoteData['freight'],
                        'total' => $salesQuoteData['total_amount'],
                        'remarks' => $salesQuoteData['remarks'],
                    )
            );

            $this->_addForm->submitbtn->setLabel("Update Sales Quote");
            $this->_addForm->setAction("/sales-quote/add/id/" . $id);
        }

//      save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                //var_dump($post);die;
                if ($post['quote_date']) {
                    $quote_date1 = str_replace("/", "-", $post['quote_date']);
                    $quote_date = date('Y/m/d', strtotime($quote_date1));
                } else {
                    $quote_date = '';
                }
                if ($post['req_ship_date']) {
                    $req_ship_date1 = str_replace("/", "-", $post['req_ship_date']);
                    $req_ship_date = date('Y/m/d', strtotime($req_ship_date1));
                } else {
                    $req_ship_date = '';
                }
                $modelTaxingScheme = new Application_Model_TaxingScheme();
                $modelTaxingScheme->setId($post['taxing_scheme']);
                $result = $modelTaxingScheme->fetchTaxinSchemeById();
                $model->setId($post['id']);
                $model->setCustomerId($post['customer']);
                $model->setDesignation($post['des']);
                $model->setAddress_type($post['address_type']);
                $model->setCustomerAddress($post['address']);
                $model->setAddress1($post['address1']);
                $model->setAddress2($post['address2']);
                $model->setPhone($post['phone']);
                $model->setPaymentTermsId($post['payment_terms_id']);
                $model->setPo_ref($post['p_o']);
                $model->setLocationId($post['location']);
                $model->setSalesRepsId($post['sales_reps']);
                $model->setStatus($post['status']);
                $model->setOrderDate($quote_date);
                $model->setShippingAddress($post['shipping_address']);
                $model->setNonCustomerCosts($post['non_customer_cost']);
                $model->setTaxingSchemeId($post['taxing_scheme']);
                $model->setTax1_name($result['tax_name']);
                $model->setTax1_amount($post['tax1_name']);
                $model->setTax1_percent($result['tax_rate']);
                $model->setTax2_name($result['secondary_tax_name']);
                $model->setTax2_amount($post['tax2_name']);
                $model->setTax2_percent($result['secondary_tax_rate']);
                $model->setPricingCurrencyId($post['pricing_currency']);
                $model->setReqShippingDate($req_ship_date);
                $model->setSalesSubTotal($post['sub_total']);
                $model->setFreight($post['freight']);
                $model->setSalesTotal($post['total']);
                $model->setRemarks($post['remarks']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->save();

                $id = $model->getId();
//                    $order_id = str_pad($id, 6, '0', STR_PAD_LEFT);
                $newOrderId = "SQ-" . $id;
                $model->setOrderId($newOrderId);
                $model->updateSalesQuoteOrderId();
                if ($post['id'] == '') {
                    $model->setSessionid(session_id());
                    $model->updateOrderId();
                    
                    $this->_helper->flashMessenger()->addMessage('Sales Quote has been added successfully', 'success');
                    $this->_redirect('/sales-quote/add/id/'.$id);
                } else {
                    $this->_helper->flashMessenger()->addMessage('Sales Quote has been updated successfully', 'success');
                    $this->_redirect('/sales-quote/add/id/'.$id);
                }
            }
        }

        if ($id == null || $id < 1) {
            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductBySession(session_id());
        } else {
            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductsByOrderId($id);
        }
        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('tra_sales_quote_order_mas');
        exit;
    }

    /*
     * Add Sales Quote Item Action
     * this is default action to add the item in user table.
     * @return
     */

    public function addSalesQuoteItemAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesQuote();
        $modelPeer = new Application_Model_SalesQuotePeer();
        $request = $this->getRequest();
        $post = $request->getParams();
        //var_dump($post);die;
        $quantity = $post['sales_quantity'];
        $unit_price = $post['sales_unit_price'];
        $discount = $post['sales_discount'];
        $net = ($discount / 100 * $unit_price);
        $cost = $unit_price - $net;
        $sub_total = $cost * $quantity;
        $id = $post['id'];
        $order_id = $post['order_id'];
        $model->setId($post['id']);
        //get product Id
        $productData = $modelPeer->fetchProductById($post['sales_item']);
        $model->setId($post['id']);
        $model->setOrderId($order_id);
        $model->setSalesProductId($productData['id']);
        $model->setSalesQuantity($post['sales_quantity']);
        $model->setSalesUnitPrice($post['sales_unit_price']);
        $model->setSalesDiscount($post['sales_discount']);
        $model->setSalesTotal($sub_total);

        if ($post['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveSalesItem();

        if ($id == null || $id < 1) {
            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductBySession(session_id());
        } else {

            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductsByOrderId($id);
        }
    }

    /*
     * Edit Sales Quote Item Action
     * use to edit Sales Quote Item
     * @return
     */

    public function editSalesQuoteItemAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $model->setId($id);
        $SalesQuoteData = $model->fetchSalesQuoteByQuoteId();
        echo json_encode($SalesQuoteData);
        exit;
    }

    /*
     * delete Sales Quote Item Action
     * use to delete Sales Quote Item  from user table
     * @return
     */

    public function deleteSalesQuoteItemAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $order_id = $this->_request->getParam('order_id');
        $model = new Application_Model_SalesQuote();
        $modelPeer = new Application_Model_SalesQuotePeer();
        $model->setId($id);
        $model->deleteData('tra_sales_quote_order_det');
        if ($order_id == null || $order_id < 1) {
            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductBySession(session_id());
        } else {
            $this->view->sqList = Application_Model_SalesQuote::fetchSalesProductsByOrderId($order_id);
        }
    }

    /*
     * Fetch Customer Details Action
     * use to fetch customer details from user table
     * @return
     */

    public function fetchCustomerDetailsAction() {
        $this->_helper->layout->disableLayout();
        $customer = $this->_request->getParam('customer_id');
        $model = new Application_Model_SalesQuote();
        $model->setCustomerId($customer);
        $data = $model->fetchCustomerDetails();
        $result = array();
        $result['name'] = $data['contact_name'];
        $result['address'] = $data['address'] . ' ' . $data['city_name'] . ' ' . $data['state_name'] . ' ' . $data['country_name'] . ' ' . $data['zip_code'];
        $result['pricing_currency_id'] = $data['pricing_currency_id'];
        $result['payment_terms'] = $data['payment_terms'];
        $result['taxing_schemes'] = $data['taxing_schemes'];
        $result['default_location_id'] = $data['default_location_id'];
        $result['phone'] = $data['contact_phone'];
        $result['sales_reps'] = $data['sales_reps'];
        $result['designation'] = $data['designation'];
        $result['address_type'] = $data['address_type'];
        $result['address1'] = $data['address1'];
        $result['address2'] = $data['address2'];
        echo json_encode($result);
        die;
    }

    /*
     * Fetch Taxing Scheme Action
     * use to calculate tax and subtotal
     * @return
     */

    public function fetchTaxingSchemeAction() {
        $this->_helper->layout->disableLayout();
        $tax = $this->_request->getParam('taxing_scheme');
        $freight = $this->_request->getParam('freight');
        $order_id = $this->_request->getParam('order_id');
        $modeltax = new Application_Model_TaxingScheme();
        $modeltax->setId($tax);
        $result = $modeltax->fetchTaxinSchemeById();
        $model = new Application_Model_SalesQuote();
        $model->setSessionid(session_id());
        if ($order_id == null || $order_id < 1) {
            $sub_total = $model->fetchSubTotal();
            $netSubtotal = 0;
            foreach ($sub_total as $key => $value) {
                $netSubtotal += $value['sub_total'];
            }
        } else {
            $model->setId($order_id);
            $sub_total = $model->fetchSubTotalById();
            $netSubtotal = 0;
            foreach ($sub_total as $key => $value) {
                $netSubtotal += $value['sub_total'];
            }
        }
        if (($result['tax_on_shipping'] == '0') && ($result['secondary_tax_on_shipping'] == '0') && ($result['compound_secondary'] == '0')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $tax_1 = ($tax1 / 100) * $netSubtotal;
            $tax_2 = ($tax2 / 100) * $netSubtotal;
            $total = $tax_1 + $tax_2 + $freight + $netSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '0') && ($result['secondary_tax_on_shipping'] == '0') && ($result['compound_secondary'] == '1')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $tax_1 = ($tax1 / 100) * $netSubtotal;
            $newSubtotal = $netSubtotal + $tax_1;
            $tax_2 = ($tax2 / 100) * $newSubtotal;
            $total = + $tax_2 + $freight + $newSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '0') && ($result['secondary_tax_on_shipping'] == '1') && ($result['compound_secondary'] == '0')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $tax_1 = ($tax1 / 100) * $netSubtotal;
            $newSubtotal = $netSubtotal + $freight;
            $tax_2 = ($tax2 / 100) * $newSubtotal;
            $total = $tax_1 + $tax_2 + $newSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '0') && ($result['secondary_tax_on_shipping'] == '1') && ($result['compound_secondary'] == '1')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $tax_1 = ($tax1 / 100) * $netSubtotal;
            $newSubtotal = $sub_total + $tax_1 + $freight;
            $tax_2 = ($tax2 / 100) * $newSubtotal;
            $total = $tax_2 + $netSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '1') && ($result['secondary_tax_on_shipping'] == '0') && ($result['compound_secondary'] == '0')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $newSubtotal = $netSubtotal + $freight;
            $tax_1 = ($tax1 / 100) * $newSubtotal;
            $tax_2 = ($tax2 / 100) * $netSubtotal;
            $total = $tax_1 + $tax_2 + $newSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
//            var_dump($final_result);die;            
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '1') && ($result['secondary_tax_on_shipping'] == '0') && ($result['compound_secondary'] == '1')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $newSubtotal = $netSubtotal + $freight;
            $tax_1 = ($tax1 / 100) * $newSubtotal;
            $temp = $netSubtotal + $tax_1;
            $tax_2 = ($tax2 / 100) * $temp;
            $total = $temp + $tax_2 + $freight;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else if (($result['tax_on_shipping'] == '1') && ($result['secondary_tax_on_shipping'] == '1') && ($result['compound_secondary'] == '0')) {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $newSubtotal = $netSubtotal + $freight;
            $tax_1 = ($tax1 / 100) * $newSubtotal;
            $tax_2 = ($tax2 / 100) * $newSubtotal;
            $total = $tax_1 + $tax_2 + $newSubtotal;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        } else {
            $tax1 = $result['tax_rate'];
            $tax2 = $result['secondary_tax_rate'];
            $newSubtotal = $netSubtotal + $freight;
            $tax_1 = ($tax1 / 100) * $newSubtotal;
            $temp = $newSubtotal + $tax_1;
            $tax_2 = ($tax2 / 100) * $temp;
            $total = $tax_2 + $temp;
            $final_result = array();
            $final_result['total'] = $total;
            $final_result['tax1'] = $tax_1;
            $final_result['tax2'] = $tax_2;
            $final_result['sub_total'] = $netSubtotal;
            $final_result['freight'] = $freight;
            echo json_encode($final_result);
            die;
        }
    }

    /*
     * fetch subtotal Action
     * use to fetch subtotal from the user table
     * @return
     */

    public function fetchSubTotalAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        if ($id) {
            $model->setId($id);
            $sub_total = $model->fetchSubTotalById();
            $net = 0;
            foreach ($sub_total as $key => $value) {
                $net += $value['sub_total'];
            }
            $result['sub_total'] = $net;
            echo json_encode($result);
            die;
        } else {
            $model->setSessionid(session_id());
            $sub_total = $model->fetchSubTotal();
            $net = 0;
            foreach ($sub_total as $key => $value) {
                $net += $value['sub_total'];
            }
            $result['sub_total'] = $net;
            echo json_encode($result);
            die;
        }
    }

    /*
     * fetch unit price by product id Action
     * used to fetch unit price by product id 
     * @return
     */

    public function fetchUnitPriceByProductIdAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $item = $this->_request->getParam('item');
        $model = new Application_Model_SalesQuote();
        $model->setId($item);
        $result = $model->fetchUnitPriceByProductId();
        echo json_encode($result);
        die;
    }

    /*
     * subtotal calculation Action
     * use to calculate subtotal 
     * @return
     */

    public function subTotalCalculationAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $post = $request->getParams();
        $quantity = $this->_request->getParam('quantity');
        $discount = $this->_request->getParam('discount');
        $unit_price = $this->_request->getParam('unit_price');
        $total_quantity = $quantity * $unit_price;
        $total_discount = ($discount / 100) * $total_quantity;
        $sub_total = $total_quantity - $total_discount;
        echo json_encode($sub_total);
        die;
    }

    /**
     * 
     * 
     */
    public function convertSalesQuoteToOrderAction() {
        $this->_helper->layout->disableLayout();
        $quote_id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $modelPeer = new Application_Model_SalesQuotePeer();
        $modelOrder = new Application_Model_SalesOrder();
        $modelOrderPeer = new Application_Model_SalesOrderPeer();
        $model->setId($quote_id);
        $mas_data = $model->fetchSalesQuoteMasData();
        $det_data = $model->fetchSalesQuoteDetData();
        $model->setCustomerId($mas_data['customer_id']);
        $customer = $model->fetchCustomerDetails();
        $modelOrder->setId('');
        $modelOrder->setCustomerId($mas_data['customer_id']);
        $modelOrder->setAddress($mas_data['customer_address']);
        $modelOrder->setPaymentTerms($mas_data['terms_id']);
        $modelOrder->setPerson_name($customer['name']);
        $modelOrder->setPhone($mas_data['customer_phone']);
        $modelOrder->setPo_Ref($mas_data['po_ref']);
        $modelOrder->setLocation($mas_data['location_id']);
        $modelOrder->setSalesOrderId($mas_data['order_id']);
        $modelOrder->setTax1($mas_data['tax1_name']);
        $modelOrder->setTaxAmount1($mas_data['tax1_amount']);
        $modelOrder->setTaxper1($mas_data['tax1_per']);
        $modelOrder->setTax2($mas_data['tax2_name']);
        $modelOrder->setTaxAmount2($mas_data['tax2_amount']);
        $modelOrder->setTaxPer2($mas_data['tax2_per']);
        $modelOrder->setSalesRepsId($mas_data['sales_rep_id']);
        $modelOrder->setTaxScheme($mas_data['taxing_scheme_id']);
        $modelOrder->setSippingAddress($mas_data['shipping_address']);
        $modelOrder->setNonCustomerCost($mas_data['non_customer_costs']);
        $modelOrder->setOrderDate($mas_data['order_date']);
        $modelOrder->setDueDate($mas_data['due_date']);
        $modelOrder->setPricingCurrency($mas_data['pricing_currency_id']);
        $modelOrder->setRegShipDate($mas_data['req_ship_date']);
        $modelOrder->setNetSubTotal($mas_data['sub_total']);
        $modelOrder->setGrandSubTotal($mas_data['total_amount']);
        $modelOrder->setPaidAmount($mas_data['paid_amount']);
        $modelOrder->setBalanceAmount($mas_data['total_amount'] - $mas_data['paid_amount']);
        $modelOrder->setFulfilledRemarks('');
        $modelOrder->setReturnRemarks('');
        $modelOrder->setRestockRemarks('');
        $modelOrder->setRemarks($mas_data['remarks']);
        $modelOrder->setUpdated(date('Y-m-d H:i:s'));
        $modelOrder->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelOrder->setCreated(date('Y-m-d H:i:s'));
        $modelOrder->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelOrder->save();
        $last_id = $modelOrder->getId();
        $newOrderId = "SO-" . $last_id;
        $modelOrder->setOrderId($newOrderId);
        $modelOrder->updateSalesOrderId();
        if ($det_data) {
            foreach ($det_data as $key => $value) {
                $modelOrder->setId($last_id);
                $modelOrder->setProductRecordId('');

                $modelOrder->setProductId($value['product_id']);
                $modelOrder->setItemStatus('Order');

                //$modelOrder->setVendor_product_code($value['vendor_pcode']);
                $modelOrder->setQuantity($value['quantity']);
                $modelOrder->setUnitPrice($value['unit_price']);
                $modelOrder->setLocation($mas_data['location_id']);
                $modelOrder->setDiscount($value['discount']);
                $modelOrder->setNetSubTotal($value['sub_total']);
                $modelOrder->setTransactionDate('');
                $modelOrder->setSessionid('');
                $modelOrder->setUpdated(date('Y-m-d H:i:s'));
                $modelOrder->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelOrder->setCreated(date('Y-m-d H:i:s'));
                $modelOrder->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelOrder->saveProduct();
            }   
        }
        $model->UpdateSalesQuote();
            $sales_order_id['id'] = $last_id;
            echo json_encode($sales_order_id);
            die;
    }
    /**
     * cancel order action
     * This Action will cancel the sales quote
     */
    public function cancelQuoteAction() {
        $this->_helper->layout->disableLayout();
        $quote_id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $model->setId($quote_id);
        $model->setStatus('Rejected');
        $model->CancelQuote();die;
    }
    /**
     * cancel order action
     * This Action will cancel the sales quote
     */
    public function regenerateQuoteAction() {
        $this->_helper->layout->disableLayout();
        $quote_id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $model->setId($quote_id);
        $model->setStatus('Quote');
        $model->CancelQuote();die;
    }
    
    /**
     * 
     * print order action
     */
    public function printOrderAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_SalesQuote();
        $modelPeer = new Application_Model_SalesQuotePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllSalesQuoteProductInOrder($id, '');
        
        $salesOrder=$model->fetchSalesQuoteOrderForPrint();
        $this->view->orderMasData = $salesOrder;
        $this->view->productList = $modelPeer->fetchSalesProducts($id);
        $subTotal = Application_Model_SalesQuotePeer::fetchSubTotalByOrderId($id);
        $this->view->subTotal = $subTotal['sub_total'];
        
        
        $scheme_id = $salesOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->subtotal = $salesOrder['sub_total'];
        $this->view->freight = $salesOrder['freight'];

        $taxData = Array(
            'tax_name' => $salesOrder['tax1_name'],
            'tax_rate' => $salesOrder['tax1_per'],
            'tax_on_shipping' => $salesOrder['tax_on_shipping'],
            'secondary_tax_name' => $salesOrder['tax2_name'],
            'secondary_tax_rate' => $salesOrder['tax2_per'],
            'secondary_tax_on_shipping' => $salesOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $salesOrder['compound_secondary'],
        );
        

        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
}
