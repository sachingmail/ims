<?php

/**
 * WorkOrderController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class WorkOrderController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');
        $this->view->orderData = Application_Model_WorkOrderPeer::fetchAllWorkOrder();
        
    }

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
       
        $screenData = $this->model->getScreenData('/work-order/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('You are not Authorized to access this section', 'error');
            $this->_redirect('/work-order');
        }
        
        $this->view->screenHeading = $screenData['screen_name'];
        
        $model = new Application_Model_WorkOrder();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        
        $this->view->id = $id;

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $post = $this->_request->getPost();
            
            $model->setId($post['id']);
            $model->setOrderId($post['order_id']);
            $model->setOrderDate($this->_helper->common->formatDbDate($post['order_date']));
            $model->setOrderStatus('Entered');
            $model->setLocation($post['location_id']);
            $model->setOrderType($post['order_type']);
            $model->setRemarks($post['remarks']);
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->save();

            
            if ($post['id'] == '') {
                $last_id = $model->getId();
                $newOrderId = "WO-" . $last_id;
            
                $model->setOrderId($newOrderId);
                $model->updateWorkOrderId();
                
                $model->setSessionid(session_id());
                $model->updateWorkOrderDet();
                
                $pData = Application_Model_WorkOrderPeer::fetchProductsByOrderId($last_id); 
                foreach ($pData as $prod) {
                    $model->saveBOMProduct($prod['id'],$prod['product_id'],$prod['quantity']);
                }
                                
                $this->_helper->flashMessenger()->addMessage('Work order has been created successfully', 'success');
                $this->_redirect('/work-order/edit/id/' . $model->getId());
                
            } else {
                $this->_helper->flashMessenger()->addMessage('Work order has been updated successfully', 'success');
                $this->_redirect('/work-order/edit/id/' . $model->getId());
            }
        }
        $this->view->productData = Application_Model_ProductPeer::fetchAllProduct();
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->orderTypesData = Application_Model_WorkOrderPeer::fetchOrderTypes();
        
       
        $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsBySession(session_id());       
    }

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_WorkOrder();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $product = Application_Model_ProductPeer::fetchAllProduct();

        $this->view->productData = $product;
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->orderTypesData = Application_Model_WorkOrderPeer::fetchOrderTypes();

        if ($id) {
            $model->setId($id);
            $this->view->workOrder = $model->fetchWorkOrder();
            $prodList = Application_Model_WorkOrderPeer::fetchProductsByOrderId($id);       
            $this->view->productList = $prodList;
           
            $finalArr = array();
            foreach($prodList as $prod){
                $bomData = Application_Model_WorkOrderPeer::fetchBOMProducts($prod['id']);
                 $prodArr = array();
                foreach($bomData as $bom){
                    $prodArr[] = array(
                        'id'=> $bom['id'],
                        'product_name'=> $bom['product_name'],
                        'quantity'=> $bom['quantity'],
                        'quantity_used'=> $bom['quantity_used'],
                    );
                }
                $finalArr [] =array(
                    'id'=> $prod['id'],
                    'product_name'=> $prod['product_name'],
                    'bom'=> $prodArr
                );
            }
            
            
            $this->view->bomProducts = $finalArr;
        }
    }
        
    public function addProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_WorkOrder();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $param = $request->getParams();

        $order_id = $param['id'];
                
        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setQuantity($param['quantity']);
        $model->setDateScheduled(date('Y-m-d'));
        $model->setDescription($param['description']);
        $model->setProductStatus('Entered');
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();
        
        
        if ($order_id == null || $order_id < 1) {
            $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsBySession(session_id());
        } else {
            $model->setId($order_id);
            $this->view->workOrder = $model->fetchWorkOrder();
          
            $model->saveBOMProduct($model->getProductRecordId(),$param['product_id'],$param['quantity']);
            $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsByOrderId($order_id);
        }
    }

     public function updateOrderStatusAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        
        $model = new Application_Model_WorkOrder();
        
        $model = new Application_Model_WorkOrder();
        $model->setProductStatus($param['status']);
        $model->setProductRecordId($param['id']);
        $model->statusUpdate();
        
        $model->setId($param['orderId']);
        $this->view->workOrder = $model->fetchWorkOrder();
        $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsByOrderId($param['orderId']);
    }
    
    public function issueOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        
        $model = new Application_Model_WorkOrder();
        $model->setOrderStatus($param['status']);
        $model->setId($param['id']);
        $model->setOrderDate(date('Y-m-d'));
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->issueOrder();exit;
        
    }
    
    public function finishOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        
        $model = new Application_Model_WorkOrder();
        $model->setProductStatus('Fulfilled');
        $model->setProductRecordId($param['id']);
        $model->statusUpdate();
        $model->finishStatusUpdate();
        
        
        $model->setId($param['orderId']);
        $this->view->workOrder = $model->fetchWorkOrder();
        $this->checkFulfilled($param['orderId']);
        
        $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsByOrderId($param['orderId']);
    }
    
    public function deleteProductAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $id= $param['id'];
        $order_id= $param['orderId'];
                
        $model = new Application_Model_WorkOrder();
        $model->setId($id);
        $model->deleteData('tra_work_order_det');
        
        $model->setProductRecordId($id);
        $model->deleteBOM();
         
        if ($order_id == null || $order_id < 1) {
            $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsBySession(session_id());
        } else {
            $model->setId($order_id);
            $this->view->workOrder = $model->fetchWorkOrder();
            $this->view->productList = Application_Model_WorkOrderPeer::fetchProductsByOrderId($order_id);
        }
    }
    
    public function bomPopupAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $id =$param['id'];
        $this->view->bomProducts = Application_Model_WorkOrderPeer::fetchBOMProducts($id);
        $this->view->id =$id;
        
    }
    
     public function updateBomStatusAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $model = new Application_Model_WorkOrder();
        $id =$param['id'];
        $pid =$param['pid'];
        $status =$param['status'];
        $location =$param['location'];
        
        $model->updateBomStatus($id,$status);
        if($status == 'Started'){
            $bomData = Application_Model_WorkOrderPeer::fetchBOMProductByRecordId($id);
            $this->manageInventory($location,$bomData['bom_product_id'],'D',$bomData['quantity']);
        }
        
        $this->view->bomProducts = Application_Model_WorkOrderPeer::fetchBOMProducts($pid);
        $this->view->id =$pid;
        
    }
    
     public function voidBomStatusAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $model = new Application_Model_WorkOrder();
        $id =$param['id'];
        $pid =$param['pid'];
        $status ='Entered';
        
        $model->voidBomStatus($id,$status);
         
        $location =$param['location'];      
        $bomData = Application_Model_WorkOrderPeer::fetchBOMProductByRecordId($id);
        $this->manageInventory($location,$bomData['bom_product_id'],'A',$bomData['quantity']);
            
        $this->view->bomProducts = Application_Model_WorkOrderPeer::fetchBOMProducts($pid);
        $this->view->id =$pid;
        
    }
    
    public function checkFulfilled($orderId) {
        $prodCount = Application_Model_WorkOrderPeer::getTotalProductCount($orderId);
        $completedProdCount = Application_Model_WorkOrderPeer::getTotalCompletedProductCount($orderId);
        
        if($completedProdCount > 0 && $completedProdCount < $prodCount) {
            $status ="Partial";
        }else if($completedProdCount == $prodCount) {
            $status ="Fulfilled";
        } else {
            $status ="Issued";
        }
        $model = new Application_Model_WorkOrder();
        $model->setOrderStatus($status);
        $model->setId($orderId);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->updateStatusMasterData();
        return true;
    }
    
    public function manageInventory($locationId,$productId,$type,$qty) {
        
        $modelInv = new Application_Model_Inventory();
        $modelInv->setLocation($locationId);
        $modelInv->setProduct_id($productId);
        $productInventory = $modelInv->getProductDetails();
        if($type=='D'){
            $newQuantity = $productInventory['quantity'] - $qty;
        } else {
            $newQuantity = $productInventory['quantity'] + $qty;
        }
        $modelInv->setNewQuantity($newQuantity);
        $modelInv->updateProductQuantity();
                
    }
    
     public function createPurchaseAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $ids =substr($param['pids'],0,-1);
        $data = explode(',',$ids);
        foreach ($data as $val) {
            $pdata = explode('-',$val);
            $cost= Application_Model_ProductPeer::getProductCost($pdata[0]);
            $total = $cost * $pdata[1];
            
            $model = new Application_Model_Purchase();
            
            $model->setId($param['id']);
            $model->setProductRecordId($param['product_record_id']);
            $model->setProductId($pdata[0]);
            $model->setVendor_product_code('');
            $model->setQuantity($pdata[1]);
            $model->setUnitPrice($cost);
            $model->setDiscount('0.00');
            $model->setProductSubTotal($total);
            $model->setProductStatus('Order');
            $model->setTransactionDate('');
            $model->setSessionid(session_id());
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->saveProduct();
        }
        exit;
        
    }
    
    public function workOrderByStatusAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');
        $this->view->orderData = Application_Model_WorkOrderPeer::fetchAllWorkOrder();
        
    }
    
    
    

}
