<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class RolesController extends DMC_Controller_Abstract{

    protected $model;
    private $_addForm = null;
    
    public function init() {       
        parent::init();
        /*
        * Initialise a session called 'Default'
        */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
        * Is the user logged in?  If not re-direct to login screen
        */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }
        
        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        
        $controllerName = $this->_request->getControllerName();       
        $screenData=$this->model->getScreenData('/'.$controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName=$screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        
        $modelRole = new Application_Model_Roles();
        $modelPeer = new Application_Model_RolesPeer();
         $status = $this->_request->getParam('status');
        
        $request = $this->getRequest();
        $this->_addForm =  new Application_Form_Roles_Add();
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelRole->setId($post['id']);
                $modelRole->setName($post['rolename']);
                $modelRole->setUpdated(date('Y-m-d H:i:s'));
                $modelRole->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelRole->setCreated(date('Y-m-d H:i:s'));
                $modelRole->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelRole->save();                
                
                $screens = $modelPeer->fetchAllScreens();
                foreach($screens as $val){
                    $modelRole->setScreen_id($val['id']);
                    $modelRole->setView_per(1);
                    $modelRole->setAdd_per(1);
                    $modelRole->setEdit_per(1);
                    $modelRole->setDelete_per(1);
                    $modelRole->insertPermissions();
                    
                    
                }
                $this->_helper->flashMessenger('Roles and permissions have been added successfully.');
                $this->_redirect('/customer');
               
            }
            if ($modelRole->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
            
            
        }
               
        $this->view->rolesData = $modelPeer->fetchAllRoles();
        $this->view->form = $this->_addForm;
    }
    
     /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function editRoleAction() {
        $this->_helper->layout->setLayout('layout');
        $modelRole = new Application_Model_Roles();
        $modelPeer = new Application_Model_RolesPeer();
        
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm =  new Application_Form_Roles_Add();
        
        //Populate all value on edit mode
        if ($id) {
            $modelRole->setId($id);
            $roleData = $modelRole->fetchRoleById();

            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'rolename' => $roleData['role_name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Role");
            $this->_addForm->setAction("/roles/edit-role/id/".$id);
          
        } 
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelRole->setId($post['id']);
                $modelRole->setName($post['rolename']);
                $modelRole->setUpdated(date('Y-m-d H:i:s'));
                $modelRole->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelRole->save();                 
                
               $this->_helper->flashMessenger('Roles have been updated successfully.');
                $this->_redirect('/roles');
               
            }
        }
        $this->view->form = $this->_addForm;
    }
   

    /*
    * delete Action
    * use to delete data from user table
    * @return
    */
    
    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Roles();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('user_role_m');
        $model->deletePermissions($id);
        exit;
    }
    
    public function permissionsAction() {
        $this->_helper->layout->setLayout('layout');
        $modelPeer = new Application_Model_RolesPeer(); 
        $roleId = $this->_request->getParam('id');
        $this->view->screenData = Application_Model_RolesPeer::fetchAllScreens();
        $this->view->screenRoleData = Application_Model_RolesPeer::fetchAllPermissionsByRole($roleId);
        $this->view->roleId = $roleId;
    }
    
    public function addPermissionsAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Roles();
        $params = $this->_request->getParam('data');
        $roleId = $this->_request->getParam('roleId');

        $model->setId($roleId);
        $model->deletePermissions($roleId);

        for ($j = 0; $j <= count($params) - 1; $j++) {

            $param2 = explode(',', $params[$j]);
            $model->setId($roleId);
            $model->setScreen_id($param2[0]);
            $model->setView_per($param2[1]);
//            $model->setAdd_per($param2[2]);
//            $model->setEdit_per($param2[3]);
//            $model->setDelete_per($param2[4]);
            $model->insertPermissions();
        }
       exit;
    }
}
