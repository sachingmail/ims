<?php

/**
 * Sales Controller
 *
 * @package IMS
 * @author  Amit kumar
 * @author Ankit Goel
 * @version
 *
 */
class SalesOrderController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');

        if ($status == 'success') {
            $this->view->successMsg = 'Sales Order has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'Sales Order has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'Sales Order has been deleted successfully.';
        }
        //var_dump(Application_Model_SalesOrderPeer::fetchAllSalesOrder());die;
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchAllSalesOrder();
    }

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
        $screenData = $this->model->getScreenData('/sales-order/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('You are not Authorized to access this section', 'error');
            $this->_redirect('/sales-order');
        }
        
        $this->view->screenHeading = $screenData['screen_name'];
        
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $post = $request->getPost();
            $modelTaxingScheme = new Application_Model_TaxingScheme();
            $modelTaxingScheme->setId($post['taxing_scheme']);
            $result = $modelTaxingScheme->fetchTaxinSchemeById();
            // Date Conversion
            if ($post['order_date']) {
                $order_date1 = str_replace("/", "-", $post['order_date']);
                $order_date = date('Y-m-d', strtotime($order_date1));
            } else {
                $order_date = '';
            }
            if ($post['due_date']) {
                $due_date1 = str_replace("/", "-", $post['due_date']);
                $due_date = date('Y-m-d', strtotime($due_date1));
            } else {
                $due_date = '';
            }
            if ($post['req_shipping_date']) {
                $req_ship_date1 = str_replace("/", "-", $post['req_shipping_date']);
                $req_ship_date = date('Y-m-d', strtotime($req_ship_date1));
            } else {
                $req_ship_date = '';
            }
            // Save and Update
            $model->setId($post['id']);
            $model->setCustomerId($post['customer_id']);
            $model->setAddress($post['customer_address']);
            $model->setPaymentTerms($post['terms']);
            $model->setPerson_name($post['contact']);
            $model->setPhone($post['phone']);
            $model->setPo_Ref($post['p_order']);
            $model->setLocation($post['location_id']);
            $model->setSalesOrderId($post['order_id']);
            $model->setTax1((isset($result['tax_name'])) ? $result['tax_name'] : '');
            $model->setTaxAmount1((isset($post['tax1'])) ? $post['tax1'] : '');
            $model->setTaxper1((isset($result['tax_rate'])) ? $result['tax_rate'] : '');
            $model->setTax2((isset($result['secondary_tax_name'])) ? $result['secondary_tax_name'] : '');
            $model->setTaxAmount2((isset($post['tax2'])) ? $post['tax2'] : '');
            $model->setTaxper2((isset($result['secondary_tax_rate'])) ? $result['secondary_tax_rate'] : '');
            $model->setSalesRepsId($post['sales_reps']);
            $model->setTaxScheme($post['taxing_scheme']);
            $model->setSippingAddress($post['ship_to_address']);
            $model->setNonCustomerCost($post['non_customer_costs']);
            $model->setOrderDate($order_date);
            $model->setDueDate($due_date);
            $model->setPricingCurrency($post['pricing_currency']);
            $model->setRegShipDate($req_ship_date);
            $model->setNetSubTotal($post['sub_total']);
            $model->setGrandSubTotal($post['total_amount']);
            $model->setPaidAmount($post['paid_amount']);
            $model->setBalanceAmount($post['balance']);
            $model->setFulfilledRemarks($post['fulfilled_remarks']);
            $model->setReturnRemarks($post['ret_remarks']);
            $model->setRestockRemarks($post['restock_remark']);
            $model->setShipReq($post['ship_req']);
            $model->setRemarks($post['remarks']);
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->save();

            $last_id = $model->getId();
            $newOrderId = "SO-" . $last_id;
            $model->setOrderId($newOrderId);
            $model->updateSalesOrderId();
            if ($post['id'] == '') {
                $model->setSessionid(session_id());
                $model->updateOrderDetId();
                $model->updateSalesOrderPaymentId();
//                $this->_redirect('/sales-order/');
                $this->_helper->flashMessenger()->addMessage('Order has been added successfully', 'success');
                $this->_redirect('/sales-order/edit/id/' . $model->getId());
            } else {
                $this->_helper->flashMessenger()->addMessage('Order has been updated successfully', 'success');
                $this->_redirect('/sales-order/edit/id/' . $model->getId());
            }
        }
        $this->view->ordersData = $modelPeer->fetchVendorOrders($id);

        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i = 0;
        foreach ($product as $key => $val) {
            $productArr[$i]['name'] = $val['product_name'];
            $productArr[$i]['id'] = $val['id'];
            $i++;
        }

        $this->view->productData = $productArr;
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
        $this->view->terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->taxingSchemes = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $this->view->currency = Application_Model_PricingCurrencyPeer::fetchAllPricingCurrency();
        $this->view->reps = Application_Model_SaleRepsPeer::fetchAllReps();
        if ($id == null || $id < 1) {
            $this->view->orderMasData = array();
            $this->view->productList = $modelPeer->fetchSalesOrderProductsBySession(session_id());
            $this->view->subTotal = Application_Model_SalesOrderPeer::fetchSubTotalBySession(session_id());
            $this->view->fulfillproductData = Application_Model_SalesOrderPeer::fetchFulfillProductsBySessionId(session_id());
            $this->view->returnproductData = Application_Model_SalesOrderPeer::fetchReturnProductsBySessionId(session_id());
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProductsBySession(session_id());
            $this->view->returnSubTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $model->setId($id);
            $this->view->productList = $modelPeer->fetchSalesOrderProducts($id);
            $this->view->subTotal = Application_Model_SalesOrderPeer::fetchSubTotalById($id);
            $this->view->fulfillproductData = Application_Model_SalesOrderPeer::fetchFulfillProductsByOrderId($id);
            $this->view->returnproductData = Application_Model_SalesOrderPeer::fetchReturnProductsByOrderId($id);
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProducts($id);
            $this->view->returnSubTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalById($id);
        }
        $this->view->form = $this->_addForm;
    }

    /*
     * edit Action
     * use to edit data from user table
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $product = Application_Model_ProductPeer::fetchAllProduct();

        $this->view->productData = $product;
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
        $this->view->terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->taxingSchemes = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $this->view->currency = Application_Model_PricingCurrencyPeer::fetchAllPricingCurrency();
        $this->view->carrier = Application_Model_CarrierPeer::fetchAllCarriers();
        $this->view->reps = Application_Model_SaleRepsPeer::fetchAllReps();

        if ($id) {
            $model->setId($id);
            $this->view->orderMasData = $model->fetchSalesOrder();
            $this->view->productList = $modelPeer->fetchSalesOrderProducts($id);
            $this->view->subTotal = Application_Model_SalesOrderPeer::fetchSubTotalById($id);
            $this->view->fulfillproductData = Application_Model_SalesOrderPeer::fetchFulfillProductsByOrderId($id);
            $this->view->fulfilledOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($id, 'Fulfilled', 'O');
            $this->view->orderQty = Application_Model_SalesOrderPeer::countQty($id, 'Order');
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProducts($id);
             $this->view->returnproductData = Application_Model_SalesOrderPeer::fetchReturnProductsByOrderId($id);
            $this->view->returnSubTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalById($id);
            $this->view->restockOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($id, 'Restocked', 'U');
            $this->view->fulfillQty = Application_Model_SalesOrderPeer::countQty($id, 'Fulfilled');
        }
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesQuote();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('tra_sales_order_mas');
        $this->_helper->flashMessenger()->addMessage('Order has been deleted successfully', 'success');
        $this->_redirect('/sales-order');
    }

    /*
     * @return
     * use to add product in user table
     * 
     */

    public function addProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $param = $request->getParams();
        // var_dump($param);die;
        $so_id = $param['id'];
        $model->setId($param['id']);
        $model->setProductRecordId($param['sales_product_id']);

        $model->setProductId($param['product_name']);
        $model->setItemStatus('Order');

        //$model->setVendor_product_code($param['vendor_pcode']);
        $model->setQuantity($param['quantity']);
        $model->setUnitPrice($param['unit_price']);
        $model->setLocation($param['location_id']);
        $model->setDiscount($param['discount']);
        $model->setNetSubTotal($param['sub_total']);
        $model->setTransactionDate('');
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();

        if ($so_id == null || $so_id < 1) {
            $this->view->productList = $modelPeer->fetchSalesOrderProductsBySession(session_id());
        } else {
            $this->view->productList = $modelPeer->fetchSalesOrderProducts($so_id);
        }
    }

    public function deleteProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $so_id = $this->_request->getParam('so_id');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('tra_sales_order_det');
        if ($so_id == null || $so_id < 1) {
            $this->view->productList = $modelPeer->fetchSalesOrderProductsBySession(session_id());
        } else {
            $this->view->productList = $modelPeer->fetchSalesOrderProducts($so_id);
        }
    }

    public function editProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $model->setProductRecordId($id);
        $productData = $model->getProductById();
        echo json_encode($productData);
        exit;
    }

    public function calcTaxAction() {
        $this->_helper->layout->disableLayout();
        $scheme_id = $this->_request->getParam('scheme_id');
        $ship_req = $this->_request->getParam('ship_req');
        $sub_total = $this->_request->getParam('sub_total');
        $order_id = $this->_request->getParam('order_id');
        
//        $this->view->ship_req = $ship_req;
//        $this->view->subtotal = $this->_request->getParam('sub_total');
//        $this->view->freight = ($ship_req == 1) ? '20' : '0';
//        if ($order_id == null || $order_id < 1) {
//            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
//        } else {
//            $paid = Application_Model_SalesOrderPeer::getPaidAmount($order_id);
//        }
//        if ($paid) {
//            $this->view->paid = $paid['paid_amount'];
//        } else {
//            $this->view->paid = '0.00';
//        }
        
        $model = new Application_Model_SalesOrder();
        if ($order_id) {
            $model->setId($order_id);
            $orderData = $model->fetchSalesOrder();
            $this->view->order_id = $order_id;
            $this->view->ship_req = $ship_req;
            $this->view->subtotal = $sub_total;
            $this->view->freight = $orderData['freight'];
            if ($order_id == null || $order_id < 1) {
                $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
            } else {
                $paid = $model->getPaidPayment($order_id);
            }

            $this->view->paid = $paid;
            
            $this->view->schemeData = Application_Model_TaxingSchemePeer::getTaxingSchemeById($scheme_id);
        } else {
            $this->view->ship_req = $ship_req;
            $this->view->subtotal = $this->_request->getParam('sub_total');
            $this->view->freight = ($ship_req == 1) ? '0' : '0';
            $this->view->paid = '0.00';
            $this->view->schemeData = Application_Model_TaxingSchemePeer::getTaxingSchemeById($scheme_id);
        }
    }

    public function getCustomerDetailsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $vendorData = Application_Model_CustomerPeer::fetchCustomerDetails($id);

        echo json_encode($vendorData);
        exit;
    }

    public function getProductDetailsAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesOrder();
        $product_name = $this->_request->getParam('product_name');
        $customerId = $this->_request->getParam('customer_id');
        $location_id = $this->_request->getParam('location_id');
        $model->setLocation($location_id);
        $model->setProductId($product_name);
        $result = $model->getProductPresentAtLocation();
        if ($result < 0 || $result === null) {
            $data['price'] = "No Quantity Available";
            echo json_encode($data);
            die;
        }
//        $productId = $model->fetchProductId($product_name);
        $priceData = Application_Model_SalesOrderPeer::fetchProductAndCostPrice($product_name, $customerId);
        $priceData['quantity'] = $result;
        echo json_encode($priceData);
        exit;
    }
    /**
     * 
     */
    public function getFulfillProductDetailsAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesOrder();
        $product_id = $this->_request->getParam('product_id');
        $order_id = $this->_request->getParam('order_id');
        $model->setProductId($product_id);
        $model->setOrderId($order_id);
        $result = $model->getFulfilProductDetails();
        echo json_encode($result);die;
    }
    /**
     * 
     * 
     */
    public function getSubTotalAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $so_id = $param['so_id'];

        if ($so_id == null || $so_id < 1) {
            $subTotal = Application_Model_SalesOrderPeer::fetchSubTotalBySession(session_id());
        } else {
            $subTotal = Application_Model_SalesOrderPeer::fetchSubTotalById($so_id);
        }
        $subTotalArr = array(
            'sub_total' => $subTotal,
        );
        echo json_encode($subTotalArr);
        exit;
    }

    public function fulfilledOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $id = $param['id'];
        $model->setId($id);
        $model->fulfilledOrder();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchSalesProducts($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] - $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        exit;
    }

    public function unfulfilledOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $id = $param['id'];
        $model->setId($id);
        $model->unfulfilledOrder();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchSalesOrderProducts($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            //$validateQty = $modelInv->validateProductInventory();
//            if ($validateQty > 0) {
            $productInventory = $modelInv->getProductDetails();
            $newQuantity = $productInventory['quantity'] + $products['quantity'];
            $modelInv->setNewQuantity($newQuantity);
            $modelInv->updateProductQuantity();
//            } else {
//                $modelInv->setNewQuantity($products['quantity']);
//                $modelInv->insertProductQuantity();
//            }
        }


        exit;
    }

    /**
     * 
     * 
     */
    public function salesOrderPaymentAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $order_id = $this->_request->getParam('order_id');
        $this->view->sales_order_id = $order_id;
        $this->_addForm = new Application_Form_SalesPurchasePayment();
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsBySession(session_id());
            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsByOrderId($order_id);
            $paid = Application_Model_SalesOrderPeer::getPaidAmount($order_id);
        }
        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
        $this->view->form = $this->_addForm;
    }

    /**
     * 
     * 
     */
    public function insertSalesOrderPaymentAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $order_id = $this->_request->getParam('order_id');
        $this->view->sales_order_id = $order_id;
        if ($this->getRequest()->isPost()) {
            $post = $request->getPost();
            if ($post['payment_date']) {
                $payment_date1 = str_replace("/", "-", $post['payment_date']);
                $payment_date = date('Y-m-d H:i:s', strtotime($payment_date1));
            } else {
                $payment_date = '';
            }
            $model = new Application_Model_OrderPayments();
            $model->setId($post['id']);
            $model->setOrderType('sales');
            $model->setOrderId($post['order_id']);
            $model->setPaymentMethodId($post['payment_method']);
            $model->setPaymentRef($post['payment_ref']);
            $model->setPaymentDate($this->_helper->common->formatDbDate($post['payment_date']));
            $model->setPaymentType($post['payment_type']);
            $model->setPaymentRemarks($post['payment_remarks']);
            $model->setPayAmount($post['payment_amount']);
            if ($post['order_id']) {
                $model->setSessionid('');
            } else {
                $model->setSessionid(session_id());
            }
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            if ($post['id'] == null || $post['id'] < 1) {
                $model->insertPayment();
            } else {
                $model->updatePayment();
            }
        }
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsBySession(session_id());
            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsByOrderId($order_id);
            $paid = Application_Model_SalesOrderPeer::getPaidAmount($order_id);
        }
        if ($paid) {
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
        $modelSales = new Application_Model_SalesOrder();
        if($paid['order_total'] < $paid['paid_amount']) {
            $modelSales->setPaymentStatus('Owing');
            $this->view->salesPaymentStatus = 'Owing';
        }
        else if($paid['paid_amount'] == '0') {
            $modelSales->setPaymentStatus('Uninvoiced');
            $this->view->salesPaymentStatus = 'Uninvoiced';
        }
        else if ($paid['order_total'] > $paid['paid_amount']) {

            $modelSales->setPaymentStatus('Partial');
            $this->view->salesPaymentStatus = 'Partial';
        } else {
            $modelSales->setPaymentStatus('Paid');
            $this->view->salesPaymentStatus = 'Paid';
        }
        $paid_amount = $modelSales->getPaidPayment($order_id);
        $modelSales->setId($order_id);
        $modelSales->setBalanceAmount($paid['order_total'] - $paid_amount);
        $modelSales->setPaidAmount($paid_amount);
        $modelSales->setLastPayDate($this->_helper->common->formatDbDate($post['payment_date']));
        $modelSales->setUpdated(date('Y-m-d H:i:s'));
        $modelSales->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelSales->paymentStatusUpdate();
    }

    /**
     * edit sales order payment action
     *  used to fetch payment information
     */
    public function editSalesOrderPaymentsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_OrderPayments();
        $model->setPaymentId($id);
        $model->setOrderType('Sales');
        $paymentData = $model->fetchSalesPurchaseOrderPaymentById();
        echo json_encode($paymentData);
        exit;
    }

    /**
     * delete sale order payment action
     * will delete sales order payment 
     */
    public function deleteSalesOrderPaymentsAction() {
        $this->_helper->layout->disableLayout();
        $modelSales = new Application_Model_SalesOrder();
//        $modelPeer = new Application_Model_SalesOrderPeer();
        $id = $this->_request->getParam('id');
        $order_id = $this->_request->getParam('so_id');
        $modelSales->setId($id);
        $this->view->usersData = $modelSales->deleteData('tra_order_payments');
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsBySession(session_id());
            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsByOrderId($order_id);
            $paid = Application_Model_SalesOrderPeer::getPaidAmount($order_id);
        }
        if($paid['order_total'] < $paid['paid_amount']) {
            $modelSales->setPaymentStatus('Owing');
            $modelSales->setLastPayDate($this->_helper->common->formatDbDate($paid['last_pay_date']));
            $this->view->salesPaymentStatus = 'Owing';
        }
        else if($paid['paid_amount'] == '') {
            $modelSales->setPaymentStatus('Uninvoiced');
            $modelSales->setLastPayDate('');
            $this->view->salesPaymentStatus = 'Uninvoiced';
        }
        else if ($paid['order_total'] > $paid['paid_amount']) {
            $modelSales->setPaymentStatus('Partial');
            $modelSales->setLastPayDate($this->_helper->common->formatDbDate($paid['last_pay_date']));
            $this->view->salesPaymentStatus = 'Partial';
        } else {
            $modelSales->setPaymentStatus('Paid');
            $modelSales->setLastPayDate($this->_helper->common->formatDbDate($paid['last_pay_date']));
            $this->view->salesPaymentStatus = 'Paid';
        }
        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
            
        }
        $paid_amount = $modelSales->getPaidPayment($order_id);
        $modelSales->setId($order_id);
        $modelSales->setBalanceAmount($paid['order_total'] - $paid_amount);
        $modelSales->setPaidAmount($paid_amount);
        $modelSales->setUpdated(date('Y-m-d H:i:s'));
        $modelSales->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelSales->paymentStatusUpdate();
    }

    /**
     * mark order invoiced action
     * will mark order as invoiced
     */
    public function markOrderInvoicedAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $model->setId($id);
        $model->setOrderDate($this->_helper->common->formatDbDate(date('d/m/Y')));
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->markSalesOrderInvoiced();
        die;
    }

    /**
     * mark order uninvoiced action
     * this will mark order as uninvoiced
     */
    public function markOrderUninvoicedAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $model->setId($id);
        $created_date = $model->fetchCreatedDate();
        $model->setOrderDate($created_date);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->markSalesOrderUninvoiced();
        die;
    }

    /**
     * check order id present action 
     * check whether order id is present or not in table
     */
    public function checkOrderIdPresentAction() {
        $this->_helper->layout->disableLayout();
        $order_id = $this->_request->getParam('order_id');
        $model = new Application_Model_SalesOrder();
        if (strstr($order_id, '-') !== FALSE) {
            $data = explode('-', $order_id);
            if (($data[0] === "SO") && (strlen($data[1]) >= '6') && is_numeric($data[1])) {
                $model->setOrderId($order_id);
                $result = $model->chekOrderIdPresent();
                if ($result) {
                    echo "present";
                    die;
                } else {
                    echo "not present";
                    die;
                }
            } else {
                echo "fail";
                die;
            }
        } else {
            echo "wrong format";
            die;
        }
    }

    /**
     * 
     * 
     */
    public function addFulfillProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelMovement = new Application_Model_Movement();
        $request = $this->getRequest();
        $param = $request->getParams();
//        var_dump($param);die;
        //echo '<pre>'.print_r($param);die;
        $so_id = $param['id'];

        $model->setProductId($param['product_id']);
        $model->setId($param['id']);
        $model->setItemStatus('Order');
        $productQuantity = $model->getProductByProductIdAndStatus();
        if ((int) $param['quantity'] > (int) $productQuantity) {
            echo 'qtyError';
            exit;
        }
        
        $model->setItemStatus('Fulfilled');
        $model->setProductRecordId($param['product_record_id']);
        $fulfilledQuantity = $model->getFulfilledProductQty();
       
        if ((int)$productQuantity  < (int) ($fulfilledQuantity + $param['quantity'])) {
            echo 'recQtyExistError';
            exit;
        }
        $sales_order_id = $modelPeer->fetchSoIdByOrderId($so_id);
        $sub_total = $modelPeer->fetchProductSubTotal($so_id);
        $quantity = $modelPeer->fetchQuantity($param['product_id'], $param['location_id']);
        foreach ($quantity as $key => $value) {
            $qtyBefore = $value['quantity'];
            $qty = $param['quantity'];
            $qtyAfter = $value['quantity'] - $param['quantity'];
            $modelMovement->setLocationId($param['location_id']);
            $modelMovement->setProductId($param['product_id']);
            $modelMovement->setQty($qty);
            $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
            $modelMovement->setQtyBefore($qtyBefore);
            $modelMovement->setQtyAfter($qtyAfter);
            $modelMovement->setType('Sales Order');
            $modelMovement->setOrderId($sales_order_id);
            $modelMovement->setTotal_amount(0);
            $modelMovement->saveInsert();
        }
        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setQuantity($param['quantity']);
        $model->setTransactionDate('');
        $model->setLocation($param['location_id']);
        $model->setItemStatus('Fulfilled');
        $model->setReceiveLocationId($param['location_id']);
        $model->setUnitPrice(0.00);
        $model->setDiscount(0.00);
        $model->setNetSubTotal(0.00);
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();
        
        $modelInv = new Application_Model_Inventory();
        $modelInv->setLocation($param['location_id']);
        $modelInv->setProduct_id($param['product_id']);
        //Update or insert Quantity in Inventory
//        $validateQty = $modelInv->validateProductInventory();
//        if ($validateQty > 0) {
            $productInventory = $modelInv->getProductDetails();
            $newQuantity = $productInventory['quantity'] - $param['quantity'];
            $modelInv->setNewQuantity($newQuantity);
            $modelInv->updateProductQuantity();
//        } else {
//            $modelInv->setNewQuantity($param['quantity']);
//            $modelInv->insertProductQuantity();
//        }
        if ($so_id == null || $so_id < 1) {
            $this->view->fulfilledOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($so_id, 'Fulfilled');
        } else {
            $this->view->fulfilledOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($so_id, 'Fulfilled');
        }
    }

    /**
     * 
     * 
     */
    public function deleteFulfillProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $so_id = $this->_request->getParam('so_id');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $modelInv = new Application_Model_Inventory();

        $products = $modelPeer->fetchSalesProductsByRecordId($id);

        $modelInv->setLocation($products['location_id']);
        $modelInv->setProduct_id($products['product_id']);
        //Update or insert Quantity in Inventory
//        $validateQty = $modelInv->validateProductInventory();
//        if ($validateQty > 0) {
            $productInventory = $modelInv->getProductDetails();
            $newQuantity = $productInventory['quantity'] + $products['quantity'];
            $modelInv->setNewQuantity($newQuantity);
            $modelInv->updateProductQuantity();
//        } else {
//            $modelInv->setNewQuantity($products['quantity']);
//            $modelInv->insertProductQuantity();
//        }


        $model->setId($id);
        $model->deleteData('tra_sales_order_det');

        $model->setId($so_id);
        $model->setOrderStatus('Unfulfilled');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->orderStatusUpdate();

        $this->view->fulfilledOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($so_id, 'Fulfilled');
    }

    /**
     * 
     * 
     */
    public function autofillFulfillProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();
        $modelMovement = new Application_Model_Movement();

        $orderId = $param['order_id'];
        $location_id = $param['location_id'];
        $model->setId($orderId);
        $model->setLocation($location_id);
        $products = $modelPeer->fetchProductsByStatus($param['order_id'], 'Order');
        foreach ($products as $key => $value) {
            $so_id = $modelPeer->fetchSoIdByOrderId($value['order_id']);
            $sub_total = $modelPeer->fetchProductSubTotal($value['id']);
            $quantity = $modelPeer->fetchQuantity($value['product_id'], $value['location_id']);
            foreach ($quantity as $key1 => $value1) {
                $qtyBefore = $value1['quantity'];
                $qty = $value['quantity'];
                $qtyAfter = $value1['quantity'] - $value['quantity'];
                $modelMovement->setLocationId($value['location_id']);
                $modelMovement->setProductId($value['product_id']);
                $modelMovement->setQty($qty);
                $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Sales Order');
                $modelMovement->setOrderId($so_id);
                $modelMovement->setTotal_amount($sub_total);
                $modelMovement->saveInsert();
            }
        }
         $result = $model->autoFillFulfillProducts();
         //var_dump($result);die;
        foreach ($result as $key => $value) {
            $modelInv = new Application_Model_Inventory();
            $modelInv->setLocation($value['location_id']);
            $modelInv->setProduct_id($value['product_id']);
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] - $value['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($value['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        $this->view->fulfilledOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($orderId, 'Fulfilled');
    }

    /**
     * 
     * 
     */
    public function countQuantityAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $orderQty = Application_Model_SalesOrderPeer::countQty($id, 'Order');
        $receiveQty = Application_Model_SalesOrderPeer::countQty($id, 'Fulfilled');
        $status = Application_Model_SalesOrderPeer::getOrderStatus($id, 'Fulfilled');

        $tempArr = array(
            'order_qty' => $orderQty,
            'receive_qty' => $receiveQty,
            'order_status' => $status['order_status'],
            'pay_status' => $status['payment_status']
        );

        echo json_encode($tempArr);
        exit;
    }

    /**
     * 
     * 
     */
    public function addReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $param = $request->getParams();
        $so_id = $param['id'];
        $model->setProductId($param['product_id']);
        $model->setItemStatus('Order');
        $model->setId($param['id']);
        $productQuantity = $model->getProductByProductIdAndStatus();
        if ((int) $param['quantity'] > (int) $productQuantity) {
            echo 'qtyError';
            exit;
        }

        $model->setItemStatus('Fulfilled');
        $receivedQuantity = $model->getFulfilledProductQty();

        if ((int) $productQuantity < (int) ($receivedQuantity + $param['quantity'])) {
            echo 'recQtyExistError';
            exit;
        }
        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setQuantity($param['quantity']);
        $model->setUnitPrice($param['unit_price']);
        $model->setDiscount($param['discount']);
        $model->setNetSubTotal($param['sub_total']);
        if ($param['discarded'] == '1') {
            $model->setItemStatus('Discarded');
        } else {
            $model->setItemStatus('Returned');
        }
        $model->setTransactionDate($this->_helper->common->formatDbDate($param['ret_date']));
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();




        if ($so_id == null || $so_id < 1) {
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProductsBySession(session_id());
            $this->view->returnSubTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setPaymentStatus('Owing');
            $model->statusUpdate();

            $this->view->returnProductList = $modelPeer->fetchReturnSalesProducts($so_id);
            $this->view->returnSubTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalById($so_id);
        }
    }

    public function deleteReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $so_id = $this->_request->getParam('so_id');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $model->setId($id);
        $model->deleteData('tra_sales_order_det');
        if ($so_id == null || $so_id < 1) {
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProductsBySession(session_id());
        } else {
            $this->view->returnProductList = $modelPeer->fetchReturnSalesProducts($so_id);
        }
    }

    public function editReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $model->setProductRecordId($id);
        $productData = $model->getProductById();
        echo json_encode($productData);
        exit;
    }

    public function getReturnSubtotalAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $so_id = $param['so_id'];

        if ($so_id == null || $so_id < 1) {
            $subTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $subTotal = Application_Model_SalesOrderPeer::fetchReturnSubTotalById($so_id);
        }
        $subTotalArr = array(
            'sub_total' => $subTotal,
        );
        echo json_encode($subTotalArr);
        exit;
    }

    public function autofillReturnProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $location_id = $param['location_id'];

        $model->setId($orderId);
        $model->setLocation($location_id);
        $model->autoFillReturnProducts();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchProductsByStatus($orderId, 'Returned');

        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] + $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        $this->view->returnOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($orderId, 'Returned', 'U');
    }
    /**
     * 
     * 
     */
    public function autofillRestockProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();
        $modelMovement = new Application_Model_Movement();
        
        $orderId = $param['order_id'];
        $location_id = $param['location_id'];
        $products = $modelPeer->fetchReturnedProductsByStatus($param['order_id']);
        foreach ($products as $key => $value) {
            $so_id = $modelPeer->fetchSoIdByOrderId($value['order_id']);
            $quantity = $modelPeer->fetchQuantity($value['product_id'], $value['location_id']);
            $sub_total = $modelPeer->fetchProductSubTotal($value['id']);
            foreach ($quantity as $key1 => $value1) {
                $qtyBefore = $value1['quantity'];
                $qty = $value['quantity'];
                $qtyAfter = $value1['quantity'] + $value['quantity'];
                $modelMovement->setLocationId($value['location_id']);
                $modelMovement->setProductId($value['product_id']);
                $modelMovement->setQty($qty);
                $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Sales Restocked');
                $modelMovement->setOrderId($so_id);
                $modelMovement->setTotal_amount($sub_total);
                $modelMovement->saveInsert();
            }
        }
        $model->setId($orderId);
        $model->setLocation($location_id);
        $model->autoFillRestockProducts();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchProductsByStatus($orderId, 'Returned');

        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] + $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        $this->view->restockOrderData = Application_Model_SalesOrderPeer::fetchSalesOrderByStatus($orderId, 'Restocked', 'U');
    }
    /**
     * 
     * sales order payment action
     *  
     */
    public function paymentOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
//        var_dump($param);die;
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();

        $order_id = $param['id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($order_id);
        $modelPay->setOrderType('Sales');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();

        $model->setId($order_id);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);
        $model->setPaidAmount($balance);
        $model->setLastPayDate(date('Y-m-d H:i:s'));
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();

        exit;
    }

    /**
     * 
     * 
     */
    public function paymentOrderPopupAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
//        var_dump($param);die;
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Sales');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();

        $model->setId($orderId);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);
        $model->setPaidAmount($balance);
        $model->setLastPayDate(date('Y-m-d H:i:s'));
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();

        if ($orderId == null || $orderId < 1) {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsBySession(session_id());
            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsByOrderId($orderId);
            $paid = Application_Model_SalesOrderPeer::getPaidAmount($orderId);
        }

        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
    }

    /**
     * 
     * 
     */
    public function issueRefundAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $total_amount = $param['total_amount'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Sales');
        $modelPay->deleteOrderPayment();

        $model->setId($orderId);
        $model->setPaymentStatus('Uninvoiced');
        $model->setBalanceAmount($total_amount);
        $model->setPaidAmount(0);
        $model->setLastPayDate('');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();
        if ($orderId == null || $orderId < 1) {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsBySession(session_id());
            $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_SalesOrderPeer::fetchSalesPaymentsByOrderId($orderId);
            $paid = Application_Model_SalesOrderPeer::getPaidAmount($orderId);
        }

        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
    }

    /**
     * 
     * 
     */
    public function unpayOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $total_amount = $param['total_amount'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Sales');
        $modelPay->deleteOrderPayment();

        $model->setId($orderId);
        $model->setPaymentStatus('Uninvoiced');
        $model->setBalanceAmount($total_amount);
        $model->setPaidAmount(0);
        $model->setLastPayDate('');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();
        exit;
    }

    /**
     * 
     * 
     */
    public function showUnfulfilledOrdersAction() {
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchUnfulfilledtemByStatus();
    }
    /**
     * 
     * 
     */
    public function refundPaymentOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Sales');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('Refund');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();
 
         $paidAmount = $model->getPaidPayment($orderId);
        
        $model->setId($orderId);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);         
        $model->setPaidAmount($paidAmount);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setLastPayDate(date('Y-m-d'));
        $model->paymentStatusUpdate();
        
        $model->updateTotalPayment();
        exit;
    }
    /**
     * 
     * 
     */
    public function cancelSalesOrderAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $modelInv = new Application_Model_Inventory();
        $model->setId($id);
        $model->cancelSalesOrder();
        
        $productList = Application_Model_SalesOrderPeer::fetchCancelledSalesOrder($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                if($products['item_status']=='Fulfilled'){
                    $newQuantity = $productInventory['quantity'] + $products['quantity'];
                } else if($products['item_status']=='Restocked'){
                    $newQuantity = $productInventory['quantity'] + $products['quantity'];
                }
                
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
      
        $this->_helper->flashMessenger()->addMessage('Sales order has been cancelled successfully', 'success');
        $this->_redirect('/sales-order');
    }

    /**
     * 
     * 
     */
    public function reopenSalesOrderAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SalesOrder();
        $modelInv = new Application_Model_Inventory();
        $model->setId($id);
        $model->reopenSalesOrder();
        $productList = Application_Model_SalesOrderPeer::fetchCancelledSalesOrder($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                if($products['item_status']=='Fulfilled'){
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                } else if($products['item_status']=='Restocked'){
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                }
                
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        
        $this->_helper->flashMessenger()->addMessage('Sales order has been reopened successfully', 'success');
        $this->_redirect('/sales-order');
    }
    /**
     * 
     * 
     */
    public function showUnpaidOrdersAction() {
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchUnPaidtemByStatus();
    }

    /**
     * 
     * 
     */
    public function showUninvoicedOrdersAction() {
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchUnInvoicedtemByStatus();
    }
    
    public function recentOrdersAction() {
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchAllSalesOrder();
    }
    
    /**
     * 
     * print order action
     */
    public function printOrderAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllSalesProductInOrder($id, '');
        
        $salesOrder=$model->fetchSalesOrderForPrint();
        $this->view->orderMasData = $salesOrder;
        $this->view->productList = $modelPeer->fetchSalesProducts($id);
        $subTotal = Application_Model_SalesOrderPeer::fetchSubTotalByOrderId($id);
        $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
        
        
        $scheme_id = $salesOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $salesOrder['ship_req'];
        $this->view->subtotal = $salesOrder['sub_total'];
        $this->view->freight = $salesOrder['freight'];

        $taxData = Array(
            'tax_name' => $salesOrder['tax1_name'],
            'tax_rate' => $salesOrder['tax1_per'],
            'tax_on_shipping' => $salesOrder['tax_on_shipping'],
            'secondary_tax_name' => $salesOrder['tax2_name'],
            'secondary_tax_rate' => $salesOrder['tax2_per'],
            'secondary_tax_on_shipping' => $salesOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $salesOrder['compound_secondary'],
        );
        
        $paid = $model->getPaidPayment($id);
        $this->view->paid = $paid;
        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    /**
     * 
     * print order action
     */
    public function printInvoicedOrderAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $modelTerm = new Application_Model_PaymentTerms();
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllSalesProductInOrder($id, '');
        
        $salesOrder=$model->fetchSalesOrderForPrint();
        $order_date = $salesOrder['so_date'];
        $term = $salesOrder['term_id'];
        $modelTerm->setId($term);
        $term_data = $modelTerm->fetchPaymentById();
        $term_date = '+'.' '.$term_data['days'].' '.'days';
        $due_date_new = date('d-m-Y',strtotime($order_date.$term_date));
        $salesOrder['due_date'] = $due_date_new;
        $this->view->orderMasData = $salesOrder;
        $this->view->productList = $modelPeer->fetchSalesProducts($id);
        $subTotal = Application_Model_SalesOrderPeer::fetchSubTotalByOrderId($id);
        $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
        
        
        $scheme_id = $salesOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $salesOrder['ship_req'];
        $this->view->subtotal = $salesOrder['sub_total'];
        $this->view->freight = $salesOrder['freight'];

        $taxData = Array(
            'tax_name' => $salesOrder['tax1_name'],
            'tax_rate' => $salesOrder['tax1_per'],
            'tax_on_shipping' => $salesOrder['tax_on_shipping'],
            'secondary_tax_name' => $salesOrder['tax2_name'],
            'secondary_tax_rate' => $salesOrder['tax2_per'],
            'secondary_tax_on_shipping' => $salesOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $salesOrder['compound_secondary'],
        );
        
        $paid = $model->getPaidPayment($id);
        $this->view->paid = $paid;
        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    /**
     * 
     * print order action
     */
    public function printReceiptAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllSalesProductInOrder($id, '');
        
        $salesOrder=$model->fetchSalesOrderForPrint();
        $this->view->orderMasData = $salesOrder;
        $this->view->productList = $modelPeer->fetchSalesProducts($id);
        $subTotal = Application_Model_SalesOrderPeer::fetchSubTotalByOrderId($id);
        $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
        
        
        $scheme_id = $salesOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $salesOrder['ship_req'];
        $this->view->subtotal = $salesOrder['sub_total'];
        $this->view->freight = $salesOrder['freight'];

        $taxData = Array(
            'tax_name' => $salesOrder['tax1_name'],
            'tax_rate' => $salesOrder['tax1_per'],
            'tax_on_shipping' => $salesOrder['tax_on_shipping'],
            'secondary_tax_name' => $salesOrder['tax2_name'],
            'secondary_tax_rate' => $salesOrder['tax2_per'],
            'secondary_tax_on_shipping' => $salesOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $salesOrder['compound_secondary'],
        );
        
        $paid = $model->getPaidPayment($id);
        $this->view->paid = $paid;
        $this->view->balance = $salesOrder['balance'];
        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    /**
     * 
     * print order action
     */
    public function printPickListAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_SalesOrder();
        $modelPeer = new Application_Model_SalesOrderPeer();
        
                
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllSalesProductInOrder($id, '');
        
        $salesOrder=$model->fetchSalesOrderForPrint();        
        $product_list = $modelPeer->fetchSalesOrderByStatus($id,"Order");
        $this->view->orderMasData = $salesOrder;
        $this->view->productList = $product_list;
        $total_quantity = 0;
        foreach($product_list as $key =>$value) {
            $total_quantity = $total_quantity + $value['quantity'];
        }
        
        $this->view->total_quantity = $total_quantity;
        $scheme_id = $salesOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $salesOrder['ship_req'];
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    
    public function salesOrderByStatusAction() {
        $this->_helper->layout->setLayout('layout');
        $request = $this->getRequest();
        $status = $this->_request->getParam('status');
        $this->view->salesData = Application_Model_SalesOrderPeer::fetchAllSalesOrderByStatus($status);
    }
}
