<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CityController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCity = new Application_Model_City();
        $modelPeer = new Application_Model_CityPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_City();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }
        $state = $modelPeer->fetchAllStates();
        $Data = array();
        $Data[''] = 'Please Select';
        foreach ($state as $key => $val) {
            $Data[$val['id']] = $val['state_name'];
        }

        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }
        $this->_addForm->state->setMultiOptions($Data);
        $this->_addForm->country->setMultiOptions($statusData);

        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
             $status ='';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCity->setId($post['id']);
                $modelCity->setCityName($post['city_name']);
                $modelCity->setStateId($post['state']);
                $modelCity->setCountryId($post['country']);
                $result = $modelCity->checkCityIsPresent();
                if($result) {
                    $this->_redirect('/city?status=present');
                }
                $modelCity->setUpdated(date('Y-m-d H:i:s'));
                $modelCity->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCity->setCreated(date('Y-m-d H:i:s'));
                $modelCity->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCity->save();

                $this->_redirect('/city?status=success');
            }
            if ($modelCity->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        if ($status == 'present') {
            $this->view->successMsg = 'City is already Present.';
        }
        if ($status == 'success') {
            $this->view->successMsg = 'City has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'City has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'City has been deleted successfully.';
        }

        $this->view->details = $modelPeer->fetchAllCities();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCity = new Application_Model_City();
        $modelPeer = new Application_Model_CityPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_City();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $state = $modelPeer->fetchAllStates();
        $Data = array();
        $Data[''] = 'Please Select';
        foreach ($state as $key => $val) {
            $Data[$val['id']] = $val['state_name'];
        }
        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }
        $this->_addForm->state->setMultiOptions($Data);
        $this->_addForm->country->setMultiOptions($statusData);
        //Populate all value on edit mode
        if ($id) {
            $modelCity->setId($id);
            $CityData = $modelCity->fetchCityById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'city_name' => $CityData['city_name'],
                        'state' => $CityData['state_id'],
                        'country' => $CityData['country_id']
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update City");
            $this->_addForm->setAction("/city/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCity->setId($post['id']);
                $modelCity->setCityName($post['city_name']);
                $modelCity->setCountryId($post['country']);
                $modelCity->setStateId($post['state']);
                $modelCity->setUpdated(date('Y-m-d H:i:s'));
                $modelCity->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCity->save();

                $this->_redirect('/city?status=updated');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_City();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_cities');
        exit;
    }

    /*
     * Get State Action
     * This Action is used to fetch all states .
     * @return
     */

    public function getStateAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_City();
        $modelPeer = new Application_Model_CityPeer();
        $id = $this->_request->getParam('id');
        $model->setId($id);
        $states = $model->getStatesByCountryId();
        $this->view->stateData = $states;
    }

    /*
     * Get Country Action
     * This Action is used to fetch all Countries .
     * @return
     */

    public function getCountryAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_City();
        $modelPeer = new Application_Model_CityPeer();
        $id = $this->_request->getParam('id');
        if ($id) {
            $model->setId($id);
            $country = $model->getCountryByCountryId();
            $this->view->countryData = $country;
        } else {
            $country = $modelPeer->fetchAllCountries();
            $this->view->flag = '1';
            $this->view->countryData = $country;
        }
    }

}
