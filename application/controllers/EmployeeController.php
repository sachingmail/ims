<?php

/**
 * EmployeeController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class EmployeeController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
        
        
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCard = new Application_Model_Employee();
        $modelPeer = new Application_Model_EmployeePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Employee();

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status ='';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCard->setId($post['id']);
                $modelCard->setEmpName($post['name']);
                $modelCard->setUpdated(date('Y-m-d H:i:s'));
                $modelCard->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->setCreated(date('Y-m-d H:i:s'));
                $modelCard->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->save();
                
                $this->_helper->flashMessenger()->addMessage('Employee has been added successfully', 'success');
                $this->_redirect('/employee');
            }
            if ($modelCard->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllEmployee();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCard = new Application_Model_Employee();
        $modelPeer = new Application_Model_EmployeePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_Employee();

        //Populate all value on edit mode
        if ($id) {
            $modelCard->setId($id);
            $data = $modelCard->fetchCardById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $data['name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update");
            $this->_addForm->setAction("/employee/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCard->setId($post['id']);
                $modelCard->setCardName($post['name']);
                $modelCard->setUpdated(date('Y-m-d H:i:s'));
                $modelCard->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->save();

                $this->_helper->flashMessenger()->addMessage('Employee has been updated successfully', 'success');
                $this->_redirect('/employee');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Employee();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_employee');
        
        $this->_helper->flashMessenger()->addMessage('Employee has been deleted successfully', 'success');
        $this->_redirect('/employee');
    }

}
