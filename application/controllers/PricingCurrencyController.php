<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class PricingCurrencyController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCurrency = new Application_Model_PricingCurrency();
        $modelPeer = new Application_Model_PricingCurrencyPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_PricingCurrency();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $currency = $modelPeer->fetchAllCurrency();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($currency as $key => $val) {
            $statusData[$val['currency_name']] = $val['currency_name'];
        }

        $this->_addForm->currency_name->setMultiOptions($statusData);
        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCurrency->setId($post['id']);
                $modelCurrency->setCurrency_name($post['currency_name']);
                $modelCurrency->setName($post['name']);
                $default = $modelPeer->getDefaultPricingCurrency();
                if ($default > 0) {
                    $modelCurrency->setDefault('0');
                } else {
                    $modelCurrency->setDefault('1');
                }
                $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
                $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->setCreated(date('Y-m-d H:i:s'));
                $modelCurrency->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->save();

                $this->_helper->flashMessenger()->addMessage('Pricing Currency has been added successfully', 'success');
                $this->_redirect('/pricing-currency');
            }
            if ($modelCurrency->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllPricingCurrency();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content from master table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCurrency = new Application_Model_PricingCurrency();
        $modelPeer = new Application_Model_PricingCurrencyPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_PricingCurrency();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $currency = $modelPeer->fetchAllCurrency();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($currency as $key => $val) {
            $statusData[$val['currency_name']] = $val['currency_name'];
        }
        $this->_addForm->currency_name->setMultiOptions($statusData);
        //Populate all value on edit mode
        if ($id) {
            $modelCurrency->setId($id);
            $CurrencyData = $modelCurrency->fetchPricingCurrencyById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $CurrencyData['name'],
                        'currency_name' => $CurrencyData['currency_name']
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Currency");
            $this->_addForm->setAction("/pricing-currency/edit/id/" . $id);
            //$this->_addForm->upassword->setRequired(false);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCurrency->setId($post['id']);
                $modelCurrency->setCurrency_name($post['currency_name']);
                $modelCurrency->setName($post['name']);
                $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
                $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->save();

                $this->_helper->flashMessenger()->addMessage('Pricing Currency has been updated successfully', 'success');
                $this->_redirect('/pricing-currency');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_PricingCurrency();
        $model->setId($id);
        $result = $model->checkIfPricingCurrencyPresentInRecords();
         if (count($result['sales_quote']) > 0 || count($result['sales_order']) > 0 || count($result['customers']) > 0) {
             $this->_helper->flashMessenger()->addMessage('Pricing Currency cannot be deleted as it has been used in some records', 'error');
             $this->_redirect('/pricing-currency');
         }
        $this->view->usersData = $model->deletePricingCurrency('mas_pricing_currency');
        $this->_helper->flashMessenger()->addMessage('Pricing Currency has been deleted successfully', 'success');
        $this->_redirect('/pricing-currency');
    }

    /*
     * Add New Pricing Currency Popup Action
     * This action is used to show a popup to add a new pricing currency in add new product.
     * @return
     */

    public function addNewPricingCurrencyPopupAction() {
        $this->_helper->layout->disableLayout();
        $modelCurrency = new Application_Model_PricingCurrency();
        $modelPeer = new Application_Model_PricingCurrencyPeer();
        $this->_addForm = new Application_Form_PricingCurrency();

        $currency = $modelPeer->fetchAllCurrency();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($currency as $key => $val) {
            $statusData[$val['currency_name']] = $val['currency_name'];
        }

        $this->_addForm->currency_name->setMultiOptions($statusData);
        $this->view->details = $modelPeer->fetchAllPricingCurrency();
        $this->view->form = $this->_addForm;
    }

    /*
     * Insert New Pricing Currency Popup Action
     * This action is used to insert a new pricing currency when a popup is open in add new product.
     * @return
     */

    public function insertNewPricingCurrencyPopupAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $modelCurrency = new Application_Model_PricingCurrency();
        $modelProduct = new Application_Model_Product();
        $modelPeer = new Application_Model_PricingCurrencyPeer();
        $post = $request->getPost();
        $modelCurrency->setId($post['id']);
        $modelCurrency->setCurrency_name($post['currency_name']);
        $modelCurrency->setName($post['pricing_scheme']);
        $default = $modelPeer->getDefaultPricingCurrency();
        if ($default > 0) {
            $modelCurrency->setDefault('0');
        } else {
            $modelCurrency->setDefault('1');
        }
        $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
        $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelCurrency->setCreated(date('Y-m-d H:i:s'));
        $modelCurrency->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelCurrency->save();
        $modelProduct->setId($post['product_id']);
        $productCostData = $modelProduct->fetchProductCostById();
//            var_dump($productCostData);die;
        $newArr = array();
        $pricing_currency = Application_Model_ProductPeer::fetchAllPricingCurrency();
        foreach ($productCostData as $valData) {
            $newArr[$valData['price_currency_id']] = array(
                'id' => $valData['id'],
                'price_currency_id' => $valData['price_currency_id'],
                'price' => $valData['price'],
                'markup' => $valData['markup'],
                'cost' => $valData['cost'],
                'price_currency_name' => $valData['price_currency_name'],
            );
        }
        $this->view->productCostData = $newArr;
        $this->view->pricingCurrency = $pricing_currency;
    }

    /*
     * Make Default Pricing Currency Action
     * This action is used to set a default pricing currency.
     * @return
     */

    public function makeDefaultPricingCurrencyAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $order_id = $this->_request->getParam('order_id');
        $modelCurrency = new Application_Model_PricingCurrency();
        $modelCurrency->setId($id);
        $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
        $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $result = $modelCurrency->MakeDefaultPricingCurrency($order_id);
        foreach ($result as $key => $value) {
            $quantity = $value['quantity'];
            $discount = $value['discount'];
            $price = $value['price'];
            $netsubtotal = $price * $quantity;
            $discount_total = ($discount / 100) * $netsubtotal;
            $subtotal = $netsubtotal - $discount_total;
            $model = new Application_Model_SalesOrder();
            $modelPeer = new Application_Model_SalesOrderPeer();
            $model->setId($order_id);
            $model->setProductRecordId($value['id']);

            $model->setProductId($value['product_id']);
            $model->setItemStatus('Order');
            $model->setQuantity($quantity);
            $model->setUnitPrice($price);
            $model->setDiscount($discount);
            $model->setNetSubTotal($subtotal);
            $model->setTransactionDate('');
            if ($order_id) {
                $model->setSessionid('');
            } else {
                $model->setSessionid(session_id());
            }
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->saveProduct();
            if ($order_id == null || $order_id < 1) {
                $this->view->productList = $modelPeer->fetchSalesOrderProductsBySession(session_id());
            } else {
                $this->view->productList = $modelPeer->fetchSalesOrderProducts($order_id);
            }
        }
    }

    public function getPricingUnitPriceAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $id = $this->_request->getParam('pricing_id');
        $product_id = $this->_request->getParam('product_id');
        $modelCurrency = new Application_Model_PricingCurrency();
        if ($product_id) {
            $result = $modelCurrency->fetchPriceById($id, $product_id);
            echo json_encode($result);
            die;
        } else {
            $result = '0.00';
            echo json_encode($result);
            die;
        }
    }

}
