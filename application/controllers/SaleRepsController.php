<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class SaleRepsController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelReps = new Application_Model_SaleReps();
        $modelPeer = new Application_Model_SaleRepsPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_SaleReps();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelReps->setId($post['id']);
                $modelReps->setRepsName($post['name']);
                $modelReps->setUpdated(date('Y-m-d H:i:s'));
                $modelReps->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelReps->setCreated(date('Y-m-d H:i:s'));
                $modelReps->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelReps->save();

                $this->_helper->flashMessenger()->addMessage('Sales Representative has been added successfully', 'success');
                $this->_redirect('/sale-reps');
            }
            if ($modelReps->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }
        $this->view->details = $modelPeer->fetchAllReps();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelReps = new Application_Model_SaleReps();
        $modelPeer = new Application_Model_SaleRepsPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_SaleReps();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelReps->setId($id);
            $data = $modelReps->fetchRepsById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $data['name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Representative");
            $this->_addForm->setAction("/sale-reps/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelReps->setId($post['id']);
                $modelReps->setRepsName($post['name']);
                $modelReps->setUpdated(date('Y-m-d H:i:s'));
                $modelReps->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelReps->save();

                $this->_helper->flashMessenger()->addMessage('Sales Representative has been updated successfully', 'success');
                $this->_redirect('/sale-reps');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_SaleReps();
        $model->setId($id);
        $result = $model->checkIfRepsPresentInRecords();
        if (count($result['sales_quote']) > 0 || count($result['sales_order']) > 0 || count($result['customers']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Sales Representative cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/sale-reps');
        } else {
            $this->view->usersData = $model->deleteData('mas_sales_reps');
            $this->_helper->flashMessenger()->addMessage('Sales Representative has been deleted successfully', 'success');
            $this->_redirect('/sale-reps');
        }
    }

}
