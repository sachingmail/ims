<?php

/**
 * Mail Action helper
 *
 * @package Cost Cudgeting
 * @author  Sukhvir Singh
 * @version Dev
 *
 */
class Zend_Controller_Action_Helper_Common extends Zend_Controller_Action_Helper_Abstract {
    
    public function formatDbDate($date) {
        $formattedDate = 'FALSE';
        if($date) {
            $dateParts = explode('/', $date);
            $formattedDate = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
        }
        return $formattedDate;
    }
    
    public function ValidateNullDate($date) {
        if($date=='00/00/0000' || $date=='01/01/1970') {
           $retDate ='';
        } else {
            $retDate = $date;
        }
        return $retDate;
    }

}