<?php

/**
 * Mail Action helper
 *
 * @package Cost Cudgeting
 * @author  Sukhvir Singh
 * @version Dev
 *
 */
class Zend_Controller_Action_Helper_Mail extends Zend_Controller_Action_Helper_Abstract {

    public function sendMail($name, $to, $subject = '', $body = '', $from = '') {
        
            $mail = new Zend_Mail();
            
            $mail->addTo($to, $name);
            $mail->setSubject($subject);
            $mail->setBodyHtml($body);
            $mail->setFrom('norpely@bluechip.com', 'Bluechip Engineering IMS');
            $mailsent = $mail->send();
    }

}