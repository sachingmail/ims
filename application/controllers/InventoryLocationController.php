<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class InventoryLocationController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelLocation = new Application_Model_InventoryLocation();
        $modelPeer = new Application_Model_InventoryLocationPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_InventoryLocation();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelLocation->setId($post['id']);
                $modelLocation->setLocationName($post['name']);
                $modelLocation->setUpdated(date('Y-m-d H:i:s'));
                $modelLocation->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelLocation->setCreated(date('Y-m-d H:i:s'));
                $modelLocation->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelLocation->save();
                $this->_helper->flashMessenger()->addMessage('Location has been added successfully', 'success');
                $this->_redirect('/inventory-location');
            }
            if ($modelLocation->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllLocations();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * use to edit content in user table
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelLocation = new Application_Model_InventoryLocation();
        $modelPeer = new Application_Model_InventoryLocationPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_InventoryLocation();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelLocation->setId($id);
            $data = $modelLocation->fetchLocationById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $data['name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Location");
            $this->_addForm->setAction("/inventory-location/edit/id/" . $id);
            //$this->_addForm->upassword->setRequired(false);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelLocation->setId($post['id']);
                $modelLocation->setLocationName($post['name']);
                $modelLocation->setUpdated(date('Y-m-d H:i:s'));
                $modelLocation->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelLocation->save();
                $this->_helper->flashMessenger()->addMessage('Location has been updated successfully', 'success');
                $this->_redirect('/inventory-location');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_InventoryLocation();
        $model->setId($id);
        $result = $model->checkIfLocationIsPresentInRecords();
        if (count($result['inventory_product']) > 0 || count($result['product_quantity']) > 0 || count($result['purchase_det']) > 0 || count($result['purchase_mas']) > 0 || count($result['sales_mas']) > 0 || count($result['sales_det']) > 0 || count($result['sales_quote_mas']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Location cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/inventory-location');
        } else {
            $this->view->usersData = $model->deleteData('mas_inventory_location');
            $this->_helper->flashMessenger()->addMessage('Location has been deleted successfully', 'success');
            $this->_redirect('/inventory-location');
        }
    }

}
