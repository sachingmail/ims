<?php

/**
 * VendorController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class VendorController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');

        $this->view->vendorData = Application_Model_VendorPeer::fetchAllVendors();
    }

    public function addAction() {
        $this->_helper->layout->setLayout('layout');

        $screenData = $this->model->getScreenData('/vendor/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('<strong>Alert !</strong> You are not Authorized to access this section', 'error');
            $this->_redirect('/vendor');
        }

        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_VendorPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Vendor();

        //Populate all value on edit mode
        if ($id) {
            $model->setId($id);
            $customerData = $model->fetchVendorById();
            $this->view->is_active = $customerData['is_active'];
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $customerData['name'],
                        'balance' => $customerData['balance'],
                        'credit' => $customerData['credit'],
                        'address_type' => $customerData['address_type'],
                        'address' => $customerData['address'],
                        'address1' => $customerData['address1'],
                        'address2' => $customerData['address2'],
                        'contact_name' => $customerData['contact_name'],
                        'phoneno' => $customerData['contact_phone'],
                        'fax' => $customerData['contact_fax'],
                        'email' => $customerData['contact_email'],
                        'website' => $customerData['contact_website'],
                        'currency' => $customerData['currency_id'],
                        'payment_terms' => $customerData['payment_terms'],
                        'taxing_scheme' => $customerData['taxing_schemes'],
                        'carrier' => $customerData['carrier_id'],
                        'is_active' => $customerData['is_active'],
                        'remarks' => $customerData['remarks'],
                    )
            );

            $this->_addForm->submitbtn->setLabel("Update Vendor");
            $this->_addForm->setAction("/vendor/add/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                $model->setName($post['name']);
                $model->setBalance($post['balance']);
                $model->setCredit($post['credit']);
                $model->setAddress_type($post['address_type']);
                $model->setAddress($post['address']);
                $model->setAddress1($post['address1']);
                $model->setAddress2($post['address2']);
                $model->setPerson_name($post['contact_name']);
                $model->setPhone($post['phoneno']);
                $model->setFax($post['fax']);
                $model->setEmail($post['email']);
                $model->setWebsite($post['website']);
                $model->setCurrency($post['currency']);
                $model->setPayment_term($post['payment_terms']);
                $model->setTax_scheme($post['taxing_scheme']);
                $model->setCarrier($post['carrier']);
                $model->setIs_active(1);
                $model->setRemarks($post['remarks']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->save();

                if ($post['id'] == '') {
                    $model->setSessionid(session_id());
                    $model->updateVendorId();
                    $this->_helper->flashMessenger()->addMessage('Vendor has been added successfully', 'success');
                    $this->_redirect('/vendor');
                } else {
                    $this->_helper->flashMessenger()->addMessage('Vendor has been updated successfully', 'success');
                    $this->_redirect('/vendor');
                }
            }
        }
        $this->view->ordersData = $modelPeer->fetchVendorOrders($id);

        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i = 0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }

        $this->view->productData = json_encode($productArr);
        if ($id == null || $id < 1) {
            $this->view->productList = $modelPeer->fetchVendorProductsBySession(session_id());
        } else {
            $this->view->productList = $modelPeer->fetchVendorProducts($id);
        }


//        echo '<pre>';
//        echo $this->view->productData;exit;
        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Vendor();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_vendors');

        $this->_helper->flashMessenger()->addMessage('Vendor has been deleted successfully', 'success');
        $this->_redirect('/vendor');
    }

    public function addProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_VendorPeer();
        $request = $this->getRequest();
        $post = $request->getParams();
        $vendor_id = $post['id'];
        $model->setId($post['id']);
        $model->setProductRecordId($post['vendor_product_id']);
        //get product Id
        $productId = $model->fetchProductId($post['product_code']);
        $model->setProductCode($productId);

        $model->setVendor_product_code($post['vendor_pcode']);
        $model->setProduct_cost($post['product_cost']);
        if ($post['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);

        $product_name = $model->checkProductNameIsPresent();
        if ($product_name != '') {
            echo "product-exist";
            die;
        }
        $product_code = $model->checkProductCodeIsPresent();
        if ($product_code != '') {
            echo "code-exist";
            die;
        }
        $model->saveProduct();

        if ($vendor_id == null || $vendor_id < 1) {
            $this->view->productList = $modelPeer->fetchVendorProductsBySession(session_id());
        } else {
            $this->view->productList = $modelPeer->fetchVendorProducts($vendor_id);
        }
    }

    public function deleteProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $vendor_id = $this->_request->getParam('vendor_id');
        $model = new Application_Model_Vendor();
        $modelPeer = new Application_Model_VendorPeer();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_vendors_product');
        if ($vendor_id == null || $vendor_id < 1) {
            $this->view->productList = $modelPeer->fetchVendorProductsBySession(session_id());
        } else {
            $this->view->productList = $modelPeer->fetchVendorProducts($vendor_id);
        }
    }

    public function editProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        //$vendor_id = $this->_request->getParam('vendor_id');
        $model = new Application_Model_Vendor();
        $model->setProductRecordId($id);
        $vendorProductData = $model->fetchVendorProductById();
        echo json_encode($vendorProductData);
        exit;
    }

    public function deactivateVendorAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Vendor();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->DeactivateVendor();

        $this->_helper->flashMessenger()->addMessage('Vendor has been deactivated successfully', 'success');
        $this->_redirect('/vendor');
    }

    public function activateVendorAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Vendor();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->ActivateVendor();

        $this->_helper->flashMessenger()->addMessage('Vendor has been activated successfully', 'success');
        $this->_redirect('/vendor');
    }

    public function checkProductCodeIsPresentAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $id = $this->_request->getParam('id');
        $product = $this->_request->getParam('product');
        $product_code = $this->_request->getParam('product_code');
        $productId = $model->fetchProductId($product);
        $model->setId($id);
        $model->setSessionid(session_id());
        $model->setVendor_product_code($product_code);
        $model->setProductCode($productId);
        if ($id) {
            $result = $model->checkProductCodeIsPresent();
            if ($result) {
                echo "Product Code Present";
                die;
            } else {
                echo "No Product Code Present";
                die;
            }
        } else {
            $result = $model->checkProductCodeIsPresentBySession();
            if ($result) {
                echo "Product Code Present";
                die;
            } else {
                echo "No Product Code Present";
                die;
            }
        }
    }

    public function checkProductNameAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $id = $this->_request->getParam('id');
        $product = $this->_request->getParam('product');
        $productId = $model->fetchProductId($product);
        $model->setProductCode($productId);
        if ($id) {
            $model->setId($id);
            $model->setSessionid('');
        } else {
            $model->setId('');
            $model->setSessionid(session_id());
        }
        $result = $model->checkProductNameIsPresent();
        if ($result) {
            echo "Product Present";
            die;
        } else {
            echo "No Product Present";
            die;
        }
    }

    /**
     * 
     * 
     * 
     */
    public function sendPaymentLinkAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $id = $this->_request->getParam('vendor_id');
        $this->view->id = $id;
        $this->view->paymentMethod = Application_Model_PaymentMethodPeer::fetchAllpayments();
        $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
        $this->view->vendorPurchaseList = Application_Model_VendorPeer::fetchVendorPurchaseOrderById($id);
    }

    public function insertSendPaymentLinkAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $modelPayment = new Application_Model_OrderPayments();
        $post = $this->_request->getPost();
        $modelPayment->setOrderType('Purchase');
        $modelPayment->setPaymentDate($this->_helper->common->formatDbDate($post['date_paid']));
        $modelPayment->setPaymentMethodId($post['payment_method']);
        $modelPayment->setPayAmount($post['payment_amount']);
        $modelPayment->setCurrencyId($post['currency']);
        $modelPayment->setPaymentRef($post['reference']);
        $modelPayment->setPaymentRemarks($post['memo']);
        $modelPayment->setPaymentType('payment');
        $modelPayment->setOrderId('');
        $modelPayment->setUpdated(date('Y-m-d H:i:s'));
        $modelPayment->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPayment->setCreated(date('Y-m-d H:i:s'));
        $modelPayment->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPayment->insertPayment();
        $purchase_products = Application_Model_VendorPeer::fetchVendorPurchaseOrderById($post['id']);
        if ($purchase_products) {
            foreach ($purchase_products as $key => $value) {
                if ($post['payment_amount'] > 0) {
                    $payment = $post['payment_amount'];
                    $total_amount = $value['total_amount'];
                    $balance = $value['balance'];
                    $new_paid = $payment - $balance;
                    if ($new_paid > 0) {
                        $post['payment_amount'] = $new_paid;
                        $status = "Paid";
                        $balance = 0;
                        $paid = $total_amount;
                    }
                    if ($new_paid < 0) {
                        $post['payment_amount'] = '0';
                        $balance = - $new_paid;
                        $paid = $total_amount - $balance;
                        $status = 'Partial';
                    }
                    if ($new_paid == 0) {
                        $post['payment_amount'] = '0';
                        $balance = 0;
                        $status = "Paid";
                        $paid = $total_amount;
                    }
                    $model->setBalance($balance);
                    $model->setPayment_status($status);
                    $model->setProductRecordId($value['id']);
                    $model->setPaid_amount($paid);
                    $model->UpdatePaidAndBalance();
                }
            }
            if ($post['payment_amount'] > 0) {
                $model->setId($post['id']);
                $vendor_info = $model->fetchVendorById();
                $credit = $vendor_info['credit'];
                $credit = $credit + $post['payment_amount'];
                $model->setCredit($credit);
                $model->updateCredit();
            }
        } else {
            $model->setId($post['id']);
            $vendor_info = $model->fetchVendorById();
            $credit = $vendor_info['credit'];
            $credit = $credit + $post['payment_amount'];
            $model->setCredit($credit);
            $model->updateCredit();
        }
        $this->view->vendorPurchaseList = Application_Model_VendorPeer::fetchVendorPurchaseOrderById($post['id']);
    }

    /**
     * 
     * 
     */
    public function payBalanceAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Vendor();
        $modelPayment = new Application_Model_OrderPayments();
        $post = $this->_request->getPost();
        $purchase_products = Application_Model_VendorPeer::fetchVendorPurchaseOrderById($post['vendor_id']);
        $total_final = 0;
        foreach ($purchase_products as $key => $value) {
            $total_amount = $value['total_amount'];
            $total_final = $total_amount + $total_final;
            $balance = '0';
            $paid = $total_amount;
            $model->setBalance($balance);
            $model->setPayment_status('Paid');
            $model->setProductRecordId($value['id']);
            $model->setPaid_amount($paid);
            $model->UpdatePaidAndBalance();
        }
        $modelPayment->setOrderType('Purchase');
        $modelPayment->setPaymentDate($this->_helper->common->formatDbDate(date('d/m/Y')));
        $modelPayment->setPaymentMethodId('');
        $modelPayment->setPayAmount($total_final);
        $modelPayment->setCurrencyId('');
        $modelPayment->setPaymentRef('');
        $modelPayment->setPaymentRemarks('');
        $modelPayment->setPaymentType('payment');
        $modelPayment->setOrderId('');
        $modelPayment->setUpdated(date('Y-m-d H:i:s'));
        $modelPayment->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPayment->setCreated(date('Y-m-d H:i:s'));
        $modelPayment->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPayment->insertPayment();
        $this->view->vendorPurchaseList = Application_Model_VendorPeer::fetchVendorPurchaseOrderById($post['vendor_id']);
    }

}
