<?php


/**
 * InventoryController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class InventoryController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {

        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $actionName = $this->_request->getActionName();

        //Validate Access
        $screenData = $this->model->getScreenData('/' . $controllerName);
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * Show all current stock data
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->currentStock = Application_Model_InventoryPeer::getCurrentStock();
    }

    /*
     * Index Action
     * User can adjust stock from here
     * @return
     */

    public function adjustStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $modelMovement = new Application_Model_Movement();
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Adjuststock();

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                $product_id = $post['name'];

                $model->setProduct_id($product_id);
                $model->setLocation($post['location']);
                $model->setNewQuantity($post['newqty']);
                $model->setOldQuantity($post['oldqty']);
                $model->setDiff($post['diff']);
                $model->setInvDate(date('Y-m-d'));
                $model->setStatus('Completed');
                $model->setRemarks($post['remarks']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->saveAdjustStock();
                
                $adjust_id = $model->getId();
                $quantity = Application_Model_InventoryPeer::getProductStockByLocation($post['location'], $product_id);
                $qtyBefore = $quantity['quantity'];
                $qtyAfter = $post['newqty'];
                $modelMovement->setLocationId($post['location']);
                $modelMovement->setProductId($product_id);
                $modelMovement->setQty('');
                $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Adjust Stock');
                $modelMovement->setOrderId($adjust_id);
                $modelMovement->saveInsert();
                $this->_helper->flashMessenger()->addMessage('Stock has been added successfully', 'success');
                $this->_redirect('/inventory/adjust-stock');
            }
            if ($model->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->adjustStock = Application_Model_InventoryPeer::getAdjustStock();
        $this->view->form = $this->_addForm;
    }

    public function updateAdjustStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $this->view->screenName = 'Adjust stock';

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Adjuststock();

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                $product_id = Application_Model_InventoryPeer::fetchProductId($post['name']);
                $model->setProduct_id($product_id);
                $model->setLocation($post['location']);
                $model->setNewQuantity($post['newqty']);
                $model->setOldQuantity($post['oldqty']);
                $model->setDiff($post['diff']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);

                $model->saveAdjustStock();

                $this->_helper->flashMessenger()->addMessage('Stock has been updated successfully', 'success');
                $this->_redirect('/inventory/adjust-stock');
            }
            if ($model->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        //Populate all value on edit mode
        if ($id) {
            $model->setId($id);
            $data = $model->fetchAdjustStockById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $data['product_name'],
                        'newqty' => $data['new_quantity'],
                        'oldqty' => $data['old_quantity'],
                        'diff' => $data['difference'],
                        'location' => $data['inventory_location_id'],
                        'remarks' => $data['remarks'],
                    )
            );
            $this->_addForm->setAction("/inventory/update-adjust-stock/id/" . $id);
            //$this->_addForm->upassword->setRequired(false);
        }

        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i = 0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }

        $this->view->productData = json_encode($productArr);
        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function cancelAdjustStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $id = $this->_request->getParam('id');

        $adjustData = Application_Model_InventoryPeer::getAdjustStockById($id);

        $model->setId($id);
        $model->setNewQuantity($adjustData['new_quantity']);
        $model->setProduct_id($adjustData['inventory_product_id']);
        $model->setLocation($adjustData['inventory_location_id']);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);

        $model->cancelAdjustStock();

        $this->_helper->flashMessenger()->addMessage('Stock has been cancelled successfully', 'success');
        $this->_redirect('/inventory/adjust-stock');
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function cancelReorderStockAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Inventory();
        $id = $this->_request->getParam('id');
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);

        $model->cancelReorderStock();
        exit;
    }

    public function getProductQuantityAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Inventory();
        $product_id = $this->_request->getParam('name');
        $location_id = $this->_request->getParam('location_id');

        $model->setProduct_id($product_id);
        $model->setLocation($location_id);
        $productQty = $model->getProductDetails();

        if (!is_array($productQty)) {
            $PrductData = Array(
                'id' => '',
                'product_id' => $product_id,
                'location_id' => $location_id,
                'quantity' => 0,
                'unit_id' => '',
            );
        } else {
            $PrductData = $productQty;
        }
        echo json_encode($PrductData);
        exit;
    }

    /*
     * Index Action
     * User can adjust stock from here
     * @return
     */

    public function transferStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $modelMovement = new Application_Model_Movement();
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Transferstock();
        $this->view->status = 'Open';

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                //$product_id = Application_Model_InventoryPeer::fetchProductId($post['name']);

                $productQty = Application_Model_InventoryPeer::getProductStockByLocation($post['location'], $post['name']);
                
                if ($productQty < 1) {
                    $this->_helper->flashMessenger()->addMessage('<strong>Alert!</strong> Quantity is not available for this Location', 'error');
                    $this->_redirect('/inventory/transfer-stock');
                }

                if ($post['qty'] > $productQty) {
                    $this->_helper->flashMessenger()->addMessage('<strong>Alert!</strong> Quantity is not available for this Location', 'error');
                    $this->_redirect('/inventory/transfer-stock');
                }
                
                
                
                $model->setProduct_id($post['name']);
                $model->setLocation($post['location']);
                $model->setToLocation($post['tolocation']);
                $model->setNewQuantity($post['qty']);
                $model->setTransferDate(date('Y-m-d'));
                $model->setStatus('Open');
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->insertTransferStock();
                
                $transfer_id = $model->getTransferStockId();
                $quantity = Application_Model_InventoryPeer::getProductStockByLocation($post['location'], $post['name']);
                $qtyBefore = $quantity['quantity'];
                $qtyAfter = $quantity['quantity'] - $post['qty'];
                $qty = $post['qty'];
                $modelMovement->setLocationId($post['location']);
                $modelMovement->setProductId($post['name']);
                $modelMovement->setQty($qty);
                $modelMovement->setMDate($this->_helper->common->formatDbDate($post['transfer_date']));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Transfer Stock');
                $modelMovement->setOrderId($transfer_id);
                $modelMovement->saveInsert();
                
                $quantity1 = Application_Model_InventoryPeer::getProductStockByLocation($post['tolocation'], $post['name']);
                $qtyBefore1 = $quantity1['quantity'];
                $qtyAfter1 = $quantity1['quantity'] + $post['qty'];
                $qty1 = $post['qty'];
                $modelMovement->setLocationId($post['tolocation']);
                $modelMovement->setProductId($post['name']);
                $modelMovement->setQty($qty1);
                $modelMovement->setMDate($this->_helper->common->formatDbDate($post['transfer_date']));
                $modelMovement->setQtyBefore($qtyBefore1);
                $modelMovement->setQtyAfter($qtyAfter1);
                $modelMovement->setType('Transfer Stock');
                $modelMovement->setOrderId($transfer_id);
                $modelMovement->saveInsert();
                
                $this->_helper->flashMessenger()->addMessage('Record has been added successfully', 'success');
                $this->_redirect('/inventory/transfer-stock');
            }
            if ($model->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        if ($status == 'success') {
            $this->view->successMsg = 'Record has been added successfully.';
        }
        if ($status == 'send') {
            $this->view->successMsg = 'Stock has been sent successfully.';
        }
        if ($status == 'cancelled') {
            $this->view->successMsg = 'Record has been cancelled successfully.';
        }
        if ($status == 'received') {
            $this->view->successMsg = 'Stock has been received successfully.';
        }
        if ($status == 'qtyerror') {
            $this->view->errorMsg = '<strong>Alert!</strong> Quantity is not available of this Location';
        }
        if ($status == 'error') {
            $this->view->errorMsg = '<strong>Oops!</strong> There is somthing wrong. Please try again!';
        }

        $this->view->transferStock = Application_Model_InventoryPeer::getTransferStock();
        $this->view->form = $this->_addForm;
    }

    public function sendStockAction() {
        $this->_helper->layout->disableLayout();

        $model = new Application_Model_Inventory();
        $id = $this->_request->getParam('id');

        $trfData = Application_Model_InventoryPeer::getTransferStockById($id);
        $productQty = Application_Model_InventoryPeer::getProductStockByLocation($trfData['from_location_id'], $trfData['inventory_product_id']);

        if ($productQty['quantity'] < 1) {
            $this->_helper->flashMessenger()->addMessage('<strong>Alert!</strong> Quantity is not available for this Location', 'error');
            $this->_redirect('/inventory/transfer-stock');
        }

        if ($trfData['quantity'] > $productQty['quantity']) {
            $this->_helper->flashMessenger()->addMessage('<strong>Alert!</strong> Quantity is not available for this Location', 'error');
            $this->_redirect('/inventory/transfer-stock');
        }

        $model->setTransferStockId($id);
        $transferStockData = $model->getTransferStock();

        $model->setProduct_id($transferStockData['inventory_product_id']);
        $model->setLocation($transferStockData['from_location_id']);
        $model->setToLocation($transferStockData['to_location_id']);
        $model->setNewQuantity($transferStockData['quantity']);
        $model->sendTransferProductQuantity();

        $model->setTransferStockId($id);
        $model->setSendDate(date('Y-m-d'));
        $model->setStatus('In Transit');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->sendStock();

        $this->_helper->flashMessenger()->addMessage('Record has been sent successfully', 'success');
        $this->_redirect('/inventory/transfer-stock');
    }

    public function receivedStockAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Inventory();
        $id = $this->_request->getParam('id');

        $model->setTransferStockId($id);
        $transferStockData = $model->getTransferStock();

        $model->setProduct_id($transferStockData['inventory_product_id']);
        $model->setLocation($transferStockData['from_location_id']);
        $model->setToLocation($transferStockData['to_location_id']);
        $model->setNewQuantity($transferStockData['quantity']);
        $model->setReceivedDate(date('Y-m-d'));
        $model->setStatus('Completed');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->receiveStock();

        $this->_helper->flashMessenger()->addMessage('Record has been received successfully', 'success');
        $this->_redirect('/inventory/transfer-stock');
    }

    public function reorderStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $modelQuote = new Application_Model_SalesQuote();
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_ReorderStock();

        $this->view->productList = Application_Model_ProductPeer::fetchAllProduct();
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        $this->view->reorderStock = Application_Model_InventoryPeer::getReorderStock();
        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
            $post = $request->getPost();
            //var_dump($post);die;
            foreach ($post['item_id'] as $key => $value) {
                $result[$key]['item_id'] = $value;
            }
            foreach ($post['newqty'] as $key => $value) {
                $result[$key]['newqty'] = $value;
            }
            foreach ($post['reorder_point'] as $key => $value) {
                $result[$key]['reorder_point'] = $value;
            }
            foreach ($post['suggested_quantity'] as $key => $value) {
                $result[$key]['suggested_quantity'] = $value;
            }
            foreach ($post['vendor_id'] as $key => $value) {
                $result[$key]['vendor_id'] = $value;
            }
            foreach ($result as $key => $value) {
                $model->setId($value['id']);
                $model->setProduct_id($value['item_id']);
                $model->setQuantityAvailable($value['newqty']);
                $model->setReorderPoint($value['reorder_point']);
                $model->setReorderQuantity($value['suggested_quantity']);
                $model->setVendorId($value['vendor_id']);
                if ($value['status'] == "bill of materials") {
                    $model->setItemStatus("bill of materials");
                } else if ($value['status'] == "out of stock") {
                    $model->setItemStatus("out of stock");
                } else {
                    $model->setItemStatus("Reordered");
                }
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->insertReorderStock();
                $modelQuote->setId($value['item_id']);
                $price = $modelQuote->fetchUnitPriceByProductId();
            }

            $this->_helper->flashMessenger()->addMessage('Item has been Reordered successfully', 'success');
            $this->_redirect('/inventory/reorder-stock');
        }
        if ($model->getId()) {
            $this->view->colStatus = 'N';
        } else {
            $this->view->colStatus = 'Y';
        }

        $this->view->form = $this->_addForm;
    }

    /**
     * 
     * 
     */
    public function updateReorderStockAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Inventory();
        $this->view->screenName = 'Reorder stock';

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_ReorderStock();

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                $model->setProduct_id($post['item_id']);
                $model->setQuantityAvailable($post['newqty']);
                $model->setReorderPoint($post['reorder_point']);
                $model->setReorderQuantity($post['suggested_quantity']);
                $model->setVendorId($post['vendor_id']);
                if ($post['status'] == "bill of materials") {
                    $model->setItemStatus("bill of materials");
                } else {
                    $model->setItemStatus("out of stock");
                }
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->updateReorderStock();

                $this->_helper->flashMessenger()->addMessage('Item has been updated successfully', 'success');
                $this->_redirect('/inventory/reorder-stock');
            }
            if ($model->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        //Populate all value on edit mode
        if ($id) {
            $model->setId($id);
            $data = $model->fetchReorderStockById();
            //var_dump($data);die;
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'item_id' => $data['product_id'],
                        'newqty' => $data['quantity_available'],
                        'reorder_point' => $data['reorder_point'],
                        'suggested_quantity' => $data['reorder_quantity'],
                        'vendor_id' => $data['vendor_id'],
                        'status' => $data['reorder_status'],
                    )
            );
            $this->_addForm->setAction("/inventory/update-reorder-stock/id/" . $id);
            //$this->_addForm->upassword->setRequired(false);
        }

        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i = 0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }

        $this->view->productData = json_encode($productArr);
        $this->view->form = $this->_addForm;
    }

    /**
     * 
     * 
     * 
     */
    public function movementHistoryAction() {
        $this->view->movementData = Application_Model_InventoryPeer::fetchMovementHistory();
    }

    /**
     * 
     * 
     */
    public function getSuggestedProductAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $this->_addForm = new Application_Form_ReorderStock();
        $product = $this->_request->getParam('product');
        $status = $this->_request->getParam('status');
        $model = new Application_Model_Inventory();
        $result = $model->getSuggestedProduct($product);
        $suggestedProduct =  array();
        $suggestedProduct[0]['text'] = "Suggest Products to Reorder";
        $suggestedProduct[0]['value'] = "";
        $suggestedProduct[1]['text'] = "Include Out Of Stock Items";
        $suggestedProduct[1]['value'] = "out of stock";
        $suggestedProduct[2]['text'] = "Include Products assembled with an bill of materials";
        $suggestedProduct[2]['value'] = "bill of materials";
        $this->view->suggestedProducts = $suggestedProduct;
        $this->view->reorderStock = $result;
        $this->view->status = $status;
//        $this->view->productList = Application_Model_ProductPeer::fetchAllProduct();
//        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
//        $this->view->form = $this->_addForm;
        //echo json_encode($result);
        // die;
    }

    public function reorderProductAction() {
        $this->_helper->layout->disableLayout();
        $result = $this->_request->getParam('result');
        $add = $this->_request->getParam('add');
        $model = new Application_Model_Inventory();
        $modelQuote = new Application_Model_SalesQuote();
        $modelPurchase = new Application_Model_Purchase();
        foreach ($result as $key => $value) {
            if ($add == '1') {
                $model->setId('');
                $model->setProduct_id($value['2']);
                $model->setQuantityAvailable('0');
                $model->setReorderPoint('0');
                $model->setReorderQuantity($value['3']);
                $model->setVendorId($value['1']);
                $model->setItemStatus($value['4']);
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->insertReorderStock();
            }
            $modelQuote->setId($value['2']);
            $unit_price = $modelQuote->fetchUnitPriceByProductId();
            $sub_total = $value['3'] * $unit_price['price'];
            $modelPurchase->setId('');
            $modelPurchase->setProductRecordId('');
            $modelPurchase->setProductId($value['2']);
            $modelPurchase->setVendor_product_code('');
            $modelPurchase->setQuantity($value['3']);
            $modelPurchase->setUnitPrice($unit_price['price']);
            $modelPurchase->setDiscount('');
            $modelPurchase->setProductSubTotal($sub_total);
            $modelPurchase->setProductStatus('Order');
            $modelPurchase->setTransactionDate('');
            $modelPurchase->setDiscount('');
            $modelPurchase->setSessionid(session_id());
            $modelPurchase->setUpdated(date('Y-m-d H:i:s'));
            $modelPurchase->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $modelPurchase->setCreated(date('Y-m-d H:i:s'));
            $modelPurchase->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $modelPurchase->saveProduct();
            $vendor_id['vendor_id'] = $value['1'];
        }
        echo json_encode($vendor_id);
        die;
    }

    /**
     * 
     * 
     */
    public function getReorderProductQuantityAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Inventory();
        $product_id = $this->_request->getParam('product_id');
        $model->setProduct_id($product_id);
        $quantity['quantity'] = $model->getReorderProductQuantity();
        echo json_encode($quantity);
        die;
    }

}
