<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class ReceivingAddressController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelReceivingAddress = new Application_Model_ReceivingAddress();
        $modelPeer = new Application_Model_ReceivingAddressPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_ReceivingAddress();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }
        $state = $modelPeer->fetchAllStates();
        $Data = array();
        $Data[''] = 'Please Select';
        foreach ($state as $key => $val) {
            $Data[$val['id']] = $val['state_name'];
        }

        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }

        $address_type = array('' => 'Please Select',
            'Residential' => 'Residential',
            'Commercial' => 'Commercial');


        $this->_addForm->address_type->setMultiOptions($address_type);
        $this->_addForm->state->setMultiOptions($Data);
        $this->_addForm->country->setMultiOptions($statusData);

        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelReceivingAddress->setCityName($post['city_name']);
                $modelReceivingAddress->setStateId($post['state']);
                $modelReceivingAddress->setCountryId($post['country']);
                $exist = $modelReceivingAddress->checkCityExistence();
                if ($exist) {
                    $modelReceivingAddress->setStreetName($post['street']);
                    $modelReceivingAddress->setCityId($exist[0]['id']);
                    $modelReceivingAddress->setRemarks($post['remarks']);
                    $modelReceivingAddress->setZipCode($post['zip_code']);
                    $modelReceivingAddress->setAddressType($post['address_type']);
                    $modelReceivingAddress->setReceivingAddressName($post['address_name']);
                    $modelReceivingAddress->setUpdated(date('Y-m-d H:i:s'));
                    $modelReceivingAddress->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $modelReceivingAddress->setCreated(date('Y-m-d H:i:s'));
                    $modelReceivingAddress->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $modelReceivingAddress->save();
                    $this->_redirect('/receiving-address?status=success');
                } else {
                    $this->_redirect('receiving-address?status=addCity');
                }
            }
            if ($modelReceivingAddress->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        if ($status == 'success') {
            $this->view->successMsg = 'Receiving Address has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'Receiving Address has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'Receiving Address has been deleted successfully.';
        }
        if ($status == 'addCity') {
            $this->view->successMsg = 'Please add a City First';
        }

        $this->view->details = $modelPeer->fetchAllReceivingAddress();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelReceivingAddress = new Application_Model_ReceivingAddress();
        $modelPeer = new Application_Model_ReceivingAddressPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_ReceivingAddress();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $state = $modelPeer->fetchAllStates();
        $Data = array();
        $Data[''] = 'Please Select';
        foreach ($state as $key => $val) {
            $Data[$val['id']] = $val['state_name'];
        }
        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }

        $address_type = array('' => 'Please Select',
            'Residential' => 'Residential',
            'Commercial' => 'Commercial');


        $this->_addForm->address_type->setMultiOptions($address_type);
        $this->_addForm->state->setMultiOptions($Data);
        $this->_addForm->country->setMultiOptions($statusData);
        //Populate all value on edit mode
        if ($id) {
            $modelReceivingAddress->setId($id);
            $ReceivingAddressData = $modelReceivingAddress->fetchReceivingAddressById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'address_name' => $ReceivingAddressData['address_name'],
                        'remarks' => $ReceivingAddressData['remarks'],
                        'zip_code' => $ReceivingAddressData['zip_code'],
                        'street' => $ReceivingAddressData['street'],
                        'address_type' => $ReceivingAddressData['address_type'],
                        'city_name' => $ReceivingAddressData['city_name'],
                        'state' => $ReceivingAddressData['state_id'],
                        'country' => $ReceivingAddressData['country_id']
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Receiving Address");
            $this->_addForm->setAction("/receiving-address/edit/id/" . $id);
            $this->view->form = $this->_addForm;
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelReceivingAddress->setCityName($post['city_name']);
                $modelReceivingAddress->setStateId($post['state']);
                $modelReceivingAddress->setCountryId($post['country']);
                $exist = $modelReceivingAddress->checkCityExistence();
                if ($exist) {
                    $modelReceivingAddress->setId($post['id']);
                    $modelReceivingAddress->setStreetName($post['street']);
                    $modelReceivingAddress->setCityId($exist[0]['id']);
                    $modelReceivingAddress->setRemarks($post['remarks']);
                    $modelReceivingAddress->setZipCode($post['zip_code']);
                    $modelReceivingAddress->setAddressType($post['address_type']);
                    $modelReceivingAddress->setReceivingAddressName($post['address_name']);
                    $modelReceivingAddress->setUpdated(date('Y-m-d H:i:s'));
                    $modelReceivingAddress->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $modelReceivingAddress->setCreated(date('Y-m-d H:i:s'));
                    $modelReceivingAddress->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                    $modelReceivingAddress->save();
                    $this->_redirect('/receiving-address?status=success');
                } else {
                    $this->_redirect('receiving-address?status=addCity');
                }
            }
        }
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_ReceivingAddress();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_receiving_addresses');
        exit;
    }

    /*
     * get State Action
     * use to fetch all states from user table
     * @return
     */

    public function getStateAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_ReceivingAddress();
        $modelPeer = new Application_Model_ReceivingAddressPeer();
        $id = $this->_request->getParam('id');
        $model->setId($id);
        $states = $model->getStatesByCountryId();
        $this->view->stateData = $states;
    }

    /*
     * get Country Action
     * use to fetch all countries from user table
     * @return
     */

    public function getCountryAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_ReceivingAddress();
        $modelPeer = new Application_Model_ReceivingAddressPeer();
        $id = $this->_request->getParam('id');
        if ($id) {
            $model->setId($id);
            $country = $model->getCountryByCountryId();
            $this->view->countryData = $country;
        } else {
            $country = $modelPeer->fetchAllCountries();
            $this->view->flag = '1';
            $this->view->countryData = $country;
        }
    }

    /*
     * get State Country Action
     * use to fetch all states and countries by city name from user table
     * @return
     */

    public function getStateCountryAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_ReceivingAddress();
        $modelPeer = new Application_Model_ReceivingAddressPeer();
        $name = $this->_request->getParam('name');
        $model->setCityName($name);
        $statecountry = $model->getStateCountryByCityName();
        echo json_encode($statecountry[0]);
        die;
    }

}
