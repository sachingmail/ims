<?php

/**
 * CustomerController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CustomerController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $action = $this->_request->getActionName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelPeer = new Application_Model_CustomerPeer();

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');

        if ($status == 'success') {
            $this->view->successMsg = 'User has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'User has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'Roles has been deleted successfully.';
        }

        $this->view->customerData = $modelPeer->fetchAllCustomersList();
    }

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
        
        $screenData = $this->model->getScreenData('/customer/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger()->addMessage('You are not Authorized to access this section', 'error');
            $this->_redirect('/customer');
        }
        
        $model = new Application_Model_Customer();
        $modelPeer = new Application_Model_CustomerPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Customer();

        //Populate all value on edit mode
        if ($id) {
            $model->setId($id);
            $customerData = $model->fetchCustomerById();
            $this->view->is_active = $customerData['is_active'];
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $customerData['name'],
                        'abn_no' => $customerData['abn_no'],
                        'balance' => $customerData['balance'],
                        'credit' => $customerData['credit'],
                        'address_type' => $customerData['address_type'],
                        'address' => $customerData['address'],
                        'address1' => $customerData['address1'],
                        'address2' => $customerData['address2'],
                        'contact_name' => $customerData['contact_name'],
                        'des' => $customerData['designation'],
                        'phoneno' => $customerData['contact_phone'],
                        'fax' => $customerData['contact_fax'],
                        'email' => $customerData['contact_email'],
                        'website' => $customerData['contact_website'],
                        'pricing_currency' => $customerData['pricing_currency_id'],
                        'discount' => $customerData['discount'],
                        'payment_terms' => $customerData['payment_terms'],
                        'taxing_scheme' => $customerData['taxing_schemes'],
                        'tax_exempt' => $customerData['tax_exempt'],
                        'location' => $customerData['default_location_id'],
                        'sales_reps' => $customerData['sales_reps'],
                        'carrier' => $customerData['carrier_id'],
                        'payment_method' => $customerData['payment_method'],
                        'card_type' => $customerData['card_type'],
                        'card_no' => $customerData['card_number'],
                        'expiration_date' => $customerData['card_expiry'],
                        'card_security_code' => $customerData['card_security_code'],
                        'is_active' => $customerData['is_active'],
                        'remarks' => $customerData['remarks'],
                    )
            );

            $this->_addForm->submitbtn->setLabel("Update Customer");
            $this->_addForm->setAction("/customer/add/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $model->setId($post['id']);
                $model->setName($post['name']);
                $model->setAbnNo($post['abn_no']);
                $model->setBalance($post['balance']);
                $model->setCredit($post['credit']);
                $model->setAddress_type($post['address_type']);
                $model->setAddress($post['address']);
                $model->setAddress1($post['address1']);
                $model->setAddress2($post['address2']);
                $model->setPerson_name($post['contact_name']);
                $model->setDesignation($post['des']);
                $model->setPhone($post['phoneno']);
                $model->setFax($post['fax']);
                $model->setEmail($post['email']);
                $model->setWebsite($post['website']);
                $model->setPricing_currency($post['pricing_currency']);
                $model->setDiscount($post['discount']);
                $model->setPayment_term($post['payment_terms']);
                $model->setTax_scheme($post['taxing_scheme']);
                $model->setTax_exempt($post['tax_exempt']);
                $model->setLocation($post['location']);
                $model->setSales_reps($post['sales_reps']);
                $model->setCarrier($post['carrier']);
                $model->setPayment_method($post['payment_method']);
                $model->setCard_type($post['card_type']);
                $model->setCard_no($post['card_no']);
                $model->setExpiry_date($post['expiration_date']);
                $model->setSecurity_code($post['card_security_code']);
                $model->setIs_active(1);
                $model->setRemarks('');
                $model->setUpdated(date('Y-m-d H:i:s'));
                $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->setCreated(date('Y-m-d H:i:s'));
                $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $model->save();

                if ($post['id'] == '') {
                    $this->_redirect('/customer?status=success');
                } else {
                    $this->_redirect('/customer?status=updated');
                }
            }
        }
        $this->view->ordersData = $modelPeer->fetchCustomerOrders($id);
        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Users();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_customers');
        exit;
    }
    /**
     * 
     * 
     */
    public function deactivateCustomerAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Customer();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->DeactivateCustomer();die;
    }
    /**
     * 
     * 
     */
    public function activateCustomerAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Customer();
        $model->setId($id);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->ActivateCustomer();die;
    }
}
