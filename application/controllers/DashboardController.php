<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class DashboardController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $from = date("d-m-Y", strtotime("-6 months"));
        $to = date('d-m-Y');
        $this->redirect('/dashboard/dashboard-chart/line/Sales Completed/from/' . $from . '/to/' . $to . '/group/Month');
    }

    /**
     * 
     * 
     */
    public function dashboardChartAction() {
        $modelPeer = new Application_Model_DashboardPeer();
        $this->_helper->layout->setLayout('layout-chart');
        $lines = $this->_request->getParam('line');
        $from = $this->_request->getParam('from');
        $to = $this->_request->getParam('to');
        $group = $this->_request->getParam('group');
        $lines_option[0]['text'] = "Please Select";
        $lines_option[0]['value'] = "";
        $lines_option[1]['text'] = "Sales Completed";
        $lines_option[1]['value'] = "Sales Completed";
        $lines_option[2]['text'] = "Sales Profit";
        $lines_option[2]['value'] = "Sales Profit";
        $lines_option[3]['text'] = "Sales Orders";
        $lines_option[3]['value'] = "Sales Orders";
        $lines_option[4]['text'] = "Purchase Orders";
        $lines_option[4]['value'] = "Purchase Orders";
        $lines_option[5]['text'] = "Customer Payments Due";
        $lines_option[5]['value'] = "Customer Payments";
        $lines_option[6]['text'] = "Vendor Payments Due";
        $lines_option[6]['value'] = "Vendor Payments";

        $group_by[0]['text'] = "Please Select";
        $group_by[0]['value'] = "";
        $group_by[1]['text'] = "Days";
        $group_by[1]['value'] = "Day";
        $group_by[2]['text'] = "Weeks";
        $group_by[2]['value'] = "Week";
        $group_by[3]['text'] = "Months";
        $group_by[3]['value'] = "Month";
        $group_by[4]['text'] = "Years";
        $group_by[4]['value'] = "Year";

        $this->view->group_by = $group_by;
        $this->view->group = $group;
        $this->view->topList = $modelPeer->fetchTopList();
        $this->view->outstanding = $modelPeer->fetchOutstandingList();
        $this->view->line_selected = $lines;
        $this->view->from_selected = $from;
        $this->view->to_selected = $to;
        $this->view->lines_option = $lines_option;
        $this->view->lines = $lines;
        $this->view->from = $from;
        $this->view->to = $to;
        $modelPeer = new Application_Model_DashboardPeer();
    }

    /**
     * 
     * 
     */
    public function getDataAction() {
        $this->_helper->layout->disableLayout();
        $modelPeer = new Application_Model_DashboardPeer();
        $lines = $this->_request->getParam('line');
        $from = $this->_request->getParam('from');
        $to = $this->_request->getParam('to');
        $group = $this->_request->getParam('group');
        if ($from != '') {
            $from_db = date('Y/m/d', strtotime($from));
        } else {
            $from_db = $from;
        }
        if ($to != '') {
            $to_db = date('Y/m/d', strtotime($to));
        } else {
            $to_db = $to;
        }
        /**
         * 
         * Sales Completed
         */
        if ($lines == "Sales Completed") {
            $result = array();
            $data = $modelPeer->fetchSalesCompleted($group, $from_db, $to_db);
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                $result[$key]['total'] = $value['total_amount'];
                $result[$key]['total_name'] = "Sales Completed";
                $result[$key]['date'] = $value['order_date'];
            }
            echo json_encode($result);
            die;
        }
        /**
         * Sales Orders
         */ else if ($lines == "Sales Orders") {
            $data = $modelPeer->fetchSalesOrders($group, $from_db, $to_db);
            $result = array();
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                $result[$key]['total'] = $value['total_amount'];
                $result[$key]['total_name'] = "Sales Orders";
                $result[$key]['date'] = $value['order_date'];
            }
            echo json_encode($result);
            die;
        }
        /**
         * Purchase Orders
         */ else if ($lines == "Purchase Orders") {
            $data = $modelPeer->fetchPurchaseOrders($group, $from_db, $to_db);
            $result = array();
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                $result[$key]['total'] = $value['total_amount'];
                $result[$key]['total_name'] = "Purchase Orders";
                $result[$key]['date'] = $value['order_date'];
            }
            echo json_encode($result);
            die;
        }
        /**
         * sales profit
         */ else if ($lines == "Sales Profit") {
            $data = $modelPeer->getSalesOrderProfit($group, $from_db, $to_db);
            $result = array();
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                //$result[$key]['total'] = $value['profit'] < 0 ? '('.- $value['profit'] .')':$value['profit'];
                $result[$key]['total'] = $value['profit'];
                $result[$key]['total_name'] = "Sales Profit";
                $result[$key]['date'] = $value['order_date'];
            }
            echo json_encode($result);
            die;
        } else if ($lines == "Customer Payments") {
            $data = $modelPeer->getCustomerPayments($group, $from_db, $to_db);
            $result = array();
            $total = 0;
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                $result[$key]['total'] = $value['total_amount'];
                $result[$key]['total_name'] = "Customer Payments";
                $result[$key]['date'] = $value['order_date'];
            }
//            $customer_payment = $data[0]['total_remaining'] - $data[0]['total_paid'];
//            $final_payment = $customer_payment < 0 ? "(" . -$customer_payment . ")" : $customer_payment;
//            //$result[0]['total'] = number_format($final_payment,0,'.', '');
//            $result = array();
//            $result[0]['total'] = $final_payment;
//            $result[0]['total_name'] = " Payment Due";
//            $result[0]['date'] = $this->_getDate($data, $from, $to);
            echo json_encode($result);
            die;
        } else if ($lines == "Vendor Payments") {
            $data = $modelPeer->getVendorPayments($group, $from_db, $to_db);
            $result = array();
            $total = 0;
            if ($group == "") {
                $group = "Month";
            }
            foreach ($data as $key => $value) {
                $result[$key]['total'] = $value['total_amount'];
                $result[$key]['total_name'] = "Vendor Payments";
                $result[$key]['date'] = $value['order_date'];
            }
//            $vendor_payment = $data[0]['total_remaining'] - $data[0]['total_paid'];
//            $final_payment = $vendor_payment < 0 ? "(" . -$vendor_payment . ")" : $vendor_payment;
//            //$result[0]['total'] = number_format($final_payment,0,'.', '');
//            $result[0]['total'] = $final_payment;
//            $result[0]['total_name'] = " Payment Due";
//            $result[0]['date'] = $this->_getDate($data, $from, $to);
            echo json_encode($result);
            die;
        } else {
            $data[0]['total_name'] = '0';
            $result[0]['total'] = '0';
            echo json_encode($result);
            die;
        }
    }

    private function _getDate($data, $from, $to) {
        $date = '';
        if ($from && $to == '') {
            $date = $from;
        }
        if ($to && $from == '') {
            $date = $to;
        }
        if ($from != '' && $to != '') {
            $date = $from . ' ' . 'To' . ' ' . $to;
        } else {
            if (count($date) > 0) {
                $date = $data[0]['from'] . ' ' . 'To' . ' ' . $data[0]['to'];
            } else {
                $date = '';
            }
        }
        return $date;
    }

    public function getTopBottomListAction() {
        $this->_helper->layout->disableLayout();
        $modelPeer = new Application_Model_DashboardPeer();
        $request = $this->getRequest();
        $post = $request->getPost();
        $top_bottom_option = $post['top_bottom_option'];
        $top_or_bottom = $post['top_bottom'];
        $top_bottom_limit = $post['top_bottom_limit'];
        $date_from_top_bottom = $post['date_from_top_bottom'];
        $date_to_top_bottom = $post['date_to_top_bottom'];
        if ($date_from_top_bottom) {
            $date_from = $this->_helper->common->formatDbDate($date_from_top_bottom);
        } else {
            $date_from = "";
        }
        if ($date_to_top_bottom) {
            $date_to = $this->_helper->common->formatDbDate($date_to_top_bottom);
        } else {
            $date_to = "";
        }
        if ($post['top_bottom_option'] == '1') {
            $result = $modelPeer->fetchSalesOrdersWithHighestSales($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/sales-order/edit/id/".$value['id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Sales Order With Highest Sales";
            $this->view->head = "Order Id";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '2') {
            $result = $modelPeer->fetchSalesOrdersWithHighestNonInvoicedTotal($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/sales-order/edit/id/".$value['id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Sales Order With Highest Non-Invoiced Total";
            $this->view->head = "Order Id";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '3') {
            $result = $modelPeer->fetchCustomerWithHighestSales($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/customer/add/id/".$value['customer_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Customer With Highest Sales";
            $this->view->head = "Customer Name";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '4') {
            $result = $modelPeer->fetchProductWithHighestQuantityPurchased($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/product/add/id/".$value['product_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Product With Highest Quantity Purchased";
            $this->view->head = "Product Name";
            $this->view->second_head = "Quantity";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '5') {
            $result = $modelPeer->fetchProductWithHighestQuantitySold($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/product/add/id/".$value['product_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Product With Highest Quantity Sold";
            $this->view->head = "Product Name";
            $this->view->second_head = "Quantity";
            $this->view->orders = $result;
        }
//        else if($post['top_bottom_option'] == '6') {
//            
//        }
        else if ($post['top_bottom_option'] == '7') {
            $result = $modelPeer->fetchProductWithHighestValueSold($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/product/add/id/".$value['product_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Product With Highest Value Sold";
            $this->view->head = "Product Name";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '8') {
            $result = $modelPeer->fetchProductWithHighestValuePurchased($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/product/add/id/".$value['product_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Product With Highest Value Purchased";
            $this->view->head = "Product Name";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        } else if ($post['top_bottom_option'] == '9') {
            $result = $modelPeer->fetchProductWithHighestInventoryValue($top_or_bottom, $top_bottom_limit, $date_from, $date_to);
            foreach ($result as $k => $v) {
                $tArray[$k] = $v['total_amount'];
              }
              $max_value = max($tArray);
              if($max_value == 0) {
                  $max_value = 1;
              }
            foreach($result as $key=>$value) {
                $result[$key]['url'] = "/product/add/id/".$value['product_id'];
                $percentage = ($value['total_amount']/$max_value) * 100;
                 $result[$key]['percentage'] = "width : $percentage% ";
            }
            if (count($result) == 0) {
                $result[0]['name'] = "No Data";
                $result[0]['total_amount'] = "No Data";
            }
            if ($top_bottom_limit == "" || $top_bottom_limit == "0" || $top_bottom_limit < 0) {
                $top_bottom_limit = "5";
            }
            $this->view->list = $top_or_bottom . " " . $top_bottom_limit . " " . "Product With Highest Inventory Value ";
            $this->view->head = "Product Name";
            $this->view->second_head = "Amount";
            $this->view->orders = $result;
        }
//        else {
//            
//        }
    }

    public function displaySummaryAction() {
        $this->_helper->layout->disableLayout();
        $modelPeer = new Application_Model_DashboardPeer();
        $this->view->outstanding = $modelPeer->fetchOutstandingList();
    }

    public function displayDetailsAction() {
        $this->_helper->layout->disableLayout();
        $modelPeer = new Application_Model_DashboardPeer();
        $salesDetails = $modelPeer->getSalesDetails();
        $purchaseDetails = $modelPeer->getPurchaseDetails();
        $reOrders = $modelPeer->fetchReorderDetails();
        $workOrders = $modelPeer->getWorkOrderDetails();
        $this->view->salesDetails = $salesDetails;
        $this->view->purchaseDetails = $purchaseDetails;
        $this->view->reOrder = $reOrders;
        $this->view->workorders = $workOrders;
    }

}
