<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class ProductCategoryController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelProduct = new Application_Model_ProductCategory();
        $modelPeer = new Application_Model_ProductCategoryPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_ProductCategory();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();
        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $parent = $modelPeer->fetchAllProductCategories();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($parent as $key => $val) {
            $statusData[$val['id']] = $val['name'];
        }

        $this->_addForm->parent->setMultiOptions($statusData);
        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelProduct->setId($post['id']);
                $modelProduct->setProductName($post['name']);
                $modelProduct->setParentId($post['parent']);
                $modelProduct->setUpdated(date('Y-m-d H:i:s'));
                $modelProduct->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelProduct->setCreated(date('Y-m-d H:i:s'));
                $modelProduct->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelProduct->save();

                $this->_helper->flashMessenger()->addMessage('Product Category has been added successfully', 'success');
                $this->_redirect('/product-category');
            }
            if ($modelProduct->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelProduct = new Application_Model_ProductCategory();
        $modelPeer = new Application_Model_ProductCategoryPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_ProductCategory();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }
        $modelProduct->setId($id);
        $parent = $modelProduct->fetchProductCategoryEdit();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($parent as $key => $val) {
            $statusData[$val['id']] = $val['name'];
        }
        $this->_addForm->parent->setMultiOptions($statusData);
        //Populate all value on edit mode
        if ($id) {
            $modelProduct->setId($id);
            $ProductData = $modelProduct->fetchProductById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $ProductData['name'],
                        'parent' => $ProductData['parent_id']
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Product Category");
            $this->_addForm->setAction("/product-category/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelProduct->setId($post['id']);
                $modelProduct->setProductName($post['name']);
                $modelProduct->setParentId($post['parent']);
                $modelProduct->setUpdated(date('Y-m-d H:i:s'));
                $modelProduct->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelProduct->save();

                $this->_helper->flashMessenger()->addMessage('Product Category has been updated successfully', 'success');
                $this->_redirect('/product-category');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_ProductCategory();
        $model->setId($id);
        $result = $model->checkIfProductCategoryIsPresentInRecord();
        if (count($result) > 0) {
            $this->_helper->flashMessenger()->addMessage('Product Category cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/product-category');
        } else {
            $this->view->usersData = $model->deleteData('mas_product_category');
            $this->_helper->flashMessenger()->addMessage('Product Category has been deleted successfully', 'success');
            $this->_redirect('/product-category');
        }
    }

    /*
     * Data Action
     * This action is used to fetch all product parents from the user table
     * @return
     */

    public function dataAction() {
        $modelPeer = new Application_Model_ProductCategoryPeer();
        $data = $modelPeer->fetchAllProductParents();
        echo json_encode($data);
        die;
    }

    /*
     * Get product Action
     * used to fetch product price by id
     * @return
     */

    public function getProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('row');
        $model = new Application_Model_ProductCategory();
        $model->setId($id);
        $data = $model->fetchProductNamePrice();
        $this->view->productData = $data;
    }

}
