<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class PaymentMethodController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelPayment = new Application_Model_PaymentMethod();
        $modelPeer = new Application_Model_PaymentMethodPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_PaymentMethod();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelPayment->setId($post['id']);
                $modelPayment->setPaymentName($post['name']);
                $modelPayment->setUpdated(date('Y-m-d H:i:s'));
                $modelPayment->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelPayment->setCreated(date('Y-m-d H:i:s'));
                $modelPayment->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelPayment->save();
                $this->_helper->flashMessenger()->addMessage('Payment Method has been added successfully', 'success');
                $this->_redirect('/payment-method');
            }
            if ($modelPayment->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }
        $this->view->details = $modelPeer->fetchAllPayments();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelPayment = new Application_Model_PaymentMethod();
        $modelPeer = new Application_Model_PaymentMethodPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_PaymentMethod();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelPayment->setId($id);
            $data = $modelPayment->fetchPaymentById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'name' => $data['name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Payment");
            $this->_addForm->setAction("/payment-method/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelPayment->setId($post['id']);
                $modelPayment->setPaymentName($post['name']);
                $modelPayment->setUpdated(date('Y-m-d H:i:s'));
                $modelPayment->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelPayment->save();
                $this->_helper->flashMessenger()->addMessage('Payment Method has been updated successfully', 'success');
                $this->_redirect('/payment-method');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_PaymentMethod();
        $model->setId($id);
        $result = $model->checkIfMethodIsPresentInRecords();
        if (count($result['customers']) > 0 || count($result['payments']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Payment Method cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/payment-method');
        } else {
            $this->view->usersData = $model->deleteData('mas_payment_method');
            $this->_helper->flashMessenger()->addMessage('Payment Method has been deleted successfully', 'success');
            $this->_redirect('/payment-method');
        }
    }
}
