<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginController extends Zend_Controller_Action {

    public function init() {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
        $this->_helper->layout->setLayout('/authlayout');
        
    }

    public function indexAction() {
        $this->form = new Application_Form_Signin();

        $this->view->form = $this->form;
        if ($this->getRequest()->isPost()) {
            if ($this->form->isValid($this->getRequest()->getPost())) {
                $this->_forward('auth-process');
            }
        }
    }

    public function authProcessAction() {
        $request = $this->getRequest();
        $params = $request->getPost();

        if (($params['loginname']) && ($params['loginpassword'])) {
            if ($this->_process($params)) {
                // We're authenticated! Redirect to the home page
                $this->_redirect('/');
            } else {
                $this->_helper->flashMessenger()->addMessage('Please enter valid Username or Password,', 'error');
                $this->_redirect('/login');
            }
        } else {
            $this->_helper->flashMessenger()->addMessage('Please enter all required fields,', 'error');
            $this->_redirect('/login');
        }
    }

    private function _process($user = array()) {
//        var_dump($user);die;
        // Get our authentication adapter and check credentials
        $adapter = $this->getAuthAdapter();
        $adapter->setIdentity($user['loginname']);
        $adapter->setCredential($user['loginpassword']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();
            $auth->getStorage()->write($user);
            if(Zend_Auth::getInstance()->getStorage()->read()->is_active == 1) {
                return true;
            }
            else {
                Zend_Auth::getInstance()->clearIdentity();
               return false; 
            }
        }
        return false;
    }

    protected function getAuthAdapter() {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('MD5(?)');
        return $authAdapter;
    }
    
    /**
     * forgetPassword Action
     * this action used to show the forget password page.
     * 
     * @return string
     */
    public function forgetPasswordAction()
    {
        //check user loged in or not
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }
        
        $model = new Application_Model_Users();

        $forgetPasswordForm = new Application_Form_Users_ForgetPassword(array(
            'action' => 'forget-password',
            'method' => 'post',
        ));

        $request = $this->getRequest();

        if ($request->isPost()) {
            if ($forgetPasswordForm->isValidPartial($request->getPost())) {                
                $loginFormValues = $request->getParams();
                $email = $loginFormValues['email'];
                $userData= Application_Model_UsersPeer::validateEmail($email);
                if ($userData) {
                   $mailSubject = 'Reset password of Bluechip Engineering - IMS';
                        
                   $config = Zend_Registry::get('config');
                   $domainUrl = $config->domain->host.'/login/reset-password/jSession/'.md5($userData['email']).'/IMS_validate/'.strtotime(date('Y-m-d H:i:s'));
                   //echo $domainUrl;exit;
                   $HtmlBody = 'Hi ' . $userData['name'] . ' , <br/>';
                   $HtmlBody.='<p>Thanks for contact  Blue Chip Engineering. Please click on reset password link</p>';
                   $HtmlBody.='<p><a href='.$domainUrl.'><strong>Reset Password</strong></a>' . '</p><br >';
                   $HtmlBody.='<p>This link will active for 30 min only.</p><br>';
                   $HtmlBody.='Thanks ' . '<br />' . 'Bluechip Engineering Team';  
                   $this->_helper->mail->sendMail($userData['name'],$userData['email'], $mailSubject, $HtmlBody);
                  
                  $this->_helper->flashMessenger()->addMessage('Reset password link has been sent to your email id', 'success');
                  $this->_redirect('/login');
                } else {
                     $this->_helper->flashMessenger()->addMessage('Please enter currect and valid email', 'error');
                        $this->_redirect('/login/forget-password');
                }
            }
        }
        $this->view->forgetPasswordForm = $forgetPasswordForm;
    }
    
    /** this action has been used for change password
     *
     * @return void
     */
    public function resetPasswordAction()
    {
        $userModel = new Application_Model_Users();
    	$email = $this->_request->getParam('jSession');
    	$timestamp = $this->_request->getParam('IMS_validate');
    	if (!$email) {
                $this->_helper->flashMessenger()->addMessage('Invalid Email address, please contact to admin', 'error');
    		$this->_redirect('/login');
    	}
    	
    	//Validate time expire or not
    	$current_time = strtotime(date('Y-m-d H:i:s'));
    	$diff = ($current_time - $timestamp) / 60;
    	if ($diff > 30) {
                $this->_helper->flashMessenger()->addMessage('You link has been exired or invalid access. Please go to forget password section again.', 'error');
    		$this->_redirect('/login');
    	}
    	
    	//Validate Email
    	$userData = Application_Model_UsersPeer::validateMd5Email($email);
    	
    	if(!$userData) {
    		$this->_helper->flashMessenger()->addMessage('You ssssssssssslink has been exired or invalid access. Please go to forget password section again.', 'error');
    		$this->_redirect('/login');
    	}
    	
    	
    	/*
    	* set up the chagne password In form
    	*/
    	$request = $this->getRequest();
    	$signin_message = '';
    
    	$uid =$userData['id'];
    	$form = new Application_Form_Users_ResetPassword(array(
    			'action' => '/login/reset-password/jSession/'.$email.'/IMS_validate/'.$timestamp,
    			'method' => 'post',
    	));
    	if ($request->isPost()) {
    		if ($form->isValidPartial($request->getPost())) {
    			$formValues = $request->getParams();

                        $userModel->setId($uid);
                        $userModel->setPassword(md5($formValues['password']));
                        $userModel->updatePassword();
                        
                        $this->_helper->flashMessenger()->addMessage('New Password has been updated successfully', 'success');
                        $this->_redirect('/');
    			
    		}
    	}

    	$this->view->form = $form;
    }

}
