<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class UnitController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        
        $controllerName = $this->_request->getControllerName();       
        $screenData=$this->model->getScreenData('/'.$controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName=$screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelUnit = new Application_Model_Unit();
        $modelPeer = new Application_Model_UnitPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Unit();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();
//        var_dump($roles);die;

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
             $status ='';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelUnit->setId($post['id']);
                $modelUnit->setUnitName($post['unit_name']);
                $modelUnit->setUpdated(date('Y-m-d H:i:s'));
                $modelUnit->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUnit->setCreated(date('Y-m-d H:i:s'));
                $modelUnit->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUnit->save();
                $this->_helper->flashMessenger()->addMessage('Unit has been added successfully', 'success');
                $this->_redirect('/unit');
            }
            if ($modelUnit->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }
        $this->view->details = $modelPeer->fetchAllUnits();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelUnit = new Application_Model_Unit();
        $modelPeer = new Application_Model_UnitPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_Unit();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelUnit->setId($id);
            $data = $modelUnit->fetchUnitById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'unit_name' => $data['unit_name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Unit");
            $this->_addForm->setAction("/unit/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelUnit->setId($post['id']);
                $modelUnit->setUnitName($post['unit_name']);
                $modelUnit->setUpdated(date('Y-m-d H:i:s'));
                $modelUnit->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUnit->save();

                $this->_helper->flashMessenger()->addMessage('Unit has been updated successfully', 'success');
                $this->_redirect('/unit');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Unit();
        $model->setId($id);
        $result = $model->checkIfUnitIsPresentInRecords();
        if(count($result) > 0) {
            $this->_helper->flashMessenger()->addMessage('Unit cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/unit');
        }
        else {
        $this->view->usersData = $model->deleteData('mas_units');
        $this->_helper->flashMessenger()->addMessage('Unit has been deleted successfully', 'success');
        $this->_redirect('/unit');
        }   
    }
}
