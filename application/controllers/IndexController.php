<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class IndexController extends DMC_Controller_Abstract {

    protected $model;

    public function init() {
        parent::init();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }
        
        $this->view->controllerName = $this->_request->getControllerName();
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $status = $this->_request->getParam('status');
        if ($status) {
            $this->_redirect('/login?status=' . $status);
        }
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/login');
        }
        $this->_helper->layout->setLayout('layout-home');
        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        $this->view->homeMenuData = $this->model->getHomeMenu();
    }
    
    /**
    * Logout Action
    *
    * This action is used for logout the application and destroy all the sessions
    * redirect to login page
    * 
    * @return void
    */
    
    public function logoutAction() {
        
        Zend_Auth::getInstance()->clearIdentity();
        // Redirect to login page
        $this->_redirect('/login?status=logout');
        
        
    }
}
