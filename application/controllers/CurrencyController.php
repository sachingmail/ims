<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CurrencyController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }

        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCurrency = new Application_Model_Currency();
        $modelPeer = new Application_Model_CurrencyPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Currency();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCurrency->setId($post['id']);
                $modelCurrency->setCurrency_name($post['currency_name']);
                $modelCurrency->setCurrency_code($post['currency_code']);
                $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
                $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->setCreated(date('Y-m-d H:i:s'));
                $modelCurrency->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->save();

                $this->_helper->flashMessenger()->addMessage('Currency has been added successfully', 'success');
                $this->_redirect('/currency');
            }
            if ($modelCurrency->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllCurrency();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCurrency = new Application_Model_Currency();
        $modelPeer = new Application_Model_CurrencyPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_Currency();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelCurrency->setId($id);
            $CurrencyData = $modelCurrency->fetchCurrencyById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'currency_name' => $CurrencyData['currency_name'],
                        'currency_code' => $CurrencyData['currency_code'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Currency");
            $this->_addForm->setAction("/currency/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCurrency->setId($post['id']);
                $modelCurrency->setCurrency_name($post['currency_name']);
                $modelCurrency->setCurrency_code($post['currency_code']);
                $modelCurrency->setUpdated(date('Y-m-d H:i:s'));
                $modelCurrency->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCurrency->save();

                $this->_helper->flashMessenger()->addMessage('Currency has been updated successfully', 'success');
                $this->_redirect('/currency');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Currency();
        $model->setId($id);
        $result = $model->checkIfCurrencyIsPresentInRecords();
        if (count($result['vendors']) > 0 || count($result['purchase']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Currency cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/currency');
        } else {
            $this->view->usersData = $model->deleteData('mas_currency');
            $this->_helper->flashMessenger()->addMessage('Currency has been deleted successfully', 'success');
            $this->_redirect('/currency');
        }
    }

}
