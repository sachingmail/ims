<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CarrierController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCarrier = new Application_Model_Carrier();
        $modelPeer = new Application_Model_CarrierPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Carrier();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCarrier->setId($post['id']);
                $modelCarrier->setCarrierName($post['carrier_name']);
                $modelCarrier->setUpdated(date('Y-m-d H:i:s'));
                $modelCarrier->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCarrier->setCreated(date('Y-m-d H:i:s'));
                $modelCarrier->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCarrier->save();

                $this->_helper->flashMessenger()->addMessage('Carrier has been added successfully', 'success');
                $this->_redirect('/carrier');
            }
            if ($modelCarrier->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllCarriers();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCarrier = new Application_Model_Carrier();
        $modelPeer = new Application_Model_CarrierPeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_Carrier();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelCarrier->setId($id);
            $data = $modelCarrier->fetchCarrierById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'carrier_name' => $data['carrier_name'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Carrier");
            $this->_addForm->setAction("/carrier/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCarrier->setId($post['id']);
                $modelCarrier->setCarrierName($post['carrier_name']);
                $modelCarrier->setUpdated(date('Y-m-d H:i:s'));
                $modelCarrier->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCarrier->save();

                $this->_helper->flashMessenger()->addMessage('Carrier has been updated successfully', 'success');
                $this->_redirect('/carrier');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Carrier();
        $model->setId($id);
        $result = $model->checkIfCarrierIsPresentInRecords();
        if (count($result['customer']) > 0 || count($result['purchase']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Carrier cannot be deleted as it has been used in some records', 'error');
            $this->_redirect('/carrier');
        } else {
            $this->view->usersData = $model->deleteData('mas_carriers');
            $this->_helper->flashMessenger()->addMessage('Carrier has been deleted successfully', 'success');
            $this->_redirect('/carrier');
        }
    }

}
