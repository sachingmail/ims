<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class TaxingSchemeController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelTaxing = new Application_Model_TaxingScheme();
        $modelPeer = new Application_Model_TaxingSchemePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_TaxingScheme();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelTaxing->setTaxingSchemeName($post['tax_scheme_name']);
                $modelTaxing->setTaxName($post['tax_name']);
                $modelTaxing->setTaxRate($post['tax_rate']);
                $modelTaxing->setTaxOnShipping($post['tax_on_shipping']);
                $modelTaxing->setSecondaryTaxName($post['secondary_tax_name']);
                $modelTaxing->setSecondaryTaxRate($post['secondary_tax_rate']);
                $modelTaxing->setShowSecondaryTaxName($post['show_secondary_tax_name']);
                $modelTaxing->setSecondaryTaxOnShipping($post['secondary_tax_on_shipping']);
                $modelTaxing->setCompoundSecondary($post['compound_secondary']);
                $modelTaxing->setUpdated(date('Y-m-d H:i:s'));
                $modelTaxing->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTaxing->setCreated(date('Y-m-d H:i:s'));
                $modelTaxing->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTaxing->save();

                $this->_helper->flashMessenger()->addMessage('Taxing Scheme has been added successfully', 'success');
                $this->_redirect('/taxing-scheme');
            }
            if ($modelTaxing->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }
        $this->view->details = $modelPeer->fetchAllTaxingSchemes();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelTaxing = new Application_Model_TaxingScheme();
        $modelPeer = new Application_Model_TaxingSchemePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_TaxingScheme();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelTaxing->setId($id);
            $TaxingData = $modelTaxing->fetchTaxinSchemeById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'tax_scheme_name' => $TaxingData['tax_scheme_name'],
                        'tax_on_shipping' => $TaxingData['tax_on_shipping'],
                        'show_secondary_tax_name' => $TaxingData['show_secondary_tax_rate'],
                        'secondary_tax_on_shipping' => $TaxingData['secondary_tax_on_shipping'],
                        'compound_secondary' => $TaxingData['compound_secondary'],
                        'tax_name' => $TaxingData['tax_name'],
                        'tax_rate' => $TaxingData['tax_rate'],
                        'secondary_tax_name' => $TaxingData['secondary_tax_name'],
                        'secondary_tax_rate' => $TaxingData['secondary_tax_rate'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Taxing Scheme");
            $this->_addForm->setAction("/taxing-scheme/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();

                $modelTaxing->setTaxingSchemeName($post['tax_scheme_name']);
                $modelTaxing->setTaxName($post['tax_name']);
                $modelTaxing->setTaxRate($post['tax_rate']);
                $modelTaxing->setTaxOnShipping($post['tax_on_shipping']);
                $modelTaxing->setSecondaryTaxName($post['secondary_tax_name']);
                $modelTaxing->setSecondaryTaxRate($post['secondary_tax_rate']);
                $modelTaxing->setSecondaryTaxOnShipping($post['secondary_tax_on_shipping']);
                $modelTaxing->setCompoundSecondary($post['compound_secondary']);
                $modelTaxing->setShowSecondaryTaxName($post['show_secondary_tax_name']);
                $modelTaxing->setUpdated(date('Y-m-d H:i:s'));
                $modelTaxing->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTaxing->setCreated(date('Y-m-d H:i:s'));
                $modelTaxing->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTaxing->save();
                $this->_helper->flashMessenger()->addMessage('Taxing Scheme has been updated successfully', 'success');
                $this->_redirect('/taxing-scheme');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_TaxingScheme();
        $model->setId($id);
        $result = $model->checkTaxingScehemePresentInRecords();
        if (count($result['sales_quote']) > 0 || count($result['vendors']) > 0 || count($result['customers']) > 0 || count($result['purchase']) > 0 || count($result['sales_order']) > 0) {
            $this->_helper->flashMessenger()->addMessage('Taxing Scheme cannot be deleted as it has been used by some records', 'error');
            $this->_redirect('/taxing-scheme');
        } else {
            $this->view->usersData = $model->deleteData('mas_taxing_schemes');
            $this->_helper->flashMessenger()->addMessage('Taxing Scheme has been deleted successfully', 'success');
            $this->_redirect('/taxing-scheme');
        }
    }

    /*
     * Calculation Primary Action
     * Calculate subtotal,total,tax1,tax2
     * @return
     */

    public function calculationPrimaryAction() {
        $this->_helper->layout->disableLayout();
        $secondary = $this->_request->getParam('secondary');
        if ($secondary == '1') {
            $taxonshipping = $this->_request->getParam('taxonshipping');
            $sectaxonshipping = $this->_request->getParam('sectaxonshipping');
            if ($taxonshipping == '0') {
                if ($sectaxonshipping == '0') {
                    $subtotal = $this->_request->getParam('subtotal');
                    $freight = $this->_request->getParam('freight');
                    $tax1 = $this->_request->getParam('tax1');
                    $tax2 = $this->_request->getParam('tax2');
                    $tax_1 = (($tax1 / 100) * $subtotal);
                    $tax_2 = (($tax2 / 100) * $subtotal);
                    $total = $tax_1 + $tax_2 + $subtotal + $freight;
                    $result['total'] = $total;
                    $result['tax1'] = $tax_1;
                    $result['tax2'] = $tax_2;
                    echo json_encode($result);
                    die;
                } else {
                    $subtotal = $this->_request->getParam('subtotal');
                    $freight = $this->_request->getParam('freight');
                    $tax1 = $this->_request->getParam('tax1');
                    $tax2 = $this->_request->getParam('tax2');
                    $net = $subtotal + $freight;
                    $tax_1 = ($tax1 / 100) * $subtotal;
                    $tax_2 = ($tax2 / 100) * $net;
                    $total = $net + $tax_1 + $tax_2;
                    $result['total'] = $total;
                    $result['tax1'] = $tax_1;
                    $result['tax2'] = $tax_2;
                    echo json_encode($result);
                    die;
                }
            } else {
                if ($sectaxonshipping == '1') {
                    $subtotal = $this->_request->getParam('subtotal');
                    $freight = $this->_request->getParam('freight');
                    $tax1 = $this->_request->getParam('tax1');
                    $tax2 = $this->_request->getParam('tax2');
                    $net = $subtotal + $freight;
                    $tax_1 = ($tax1 / 100) * $net;
                    $tax_2 = ($tax2 / 100) * $net;
                    $total = $net + $tax_1 + $tax_2;
                    $result['total'] = $total;
                    $result['tax1'] = $tax_1;
                    $result['tax2'] = $tax_2;
                    echo json_encode($result);
                    die;
                } else {
                    $subtotal = $this->_request->getParam('subtotal');
                    $freight = $this->_request->getParam('freight');
                    $tax1 = $this->_request->getParam('tax1');
                    $tax2 = $this->_request->getParam('tax2');
                    $net = $subtotal + $freight;
                    $tax_1 = ($tax1 / 100) * $net;
                    $tax_2 = ($tax2 / 100) * $subtotal;
                    $total = $net + $tax_1 + $tax_2;
                    $result['total'] = $total;
                    $result['tax1'] = $tax_1;
                    $result['tax2'] = $tax_2;
                    echo json_encode($result);
                    die;
                }
            }
        } else {
            $compound_secondary = $this->_request->getParam('compound_secondary');
            $taxonshipping = $this->_request->getParam('taxonshipping');
            $sectaxonshipping = $this->_request->getParam('sectaxonshipping');
            $subtotal = $this->_request->getParam('subtotal');
            $freight = $this->_request->getParam('freight');
            $tax1 = $this->_request->getParam('tax1');
            $tax2 = $this->_request->getParam('tax2');
            if ($compound_secondary == '1') {
                if ($taxonshipping == '1') {
                    if ($sectaxonshipping == '1') {
                        $net = $subtotal + $freight;
                        $tax_1 = ($tax1 / 100) * $net;
                        $net1 = $tax_1 + $net;
                        $tax_2 = ($tax2 / 100) * $net1;
                        $total = $net1 + $tax_2;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    } else {
                        $net = $subtotal + $freight;
                        $tax_1 = ($tax1 / 100) * $net;
                        $net1 = $subtotal + $tax_1;
                        $tax_2 = ($tax2 / 100) * $net1;
                        $total = $net1 + $tax_2 + $freight;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    }
                } else {
                    if ($sectaxonshipping == '1') {
                        $net = $subtotal;
                        $tax_1 = ($tax1 / 100) * $net;
                        $net1 = $tax_1 + $net + $freight;
                        $tax_2 = ($tax2 / 100) * $net1;
                        $total = $net1 + $tax_2;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    } else {
                        $net = $subtotal;
                        $tax_1 = ($tax1 / 100) * $net;
                        $net1 = $subtotal + $tax_1;
                        $tax_2 = ($tax2 / 100) * $net1;
                        $total = $net1 + $tax_2 + $freight;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    }
                }
            } else {
                if ($taxonshipping == '1') {
                    if ($sectaxonshipping == '1') {
                        $net = $subtotal + $freight;
                        $tax_1 = ($tax1 / 100) * $net;
                        $tax_2 = ($tax2) / 100 * $net;
                        $total = $net + $tax_1 + $tax_2;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    } else {
                        $net = $subtotal + $freight;
                        $tax_1 = ($tax1 / 100) * $net;
                        $total = $net + $tax_1;
                        echo json_encode($total);
                        die;
                    }
                } else {
                    if ($sectaxonshipping == '1') {
                        $net = $subtotal + $freight;
                        $tax_1 = ($tax1 / 100) * $subtotal;
                        $tax_2 = ($tax2) / 100 * $net;
                        $total = $net + $tax_2 + $tax_1;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    } else {
                        $tax_1 = ($tax1 / 100) * $subtotal;
                        $tax_2 = ($tax2 / 100) * $subtotal;
                        $total = $subtotal + $freight + $tax_1 + $tax_2;
                        $result['total'] = $total;
                        $result['tax1'] = $tax_1;
                        $result['tax2'] = $tax_2;
                        echo json_encode($result);
                        die;
                    }
                }
            }
        }
    }

    /*
     * Calculation Secondary Action
     * Calculate subtotal,total,tax1,tax2
     * @return
     */

    public function calculationSecondaryAction() {
        $this->_helper->layout->disableLayout();
        $subtotal = $this->_request->getParam('subtotal');
        $freight = $this->_request->getParam('freight');
        $tax1 = $this->_request->getParam('tax1');
        $tax2 = $this->_request->getParam('tax2');
        $tax_1 = ($tax1 / 100) * $subtotal;
        $tax_2 = ($tax2 / 100) * $subtotal;
        $total = $tax_1 + $tax_2 + $subtotal + $freight;
        $result['total'] = $total;
        $result['tax1'] = $tax_1;
        $result['tax2'] = $tax_2;
        echo json_encode($result);
        die;
    }

}
