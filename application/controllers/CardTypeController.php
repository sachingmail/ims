<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CardTypeController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCard = new Application_Model_CardType();
        $modelPeer = new Application_Model_CardTypePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_CardType();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCard->setId($post['id']);
                $modelCard->setCardName($post['card_type']);
                $modelCard->setUpdated(date('Y-m-d H:i:s'));
                $modelCard->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->setCreated(date('Y-m-d H:i:s'));
                $modelCard->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->save();

                $this->_helper->flashMessenger()->addMessage('Card has been added successfully', 'success');
                $this->_redirect('/card-type');
            }
            if ($modelCard->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->details = $modelPeer->fetchAllCards();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCard = new Application_Model_CardType();
        $modelPeer = new Application_Model_CardTypePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_CardType();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        //Populate all value on edit mode
        if ($id) {
            $modelCard->setId($id);
            $data = $modelCard->fetchCardById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'card_type' => $data['card_type'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Card");
            $this->_addForm->setAction("/card-type/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCard->setId($post['id']);
                $modelCard->setCardName($post['card_type']);
                $modelCard->setUpdated(date('Y-m-d H:i:s'));
                $modelCard->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCard->save();

                $this->_helper->flashMessenger()->addMessage('Card has been updated successfully', 'success');
                $this->_redirect('/card-type');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_CardType();
        $model->setId($id);
        $result = $model->checkIfCardTypeIsPresentInRecords();
        if (count($result) > 0) {
            $this->_helper->flashMessenger()->addMessage('Card Type cannot be deletd as it has been used in some records', 'error');
            $this->_redirect('/card-type');
        } else {
            $this->view->usersData = $model->deleteData('mas_card_type');
            $this->_helper->flashMessenger()->addMessage('Card has been deleted successfully', 'success');
            $this->_redirect('/card-type');
        }
    }

}
