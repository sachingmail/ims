<?php

/**
 * VendorController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class PurchaseOrderController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $actionName = $this->_request->getActionName();
        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');
        $this->view->purchaseData = Application_Model_PurchasePeer::fetchAllPurchaseOrder();
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function unreceivedOrdersAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->purchaseData = Application_Model_PurchasePeer::fetchAllOrderByStatus('Unfulfilled', '', '');
    }

    public function unpaidOrdersAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->purchaseData = Application_Model_PurchasePeer::fetchAllOrderByStatus('', 'Unpaid', '');
    }

    public function recentOrdersAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->purchaseData = Application_Model_PurchasePeer::fetchAllOrderByStatus('', '', '15');
    }

    public function addAction() {
        $this->_helper->layout->setLayout('layout');
        
        $screenData = $this->model->getScreenData('/purchase-order/add');
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
             $this->_helper->flashMessenger()->addMessage('<strong>Alert !</strong> You are not Authorized to access this section', 'error');
            $this->_redirect('/purchase-order');
        }
        
        $this->view->screenHeading = $screenData['screen_name'];
        
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');
        $vendor = $this->_request->getParam('vendor');
        if ($vendor) {
            $this->view->vendor_url = $vendor;
        }
        $this->view->id = $id;
        $copy = $this->_request->getParam('copy');
        if ($copy != '') {
            $model->setId($copy);
            $result = $model->fetchPurchaseOrder();
            $this->view->vendor_url = $result['vendor_id'];
        }
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $post = $this->_request->getPost();

            $model->setId($post['id']);
            $model->setPoId($post['order_id']);
            $model->setOrderDate($this->_helper->common->formatDbDate($post['order_date']));
            $model->setOrderStatus('Unfulfilled');
            $model->setPaymentStatus('Unpaid');
            $model->setVendorId($post['vendor_id']);
            $model->setVendorContact($post['vendor_contact']);
            $model->setVendorPhone($post['phone']);
            $model->setAddressType($post['address_type']);
            $model->setAddress($post['address']);
            $model->setAddress1($post['address1']);
            $model->setAddress2($post['address2']);
            $model->setVendorOrderNo($post['v_order']);
            $model->setAddressType($post['address_type']);
            $model->setShipReq($post['ship_req']);
            $model->setLocation($post['location_id']);
            $model->setRegShipDate($this->_helper->common->formatDbDate($post['ship_date']));
            $model->setPaymentTerms($post['terms']);
            $model->setSippingAddress($post['shipping_address']);
            $model->setCarrierId($post['carrier']);
            $model->setDueDate($this->_helper->common->formatDbDate($post['due_date']));
            $model->setNonVendorCost($post['non_vendor_cost']);
            $model->setCurrency($post['currency_id']);
            $model->setNetSubTotal($post['sub_total_products']);
            $model->setFreight((isset($post['freight'])) ? $post['freight'] : '');
            $model->setTaxScheme($post['taxing_scheme']);
            $model->setTax1((isset($post['tax1_name'])) ? $post['tax1_name'] : '');
            $model->setTaxper1((isset($post['tax1_per'])) ? $post['tax1_per'] : '');
            $model->setTaxAmount1((isset($post['tax2'])) ? $post['tax2'] : '');
            $model->setTax2((isset($post['tax2_name'])) ? $post['tax2_name'] : '');
            $model->setTaxper2((isset($post['tax2_per'])) ? $post['tax2_per'] : '');
            $model->setTaxAmount2((isset($post['tax2'])) ? $post['tax2'] : '');
            $model->setNetAmount($post['total_amount']);
            $model->setPaidAmount($post['paid_amount']);
            $model->setBalanceAmount($post['balance']);
            $model->setRemarks($post['remarks']);
            $model->setReceiveRemark($post['receive_remark']);
            $model->setReturnRemark($post['ret_remarks']);
            $model->setUnstockRemark($post['unstock_remark']);
            $model->setReturnSubtotal($post['ret_net_sub_total']);
            $model->setReturnFee($post['ret_fee']);
            $model->setReturnTotal($post['ret_total']);
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->save();

            $last_id = $model->getId();
            $newOrderId = "PO-" . $last_id;
            $model->setPoId($newOrderId);
            $model->updatePurchaseOrderId();
            if ($post['id'] == '') {
                $model->setSessionid(session_id());
                $model->updatePurchaseOrder();
                $products_det = $modelPeer->fetchPurchaseProducts($model->getId());
                foreach($products_det as $key=>$value) {
                    $modelPeer->updateLastVendorId($value['product_id'],$post['vendor_id']);
                }
                $this->_helper->flashMessenger()->addMessage('Purchase order has been created successfully', 'success');
                $this->_redirect('/purchase-order/edit/id/' . $model->getId());
            } else {
                
                $this->updateVendorbalance($post['vendor_id']);
                $products_det = $modelPeer->fetchPurchaseProducts($model->getId());
                foreach($products_det as $key => $value) {
                    $modelPeer->updateLastVendorId($value['product_id'],$post['vendor_id']);
                }
                $this->_helper->flashMessenger()->addMessage('Purchase order has been updated successfully', 'success');
                $this->_redirect('/purchase-order/edit/id/' . $model->getId());
            }
        }
        $this->view->productData = Application_Model_ProductPeer::fetchAllProduct();
        $this->view->vendors = Application_Model_VendorPeer::fetchAllActiveVendors();
        $this->view->terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->taxingSchemes = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
        $this->view->carrier = Application_Model_CarrierPeer::fetchAllCarriers();

        if ($id == null || $id < 1) {
            $this->view->orderMasData = array();
            $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder('', session_id());
            $this->view->productList = $modelPeer->fetchPurchaseProductsBySession(session_id());
            $subTotal = Application_Model_PurchasePeer::fetchSubTotalBySession(session_id());

            //$subTotal['sub_total'] - $subTotal['ret_sub_total']$this->view->subTotal = (float)$subTotal['sub_total'] - (float)$subTotal['ret_sub_total'];
            $this->view->subTotal = number_format($subTotal['sub_total'] - $subTotal['ret_sub_total'], 2, '.', '');
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProductsBySession(session_id());
            $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $model->setId($id);
            $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder($id, '');
            $this->view->orderMasData = $model->fetchPurchaseOrder();
            $this->view->productList = $modelPeer->fetchPurchaseProducts($id);
            $subTotal = Application_Model_PurchasePeer::fetchSubTotalById($id);
            $this->view->subTotal = number_format($subTotal['sub_total'] - $subTotal['ret_sub_total'], 2, '.', '');
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProducts($id);
            $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalById($id);
            $this->view->unstockOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($id, 'Unstock', 'U');
        }
    }

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $product = Application_Model_ProductPeer::fetchAllProduct();

        $this->view->productData = $product;
        $this->view->vendors = Application_Model_VendorPeer::fetchAllActiveVendors();
        $this->view->terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $this->view->taxingSchemes = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
        $this->view->carrier = Application_Model_CarrierPeer::fetchAllCarriers();

        if ($id) {
            $model->setId($id);
            $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder($id, '');
            $this->view->orderMasData = $model->fetchPurchaseOrder();
            $this->view->productList = $modelPeer->fetchPurchaseProducts($id);
            $subTotal = Application_Model_PurchasePeer::fetchSubTotalById($id);
            $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
            $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($id, 'Receive', 'O');
            $this->view->orderQty = Application_Model_PurchasePeer::countQty($id, 'Order');
            $this->view->receiveQty = Application_Model_PurchasePeer::countQty($id, 'Receive');
            $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalById($id);
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProducts($id);
            $this->view->unstockOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($id, 'Unstock', 'U');
        }
    }
    
    public function printOrderAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder($id, '');
        
        $purchaseOrder=$model->fetchPurchaseOrderForPrint();
        $this->view->orderMasData = $purchaseOrder;
        $this->view->productList = $modelPeer->fetchPurchaseProducts($id);
        $subTotal = Application_Model_PurchasePeer::fetchSubTotalById($id);
        $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
        
        
        $scheme_id = $purchaseOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $purchaseOrder['ship_req'];
        $this->view->subtotal = $purchaseOrder['sub_total'];
        $this->view->freight = $purchaseOrder['freight'];

        $taxData = Array(
            'tax_name' => $purchaseOrder['tax1_name'],
            'tax_rate' => $purchaseOrder['tax1_per'],
            'tax_on_shipping' => $purchaseOrder['tax_on_shipping'],
            'secondary_tax_name' => $purchaseOrder['tax2_name'],
            'secondary_tax_rate' => $purchaseOrder['tax2_per'],
            'secondary_tax_on_shipping' => $purchaseOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $purchaseOrder['compound_secondary'],
        );
        
        $paid = $model->getPaidPayment($id);
        $this->view->paid = $paid;
        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    public function printInvoicePurchaseAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $model->setId($id);
        $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder($id, '');
        
        $purchaseOrder=$model->fetchPurchaseOrderForPrint();
        $this->view->orderMasData = $purchaseOrder;
        $this->view->productList = $modelPeer->fetchPurchaseProducts($id);
        $subTotal = Application_Model_PurchasePeer::fetchSubTotalById($id);
        $this->view->subTotal = $subTotal['sub_total'] - $subTotal['ret_sub_total'];
        $this->view->balance = $purchaseOrder['balance'];
        
        $scheme_id = $purchaseOrder['taxing_scheme_id'];
               
        $this->view->order_id = $id;
        $this->view->ship_req = $purchaseOrder['ship_req'];
        $this->view->subtotal = $purchaseOrder['sub_total'];
        $this->view->freight = $purchaseOrder['freight'];

        $taxData = Array(
            'tax_name' => $purchaseOrder['tax1_name'],
            'tax_rate' => $purchaseOrder['tax1_per'],
            'tax_on_shipping' => $purchaseOrder['tax_on_shipping'],
            'secondary_tax_name' => $purchaseOrder['tax2_name'],
            'secondary_tax_rate' => $purchaseOrder['tax2_per'],
            'secondary_tax_on_shipping' => $purchaseOrder['secondary_tax_on_shipping'],
            'compound_secondary' => $purchaseOrder['compound_secondary'],
        );
        
        $paid = $model->getPaidPayment($id);
        $this->view->paid = $paid;
        $this->view->schemeData = $taxData;
        
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    public function printReceivePurchaseAction() {
        $this->_helper->layout->setLayout('print');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $model->setId($id);
        $purchaseOrder=$model->fetchPurchaseOrderForPrint();
        $this->view->orderMasData = $purchaseOrder;
       // $this->view->recProductData = Application_Model_ProductPeer::fetchAllProductInOrder($id, '');
        $product_list = $modelPeer->fetchPurchaseOrderByStatus($id,"Receive");
        $quantity = 0;
        foreach($product_list as $key => $value) {
            $quantity = $quantity + $value['quantity'];
        }
        $this->view->total_quantity = $quantity;
        $this->view->productList = $product_list;
        $this->view->order_id = $id; 
        $this->view->companyInfo = Application_Model_SettingPeer::fetchCompanyDetails();
       
    }
    
    public function addProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $param = $request->getParams();

        $po_id = $param['id'];

        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setVendor_product_code($param['vendor_pcode']);
        $model->setQuantity($param['quantity']);
        $model->setUnitPrice($param['unit_price']);
        $model->setDiscount($param['discount']);
        $model->setProductSubTotal($param['sub_total']);
        $model->setProductStatus('Order');
        $model->setTransactionDate('');
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();
        
        $modelVendor = new Application_Model_Vendor();

        $modelVendor->setId($param['vendor_id']);
        $modelVendor->setProductCode($param['product_id']);
        $modelVendor->setVendor_product_code($param['vendor_pcode']);
        $modelVendor->setProduct_cost($param['unit_price']);
        $modelVendor->setSessionid('');
        $modelVendor->setUpdated(date('Y-m-d H:i:s'));
        $modelVendor->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelVendor->setCreated(date('Y-m-d H:i:s'));
        $modelVendor->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);

        $validateVendorProduct = $modelVendor->checkVendorProduct();
        if($validateVendorProduct == 0) {
            $modelVendor->insertProduct();
        }


        if ($po_id == null || $po_id < 1) {
            $this->view->productList = $modelPeer->fetchPurchaseProductsBySession(session_id());
        } else {
            $model->setOrderStatus('Unfulfilled');
            $model->setId($po_id);
            $model->orderStatusUpdate();
            
            $model->updateTotalPayment();
            $this->view->productList = $modelPeer->fetchPurchaseProducts($po_id);
        }
    }

    public function refreshOrderProductsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $this->view->productList = Application_Model_PurchasePeer::fetchPurchaseProducts($id);
    }

    public function deleteProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $po_id = $this->_request->getParam('po_id');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelInv = new Application_Model_Inventory();
        
        $productList = Application_Model_PurchasePeer::getProductByRecordId($id);
        
        $model->setId($id);
        $model->deleteData('tra_purchase_order_det');
        
        if(count($productList)>0){
            //manage Inventory
            foreach ($productList as $products) {
                $modelInv->setLocation($products['location_id']);
                $modelInv->setProduct_id($products['product_id']);
                //Update or insert Quantity in Inventory
                $validateQty = $modelInv->validateProductInventory();
                if ($validateQty > 0) {
                    $productInventory = $modelInv->getProductDetails();
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                    $modelInv->setNewQuantity($newQuantity);
                    $modelInv->updateProductQuantity();
                } else {
                    $modelInv->setNewQuantity($products['quantity']);
                    $modelInv->insertProductQuantity();
                }
            }

            $model->setParant_item_id($id);
            $model->deleteReceived();
        }
        
        
        if ($po_id == null || $po_id < 1) {
            $this->view->productList = $modelPeer->fetchPurchaseProductsBySession(session_id());
        } else {
            $model->setOrderStatus('Unfulfilled');
            $model->setId($po_id);
            $model->orderStatusUpdate();
            
            $model->setPaymentStatus('Unpaid');
            $model->statusUpdate();
            
            $this->view->productList = $modelPeer->fetchPurchaseProducts($po_id);
        }
    }

     public function refreshReceiveProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
        $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($param['order_id'], 'Receive');
    }
    
    public function editProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Purchase();
        $model->setProductRecordId($id);
        $productData = $model->getProductById();
        echo json_encode($productData);
        exit;
    }

    public function addReceiveProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelMovement = new Application_Model_Movement();
        $request = $this->getRequest();
        $param = $request->getParams();
        $po_id = $param['id'];

        $model->setProductId($param['product_id']);
        $model->setProductStatus('Order');
        $model->setId($param['id']);
        $productQuantity = $model->getProductByProductIdAndStatus();
        if ((int) $param['quantity'] > (int) $productQuantity) {
            echo 'qtyError';
            exit;
        }

        $model->setProductStatus('Receive');
        $model->setProductRecordId($param['product_record_id']);
        $receivedQuantity = $model->getReceivedProductQty();

        if ((int) $productQuantity < (int) ($receivedQuantity + $param['quantity'])) {
            echo 'recQtyExistError';
            exit;
        }
        
        $po_order_id = $modelPeer->fetchPoIdByOrderId($po_id);
        $sub_total = $modelPeer->fetchProductSubTotal($po_id);
        $quantity = $modelPeer->fetchQuantity($param['product_id'], $param['location_id']);
        foreach ($quantity as $key => $value) {
            $qtyBefore = $value['quantity'];
            $qty = $param['quantity'];
            $qtyAfter = $value['quantity'] + $param['quantity'];
            $modelMovement->setLocationId($param['location_id']);
            $modelMovement->setProductId($param['product_id']);
            $modelMovement->setQty($qty);
            $modelMovement->setMDate($this->_helper->common->formatDbDate($param['receive_date']));
            $modelMovement->setQtyBefore($qtyBefore);
            $modelMovement->setQtyAfter($qtyAfter);
            $modelMovement->setType('Purchase Order');
            $modelMovement->setOrderId($po_order_id);
            $modelMovement->setTotal_amount($sub_total);
            $modelMovement->saveInsert();
        }
        
        $this->updateProdcutAvgCost($param['product_id']);
        
        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setVendor_product_code($param['vendor_pcode']);
        $model->setQuantity($param['quantity']);
        $model->setTransactionDate($this->_helper->common->formatDbDate($param['receive_date']));
        $model->setReceiveLocationId($param['location_id']);
        $model->setProductStatus('Receive');
        $model->setUnitPrice(0.00);
        $model->setDiscount(0.00);
        $model->setProductSubTotal(0.00);
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();
        

        if ($po_id == null || $po_id < 1) {
            $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($po_id, 'Receive');
        } else {
            $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($po_id, 'Receive');
        }
    }

   
    
    public function countQuantityAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $orderQty = Application_Model_PurchasePeer::countQty($id, 'Order');
        $receiveQty = Application_Model_PurchasePeer::countQty($id, 'Receive');
        $status = Application_Model_PurchasePeer::getOrderStatus($id, 'Receive');
        $tempArr = array(
            'order_qty' => $orderQty,
            'receive_qty' => $receiveQty,
            'order_status' => $status['order_status'],
            'pay_status' => $status['payment_status']
        );

        echo json_encode($tempArr);
        exit;
    }

    public function deleteReceiveProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $po_id = $this->_request->getParam('po_id');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $modelInv = new Application_Model_Inventory();

        $products = $modelPeer->fetchPurchaseProductsByRecordId($id);

        $modelInv->setLocation($products['location_id']);
        $modelInv->setProduct_id($products['product_id']);
        //Update or insert Quantity in Inventory
        $validateQty = $modelInv->validateProductInventory();
        if ($validateQty > 0) {
            $productInventory = $modelInv->getProductDetails();
            $newQuantity = $productInventory['quantity'] - $products['quantity'];
            $modelInv->setNewQuantity($newQuantity);
            $modelInv->updateProductQuantity();
        } else {
            $newQuantity = $products['quantity'];
            $modelInv->setNewQuantity($newQuantity);
            $modelInv->insertProductQuantity();
        }



        $model->setId($id);
        $model->deleteData('tra_purchase_order_det');

        $model->setId($po_id);
        $model->setOrderStatus('Unfulfilled');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->orderStatusUpdate();

        $this->updateProdcutAvgCost($products['product_id']);
                
        $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($po_id, 'Receive');
    }

    public function calcTaxAction() {
        $this->_helper->layout->disableLayout();
        $scheme_id = $this->_request->getParam('scheme_id');
        $sub_total = $this->_request->getParam('sub_total');
        $ship_req = $this->_request->getParam('ship_req');
        $order_id = $this->_request->getParam('id');
        $freight = $this->_request->getParam('freight');
        $this->view->order_id = $order_id;
        $model = new Application_Model_Purchase();
        if ($order_id) {
            $model->setId($order_id);
            $orderData = $model->fetchPurchaseOrder();
            $this->view->ship_req = $orderData['ship_req'];
            $this->view->subtotal = $sub_total;
            $this->view->freight = $orderData['freight'];
             $this->view->freight = ($orderData['freight']) ?$orderData['freight']: $this->_request->getParam('freight');

            $taxData = Array(
                'tax_name' => $orderData['tax1_name'],
                'tax_rate' => $orderData['tax1_per'],
                'tax_on_shipping' => $orderData['tax_on_shipping'],
                'secondary_tax_name' => $orderData['tax2_name'],
                'secondary_tax_rate' => $orderData['tax2_per'],
                'secondary_tax_on_shipping' => $orderData['secondary_tax_on_shipping'],
                'compound_secondary' => $orderData['compound_secondary'],
            );
            if ($order_id == null || $order_id < 1) {
                $paid = Application_Model_SalesOrderPeer::getPaidAmountBySessionId(session_id());
            } else {
                $paid = $model->getPaidPayment($order_id);
            }

            $this->view->paid = $paid;
            
            $this->view->schemeData = $taxData;
        } else {
            $this->view->ship_req = $ship_req;
            $this->view->subtotal = $this->_request->getParam('sub_total');
            $this->view->freight = ($ship_req == 1) ?$this->_request->getParam('freight') : '0';
            $this->view->paid = '0.00';
            $this->view->schemeData = Application_Model_TaxingSchemePeer::getTaxingSchemeById($scheme_id);
        }
       
    }

    public function getVendorDetailsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $vendorData = Application_Model_VendorPeer::fetchVendorDetails($id);

        echo json_encode($vendorData);
        exit;
    }

    public function getProductDetailsAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Purchase();
        $productId = $this->_request->getParam('product_id');
        $vendorId = $this->_request->getParam('vendor_id');
        $locationId = $this->_request->getParam('location_id');

        $modelInv = new Application_Model_Inventory();
        $modelInv->setProduct_id($productId);
        $modelInv->setLocation($locationId);
        $availQty = $modelInv->getProductDetails();

        $priceData = Application_Model_PurchasePeer::fetchProductAndVendorPrice($productId, $vendorId);
        $price = ($priceData['vendor_price'] < 1) ? $priceData['product_price'] : $priceData['vendor_price'];
        $pData = array(
            'price' => $price,
            'vendor_product_code' => $priceData['vendor_product_code'],
            'avail_qty' => $availQty['quantity'],
        );
        echo json_encode($pData);
        exit;
    }

    public function getSubTotalAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $po_id = $param['po_id'];

        if ($po_id == null || $po_id < 1) {
            $subTotal = Application_Model_PurchasePeer::fetchSubTotalBySession(session_id());
        } else {
            $subTotal = Application_Model_PurchasePeer::fetchSubTotalById($po_id);
        }
        $netSubTotal = number_format($subTotal['sub_total'] - $subTotal['ret_sub_total'], 2, '.', '');
        $subTotalArr = array(
            'sub_total' => $netSubTotal,
            'ret_sub_total' => number_format($subTotal['ret_sub_total'], 2, '.', ''),
        );

        echo json_encode($subTotalArr);
        exit;
    }

    public function fulfilledOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelInv = new Application_Model_Inventory();
        $modelPay = new Application_Model_OrderPayments();
        $modelMovement = new Application_Model_Movement();

        $id = $param['id'];
        $location_id = $param['location_id'];

        $model->setId($id);
        $model->setLocation($location_id);
        $model->autoFillReceiveProducts();

        $id = $param['id'];
        $model->setId($id);
        $model->fulfilledOrder();
        
        $productList = $modelPeer->fetchPurchaseOrderProducts($id);
        foreach ($productList as $products) {
            
            $this->updateProdcutAvgCost($products['product_id']);
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] + $products['quantity'];
                $oldQty = $productInventory['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $oldQty = 0;
                $newQuantity = $productInventory['quantity'];
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
            

            $qtyBefore = $oldQty;
            $qty = $products['quantity'];
            $qtyAfter = $newQuantity;
            $modelMovement->setLocationId($products['location_id']);
            $modelMovement->setProductId($products['product_id']);
            $modelMovement->setQty($qty);
            $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
            $modelMovement->setQtyBefore($qtyBefore);
            $modelMovement->setQtyAfter($qtyAfter);
            $modelMovement->setType('Purchase Order');
            $modelMovement->setOrderId($id);
            $modelMovement->setTotal_amount($sub_total);
            $modelMovement->saveInsert();
            
            
        }

        $model->setId($id);
        $model->setLocation($location_id);
        $model->autoFillUnstockProducts();

        $productList = $modelPeer->fetchProductsByStatus($id, 'Unstock');
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] - $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity(-$products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
      
        exit;
    }

    public function unfulfilledOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $id = $param['id'];
        $model->setId($id);
        $model->unfulfilledOrder();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchPurchaseProducts($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] - $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }


        exit;
    }

    public function paymentOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Purchase');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('Payment');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();
 
        $paidAmount = $model->getPaidPayment($orderId);
        
        $model->setId($orderId);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);         
        $model->setPaidAmount($paidAmount);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setLastPayDate(date('Y-m-d'));
        $model->paymentStatusUpdate();
        
        exit;
    }
    
    public function refundPaymentOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Purchase');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('Refund');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();
 
         $paidAmount = $model->getPaidPayment($orderId);
        
        $model->setId($orderId);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);         
        $model->setPaidAmount($paidAmount);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setLastPayDate(date('Y-m-d'));
        $model->paymentStatusUpdate();
        
        $model->updateTotalPayment();
        exit;
    }

    public function unpayOrderAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $total_amount = $param['total_amount'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Purchase');
        $modelPay->deleteOrderPayment();

        $paidAmount = $model->getPaidPayment($orderId);
         
        $model->setId($orderId);
        $model->setPaymentStatus('Unpaid');
        $model->setBalanceAmount($paidAmount);
        $model->setPaidAmount(0);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setLastPayDate(date('Y-m-d'));
        $model->paymentStatusUpdate();
        
    
        exit;
    }

    public function autofillReceiveProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPay = new Application_Model_OrderPayments();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelMovement = new Application_Model_Movement();
        $modelInv = new Application_Model_Inventory();

        $orderId = $param['order_id'];
        $location_id = $param['location_id'];
        $products = $modelPeer->fetchProductsByStatus($param['order_id'], 'Order');
        foreach ($products as $key => $value) {
            $po_order_id = $modelPeer->fetchPoIdByOrderId($value['order_id']);
            $sub_total = $modelPeer->fetchProductSubTotal($value['id']);
            $quantity = $modelPeer->fetchQuantity($value['product_id'], $value['location_id']);
            foreach ($quantity as $key1 => $value1) {
                $qtyBefore = $value1['quantity'];
                $qty = $value['quantity'];
                $qtyAfter = $value1['quantity'] + $value['quantity'];
                $modelMovement->setLocationId($value['location_id']);
                $modelMovement->setProductId($value['product_id']);
                $modelMovement->setQty($qty);
                $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Purchase Order');
                $modelMovement->setOrderId($po_order_id);
                $modelMovement->setTotal_amount($sub_total);
                $modelMovement->saveInsert();
            }
        }
        $model->setId($orderId);
        $model->setLocation($location_id);
        $model->autoFillReceiveProducts();


        $productList = $modelPeer->fetchPurchaseOrderProducts($orderId);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] + $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }

        $this->view->receivedOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($orderId, 'Receive');
    }
    
    public function autofillReturnsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();      
        $orderId = $param['order_id'];
        $location_id = $param['location_id'];
        
        $model->setId($orderId);
        $model->setLocation($location_id);
        $model->autoFillReturnsProducts();
       
        $this->view->returnProductList = Application_Model_PurchasePeer::fetchReturnPurchaseProducts($orderId);
        $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalById($orderId);
    }

    public function addReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();

        $request = $this->getRequest();
        $param = $request->getParams();
        $po_id = $param['id'];

        $model->setId($param['id']);
        $model->setProductRecordId($param['product_record_id']);
        $model->setProductId($param['product_id']);
        $model->setVendor_product_code($param['vendor_pcode']);
        $model->setQuantity($param['quantity']);
        $model->setUnitPrice($param['unit_price']);
        $model->setDiscount($param['discount']);
        $model->setProductSubTotal($param['sub_total']);
        $model->setProductStatus('Return');
        $model->setTransactionDate($this->_helper->common->formatDbDate($param['ret_date']));
        if ($param['id']) {
            $model->setSessionid('');
        } else {
            $model->setSessionid(session_id());
        }
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->setCreated(date('Y-m-d H:i:s'));
        $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->saveProduct();
        
        if ($po_id == null || $po_id < 1) {
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProductsBySession(session_id());
            $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setPaymentStatus('Owing');
            $model->statusUpdate();

            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProducts($po_id);
            $this->view->returnSubTotal = Application_Model_PurchasePeer::fetchReturnSubTotalById($po_id);
        }
        
        $model->setId($po_id);
        $model->setOrderStatus('Started');
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->orderStatusUpdate();
    }

    public function deleteReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $po_id = $this->_request->getParam('po_id');
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();  
        $modelInv = new Application_Model_Inventory();
        
        $productList = Application_Model_PurchasePeer::getProductByRecordId($id);
        
        $model->setId($id);
        $model->deleteData('tra_purchase_order_det');
        
        if(count($productList)>0){
            //manage Inventory
            foreach ($productList as $products) {
                $modelInv->setLocation($products['location_id']);
                $modelInv->setProduct_id($products['product_id']);
                //Update or insert Quantity in Inventory
                $validateQty = $modelInv->validateProductInventory();
                if ($validateQty > 0) {
                    $productInventory = $modelInv->getProductDetails();
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                    $modelInv->setNewQuantity($newQuantity);
                    $modelInv->updateProductQuantity();
                } else {
                    $modelInv->setNewQuantity($products['quantity']);
                    $modelInv->insertProductQuantity();
                }
            }

            $model->setReturnId($id);
            $model->deleteUnstock();
        }
        
        if ($po_id == null || $po_id < 1) {
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProductsBySession(session_id());
        } else {
            $this->view->returnProductList = $modelPeer->fetchReturnPurchaseProducts($po_id);
        }
    }

    public function editReturnProductAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Purchase();
        $model->setProductRecordId($id);
        $productData = $model->getProductById();
        echo json_encode($productData);
        exit;
    }

    public function getReturnSubtotalAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $po_id = $param['po_id'];

        if ($po_id == null || $po_id < 1) {
            $subTotal = Application_Model_PurchasePeer::fetchReturnSubTotalBySession(session_id());
        } else {
            $subTotal = Application_Model_PurchasePeer::fetchReturnSubTotalById($po_id);
        }
        $subTotalArr = array(
            'sub_total' => $subTotal,
        );
        echo json_encode($subTotalArr);
        exit;
    }

    public function autofillReturnProductsAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelPay = new Application_Model_OrderPayments();
        $modelMovement = new Application_Model_Movement();

        $orderId = $param['order_id'];
        $location_id = $param['location_id'];
        $products = $modelPeer->fetchProductsByStatus($param['order_id'], 'Return');
        foreach ($products as $key => $value) {
            $po_order_id = $modelPeer->fetchPoIdByOrderId($value['order_id']);
            $sub_total = $modelPeer->fetchProductSubTotal($value['id']);
            $quantity = $modelPeer->fetchQuantity($value['product_id'], $value['location_id']);
            foreach ($quantity as $key1 => $value1) {
                $qtyBefore = $value1['quantity'];
                $qty = $value['quantity'];
                $qtyAfter = $value1['quantity'] - $value['quantity'];
                $modelMovement->setLocationId($value['location_id']);
                $modelMovement->setProductId($value['product_id']);
                $modelMovement->setQty($qty);
                $modelMovement->setMDate($this->_helper->common->formatDbDate(date('d/m/Y')));
                $modelMovement->setQtyBefore($qtyBefore);
                $modelMovement->setQtyAfter($qtyAfter);
                $modelMovement->setType('Purchase Returned');
                $modelMovement->setOrderId($po_order_id);
                $modelMovement->setTotal_amount($sub_total);
                $modelMovement->saveInsert();
            }
        }

        $model->setId($orderId);
        $model->setLocation($location_id);
        $model->autoFillUnstockProducts();

        $modelInv = new Application_Model_Inventory();

        $productList = $modelPeer->fetchProductsByStatus($orderId, 'Unstock');
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                $newQuantity = $productInventory['quantity'] - $products['quantity'];
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity(-$products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        $this->view->unstockOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($orderId, 'Unstock', 'U');
    }

    
    public function refreshUnstockProductsAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $orderId = $param['order_id'];
        $this->view->unstockOrderData = Application_Model_PurchasePeer::fetchPurchaseOrderByStatus($orderId, 'Unstock', 'U');
    }
    /**
     * 
     * 
     */
    public function purchaseOrderPaymentAction() {
        $this->_helper->layout->disableLayout();
        $this->_addForm = new Application_Form_SalesPurchasePayment();
        $request = $this->getRequest();
        $order_id = $this->_request->getParam('order_id');
        $this->view->sales_order_id = $order_id;
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($order_id);
            $paid = Application_Model_PurchasePeer::getPaidAmount($order_id);
        }
        if ($paid) {
            $this->view->purchaePaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
        $this->view->form = $this->_addForm;
    }

    /**
     * 
     * 
     */
    public function insertPurchaseOrderPaymentAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $order_id = $this->_request->getParam('order_id');
        $this->view->sales_order_id = $order_id;
        if ($this->getRequest()->isPost()) {
            $post = $request->getPost();
            if ($post['payment_date']) {
                $payment_date1 = str_replace("/", "-", $post['payment_date']);
                $payment_date = date('Y-m-d', strtotime($payment_date1));
            } else {
                $payment_date = '';
            }
            $model = new Application_Model_OrderPayments();
            $model->setId($post['id']);
            $model->setOrderType('Purchase');
            $model->setOrderId($post['order_id']);
            $model->setPaymentMethodId($post['payment_method']);
            $model->setPaymentRef($post['payment_ref']);
            $model->setPaymentDate($payment_date);
            $model->setPaymentType($post['payment_type']);
            $model->setPaymentRemarks($post['payment_remarks']);
            $model->setPayAmount($post['payment_amount']);
            if ($post['order_id']) {
                $model->setSessionid('');
            } else {
                $model->setSessionid(session_id());
            }
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            if ($post['id'] == null || $post['id'] < 1) {
                $model->insertPayment();
            } else {
                $model->updatePayment();
            }
        }
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($order_id);
            $paid = Application_Model_PurchasePeer::getPaidAmount($order_id);
        }
        
        $modelPurchase = new Application_Model_Purchase();
        if ($paid['order_total'] > $paid['paid_amount']) {

            $modelPurchase->setPaymentStatus('Partial');
            $this->view->purchaePaymentStatus = 'Partial';
        }
        else if($paid['order_total'] < $paid['paid_amount']) {
            $modelPurchase->setPaymentStatus('Owing');
            $this->view->purchaePaymentStatus = 'Owing';
        }
        else {
            $modelPurchase->setPaymentStatus('Paid');
            $this->view->purchaePaymentStatus = 'Paid';
        }
        
        $paid_amount = $modelPurchase->getPaidPayment($order_id);
        
        $modelPurchase->setId($order_id);
        $modelPurchase->setBalanceAmount($paid['order_total'] - $paid_amount);
        $modelPurchase->setPaidAmount($paid_amount);
        $modelPurchase->setLastPayDate($this->_helper->common->formatDbDate($post['payment_date']));
        $modelPurchase->setUpdated(date('Y-m-d H:i:s'));
        $modelPurchase->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPurchase->paymentStatusUpdate();
        
         if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($order_id);
            $paid = Application_Model_PurchasePeer::getPaidAmount($order_id);
        }
        
       
        if ($paid['paid_amount'] != '') {
            $this->view->purchaePaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
        
    }

    /**
     * edit Purchase order payment action
     *  used to fetch payment information
     */
    public function editPurchaseOrderPaymentsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_OrderPayments();
        $model->setPaymentId($id);
        $model->setOrderType('Purchase');
        $paymentData = $model->fetchSalesPurchaseOrderPaymentById();
        echo json_encode($paymentData);
        exit;
    }

    /**
     * delete Purchase order payment action
     * will delete sales order payment 
     */
    public function deletePurchaseOrderPaymentsAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $order_id = $this->_request->getParam('po_id');
        $model = new Application_Model_SalesOrder();
        $modelPurchase = new Application_Model_Purchase();
        $modelPeer = new Application_Model_SalesOrderPeer();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('tra_order_payments');
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($order_id);
            $paid = Application_Model_PurchasePeer::getPaidAmount($order_id);
        }
        if($paid['paid_amount'] == '0') {
            $modelPurchase->setPaymentStatus('Unpaid');
             $modelPurchase->setLastPayDate('');
            $this->view->purchaePaymentStatus = 'Unpaid';
        }
        else if($paid['order_total'] < $paid['paid_amount']) {
            $modelPurchase->setPaymentStatus('Owing');
            $modelPurchase->setLastPayDate($paid['last_pay_date']);
            $this->view->salesPaymentStatus = 'Owing';
        }
        else if ($paid['order_total'] > $paid['paid_amount']) {
            $modelPurchase->setPaymentStatus('Partial');
            $modelPurchase->setLastPayDate($paid['last_pay_date']);
            $this->view->purchaePaymentStatus = 'Partial';
        }
        else {
            $modelPurchase->setPaymentStatus('Paid');
            $modelPurchase->setLastPayDate($paid['last_pay_date']);
            $this->view->purchaePaymentStatus = 'Paid';
        }
        
        $paid_amount = $modelPurchase->getPaidPayment($order_id);
        
        $modelPurchase->setId($order_id);
        $modelPurchase->setBalanceAmount($paid['order_total'] - $paid_amount);
        $modelPurchase->setPaidAmount($paid['paid_amount']);
        $modelPurchase->setUpdated(date('Y-m-d H:i:s'));
        $modelPurchase->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPurchase->paymentStatusUpdate();
        
        
        if ($order_id == null || $order_id < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($order_id);
            $paid = Application_Model_PurchasePeer::getPaidAmount($order_id);
        }
        if ($paid['paid_amount'] != '') {
            $this->view->purchaePaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
    }

    /**
     * 
     * 
     */
    public function paymentOrderPopupAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();
//        var_dump($param);die;
        $model = new Application_Model_Purchase();
        $modelPeer = new Application_Model_PurchasePeer();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['id'];
        $balance = $param['balance'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Purchase');
        $modelPay->setPaymentDate(date('Y-m-d'));
        $modelPay->setPaymentType('');
        $modelPay->setPaymentMethodId('');
        $modelPay->setPaymentRef('');
        $modelPay->setPaymentRemarks('');
        $modelPay->setPayAmount($balance);
        $modelPay->setUpdated(date('Y-m-d H:i:s'));
        $modelPay->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->setCreated(date('Y-m-d H:i:s'));
        $modelPay->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $modelPay->insertPayment();

        $model->setId($orderId);
        $model->setPaymentStatus('Paid');
        $model->setBalanceAmount(0);
        $model->setPaidAmount($balance);
        $model->setLastPayDate(date('Y-m-d'));
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();

        if ($orderId == null || $orderId < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($orderId);
            $paid = Application_Model_PurchasePeer::getPaidAmount($orderId);
        }

        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
    }

    /**
     * 
     * 
     */
    public function issueRefundAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $param = $request->getParams();

        $model = new Application_Model_Purchase();
        $modelPay = new Application_Model_OrderPayments();

        $orderId = $param['order_id'];
        $total_amount = $param['total_amount'];

        $modelPay->setOrderId($orderId);
        $modelPay->setOrderType('Purchase');
        $modelPay->deleteOrderPayment();

        $model->setId($orderId);
        $model->setPaymentStatus('Unpaid');
        $model->setBalanceAmount($total_amount);
        $model->setPaidAmount(0);
        $model->setUpdated(date('Y-m-d H:i:s'));
        $model->setLastPayDate('');
        $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
        $model->paymentStatusUpdate();
        if ($orderId == null || $orderId < 1) {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsBySession(session_id());
            $paid = Application_Model_PurchasePeer::getPaidAmountBySessionId(session_id());
        } else {
            $this->view->paymentList = Application_Model_PurchasePeer::fetchPurchasePaymentsByOrderId($orderId);
            $paid = Application_Model_PurchasePeer::getPaidAmount($orderId);
        }

        if ($paid) {
            $this->view->salesPaymentStatus = $paid['payment_status'];
            $this->view->paid_amount = $paid['paid_amount'];
            $this->view->balance = $paid['balance'];
            $this->view->order_total = $paid['order_total'];
        } else {
            $this->view->paid_amount = '0.00';
            $this->view->balance = '0.00';
            $this->view->order_total = '0.00';
        }
    }

    public function copyPurchaseOrderAction() {
        $this->_helper->layout->disableLayout();
        $purchase_id = $this->_request->getParam('purchase_id');
        $model = new Application_Model_Purchase();
        $model->setPoId($purchase_id);
        $mas_det = $model->fetchPurchaseOrderDetails();
        foreach ($mas_det as $key => $value) {
            $model->setId('');
            $model->setProductRecordId('');
            $model->setProductId($value['product_id']);
            $model->setVendor_product_code($value['vendor_product_code']);
            $model->setQuantity($value['quantity']);
            $model->setUnitPrice($value['unit_price']);
            $model->setDiscount($value['discount']);
            $model->setProductSubTotal($value['sub_total']);
            $model->setProductStatus($value['item_status']);
            $model->setTransactionDate('');
            $model->setLocation($value['location_id']);
            $model->setSessionid(session_id());
            $model->setUpdated(date('Y-m-d H:i:s'));
            $model->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->setCreated(date('Y-m-d H:i:s'));
            $model->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
            $model->saveProduct();
        }
        echo $purchase_id;
        die;
    }

    public function cancelPurchaseOrderAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Purchase();
        $modelInv = new Application_Model_Inventory();
        $model->setId($id);
        $model->cancelPurchaseOrder();
        
        $productList = Application_Model_PurchasePeer::fetchCancelledPurchaseOrder($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                if($products['item_status']=='Order'){
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                } else if($products['item_status']=='Unstock'){
                    $newQuantity = $productInventory['quantity'] + $products['quantity'];
                }
                
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
      
        $this->_helper->flashMessenger()->addMessage('Purchase order has been cancelled successfully', 'success');
        $this->_redirect('/purchase-order');
        
    }

    /**
     * 
     * 
     */
    public function reopenPurchaseOrderAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Purchase();
         $modelInv = new Application_Model_Inventory();
        $model->setId($id);
        $model->reopenPurchaseOrder();
        
       $productList = Application_Model_PurchasePeer::fetchCancelledPurchaseOrder($id);
        foreach ($productList as $products) {
            $modelInv->setLocation($products['location_id']);
            $modelInv->setProduct_id($products['product_id']);
            //Update or insert Quantity in Inventory
            $validateQty = $modelInv->validateProductInventory();
            if ($validateQty > 0) {
                $productInventory = $modelInv->getProductDetails();
                if($products['item_status']=='Order'){
                    $newQuantity = $productInventory['quantity'] + $products['quantity'];
                } else if($products['item_status']=='Unstock'){
                    $newQuantity = $productInventory['quantity'] - $products['quantity'];
                }
                
                $modelInv->setNewQuantity($newQuantity);
                $modelInv->updateProductQuantity();
            } else {
                $modelInv->setNewQuantity($products['quantity']);
                $modelInv->insertProductQuantity();
            }
        }
        
        $this->_helper->flashMessenger()->addMessage('Purchase order has been reopened successfully', 'success');
        $this->_redirect('/purchase-order');
    }
    
    public function updateVendorbalance($vendor_id){
         //update Vendor balance
        $modelVendor = new Application_Model_Vendor();
        $vendorBalance = Application_Model_VendorPeer::getVendorBalance($vendor_id);
      //  echo $vendorBalance;exit;
        $modelVendor->setId($vendor_id);
        $modelVendor->setBalance($vendorBalance);
        $modelVendor->updateBalance();
    }
    
    public function updateProdcutAvgCost($product_id){
         //fetch produst avrage Cost
        $model = new Application_Model_Purchase();
        $avgCost = Application_Model_PurchasePeer::getAvarageCost($product_id);
        
        $model->setProductId($product_id);
        $model->setUnitPrice($avgCost);
        $model->updateProductAvgCost();
    }
    public function purchaseOrderByStatusAction() {
        $this->_helper->layout->setLayout('layout');

        $request = $this->getRequest();
        $status = $this->_request->getParam('status');
        $this->view->purchaseData = Application_Model_PurchasePeer::fetchAllPurchaseOrderByStatus($status);
    }
}
