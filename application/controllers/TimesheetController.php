<?php

/**
 * TimesheetController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class TimesheetController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();

        $screenData = $this->model->getScreenData('/' . $controllerName);

        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'], 'view');
        if ($userAccess == 0) {
            $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelTime = new Application_Model_Timesheet();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_Timesheet();

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            $status = '';
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();

                $modelTime->setId($post['id']);
                $modelTime->setEmpId($post['emp_name']);
                $modelTime->setTimesheetDate($this->_helper->common->formatDbDate($post['timesheet_date']));
                $modelTime->setJobNo($post['job_card']);
                $modelTime->setHourlyRate($post['hourly_rate']);
                $modelTime->setOrd($post['ord']);
                $modelTime->setOrd15($post['ord_15']);
                $modelTime->setOrd20($post['ord_20']);
                $modelTime->setOrdTotal($post['total']);                
                $modelTime->setRateType($post['rate_type']);                
                $modelTime->setUpdated(date('Y-m-d H:i:s'));
                $modelTime->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTime->setCreated(date('Y-m-d H:i:s'));
                $modelTime->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTime->save();

                $this->_helper->flashMessenger()->addMessage('Timesheet has been added successfully', 'success');
                $this->_redirect('/timesheet');
            }
            if ($modelTime->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        $this->view->timesheetData = Application_Model_TimesheetPeer::fetchAllTimesheet();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelTime = new Application_Model_Timesheet();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_Timesheet();

        //Populate all value on edit mode
        if ($id) {
            $modelTime->setId($id);
            $data = $modelTime->fetchTimesheetById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'emp_name' => $data['emp_id'],
                        'timesheet_date' => $data['timesheet_date'],
                        'job_card' => $data['job_no'],
                        'hourly_rate' => $data['hourly_rate'],
                        'ord' => $data['total_ord'],
                        'ord_15' => $data['total_1_5'],
                        'ord_20' => $data['total_2_0'],
                        'total' => $data['sub_total'],
                        'rate_type' => $data['rate_type'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update");
            $this->_addForm->setAction("/timesheet/edit/id/" . $id);
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelTime->setId($post['id']);
                $modelTime->setEmpId($post['emp_name']);
                $modelTime->setTimesheetDate($this->_helper->common->formatDbDate($post['timesheet_date']));
                $modelTime->setJobNo($post['job_card']);
                $modelTime->setHourlyRate($post['hourly_rate']);
                $modelTime->setOrd($post['ord']);
                $modelTime->setOrd15($post['ord_15']);
                $modelTime->setOrd20($post['ord_20']);
                $modelTime->setOrdTotal($post['total']); 
                $modelTime->setRateType($post['rate_type']); 
                $modelTime->setUpdated(date('Y-m-d H:i:s'));
                $modelTime->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelTime->save();

                $this->_helper->flashMessenger()->addMessage('Timesheet has been updated successfully', 'success');
                $this->_redirect('/timesheet');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->setLayout('layout');
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Timesheet();
        $model->setId($id);
        
        $this->view->usersData = $model->deleteData('tra_emp_timesheet');
        $this->_helper->flashMessenger()->addMessage('Timesheet has been deleted successfully', 'success');
        $this->_redirect('/timesheet');
        
    }

    public function timesheetSummaryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->employee = Application_Model_EmployeePeer::fetchAllEmployee();     
    }
    
    public function getTimesheetSummaryAction() {
        $this->_helper->layout->disableLayout();
        $date_from = $this->_request->getParam('date_from');
        $date_to = $this->_request->getParam('date_to');
        $emp_id = $this->_request->getParam('emp_id');
        
        $dFrom = ($date_from)?$this->_helper->common->formatDbDate($date_from):'';
        $dTo = ($date_to)?$this->_helper->common->formatDbDate($date_to):'';
        
        $this->view->emp_id = $emp_id;
        $this->view->date_from = $date_from;
        $this->view->date_to = $date_to;
        
        $this->view->timesheetData = Application_Model_TimesheetPeer::getTimesheetSummary($dFrom,$dTo,$emp_id);         
    }
    
    public function printTimesheetSummaryAction() {
        $this->_helper->layout->setLayout('print');
        $date_from = $this->_request->getParam('date_from');
        $date_to = $this->_request->getParam('date_to');
        $emp_id = $this->_request->getParam('emp_id');
        
        $dFrom = ($date_from)?$this->_helper->common->formatDbDate($date_from):'';
        $dTo = ($date_to)?$this->_helper->common->formatDbDate($date_to):'';
        
        $this->view->timesheetData = Application_Model_TimesheetPeer::getTimesheetSummary($dFrom,$dTo,$emp_id);         
    }
    
     public function timesheetDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->employee = Application_Model_EmployeePeer::fetchAllEmployee();     
    }
    
    public function getTimesheetDetailsAction() {
        $this->_helper->layout->disableLayout();
        $date_from = $this->_request->getParam('date_from');
        $date_to = $this->_request->getParam('date_to');
        $emp_id = $this->_request->getParam('emp_id');
        
        $dFrom = ($date_from)?$this->_helper->common->formatDbDate($date_from):'';
        $dTo = ($date_to)?$this->_helper->common->formatDbDate($date_to):'';
        
        $empData = Application_Model_TimesheetPeer::getEmployeeForTimesheet($dFrom,$dTo,$emp_id);         
        echo '<pre>';
        
        $timesheetTemp = array();
         
        foreach ($empData as $emp){
            $timesheetData = Application_Model_TimesheetPeer::getEmployeeTimesheetDates($dFrom,$dTo,$emp['emp_id']);
            $timeArr = array();
 
            foreach ($timesheetData as $tData){
                $timeInnerArr = array();
                $detailsData = Application_Model_TimesheetPeer::getEmployeeTimesheetDetails($this->_helper->common->formatDbDate($tData['timesheet_date']),$tData['emp_id']);
                foreach ($detailsData as $dData){
                    $timeInnerArr[] = array(
                        'job_no' => $dData['job_no'],
                        'hourly_rate' => $dData['hourly_rate'],
                        'total_ord' => $dData['total_ord'],
                        'total_1_5' => $dData['total_1_5'],
                        'total_2_0' => $dData['total_2_0'],
                        'sub_total' => $dData['sub_total']
                    );
                }
                $timeArr[] = array(
                        'timesheet_date' => $tData['timesheet_date'],
                        'total_ord' => $tData['total_ord'],
                        'total_1_5' => $tData['total_1_5'],
                        'total_2_0' => $tData['total_2_0'],
                        'sub_total' => $tData['sub_total'],
                        'details' => $timeInnerArr
               );
    
            }
            $timesheetTemp[$emp['empname']] =$timeArr;
        }
        
        //print_r($timesheetTemp);exit;
        
        $this->view->emp_id = $emp_id;
        $this->view->date_from = $date_from;
        $this->view->date_to = $date_to;
        
        $this->view->timesheetData = $timesheetTemp;         
    }
    
    public function printTimesheetDetailsAction() {
        $this->_helper->layout->setLayout('print');
       $date_from = $this->_request->getParam('date_from');
        $date_to = $this->_request->getParam('date_to');
        $emp_id = $this->_request->getParam('emp_id');
        
        $dFrom = ($date_from)?$this->_helper->common->formatDbDate($date_from):'';
        $dTo = ($date_to)?$this->_helper->common->formatDbDate($date_to):'';
        
        $empData = Application_Model_TimesheetPeer::getEmployeeForTimesheet($dFrom,$dTo,$emp_id);         
        echo '<pre>';
        
        $timesheetTemp = array();
         
        foreach ($empData as $emp){
            $timesheetData = Application_Model_TimesheetPeer::getEmployeeTimesheetDates($dFrom,$dTo,$emp['emp_id']);
            $timeArr = array();
 
            foreach ($timesheetData as $tData){
                $timeInnerArr = array();
                $detailsData = Application_Model_TimesheetPeer::getEmployeeTimesheetDetails($this->_helper->common->formatDbDate($tData['timesheet_date']),$tData['emp_id']);
                foreach ($detailsData as $dData){
                    $timeInnerArr[] = array(
                        'job_no' => $dData['job_no'],
                        'hourly_rate' => $dData['hourly_rate'],
                        'total_ord' => $dData['total_ord'],
                        'total_1_5' => $dData['total_1_5'],
                        'total_2_0' => $dData['total_2_0'],
                        'sub_total' => $dData['sub_total']
                    );
                }
                $timeArr[] = array(
                        'timesheet_date' => $tData['timesheet_date'],
                        'total_ord' => $tData['total_ord'],
                        'total_1_5' => $tData['total_1_5'],
                        'total_2_0' => $tData['total_2_0'],
                        'sub_total' => $tData['sub_total'],
                        'details' => $timeInnerArr
               );
    
            }
            $timesheetTemp[$emp['empname']] =$timeArr;
        }
                
        $this->view->timesheetData = $timesheetTemp;
    }

}
