<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class CountryController extends DMC_Controller_Abstract{

    protected $model;
    private $_addForm = null;
    public function init() {       
        /*
        * Initialise a session called 'Default'
        */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
        * Is the user logged in?  If not re-direct to login screen
        */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }
        
        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        
        $controllerName = $this->_request->getControllerName();
       
        $screenData=$this->model->getScreenData('/'.$controllerName);
        $this->view->screenName=$screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCountry = new Application_Model_Country();
        $modelPeer = new Application_Model_CountryPeer();
        
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');
        
        $this->view->id = $id;
        $this->_addForm =  new Application_Form_Country();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
                $rolesData[$val['id']] = $val['role_name'];
        }
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCountry->setId($post['id']);
                $modelCountry->setCountry_name($post['country_name']);
                $modelCountry->setCountry_code($post['country_code']);
                $modelCountry->setUpdated(date('Y-m-d H:i:s'));
                $modelCountry->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCountry->setCreated(date('Y-m-d H:i:s'));
                $modelCountry->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCountry->save();                
                
                $this->_redirect('/country?status=success');
               
            }
            if ($modelCountry->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
            
            
        }
     
         if($status=='present'){
            $this->view->successMsg = 'Country is already Present.';
        }
         if($status=='success'){
            $this->view->successMsg = 'Country has been added successfully.';
        }
        if($status=='updated'){
            $this->view->successMsg = 'Country has been updated successfully.';
        }
        if($status=='deleted'){
            $this->view->successMsg = 'Country has been deleted successfully.';
        }

        $this->view->details = $modelPeer->fetchAllCountries();
        $this->view->form = $this->_addForm;
    }
    
     /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelCountry = new Application_Model_Country();
        $modelPeer = new Application_Model_CountryPeer();
        
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm =  new Application_Form_Country();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
                $rolesData[$val['id']] = $val['role_name'];
        }
        
        //Populate all value on edit mode
        if ($id) {
            $modelCountry->setId($id);
            $CountryData = $modelCountry->fetchCountryById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'country_name' => $CountryData['country_name'],
                        'country_code' => $CountryData['country_code'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update Country");
            $this->_addForm->setAction("/country/edit/id/".$id);
        }
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelCountry->setId($post['id']);
                $modelCountry->setCountry_name($post['country_name']);
                $modelCountry->setCountry_code($post['country_code']);
                $modelCountry->setUpdated(date('Y-m-d H:i:s'));
                $modelCountry->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelCountry->save();                
                
                $this->_redirect('/country?status=updated');
               
            }
        }
        
        $this->view->form = $this->_addForm;
    }
   

    /*
    * delete Action
    * use to delete data from user table
    * @return
    */
    
    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Country();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_countries');
        exit;
    }  
}
