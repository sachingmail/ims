<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class UsersController extends DMC_Controller_Abstract{

    protected $model;
    private $_addForm = null;
    public function init() {   
        parent::init();
        /*
        * Initialise a session called 'Default'
        */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
        * Is the user logged in?  If not re-direct to login screen
        */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }
        
        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        
        $controllerName = $this->_request->getControllerName();       
        $screenData=$this->model->getScreenData('/'.$controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName=$screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelUser = new Application_Model_Users();
        $modelPeer = new Application_Model_UsersPeer();
        
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');
        
        $this->view->id = $id;
        $this->_addForm =  new Application_Form_Users_Add();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
                $rolesData[$val['id']] = $val['role_name'];
        }
        $isactive = $genericPeer->fetchStatus();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($isactive as $key => $val) {
                $statusData[$val['code']] = $val['meaning'];
        }
//        $this->_addForm->upassword->setRequired(false);
//        $this->_addForm->cpassword->setRequired(false);
        $this->_addForm->role_id->setMultiOptions($rolesData);
        $this->_addForm->isactive->setMultiOptions($statusData);
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelUser->setId($post['id']);
                $modelUser->setUsername($post['uname']);
                $modelUser->setPassword(md5($post['upassword']));
                $modelUser->setName($post['name']);
                $modelUser->setEmail($post['email']);
                $modelUser->setPhone($post['phoneno']);
                $modelUser->setRole_id($post['role_id']);
                $modelUser->setIsactive($post['isactive']);
                $modelUser->setUpdated(date('Y-m-d H:i:s'));
                $modelUser->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUser->setCreated(date('Y-m-d H:i:s'));
                $modelUser->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUser->save();                
                
                $this->_redirect('/users?status=success');
               
            }
            if ($modelUser->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
            
            
        }
     
         if($status=='present'){
            $this->view->successMsg = 'Username is already Present. Please Try Another Username';
        }
         if($status=='success'){
            $this->view->successMsg = 'User has been added successfully.';
        }
        if($status=='updated'){
            $this->view->successMsg = 'User has been updated successfully.';
        }
        if($status=='deleted'){
            $this->view->successMsg = 'User has been deleted successfully.';
        }
        
        $this->view->usersData = $modelPeer->fetchAllUsers();
        $this->view->form = $this->_addForm;
    }
    
     /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function editUserAction() {
        $this->_helper->layout->setLayout('layout');
        $modelUser = new Application_Model_Users();
        $modelPeer = new Application_Model_UsersPeer();
        
        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm =  new Application_Form_Users_Add();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
                $rolesData[$val['id']] = $val['role_name'];
        }
        
        $status = $genericPeer->fetchStatus();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($status as $key => $val) {
                $statusData[$val['code']] = $val['meaning'];
        }
        
        $this->_addForm->role_id->setMultiOptions($rolesData);
        $this->_addForm->isactive->setMultiOptions($statusData);
        
        //Populate all value on edit mode
        if ($id) {
            $modelUser->setId($id);
            $userData = $modelUser->fetchUserById();

            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'uname' => $userData['username'],
                        'name' => $userData['name'],
                        'email' => $userData['email'],
                        'phoneno' => $userData['phone'],
                        'role_id' => $userData['role_id'],
                        'isactive' => $userData['is_active'],
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update User");
            $this->_addForm->setAction("/users/edit-user/id/".$id);
            $this->_addForm->upassword->setRequired(false);
            $this->_addForm->cpassword->setRequired(false);
        }
        
        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelUser->setId($post['id']);
                $modelUser->setUsername($post['uname']);
                if($post['upassword']){
                    $modelUser->setPassword(md5($post['upassword']));
                }
                $modelUser->setName($post['name']);
                $modelUser->setEmail($post['email']);
                $modelUser->setPhone($post['phoneno']);
                $modelUser->setRole_id($post['role_id']);
                $modelUser->setIsactive($post['isactive']);
                $modelUser->setUpdated(date('Y-m-d H:i:s'));
                $modelUser->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelUser->save();                
                
                $this->_redirect('/users?status=updated');
               
            }
        }
        
        $this->view->form = $this->_addForm;
    }
   

    /*
    * delete Action
    * use to delete data from user table
    * @return
    */
    
    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_Users();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('users');
        exit;
    }  
}
