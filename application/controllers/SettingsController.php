<?php

/**
 * reportsController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class ReportsController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        
        parent::init();
        
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();
        
        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        
        $this->view->screenName = $screenData['display_name'];
       
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
    }
    
    
}
