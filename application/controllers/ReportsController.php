<?php

/**
 * reportsController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class ReportsController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        
        parent::init();
        
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        
        $this->view->screenName = $screenData['display_name'];
       
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
    }
    
    public function getProductsAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product = Application_Model_ProductPeer::fetchAllProductByCategory($category_id);
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }

    public function inventorySummaryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }

    public function getInventorySummaryAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $product = Application_Model_ReportsPeer::getInventorySummary($category_id,$product_name);
        $this->view->category_id =$category_id;
        $this->view->product_name =$product_name;
        $this->view->productData =$product;
    }
    
    public function printInventorySummaryAction() {
         $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $product = Application_Model_ReportsPeer::getInventorySummary($category_id,$product_name);
        $this->view->productData =$product;
    }
    
    public function inventoryDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
    }


    public function getInventoryDetailsAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $location_id = $this->_request->getParam('location');
        
        $locationList =Application_Model_ReportsPeer::getInventoryLocations($category_id,$product_name,$location_id);   
            
        $Purtemp = array();    
        foreach ($locationList as $data){
            $purDetailsData = Application_Model_ReportsPeer::getInventoryDetails($data['location_id'],$category_id,$product_name);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $total =$vData['cost']*$vData['quantity'];
                $purArr[] = array(
                    'name' => $vData['product_name'],
                    'category' => $vData['category'],
                    'location' => $vData['location'],
                    'qty' => $vData['quantity'],
                    'cost' => $vData['cost'],
                    'total' => $total
                );  
            }
            $Purtemp[$data['location_name']] =$purArr;
        }  
      
        $this->view->pDetailsData =$Purtemp;
        
        
        $this->view->category_id =$category_id;
        $this->view->location =$location_id;
        $this->view->product_name =$product_name;

    }
    
    public function printInventoryDetailsAction() {
         $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $location_id = $this->_request->getParam('location');
        
        $locationList =Application_Model_ReportsPeer::getInventoryLocations($category_id,$product_name,$location_id);   
            
        $Purtemp = array();    
        foreach ($locationList as $data){
            $purDetailsData = Application_Model_ReportsPeer::getInventoryDetails($data['location_id'],$category_id,$product_name);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $total =$vData['cost']*$vData['quantity'];
                $purArr[] = array(
                    'name' => $vData['product_name'],
                    'category' => $vData['category'],
                    'location' => $vData['location'],
                    'qty' => $vData['quantity'],
                    'cost' => $vData['cost'],
                    'total' => $total
                );  
            }
            $Purtemp[$data['location_name']] =$purArr;
        }  
      
        $this->view->pDetailsData =$Purtemp;
        $this->view->product_name =$product_name;
    }
    
    public function inventoryByLocationAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
    }

    public function getInventoryByLocationAction() {
        $this->_helper->layout->disableLayout();
        $location_id = $this->_request->getParam('location_id');
        if($location_id){
            $locationList = Application_Model_ReportsPeer::getInventoryLocations('','',$location_id);
        } else {
            $locationList = Application_Model_ReportsPeer::fetchAllLocations();
        }

        $this->view->location_id =$location_id;
        $this->view->locationData =$locationList;
    }
    
    public function printInventoryByLocationAction() {
        $this->_helper->layout->setLayout('print');
        $location_id = $this->_request->getParam('location_id');
        if($location_id){
            $locationList = Application_Model_ReportsPeer::getInventoryLocations('','',$location_id);
        } else {
            $locationList = Application_Model_ReportsPeer::fetchAllLocations();
        }

        $this->view->location_id =$location_id;
        $this->view->locationData =$locationList;
    }
    
    public function productPriceListAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }


    public function getProductPriceListAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $product = Application_Model_ReportsPeer::getProductList($category_id,$product_name);
        $this->view->category_id =$category_id;
        $this->view->product_name =$product_name;
        $this->view->productData =$product;
    }
    
    public function printProductPriceListAction() {
         $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $product = Application_Model_ReportsPeer::getProductList($category_id,$product_name);
        $this->view->productData =$product;
    }
    
    public function vendorProductListAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }


    public function getVendorProductListAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $vendor_id = $this->_request->getParam('vendor_id');
        $vendors = Application_Model_ReportsPeer::getVendorList($category_id,$product_name,$vendor_id);      

        $vendortemp = array();    
        foreach ($vendors as $vendor){
            $vendorData = Application_Model_ReportsPeer::getVendorProductList($category_id,$product_name,$vendor['vendor_id']);
            $vendorsArr = array();
            foreach ($vendorData as $vData){
                $vendorsArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'product_category_id' => $vData['product_category_id'],
                    'category' => $vData['category'],
                    'vendor_product_code' => $vData['vendor_product_code'],
                    'vendor_name' => $vData['vendor_name'],
                    'price' => $vData['price']
                );  
            }
            $vendortemp[$vendor['vendor_name']] =$vendorsArr;
            
        }  

//        echo '<pre>';
//        print_r($vendortemp);exit;
        
        $this->view->vendor_id =$vendor_id;
        $this->view->category_id =$category_id;
        $this->view->product_name =$product_name;
        $this->view->vendorData =$vendortemp;
    }
    
    public function printVendorProductListAction() {
        $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $vendor_id = $this->_request->getParam('vendor_id');
        $vendors = Application_Model_ReportsPeer::getVendorList($category_id,$product_name,$vendor_id);      
            
        $vendortemp = array();    
        foreach ($vendors as $vendor){
            $vendorData = Application_Model_ReportsPeer::getVendorProductList($category_id,$product_name,$vendor['vendor_id']);
            $vendorsArr = array();
            foreach ($vendorData as $vData){
                $vendorsArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'product_category_id' => $vData['product_category_id'],
                    'category' => $vData['category'],
                    'vendor_product_code' => $vData['vendor_product_code'],
                    'vendor_name' => $vData['vendor_name'],
                    'price' => $vData['price']
                );  
            }
            $vendortemp[$vendor['vendor_name']] =$vendorsArr;
            
        }  
        $this->view->vendorData =$vendortemp;
    }
    
    public function vendorListAction() {
       $this->_helper->layout->setLayout('layout');
       $this->view->terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
       $this->view->taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
       $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
       
    }


    public function getVendorListAction() {
        $this->_helper->layout->disableLayout();
        $payment_terms = $this->_request->getParam('payment_terms');
        $taxing_schemes = $this->_request->getParam('taxing_schemes');
        $currency = $this->_request->getParam('currency_id');
        $vendors = Application_Model_ReportsPeer::getVendors($payment_terms,$taxing_schemes,$currency);      
        
        $this->view->vendor_id =$payment_terms;
        $this->view->category_id =$taxing_schemes;
        $this->view->product_name =$currency;
        $this->view->vendorData =$vendors;
    }
    
    public function printVendorListAction() {
        $this->_helper->layout->setLayout('print');
        $payment_terms = $this->_request->getParam('payment_terms');
        $taxing_schemes = $this->_request->getParam('taxing_schemes');
        $currency = $this->_request->getParam('currency_id');
        $vendors = Application_Model_ReportsPeer::getVendors($payment_terms,$taxing_schemes,$currency);      
        
        $this->view->vendorData =$vendors;
    }
    
    
    public function purchaseSummaryAction() {
       $this->_helper->layout->setLayout('layout');
       $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
       $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
       
    }


    public function getPurchaseSummaryAction() {
        $this->_helper->layout->disableLayout();
        $vendor_id = $this->_request->getParam('vendor_id');
        $purchaseData = Application_Model_ReportsPeer::getPurchaseSummary($vendor_id);      
        
        $this->view->vendor_id =$vendor_id;
        $this->view->purchaseData =$purchaseData;
    }
    
    public function printPurchaseSummaryAction() {
        $this->_helper->layout->setLayout('print');
        $vendor_id = $this->_request->getParam('vendor_id');
        $purchaseData = Application_Model_ReportsPeer::getPurchaseSummary($vendor_id);      
        $this->view->purchaseData =$purchaseData;
    }
    
    
    public function purchaseDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        
    }


    public function getPurchaseDetailsAction() {
        $this->_helper->layout->disableLayout();
        $vendor_id = $this->_request->getParam('vendor_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseDetialsOrderIds($vendor_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::fetchPurchaseProducts($poId['id'],$vendor_id);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['order_id'],
                    'po_date' => $vData['po_date'],
                    'status' => $vData['payment_status'],
                    'vendor_name' => $vData['vendor_name'],
                    'currency_code' => $vData['currency_code'],
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total'],
                    'payment_status' => $vData['payment_status']
                );  
            }
            $Purtemp[$poId['id']] =$purArr;
        }  
        
        $this->view->vendor_id =$vendor_id;
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function printPurchaseDetailsAction() {
        $this->_helper->layout->setLayout('print');
        $vendor_id = $this->_request->getParam('vendor_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseDetialsOrderIds($vendor_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::fetchPurchaseProducts($poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['order_id'],
                    'po_date' => $vData['po_date'],
                    'status' => $vData['payment_status'],
                    'vendor_name' => $vData['vendor_name'],
                    'currency_code' => $vData['currency_code'],
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total'],
                    'payment_status' => $vData['payment_status']
                );  
            }
            $Purtemp[$poId['id']] =$purArr;
        }  
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function salesSummaryAction() {
       $this->_helper->layout->setLayout('layout');
       $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
       $this->view->currency = Application_Model_CurrencyPeer::fetchAllCurrency();
       
    }


    public function getSalesSummaryAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);      
        
        $this->view->customer_id =$customer_id;
        $this->view->salesData =$salesData;
    }
    
    public function printSalesSummaryAction() {
        $this->_helper->layout->setLayout('print');
       $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);      

        $this->view->salesData =$salesData;
    }
    
    
    public function salesDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        
    }


    public function getSalesDetailsAction() {
        $this->_helper->layout->disableLayout();
        $vendor_id = $this->_request->getParam('vendor_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseDetialsOrderIds($vendor_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::fetchPurchaseProducts($poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['order_id'],
                    'po_date' => $vData['po_date'],
                    'status' => $vData['payment_status'],
                    'vendor_name' => $vData['vendor_name'],
                    'currency_code' => $vData['currency_code'],
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total'],
                    'payment_status' => $vData['payment_status']
                );  
            }
            $Purtemp[$poId['id']] =$purArr;
        }  
        
        $this->view->vendor_id =$vendor_id;
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function printSalesDetailsAction() {
        $this->_helper->layout->setLayout('print');
        $vendor_id = $this->_request->getParam('vendor_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseDetialsOrderIds($vendor_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::fetchPurchaseProducts($poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['order_id'],
                    'po_date' => $vData['po_date'],
                    'status' => $vData['payment_status'],
                    'vendor_name' => $vData['vendor_name'],
                    'currency_code' => $vData['currency_code'],
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total'],
                    'payment_status' => $vData['payment_status']
                );  
            }
            $Purtemp[$poId['id']] =$purArr;
        }      
        $this->view->pDetailsData =$Purtemp;
    }
    
    
    public function purchaseTaxAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        $this->view->taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        
    }


    public function getPurchaseTaxAction() {
        $this->_helper->layout->disableLayout();
        $vendor_id = $this->_request->getParam('vendor_id');
        $taxing_scheme_id = $this->_request->getParam('taxing_scheme_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseTaxOrderIds($vendor_id,$taxing_scheme_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::getPurchaseForTax($poId['id'],$vendor_id,$taxing_scheme_id);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'po_id' => $vData['po_id'],
                    'po_date' => $vData['po_date'],
                    'vendor_name' => $vData['vendor_name'],                    
                    'currency_code' => $vData['currency_code'],
                    'sub_total' => $vData['sub_total'],
                    'freight' => $vData['freight'],
                    'tax1_amount' => $vData['tax1_amount'],
                    'tax2_amount' => $vData['tax2_amount']
                );  
            }
            $Purtemp[$poId['tax_scheme_name']] =$purArr;
        }  
        
        $this->view->vendor_id =$vendor_id;
        $this->view->taxing_scheme_id =$taxing_scheme_id;
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function printPurchaseTaxAction() {
        $this->_helper->layout->setLayout('print');
        $vendor_id = $this->_request->getParam('vendor_id');
        $taxing_scheme_id = $this->_request->getParam('taxing_scheme_id');
        $poIds = Application_Model_ReportsPeer::getPurchaseTaxOrderIds($vendor_id,$taxing_scheme_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::getPurchaseForTax($poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'po_id' => $vData['po_id'],
                    'po_date' => $vData['po_date'],
                    'vendor_name' => $vData['vendor_name'],                    
                    'currency_code' => $vData['currency_code'],
                    'sub_total' => $vData['sub_total'],
                    'freight' => $vData['freight'],
                    'tax1_amount' => $vData['tax1_amount'],
                    'tax2_amount' => $vData['tax2_amount']
                );  
            }
            $Purtemp[$poId['tax_scheme_name']] =$purArr;
        }  
        
        $this->view->pDetailsData =$Purtemp;
    }
    
    
    public function salesByProductSummaryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }
    
    public function getSalesByProductSummaryAction() {
      $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $soIds = Application_Model_ReportsPeer::getCategoryIdsFromSales($category_id,$product_name);      

        $salestemp = array();    
        foreach ($soIds as $soId){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesDetailsProducts($soId['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['so_id'],
                    'order_date' => $vData['order_date'], 
                    'quantity' => $vData['quantity'],
                    'price' => $vData['unit_price'],
                    'cost' => $vData['cost'],
                    'sub_total' => $vData['sub_total']
                   
                );  
            }
            $salestemp[$soId['category']] =$salesArr;
        }  
                
        $this->view->category_id =$category_id;
        $this->view->product_name =$product_name;
      
        $this->view->pDetailsData =$salestemp;
    }
    
    public function printSalesByProductSummaryAction() {
        $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $soIds = Application_Model_ReportsPeer::getCategoryIdsFromSales($category_id,$product_name);      

        $salestemp = array();    
        foreach ($soIds as $soId){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesDetailsProducts($soId['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['so_id'],
                    'order_date' => $vData['order_date'], 
                    'quantity' => $vData['quantity'],
                    'price' => $vData['unit_price'],
                    'cost' => $vData['cost'],
                    'sub_total' => $vData['sub_total']
                   
                );  
            }
            $salestemp[$soId['category']] =$salesArr;
        } 
      
        $this->view->pDetailsData =$salestemp;
    }
    
    
    public function salesByProductDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
                $productArr[$i]['text'] = $val['product_name'];
                $productArr[$i]['value'] = $val['id'];
                $i++;
        }
       
        $this->view->productData = json_encode($productArr);
    }
    
    public function getSalesByProductDetailsAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $soIds = Application_Model_ReportsPeer::getCategoryIdsFromSales($category_id,$product_name);      

        $salestemp = array();    
        foreach ($soIds as $soId){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesProducts($soId['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['so_id'],
                    'order_date' => $vData['order_date'], 
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total']
                   
                );  
            }
            $salestemp[$soId['category']] =$salesArr;
        }  
                
        $this->view->category_id =$category_id;
        $this->view->product_name =$product_name;
      
        $this->view->pDetailsData =$salestemp;
    }
    
    public function printSalesByProductDetailsAction() {
        $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $soIds = Application_Model_ReportsPeer::getCategoryIdsFromSales($category_id,$product_name);      

        $salestemp = array();    
        foreach ($soIds as $soId){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesProducts($soId['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'product_name' => $vData['product_name'],
                    'order_id' => $vData['so_id'],
                    'order_date' => $vData['order_date'], 
                    'quantity' => $vData['quantity'],
                    'sub_total' => $vData['sub_total']
                   
                );  
            }
            $salestemp[$soId['category']] =$salesArr;
        }  
      
        $this->view->pDetailsData =$salestemp;
    }
    
    public function salesOrderProfitAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
        
    }
    
    public function getSalesOrderProfitAction() {
      $this->_helper->layout->disableLayout();
        $inv_status = $this->_request->getParam('inv_status');
        $pay_status = $this->_request->getParam('pay_status');
        $customer_id = $this->_request->getParam('customer_id');
        $salesDetailsData = Application_Model_ReportsPeer::getSalesOrderProfit($inv_status,$pay_status,$customer_id);
       
        $this->view->inv_status =$inv_status;
        $this->view->pay_status =$pay_status;
        $this->view->customer_id =$customer_id;
      
        $this->view->reportData =$salesDetailsData;
    }
    
    public function printSalesOrderProfitAction() {
        $this->_helper->layout->setLayout('print');
        $inv_status = $this->_request->getParam('inv_status');
        $pay_status = $this->_request->getParam('pay_status');
        $customer_id = $this->_request->getParam('customer_id');
        $salesDetailsData = Application_Model_ReportsPeer::getSalesOrderProfit($inv_status,$pay_status,$customer_id);

      
        $this->view->reportData =$salesDetailsData;
    }
    
	public function salesOrderOperationalAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
    }
    
    public function getSalesOrderOperationalAction() {
      $this->_helper->layout->disableLayout();
        $pay_status = $this->_request->getParam('pay_status');
        $customer_id = $this->_request->getParam('customer_id');
        $payStateData = Application_Model_ReportsPeer::fetchOperationalData($pay_status,$customer_id);      

        $salestemp = array();    
        foreach ($payStateData as $payStatus){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesOrderOperational($payStatus['payment_status'],$customer_id);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'order_date' => $vData['order_date'],
                    'payment_status' => $vData['payment_status'],
                    'total_amount' => $vData['total_amount'], 
                    'ship_date' => $vData['req_ship_date'],
                    'customer' => $vData['customer'],
                    'order_id' => $vData['order_id']                   
                );  
            }
            $salestemp[$payStatus['payment_status']] =$salesArr;
        }  

       
        $this->view->pay_status =$pay_status;
        $this->view->customer_id =$customer_id;
      
        $this->view->pDetailsData =$salestemp;
    }
    
    public function printSalesOrderOperationalAction() {
        $this->_helper->layout->setLayout('print');
        $pay_status = $this->_request->getParam('pay_status');
        $customer_id = $this->_request->getParam('customer_id');
        $payStateData = Application_Model_ReportsPeer::fetchOperationalData($pay_status,$customer_id);      

        $salestemp = array();    
        foreach ($payStateData as $payStatus){
            $salesDetailsData = Application_Model_ReportsPeer::fetchSalesOrderOperational($payStatus['payment_status'],$customer_id);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'id' => $vData['id'],
                    'order_date' => $vData['order_date'],
                    'payment_status' => $vData['payment_status'],
                    'total_amount' => $vData['total_amount'], 
                    'ship_date' => $vData['req_ship_date'],
                    'customer' => $vData['customer'],
                    'order_id' => $vData['order_id']                   
                );  
            }
            $salestemp[$payStatus['payment_status']] =$salesArr;
        }  
      
        $this->view->pDetailsData =$salestemp;
    }
    
 	public function salesTaxAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->vendors = Application_Model_VendorPeer::fetchAllVendors();
        $this->view->taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
        
    }


    public function getSalesTaxAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $taxing_scheme_id = $this->_request->getParam('taxing_scheme_id');
        $poIds = Application_Model_ReportsPeer::getSalesTaxOrderIds($customer_id,$taxing_scheme_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::getSalesTax($customer_id,$poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'order_id' => $vData['order_id'],
                    'order_date' => $vData['order_date'],
                    'customer_name' => $vData['customer_name'],
                    'sub_total' => $vData['sub_total'],
                    'freight' => $vData['freight'],
                    'tax1_amount' => $vData['tax1_amount'],
                    'tax2_amount' => $vData['tax2_amount'],
                	'total_amount' => $vData['total_amount']
                );  
            }
            $Purtemp[$poId['tax_scheme_name']] =$purArr;
        }  
        
        $this->view->customer_id =$customer_id;
        $this->view->taxing_scheme_id =$taxing_scheme_id;
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function printSalesTaxAction() {
        $this->_helper->layout->setLayout('print');
         $customer_id = $this->_request->getParam('customer_id');
        $taxing_scheme_id = $this->_request->getParam('taxing_scheme_id');
        $poIds = Application_Model_ReportsPeer::getSalesTaxOrderIds($customer_id,$taxing_scheme_id);      
            
        $Purtemp = array();    
        foreach ($poIds as $poId){
            $purDetailsData = Application_Model_ReportsPeer::getSalesTax($customer_id,$poId['id']);
            $purArr = array();
            foreach ($purDetailsData as $vData){
                $purArr[] = array(
                    'id' => $vData['id'],
                    'order_id' => $vData['order_id'],
                    'order_date' => $vData['order_date'],
                    'customer_name' => $vData['customer_name'],
                    'sub_total' => $vData['sub_total'],
                    'freight' => $vData['freight'],
                    'tax1_amount' => $vData['tax1_amount'],
                    'tax2_amount' => $vData['tax2_amount'],
                	'total_amount' => $vData['total_amount']
                );  
            }
            $Purtemp[$poId['tax_scheme_name']] =$purArr;
        }  
      
        $this->view->pDetailsData =$Purtemp;
    }
    
    public function customerListAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
    
    }
    
    public function getCustomerListAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $customers = Application_Model_ReportsPeer::getCustomers($customer_id);
    
        $this->view->customer_id =$customer_id;
        $this->view->customerData =$customers;
    }
    
    public function printCustomerListAction() {
        $this->_helper->layout->setLayout('print');
        $customer_id = $this->_request->getParam('customer_id');
        $customers = Application_Model_ReportsPeer::getCustomers($customer_id);
    
        $this->view->customerData =$customers;
    }
    
    public function salesRepsOrderAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->salesReps = Application_Model_SaleRepsPeer::fetchAllReps();
         
    }
    
    
    public function getSalesRepsOrderAction() {
        $this->_helper->layout->disableLayout();
        $sales_reps_id = $this->_request->getParam('sales_reps_id');
        $salesData = Application_Model_ReportsPeer::getSalesRepsOrder($sales_reps_id);
    
        $this->view->sales_reps_id =$sales_reps_id;
        $this->view->salesData =$salesData;
    }
    
    public function printSalesRepsOrderAction() {
        $this->_helper->layout->setLayout('print');
     $sales_reps_id = $this->_request->getParam('sales_reps_id');
        $salesData = Application_Model_ReportsPeer::getSalesRepsOrder($sales_reps_id);
    
        $this->view->sales_reps_id =$sales_reps_id;
        $this->view->salesData =$salesData;
    }
    
    public function inventoryHistoryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }
         
        $this->view->productData = json_encode($productArr);
    }
    
    
    public function getInventoryHistoryAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $categories = Application_Model_ReportsPeer::getCategoryIdsForInventory($category_id,$product_name);      

        $salestemp = array();    
        foreach ($categories as $catData){
            $salesDetailsData = Application_Model_ReportsPeer::fetchInventoryHistory($catData['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'qty' => $vData['qty'],
                    'cost' => $vData['cost']
                   
                );  
            }
            $salestemp[$catData['category']] =$salesArr;
        } 
      
        ;
        $this->view->category_id = $category_id;
        $this->view->product_name = $product_name;
        $this->view->productData =$salestemp;
    }
    
    public function printInventoryHistoryAction() {
        $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $categories = Application_Model_ReportsPeer::getCategoryIdsForInventory($category_id,$product_name);      

        $salestemp = array();    
        foreach ($categories as $catData){
            $salesDetailsData = Application_Model_ReportsPeer::fetchInventoryHistory($catData['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'qty' => $vData['qty'],
                    'cost' => $vData['cost']
                   
                );  
            }
            $salestemp[$catData['category']] =$salesArr;
        };
        $this->view->productData =$salestemp;
    }
    
    public function inventoryMovementAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }
         
        $this->view->productData = json_encode($productArr);
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
    }
    
    
    
    public function getInventoryMovementAction() {
        $this->_helper->layout->disableLayout();
        $post = $this->getRequest()->getParams();
        $product_id = Application_Model_InventoryPeer::fetchProductId($post['product_name']);
        $productLocation = Application_Model_ReportsPeer::getInventoryHistory($product_id,$post['location'],$post['category_id']);
        $this->view->product_name = $product_id;
        $this->view->location = $post['location'];
        $this->view->category = $post['category_id'];
        
        $this->view->productData =$productLocation;
    }
    
    public function printInventoryMovementAction() {
        $this->_helper->layout->setLayout('print');
        $post = $this->getRequest()->getParams();
        $productLocation = Application_Model_ReportsPeer::getInventoryHistory($post['product_name'],$post['location'],$post['category']);
        $this->view->productData =$productLocation;
    }
    
    public function inventoryMovementSummaryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }
         
        $this->view->productData = json_encode($productArr);
        $this->view->locations = Application_Model_InventoryLocationPeer::fetchAllLocations();
    }
    
    public function getInventoryMovementSummaryAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $product_id = Application_Model_InventoryPeer::fetchProductId($product_name);
        $location = $this->_request->getParam('location');
        $locations = Application_Model_ReportsPeer::getLocationIdsForInventory($location,$category_id,$product_id);      
        $salestemp = array();    
        foreach ($locations as $locData){
            $movmentData = Application_Model_ReportsPeer::getInventoryHistory($product_id,$locData['id'],$category_id);
            $salesArr = array();
            foreach ($movmentData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'type' => $vData['type'], 
                    'order_id' => $vData['order_id'],
                    'movement_date' => $vData['movement_date'],
                    'location_name' => $vData['location_name'],
                    'qty_after' => $vData['quantity']   
                );  
            }
            $salestemp[$locData['location_name']] =$salesArr;
        } 
        $this->view->category_id = $category_id;
        $this->view->product_id = $product_id;
        $this->view->location_id = $location;
        $this->view->productData =$salestemp;
    }
    
    public function printInventoryMovementSummaryAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category');
        $product_id = $this->_request->getParam('product');
        $location = $this->_request->getParam('location');
        $locations = Application_Model_ReportsPeer::getLocationIdsForInventory($location,$category_id,$product_id);      
        $salestemp = array();    
        foreach ($locations as $locData){
            $movmentData = Application_Model_ReportsPeer::getInventoryHistory($product_id,$locData['id'],$category_id);
            $salesArr = array();
            foreach ($movmentData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'type' => $vData['type'], 
                    'order_id' => $vData['order_id'],
                    'movement_date' => $vData['movement_date'],
                    'location_name' => $vData['location_name'],
                    'qty_after' => $vData['quantity']   
                );  
            }
            $salestemp[$locData['location_name']] =$salesArr;
        } 
        $this->view->productData =$salestemp;
    }
    
    public function customerPaymentSummaryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();         
    }
    
    
    public function getCustomerPaymentSummaryAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);
    
        $this->view->customer_id =$customer_id;
        $this->view->salesData =$salesData;
    }
    
    public function printCustomerPaymentSummaryAction() {
        $this->_helper->layout->setLayout('print');
         $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);
    
        $this->view->customer_id =$customer_id;
        $this->view->salesData =$salesData;
    }
    
    public function customerPaymentDetailsAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
    }
    
    
    public function getCustomerPaymentDetailsAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);
    
        $this->view->customer_id =$customer_id;
        $this->view->salesData =$salesData;
    }
    
    public function printCustomerPaymentDetailsAction() {
        $this->_helper->layout->setLayout('print');
        $customer_id = $this->_request->getParam('customer_id');
        $salesData = Application_Model_ReportsPeer::getSalesSummary($customer_id);
    
        $this->view->customer_id =$customer_id;
        $this->view->salesData =$salesData;
    }
    
    
    public function customerOrderHistoryAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
    }
    
    public function getCustomerOrderHistoryAction() {
        $this->_helper->layout->disableLayout();
        $customer_id = $this->_request->getParam('customer_id');
        $customers = Application_Model_ReportsPeer::getCustomersFromSales($customer_id);
    
        $salestemp = array();
       
        foreach ($customers as $customer){
            $OrdersData = Application_Model_ReportsPeer::getCustomersOrderId($customer['customer_id']);
            $salesArr = array();
            $custArr = array();
            foreach ($OrdersData as $oData){
                $salesArr = array();
                $OrderProductData = Application_Model_ReportsPeer::fetchCustomersProductsBYOrderId($oData['id']);
                foreach ($OrderProductData as $vData){
                    $salesArr[] = array(
                        'id' => $vData['id'],
                        'product_name' => $vData['product_name'],
                        'order_id' => $vData['so_id'],
                        'order_date' => $vData['order_date'],
                        'quantity' => $vData['quantity'],
                        'sub_total' => $vData['sub_total'],
                        'payment_status' => $vData['payment_status'],
                        'category' => $vData['category']
                    );
                }
                $custArr[$oData['order_id']] =  $salesArr;
                
            }
            $salestemp[$customer['customer_name']] =$custArr;
        }
    
//         echo '<pre>';
//         print_r($salestemp);exit;
        $this->view->customer_id =$customer_id;
        $this->view->pDetailsData =$salestemp;
    }
    
   
    
    public function printCustomerOrderHistoryAction() {
        $this->_helper->layout->setLayout('print');
        $customer_id = $this->_request->getParam('customer_id');
        $customers = Application_Model_ReportsPeer::getCustomersFromSales($customer_id);
    
        $salestemp = array();
       
        foreach ($customers as $customer){
            $OrdersData = Application_Model_ReportsPeer::getCustomersOrderId($customer['customer_id']);
            $salesArr = array();
            $custArr = array();
            foreach ($OrdersData as $oData){
                $salesArr = array();
                $OrderProductData = Application_Model_ReportsPeer::fetchCustomersProductsBYOrderId($oData['id']);
                foreach ($OrderProductData as $vData){
                    $salesArr[] = array(
                        'id' => $vData['id'],
                        'product_name' => $vData['product_name'],
                        'order_id' => $vData['so_id'],
                        'order_date' => $vData['order_date'],
                        'quantity' => $vData['quantity'],
                        'sub_total' => $vData['sub_total'],
                        'payment_status' => $vData['payment_status'],
                        'category' => $vData['category']
                    );
                }
                $custArr[$oData['order_id']] =  $salesArr;
                
            }
            $salestemp[$customer['customer_name']] =$custArr;
        }

        $this->view->pDetailsData =$salestemp;
    }
    
    
    public function productCustomerReportAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->customers = Application_Model_CustomerPeer::fetchAllCustomers();
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }
         
        $this->view->productData = json_encode($productArr);
    }
    
    public function getProductCustomerReportAction() {
        $this->_helper->layout->disableLayout();
        $customerId = $this->_request->getParam('customer_id');
        $categoryId = $this->_request->getParam('cat_id');
        $productName = $this->_request->getParam('product_name');
        $cateData = Application_Model_ReportsPeer::getCategoryFromSales($customerId,$categoryId,$productName);

        $salestemp = array();
         
        foreach ($cateData as $cat){
            $OrdersData = Application_Model_ReportsPeer::getProductsByCategory($cat['cat_id'],$customerId,$productName);
            $salesArr = array();
            $custArr = array();
            foreach ($OrdersData as $oData){
                $salesArr = array();
                $OrderProductData = Application_Model_ReportsPeer::fetchOrdersByProducts($oData['product_id'],$customerId,$categoryId);
                foreach ($OrderProductData as $vData){
                    $salesArr[] = array(
                        'id' => $vData['id'],
                        'product_name' => $vData['product_name'],
                        'order_id' => $vData['so_id'],
                        'order_date' => $vData['order_date'],
                        'quantity' => $vData['quantity'],
                        'sub_total' => $vData['sub_total'],
                        'payment_status' => $vData['payment_status'],
                        'order_status' => $vData['order_status'],
                        'customer_name' => $vData['customer_name']
                    );
                }
                $custArr[$oData['product_name']] =  $salesArr;
    
            }
            $salestemp[$cat['category_name']] =$custArr;
        }
    
//        echo '<pre>';
//        print_r($salestemp);exit;
        $this->view->customerId =$customerId;
        $this->view->categoryId =$categoryId;
        $this->view->productName =$productName;
        $this->view->pDetailsData =$salestemp;
    }
    
     
    
    public function printProductCustomerReportAction() {
        $this->_helper->layout->setLayout('print');
        $customerId = $this->_request->getParam('customer_id');
        $categoryId = $this->_request->getParam('cat_id');
        $productName = $this->_request->getParam('product_name');
        $cateData = Application_Model_ReportsPeer::getCategoryFromSales($customerId,$categoryId,$productName);

        $salestemp = array();
         
        foreach ($cateData as $cat){
            $OrdersData = Application_Model_ReportsPeer::getProductsByCategory($cat['cat_id'],$customerId,$productName);
            $salesArr = array();
            $custArr = array();
            foreach ($OrdersData as $oData){
                $salesArr = array();
                $OrderProductData = Application_Model_ReportsPeer::fetchOrdersByProducts($oData['product_id'],$customerId,$categoryId);
                foreach ($OrderProductData as $vData){
                    $salesArr[] = array(
                        'id' => $vData['id'],
                        'product_name' => $vData['product_name'],
                        'order_id' => $vData['so_id'],
                        'order_date' => $vData['order_date'],
                        'quantity' => $vData['quantity'],
                        'sub_total' => $vData['sub_total'],
                        'payment_status' => $vData['payment_status'],
                        'order_status' => $vData['order_status'],
                        'customer_name' => $vData['customer_name']
                    );
                }
                $custArr[$oData['product_name']] =  $salesArr;
    
            }
            $salestemp[$cat['category_name']] =$custArr;
        }
    
        $this->view->pDetailsData =$salestemp;
    }
    
    
    public function productCostAction() {
        $this->_helper->layout->setLayout('layout');
        $this->view->productCategory = Application_Model_ProductPeer::fetchAllProductCategory();
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $i=0;
        foreach ($product as $key => $val) {
            $productArr[$i]['text'] = $val['product_name'];
            $productArr[$i]['value'] = $val['id'];
            $i++;
        }
         
        $this->view->productData = json_encode($productArr);
    }
    
    
    public function getProductCostAction() {
        $this->_helper->layout->disableLayout();
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $categories = Application_Model_ReportsPeer::getCategoryIdsForInventory($category_id,$product_name);      

        $salestemp = array();    
        foreach ($categories as $catData){
            $salesDetailsData = Application_Model_ReportsPeer::fetchProductCost($catData['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'type' => $vData['type'], 
                    'order_id' => $vData['order_id'],
                    'movement_date' => $vData['movement_date'],
                    'qty' => $vData['qty'],
                    'qty_after' => $vData['qty_after'],
                    'totAmount' => $vData['totAmount'],
                    'total_cost' => $vData['total_cost'],
                    'cost' => $vData['cost']
                   
                );  
            }
            $salestemp[$catData['category']] =$salesArr;
        } 
        $this->view->category_id = $category_id;
        $this->view->product_name = $product_name;
        $this->view->productData =$salestemp;
    }
    
    public function printProductCostAction() {
        $this->_helper->layout->setLayout('print');
        $category_id = $this->_request->getParam('category_id');
        $product_name = $this->_request->getParam('product_name');
        $categories = Application_Model_ReportsPeer::getCategoryIdsForInventory($category_id,$product_name);      

        $salestemp = array();    
        foreach ($categories as $catData){
            $salesDetailsData = Application_Model_ReportsPeer::fetchProductCost($catData['id'],$product_name);
            $salesArr = array();
            foreach ($salesDetailsData as $vData){
                $salesArr[] = array(
                    'product_name' => $vData['product_name'], 
                    'type' => $vData['type'], 
                    'order_id' => $vData['order_id'],
                    'movement_date' => $vData['movement_date'],
                    'qty' => $vData['qty'],
                    'qty_after' => $vData['qty_after'],
                    'totAmount' => $vData['totAmount'],
                    'total_cost' => $vData['total_cost'],
                    'cost' => $vData['cost']
                   
                );  
            }
            $salestemp[$catData['category']] =$salesArr;
        } 
        $this->view->productData =$salestemp;
    }
    
    
    
}
