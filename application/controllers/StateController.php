<?php

/**
 * IndexController
 *
 * @package IMS
 * @author  Amit kumar
 * @version
 *
 */
class StateController extends DMC_Controller_Abstract {

    protected $model;
    private $_addForm = null;

    public function init() {
        parent::init();
        /*
         * Initialise a session called 'Default'
         */
        $sessionNamespace = new Zend_Session_Namespace('Default');

        /*
         * Is the user logged in?  If not re-direct to login screen
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->layout->setLayout('authlayout');
            /*
             * Add the entrance URL to the session so the auth controller can redirect here
             */
            $sessionNamespace->authRedirect = $this->getRequest()->getRequestUri();
            $this->_redirect('/login');
        }

        $this->model = new Application_Model_Generic();
        $this->view->menuData = $this->model->getMenu();

        $controllerName = $this->_request->getControllerName();
        $screenData = $this->model->getScreenData('/' . $controllerName);
        
        //Validate access
        $userAccess = $this->model->validateScreen($screenData['id'],'view');   
        if($userAccess == 0){
             $this->_helper->flashMessenger('You are not Authorized to access this section.');
            $this->_redirect('/');
        }
        $this->view->screenName = $screenData['display_name'];
    }

    /*
     * Index Action
     * this is default action to show the home page.
     * @return
     */

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');
        $modelState = new Application_Model_State();
        $modelPeer = new Application_Model_StatePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $status = $this->_request->getParam('status');

        $this->view->id = $id;
        $this->_addForm = new Application_Form_State();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }

        $this->_addForm->country->setMultiOptions($statusData);
        //save and update record using POST method

        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelState->setId($post['id']);
                $modelState->setCountry_id($post['country']);
                $modelState->setState_name($post['state_name']);
                $result = $modelState->checkStateIsPresnt();
                if($result) {
                    $this->_redirect('/state?status=present');
                }
                $modelState->setUpdated(date('Y-m-d H:i:s'));
                $modelState->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelState->setCreated(date('Y-m-d H:i:s'));
                $modelState->setCreated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelState->save();

                $this->_redirect('/state?status=success');
            }
            if ($modelState->getId()) {
                $this->view->colStatus = 'N';
            } else {
                $this->view->colStatus = 'Y';
            }
        }

        if ($status == 'present') {
            $this->view->successMsg = 'State is already present.';
        }
        if ($status == 'success') {
            $this->view->successMsg = 'State has been added successfully.';
        }
        if ($status == 'updated') {
            $this->view->successMsg = 'State has been updated successfully.';
        }
        if ($status == 'deleted') {
            $this->view->successMsg = 'State has been deleted successfully.';
        }
        $a = $modelPeer->fetchAllState();
        $this->view->details = $modelPeer->fetchAllState();
        $this->view->form = $this->_addForm;
    }

    /*
     * Edit Action
     * this is default action to edit the content in user table.
     * @return
     */

    public function editAction() {
        $this->_helper->layout->setLayout('layout');
        $modelState = new Application_Model_State();
        $modelPeer = new Application_Model_StatePeer();

        $request = $this->getRequest();
        $id = $this->_request->getParam('id');
        $this->view->id = $id;
        $this->_addForm = new Application_Form_State();

        $genericPeer = new Application_Model_GenericPeer();
        $roles = $genericPeer->fetchRoles();

        $rolesData = array();
        $rolesData[''] = 'Please Select';
        foreach ($roles as $key => $val) {
            $rolesData[$val['id']] = $val['role_name'];
        }

        $country = $modelPeer->fetchAllCountries();
        $statusData = array();
        $statusData[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $statusData[$val['id']] = $val['country_name'];
        }
        $this->_addForm->country->setMultiOptions($statusData);
        //Populate all value on edit mode
        if ($id) {
            $modelState->setId($id);
            $CountryData = $modelState->fetchStateById();
            $this->_addForm->populate(
                    array(
                        'id' => $id,
                        'state_name' => $CountryData['state_name'],
                        'country' => $CountryData['country_id']
                    )
            );
            $this->_addForm->submitbtn->setLabel("Update State");
            $this->_addForm->setAction("/state/edit/id/" . $id);
            ;
        }

        //save and update record using POST method
        if ($this->getRequest()->isPost()) {
            if ($this->_addForm->isValid($request->getPost())) {
                $post = $request->getPost();
                $modelState->setId($post['id']);
                $modelState->setCountry_id($post['country']);
                $modelState->setState_name($post['state_name']);
                $modelState->setUpdated(date('Y-m-d H:i:s'));
                $modelState->setUpdated_by(Zend_Auth::getInstance()->getIdentity()->id);
                $modelState->save();

                $this->_redirect('/state?status=updated');
            }
        }

        $this->view->form = $this->_addForm;
    }

    /*
     * delete Action
     * use to delete data from user table
     * @return
     */

    public function deleteAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_request->getParam('id');
        $model = new Application_Model_State();
        $model->setId($id);
        $this->view->usersData = $model->deleteData('mas_states');
        exit;
    }

}
