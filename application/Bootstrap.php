<?php

/**
 * This file contains bootstrap settings for the application
 * 
 * @category   IMS
 * @package    Bootstrap
 * 
 * @author Amit Kumar <amitk@damcogroup.com>
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initDoctype() {
        
    }

    /**
     * Initialise the application
     */

    /**
     * Init Application
     * This method is used to initialize the application level settings\
     * 
     * @access protected
     * @return boolean
     */
    protected function _initApplication() {

        require_once 'Zend/Controller/Plugin/Abstract.php';
        require_once 'Zend/Controller/Front.php';
        require_once 'Zend/Controller/Request/Abstract.php';
        require_once 'Zend/Controller/Action/HelperBroker.php';


        /**
         * Zend Autoloader
         */
        require_once 'Zend/Loader/Autoloader.php';
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('DMC_');


        $root = realpath(dirname(__FILE__) . '/../');
        $this->_root = $root;

        if (!defined('TEMPUPLOAD'))
            define('TEMPUPLOAD', $this->_root . '/www/upload/products/');
        
        /**
         * Configuration settings
         */
        $config = new Zend_Config_Ini('../configs/config.ini');
        Zend_Registry::set('config', $config);     


        /**
         * Logging
         */
        $writer = new Zend_Log_Writer_Stream($config->log->app);
        $appLog = new Zend_Log($writer);
        Zend_Registry::set('appLog', $appLog);

        /**
         * Database setup
         */
        $db = Zend_Db::factory('PDO_MYSQL', array(
                    'host' => $config->db->params->host,
                    'username' => $config->db->params->username,
                    'password' => $config->db->params->password,
                    'dbname' => $config->db->params->dbname,
                    'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8;')
                        )
        );

        Zend_Db_Table_Abstract::setDefaultAdapter($db);

        Zend_Registry::set('db', $db);

        /**
         * set up session storage
         */
        $sessionDbConfig = array(
            'name' => 'session',
            'primary' => 'id',
            'modifiedColumn' => 'modified',
            'dataColumn' => 'data',
            'lifetimeColumn' => 'lifetime'
        );


        //create your Zend_Session_SaveHandler_DbTable and
        //set the save handler for Zend_Session
        Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($sessionDbConfig));
        Zend_Session::setOptions(array('strict' => true));
        //start your session!
        Zend_Session::start();


        return TRUE;
    }

    /**
     * Init Routes
     * This method is used to initialize the routes
     * 
     * @access public
     * @return void
     */
    protected function _initRoutes() {

        $ctrl = Zend_Controller_Front::getInstance();
        $router = $ctrl->getRouter();

        $routes = array(
            array('name' => 'login', 'url' => '/login', 'controller' => 'login', 'action' => 'index', 'extra' => array()),
            array('name' => 'dashboard', 'url' => '/dashboard', 'controller' => 'dashboard', 'action' => 'index', 'extra' => array()),
            array('name' => 'logout', 'url' => 'logout', 'controller' => 'index', 'action' => 'logout', 'extra' => array()),
        );

        foreach ($routes as $route) {
            $router->addRoute(
                    $route['name'], new Zend_Controller_Router_Route(
                    $route['url'], array(
                'controller' => $route['controller'],
                'action' => $route['action']
                    ), $route['extra']
                    )
            );
        }
    }

    /**
     * Init View Helper Path
     * This method is used to set the view helper path
     * 
     * @access public
     */
    public function _initViewHelperPath() {

        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->addHelperPath(
                APPLICATION_PATH . '/views/helpers/', 'Application_View_Helper');
    }

    /**
     * Init Action Helper Path
     * 
     * @access public
     * @return void
     */
    public function _initActionHelperPath() {
        // Action Helpers
        Zend_Controller_Action_HelperBroker::addPath(
                APPLICATION_PATH . '/controllers/helpers');
    }

}

