<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_ReportsPeer {

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getInventorySummary($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }
            $sql = "SELECT 
                    p.id,
                    p.`product_name`,
                    IFNULL(pq.quantity, 0) AS qty_owned,
                    IFNULL(SUM(pur.`quantity`), 0) AS qty_on_order,
                    IFNULL(SUM(sal.`quantity`), 0) AS qty_reserved,
                    IFNULL(pc.cost, 0.00) AS cost 
                  FROM
                    `tra_inventory_product_quantity` AS pq 
                    LEFT JOIN `tra_inventory_product` AS p 
                      ON p.`id` = pq.`product_id` 
                    LEFT JOIN 
                      (SELECT 
                        SUM(det.`quantity`) AS quantity,
                        det.`product_id` 
                      FROM
                        `tra_purchase_order_mas` AS mas 
                        INNER JOIN `tra_purchase_order_det` AS det 
                          ON det.`order_id` = mas.id 
                          AND det.`item_status` = 'Order' 
                      WHERE mas.`order_status` = 'Unfulfilled' 
                      GROUP BY det.`product_id`) AS pur 
                      ON pur.product_id = pq.`product_id` 
                    LEFT JOIN 
                      (SELECT 
                        SUM(det.`quantity`) AS quantity,
                        det.`product_id` 
                      FROM
                        `tra_sales_order_mas` AS mas 
                        INNER JOIN `tra_sales_order_det` AS det 
                          ON det.`order_id` = mas.id 
                          AND det.`item_status` = 'Order' 
                      WHERE mas.`order_status` = 'Unfulfilled' 
                      GROUP BY det.`product_id`) AS sal 
                      ON sal.product_id = pq.`product_id` 
                    LEFT JOIN 
                      (SELECT 
                        cost,
                        `product_id` 
                      FROM
                        `tra_inventory_product_cost` 
                      GROUP BY product_id) AS pc 
                      ON pc.product_id = pq.`product_id` 
                  WHERE p.product_name <> '' 
                  GROUP BY pq.`product_id`  " . $str . ' ' . $str1 . "    
                  
            ";

            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public static function getInventoryDetails($location, $category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }

            $sql = "SELECT 
                    p.id,
                    p.`product_name`,
                    pq.`quantity`,
                    pq.`location_id`,
                    l.`name` as location,
                    p.`product_category_id`,
                    c.name as category,
                    pc.cost

                  FROM
                    `tra_inventory_product_quantity` AS pq 
                    INNER JOIN `tra_inventory_product` AS p 
                      ON p.`id` = pq.`product_id`  
                    LEFT JOIN `mas_inventory_location` AS l
                    ON l.id = pq.location_id
                     LEFT JOIN `mas_product_category` AS c
                    ON c.id = p.product_category_id
                    LEFT JOIN (
                    SELECT cost,`product_id`  
                    FROM `tra_inventory_product_cost`
                      GROUP BY product_id) AS pc
                     ON pc.product_id = pq.`product_id`
                  WHERE pq.location_id=:location   " . $str . ' ' . $str1 . "   
            ";
            $statement = $db->prepare($sql);

            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }

            $statement->bindValue('location', $location);
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public static function getInventoryLocations($category_id, $product_name, $location) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }

            if ($location) {
                $str2 = "AND pq.location_id=:location";
            }

            $sql = "SELECT 
                    distinct pq.`location_id`,
                    l.`name` as location_name
                  FROM
                    `tra_inventory_product_quantity` AS pq 
                    INNER JOIN `tra_inventory_product` AS p 
                      ON p.`id` = pq.`product_id`  
                    LEFT JOIN `mas_inventory_location` AS l
                    ON l.id = pq.location_id
                  WHERE p.product_name <> '' " . $str . ' ' . $str1 . ' ' . $str2 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            if ($location) {
                $statement->bindValue('location', $location);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public static function fetchAllLocations() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					id as location_id,name as location_name
                                      FROM mas_inventory_location
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getProductList($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }
            $sql = "SELECT DISTINCT 
                        p.id,
                        p.`product_name`,
                        ifnull(pc.price ,'0.00') as price
                      FROM
                        `tra_inventory_product_quantity` AS pq 
                        INNER JOIN `tra_inventory_product` AS p 
                          ON p.`id` = pq.`product_id` 
                        LEFT JOIN 
                          (SELECT DISTINCT 
                            ipc.product_id,
                            ipc.price,
                            ipc.id 
                          FROM
                            `tra_inventory_product_cost` AS ipc 
                            LEFT JOIN `mas_pricing_currency` AS pcur 
                              ON pcur.id = ipc.`price_currency_id` 
                              AND pcur.`default` = 1 
                          GROUP BY product_id) AS pc 
                          ON pc.product_id = p.`id` 
                      WHERE p.product_name <> ''  " . $str . ' ' . $str1 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getVendorProductList($category_id, $product_name, $vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }
            if ($vendor_id) {
                $str2 = "AND pv.vendor_id=:vendor_id";
            }
            $sql = "SELECT DISTINCT 
                    p.id,
                    p.`product_name`,
                    p.`product_category_id`,
                    pc.`name` AS category,
                    v.`name` as vendor_name,
                    pv.`vendor_product_code`,
                    IFNULL(pv.`cost`, '0.00') AS price 
                  FROM
                    `mas_vendors_product` AS pv 
                    INNER JOIN `tra_inventory_product` AS p 
                      ON p.`id` = pv.`product_id`
                    INNER JOIN `mas_vendors` AS v 
                      ON v.`id` = pv.`vendor_id` 
                    INNER JOIN `mas_product_category` AS pc 
                      ON pc.`id` = p.`product_category_id`  
                      WHERE p.product_name <> ''  " . $str . ' ' . $str1 . ' ' . $str2 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getVendorList($category_id, $product_name, $vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($category_id) {
                $str = "AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = "AND p.product_name=:product_name";
            }
            if ($vendor_id) {
                $str2 = "AND pv.vendor_id=:vendor_id";
            }
            $sql = "SELECT DISTINCT 
                    v.id as vendor_id,
                    v.`name` as vendor_name
                  FROM
                    `mas_vendors_product` AS pv 
                    INNER JOIN `tra_inventory_product` AS p 
                      ON p.`id` = pv.`product_id`
                    LEFT JOIN `mas_vendors` AS v 
                      ON v.`id` = pv.`vendor_id` 
                    LEFT JOIN `mas_product_category` AS pc 
                      ON pc.`id` = p.`product_category_id`  
                      WHERE v.name <> ''  " . $str . ' ' . $str1 . ' ' . $str2 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getVendors($payment_terms, $taxing_schemes, $currency) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($payment_terms) {
                $str = "AND payment_terms=:payment_terms";
            }
            if ($taxing_schemes) {
                $str1 = "AND taxing_schemes=:taxing_schemes";
            }
            if ($currency) {
                $str2 = "AND currency_id=:currency_id";
            }
            $sql = "SELECT DISTINCT 
                    id AS vendor_id,
                    `name` AS vendor_name,
                    `contact_name`,
                    `contact_email`,
                    `contact_phone`,
                    `address`
                  FROM
                    `mas_vendors`
                      WHERE is_active =1  " . $str . ' ' . $str1 . ' ' . $str2 . "    
            ";
            $statement = $db->prepare($sql);
            if ($payment_terms) {
                $statement->bindValue('payment_terms', $payment_terms);
            }
            if ($taxing_schemes) {
                $statement->bindValue('taxing_schemes', $taxing_schemes);
            }
            if ($currency) {
                $statement->bindValue('currency_id', $currency);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getPurchaseSummary($vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($vendor_id) {
                $str = "AND p.vendor_id=:vendor_id";
            }
//            if($taxing_schemes){
//                $str1 = "AND taxing_schemes=:taxing_schemes";
//            }
//            if($currency){
//                $str2 = "AND currency_id=:currency_id";
//            }
            $sql = "SELECT 
                    p.`id`,
                    p.`po_id`,
                    date_format(p.`po_date`,'%d/%m/%Y') as po_date,
                    date_format(p.`req_shipping_date`,'%d/%m/%Y') as req_shipping_date,
                    date_format(p.`due_date`,'%d/%m/%Y') as due_date,
                    p.`order_status`,
                    p.`payment_status`,
                    p.`vendor_id`,
                    p.`currency_id`,
                    p.`total_amount`,
                    p.`paid_amount`,
                    p.`balance` ,
                    v.`name` AS vendor_name,
                    c.`currency_code`
                  FROM
                `tra_purchase_order_mas` AS p 
                INNER JOIN `mas_vendors` AS v ON v.id = p.`vendor_id` 
                Left JOIN `mas_currency` AS c ON c.id = p.`currency_id` 
                  WHERE is_active =1  " . $str . "    
            ";
            $statement = $db->prepare($sql);
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }
//            if($product_name){
//                $statement->bindValue('taxing_schemes', $taxing_schemes);
//            }
//            if($vendor_id){
//                $statement->bindValue('currency_id', $currency);
//            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getPurchaseDetialsOrderIds($vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($vendor_id) {
                $str = "AND p.vendor_id=:vendor_id";
            }
//            if($taxing_schemes){
//                $str1 = "AND taxing_schemes=:taxing_schemes";
//            }
//            if($currency){
//                $str2 = "AND currency_id=:currency_id";
//            }
            $sql = "SELECT 
                    distinct p.`id`
                  FROM
                `tra_purchase_order_mas` AS p 
                INNER JOIN `mas_vendors` AS v ON v.id = p.`vendor_id` 
                INNER JOIN `mas_currency` AS c ON c.id = p.`currency_id` 
                  WHERE is_active =1  " . $str . "    
            ";
            $statement = $db->prepare($sql);
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }
//            if($product_name){
//                $statement->bindValue('taxing_schemes', $taxing_schemes);
//            }
//            if($vendor_id){
//                $statement->bindValue('currency_id', $currency);
//            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseProducts($order_id, $vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if ($vendor_id) {
                $str = "AND mas.vendor_id=:vendor_id";
            }
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        date_format(mas.`po_date`,"%d/%m/%Y") as po_date,
                                        mas.`payment_status`,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        c.currency_code,
                                        v.name as vendor_name
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas on mas.id = det.order_id
                                        INNER JOIN `mas_vendors` AS v ON v.id = mas.`vendor_id` 
                                        INNER JOIN `mas_currency` AS c ON c.id = mas.`currency_id` 
                                       where det.order_id=:order_id and det.item_status="Order" ' . $str . ' ');
            $statement->bindValue('order_id', $order_id);
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesSummary($customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';

            if ($customer_id) {
                $str = "AND s.customer_id=:customer_id";
            }
            $sql = "SELECT 
                    s.`id`,
                    s.`order_id`,
                    DATE_FORMAT(s.`order_date`, '%d/%m/%Y') AS order_date,
                    DATE_FORMAT(s.`last_pay_date`, '%d/%m/%Y') AS last_payment_date,
                    DATE_FORMAT(s.`due_date`, '%d/%m/%Y') AS due_date,
                    s.`order_status`,
                    s.`payment_status`,
                    s.`customer_id`,
                    cus.`name` AS customer_name,
                    IFNULL(s.`sub_total`,0.00) AS sub_total,
                    IFNULL(s.`freight`,0.00) AS freight,
                    IFNULL(s.total_amount,0.00) AS total_amount,
                    IFNULL(s.paid_amount,0.00) AS paid_amount,
                    IFNULL(s.balance,0.00) AS balance,
                    s.customer_phone,
                    s.customer_name AS contact,
                    (
                      IFNULL(s.`tax1_amount`,0.00) + IFNULL(s.`tax2_amount`,0.00)
                    ) AS total_tax 
                  FROM
                    `tra_sales_order_mas` AS s 
                    INNER JOIN `mas_customers` AS cus 
                      ON cus.id = s.`customer_id` 
                  WHERE s.customer_id <> ''  " . $str . "    
            ";
            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
//            if($product_name){
//                $statement->bindValue('taxing_schemes', $taxing_schemes);
//            }
//            if($vendor_id){
//                $statement->bindValue('currency_id', $currency);
//            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all purchase records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getPurchaseTaxOrderIds($vendor_id, $taxing_scheme_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($vendor_id) {
                $str = "AND p.vendor_id=:vendor_id";
            }
            if ($taxing_scheme_id) {
                $str1 = "AND p.taxing_scheme_id=:taxing_schemes";
            }
            $sql = "SELECT 
                    distinct p.`taxing_scheme_id` as id,t.tax_scheme_name
                  FROM
                `tra_purchase_order_mas` AS p 
                INNER JOIN `mas_taxing_schemes` AS t ON t.id = p.`taxing_scheme_id` 
                  WHERE p.vendor_id <> ''  " . $str . "     " . $str1 . " 
            ";
            $statement = $db->prepare($sql);
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }
            if ($taxing_scheme_id) {
                $statement->bindValue('taxing_schemes', $taxing_scheme_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getPurchaseForTax($taxing_scheme_id, $vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($taxing_scheme_id) {
                $str = "AND p.taxing_scheme_id=:taxing_scheme_id";
            }

            if ($vendor_id) {
                $str1 = "AND p.vendor_id=:vendor_id";
            }


            $sql = "SELECT 
                    p.`id`,
                    p.`po_id`,
                    date_format(p.`po_date`,'%d/%m/%Y') as po_date,
                    p.`order_status`,
                    p.`payment_status`,
                    p.`vendor_id`,
                    p.`currency_id`,
                    p.`freight`,
                    p.`sub_total`,
                    p.`tax1_amount`,
                    p.`tax2_amount`,
                    v.`name` AS vendor_name,
                    c.`currency_code`
                  FROM
                `tra_purchase_order_mas` AS p 
                INNER JOIN `mas_vendors` AS v ON v.id = p.`vendor_id` 
                INNER JOIN `mas_currency` AS c ON c.id = p.`currency_id` 
                  WHERE p.vendor_id <> ''  " . $str . "  " . $str1 . "   
            ";
            $statement = $db->prepare($sql);
            if ($taxing_scheme_id) {
                $statement->bindValue('taxing_scheme_id', $taxing_scheme_id);
            }
            if ($vendor_id) {
                $statement->bindValue('vendor_id', $vendor_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCategoryIdsFromSales($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($category_id) {
                $str = " AND c.id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND p.product_name =:product_name";
            }

            $sql = "SELECT 
                    distinct c.`id`,c.name as category
                  FROM
                `tra_sales_order_mas` AS s 
                INNER JOIN `tra_sales_order_det` AS det ON det.order_id = s.`id` 
                INNER JOIN `tra_inventory_product` AS p ON p.id = det.product_id
                INNER JOIN `mas_product_category` AS c ON c.id = p.product_category_id
               WHERE s.order_id <> ''  " . $str . " " . $str1 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchSalesProducts($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';


            if ($category_id) {
                $str = " AND ip.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND ip.product_name =:product_name";
            }

            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        mas.order_id as so_id,
                                        DATE_FORMAT(mas.`order_date`,"%d %M %Y") AS order_date,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total
                                        FROM 
                                        tra_sales_order_det as det
                                        INNER join tra_sales_order_mas as mas
                                            on mas.id = det.order_id
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                          
                                       where  det.item_status="Order" ' . $str . ' ' . $str1 . '
                                        ');
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchSalesDetailsProducts($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';


            if ($category_id) {
                $str = " AND ip.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND ip.product_name =:product_name";
            }

            $statement = $db->prepare('SELECT 
                                det.id,
                                det.order_id,
                                mas.order_id AS so_id,
                                DATE_FORMAT(mas.`order_date`, "%d %M %Y") AS order_date,
                                ip.product_name,
                                det.quantity,
                                det.unit_price,
                                pc.`cost`,
                                det.discount,
                                det.sub_total 
                              FROM
                                tra_sales_order_det AS det 
                                INNER JOIN tra_sales_order_mas AS mas 
                                  ON mas.id = det.order_id 
                                INNER JOIN tra_inventory_product ip 
                                  ON ip.id = det.product_id 
                                LEFT JOIN (
                                SELECT cost,`product_id`  
                                FROM `tra_inventory_product_cost`
                                  GROUP BY product_id) AS pc
                                 ON pc.product_id = det.`product_id`
                              WHERE det.item_status = "Order"  ' . $str . ' ' . $str1 . '
                                        ');
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchOperationalData($pay_status, $customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';


            if ($pay_status) {
                $str = " AND payment_status=:payment_status";
            }
            if ($customer_id) {
                $str1 = " AND customer_id =:customer_id";
            }

            $statement = $db->prepare('SELECT 
							 distinct `payment_status`
							FROM
							  tra_sales_order_mas
							  where payment_status<>""
							   ' . $str . ' ' . $str1 . '
                            ');
            if ($pay_status) {
                $statement->bindValue('payment_status', $pay_status);
            }
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales order operational');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchSalesOrderOperational($pay_status, $customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';


            if ($pay_status) {
                $str = " AND m.payment_status=:payment_status";
            }
            if ($customer_id) {
                $str1 = " AND m.customer_id =:customer_id";
            }

            $statement = $db->prepare('SELECT 
							  m.id,
							  m.order_id,
							  DATE_FORMAT(m.`order_date`, "%d %M %Y") AS order_date,
							  m.`payment_status`,
							  m.`total_amount`,
							  DATE_FORMAT(m.`req_ship_date`, "%d %M %Y") AS req_ship_date,
							  c.`name` AS customer 
							FROM
							  tra_sales_order_mas AS m 
							  INNER JOIN `mas_customers` c 
							    ON c.id = m.`customer_id`   ' . $str . ' ' . $str1 . '
                            ');
            if ($pay_status) {
                $statement->bindValue('payment_status', $pay_status);
            }
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales order operational');
        }
    }

    /**
     * get all purchase records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesTaxOrderIds($customer_id, $taxing_scheme_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($customer_id) {
                $str = "AND p.customer_id=:customer_id";
            }
            if ($taxing_scheme_id) {
                $str1 = "AND p.taxing_scheme_id=:taxing_schemes";
            }
            $sql = "SELECT 
                    distinct p.`taxing_scheme_id` as id,t.tax_scheme_name
                  FROM
                `tra_sales_order_mas` AS p 
                INNER JOIN `mas_taxing_schemes` AS t ON t.id = p.`taxing_scheme_id` 
                  WHERE p.customer_id <> ''  " . $str . "    " . $str1 . " 
            ";
            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            if ($taxing_scheme_id) {
                $statement->bindValue('taxing_schemes', $taxing_scheme_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales taxing schemes');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesTax($customer_id, $taxing_scheme_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($customer_id) {
                $str = "AND p.customer_id=:customer_id";
            }
            if ($taxing_scheme_id) {
                $str1 = "AND p.taxing_scheme_id=:taxing_scheme_id";
            }

            $sql = "SELECT 
                    p.`id`,
                    p.`order_id`,
                    date_format(p.`order_date`,'%d/%m/%Y') as order_date,
                    p.`order_status`,
                    p.`payment_status`,
                    p.`customer_id`,
                    p.`freight`,
                    p.`sub_total`,
                    p.`tax1_amount`,
                    p.`tax2_amount`,
                    p.total_amount,
                    cus.`name` AS customer_name
                  FROM
                `tra_sales_order_mas` AS p 
                INNER JOIN `mas_customers` AS cus ON cus.id = p.`customer_id`  
                  WHERE p.customer_id <> ''  " . $str . "   " . $str1 . " 
            ";
            $statement = $db->prepare($sql);
            if ($taxing_scheme_id) {
                $statement->bindValue('taxing_scheme_id', $taxing_scheme_id);
            }

            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCustomers($customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';

            if ($customer_id) {
                $str = "WHERE id=:id";
            }

            $sql = "SELECT DISTINCT
                    id AS customer_id,
                    `name` AS customer_name,
                    `contact_name`,
                    `contact_email`,
                    `contact_phone`,
                    `address`
                  FROM
                    `mas_customers`
                       " . $str . "
            ";
            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('id', $customer_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customers Data');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesRepsOrder($sales_rep_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';

            if ($sales_rep_id) {
                $str = "AND s.sales_rep_id=:sales_rep_id";
            }
            $sql = "SELECT
                    s.`id`,
                    s.`order_id`,
                    date_format(s.`order_date`,'%d/%m/%Y') as order_date,
                    s.`order_status`,
                    s.`payment_status`,
                    s.`customer_id`,
                    cus.`name` AS customer_name,
                    s.`sub_total`,
                    s.`freight`,
                    s.`total_amount`,
                    s.`paid_amount`,
                    (s.`tax1_amount`+s.`tax2_amount`) as total_tax,
                    rep.name as sales_rep_name
    
                  FROM
                `tra_sales_order_mas` AS s
                INNER JOIN `mas_customers` AS cus ON cus.id = s.`customer_id`
                LEFT JOIN `mas_sales_reps` AS rep ON rep.id = s.`sales_rep_id`
                  WHERE s.customer_id <> ''  " . $str . "
            ";
            $statement = $db->prepare($sql);
            if ($sales_rep_id) {
                $statement->bindValue('sales_rep_id', $sales_rep_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales Reps orders');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCustomersFromSales($customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($customer_id) {
                $str = " where s.customer_id=:customer_id";
            }

            $sql = "SELECT
                    distinct s.`customer_id`,c.name as customer_name
                  FROM
                `tra_sales_order_mas` AS s
                INNER JOIN `mas_customers` AS c ON c.id = s.customer_id
              " . $str . "
            ";
            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customers from Sales Order');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCustomersOrderId($customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($customer_id) {
                $str = " where s.customer_id=:customer_id";
            }

            $sql = "SELECT
                    distinct s.`id`,s.order_id
                  FROM
                `tra_sales_order_mas` AS s
              " . $str . "
            ";
            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customers from Sales Order');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCustomersProductsBYOrderId($orderId) {
        $db = Zend_Registry::get('db');
        try {

            $statement = $db->prepare('SELECT 
                                      det.id,
                                      det.order_id,
                                      mas.order_id AS so_id,
                                      DATE_FORMAT(mas.`order_date`, "%d %M %Y") AS order_date,
                                      ip.product_name,
                                      det.quantity,
                                      det.unit_price,
                                      det.discount,
                                      det.sub_total,
                                      mas.payment_status,
                                      c.`name` AS category 
                                    FROM
                                      tra_sales_order_det AS det 
                                      INNER JOIN tra_sales_order_mas AS mas 
                                        ON mas.id = det.order_id 
                                      INNER JOIN tra_inventory_product ip 
                                        ON ip.id = det.product_id 
                                      LEFT JOIN `mas_product_category` AS c 
                                        ON c.`id` = ip.`product_category_id` 
                                       where  det.item_status="Order" AND det.order_id =:id
                                        ');
            $statement->bindValue('id', $orderId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCategoryFromSales($customer_id, $categoryId, $productName) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($customer_id) {
                $str = " AND m.customer_id=:customer_id";
            }
            if ($categoryId) {
                $str1 = " AND ip.product_category_id=:product_category_id";
            }
            if ($productName) {
                $str2 = " AND ip.product_name=:product_name";
            }

            $sql = "SELECT
                    distinct c.id as cat_id,c.name as category_name
                  FROM
                `tra_sales_order_det` AS d
                 INNER JOIN tra_sales_order_mas m
                     ON m.id = d.order_id
                INNER JOIN tra_inventory_product ip
                     ON ip.id = d.product_id
                 INNER JOIN `mas_product_category` AS c
                     ON c.`id` = ip.`product_category_id`
                where c.name<>''
              " . $str . " " . $str1 . " " . $str2 . "
            ";
            $statement = $db->prepare($sql);

            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            if ($categoryId) {
                $statement->bindValue('product_category_id', $categoryId);
            }
            if ($productName) {
                $statement->bindValue('product_name', $productName);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $categoryIds = array();

            if (count($resultSet)) {
                $categoryIds = $resultSet;
            }
            return $categoryIds;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch categoryIds Sales Order');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getProductsByCategory($category_id, $customer_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($category_id) {
                $str = " AND ip.`product_category_id`=:category_id";
            }
            if ($customer_id) {
                $str1 = " AND m.customer_id=:customer_id";
            }
            if ($product_name) {
                $str2 = " AND ip.product_name=:product_name";
            }

            $sql = "SELECT DISTINCT 
                    det.product_id, ip.product_name
                  FROM
                    `tra_sales_order_det` AS det 
                    INNER JOIN tra_sales_order_mas AS m 
                      ON m.`id` = det.`order_id` 
                    INNER JOIN `tra_inventory_product` AS ip 
                      ON ip.`id` = det.`product_id` 
                  WHERE ip.product_name <> ''   
              " . $str . " " . $str1 . " " . $str2 . "
            ";

            $statement = $db->prepare($sql);

            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $products = array();

            if (count($resultSet)) {
                $products = $resultSet;
            }
            return $products;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch products using category,product name and custormer id');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchOrdersByProducts($product_id, $customer_id, $category_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($category_id) {
                $str = " AND ip.`product_category_id`=:category_id";
            }
            if ($customer_id) {
                $str1 = " AND mas.customer_id=:customer_id";
            }
            if ($product_id) {
                $str2 = " AND det.product_id=:product_id";
            }
            $sql = "SELECT
                    det.id,
                    det.order_id,
                    mas.order_id AS so_id,
                    DATE_FORMAT(mas.`order_date`, '%d %b %Y') AS order_date,
                    ip.product_name,
                    det.quantity,
                    det.unit_price,
                    det.discount,
                    det.sub_total,
                    mas.payment_status,
                    mas.order_status,
                    cus.`name` AS customer_name
                  FROM
                    tra_sales_order_det AS det
                    INNER JOIN tra_sales_order_mas AS mas
                      ON mas.id = det.order_id
                    INNER JOIN tra_inventory_product ip
                      ON ip.id = det.product_id
                    LEFT JOIN `mas_product_category` AS c
                      ON c.`id` = ip.`product_category_id`
                     LEFT JOIN `mas_customers` AS cus
                      ON cus.`id` = mas.`customer_id`
                     where  det.item_status='Order' 
                     " . $str . " " . $str1 . " " . $str2 . "";

            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            if ($category_id) {
                $statement->bindValue('product_category_id', $category_id);
            }
            if ($product_id) {
                $statement->bindValue('product_id', $product_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer products');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesOrderProfit($inv_status, $pay_status, $customer_id) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';

            if ($inv_status) {
                $str = " AND mas.order_status=:order_status";
            }
            if ($pay_status) {
                $str1 = " AND mas.payment_status=:payment_status";
            }
            if ($customer_id) {
                $str2 = " AND mas.customer_id =:customer_id";
            }

            $sql = "SELECT 
                    det.id,
                    det.order_id,
                    mas.order_id AS so_id,
                    DATE_FORMAT(mas.`order_date`, '%d %b %Y') AS order_date,
                    ip.product_name,
                    det.quantity,
                    det.unit_price,
                    pc.`cost`,
                    det.discount,
                    det.sub_total,
                    (det.sub_total-(pc.`cost`*det.quantity)) AS profit,
                    mas.order_status,
                    mas.payment_status,
                    cus.name as customer
                  FROM
                    tra_sales_order_det AS det 
                    INNER JOIN tra_sales_order_mas AS mas 
                      ON mas.id = det.order_id 
                    INNER JOIN mas_customers as cus 
                      ON cus.id = mas.customer_id 
                    INNER JOIN tra_inventory_product ip 
                      ON ip.id = det.product_id 
                    LEFT JOIN (
                    SELECT cost,`product_id`  
                    FROM `tra_inventory_product_cost`
                      GROUP BY product_id) AS pc
                     ON pc.product_id = det.`product_id`
                  WHERE det.item_status = 'Order' " . $str . " " . $str1 . "  " . $str2 . " 
            ";

            $statement = $db->prepare($sql);
            if ($customer_id) {
                $statement->bindValue('customer_id', $customer_id);
            }
            if ($pay_status) {
                $statement->bindValue('payment_status', $pay_status);
            }
            if ($inv_status) {
                $statement->bindValue('order_status', $inv_status);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getCategoryIdsForInventory($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($category_id) {
                $str = " AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND p.product_name =:product_name";
            }

            $sql = "SELECT 
                    distinct c.`id`,c.name as category
                  FROM
                `tra_inventory_product` AS p 
                INNER JOIN `mas_product_category` AS c ON c.id = p.product_category_id
               WHERE p.product_name <> ''  " . $str . " " . $str1 . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch category for inventory history');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchInventoryHistory($category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';


            if ($category_id) {
                $str = " AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND p.product_name =:product_name";
            }

            $statement = $db->prepare("SELECT 
                    p.`product_name`,
                    SUM(q.`quantity`) AS qty,
                    pc.cost
                  FROM
                    tra_inventory_product AS p
                    INNER JOIN `tra_inventory_product_quantity` AS q
                    ON q.`product_id` = p.`id`
                    LEFT JOIN 
                      (SELECT 
                        cost,
                        `product_id` 
                      FROM
                        `tra_inventory_product_cost` 
                      GROUP BY product_id) AS pc 
                      ON pc.product_id = p.`id`
                      where p.product_name<>''
                    " . $str . " " . $str1 . " 
                        GROUP BY p.id");
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }

    public static function getInventoryHistory($product_id,$location_id,$product_category) {
        $db = Zend_Registry::get('db');
        try {
            $str = "";
            $str1 = "";
            $str2 = "";
            if($product_id) {
                $str = "AND mov.product_id =:product_id";
            }
            if($location_id) {
                $str1 = "AND mov.location =:location_id";
            }
            if($product_category) {
                $str2 = "AND ip.product_category_id =:product_category";
            }
            $statement = $db->prepare('SELECT DISTINCT mov.product_id,
                                                mov.type,
                                                mov.order_id,
                                                DATE_FORMAT(mov.movement_date,"%d/%m/%Y") AS movement_date ,
                                                mov.location,
                                                mov.qty_after AS quantity,
                                                ip.product_name,
                                                loc.name AS location_name
                                        FROM  
                                                tra_movement_history AS mov
                                        LEFT JOIN
                                                tra_inventory_product AS ip
                                        ON
                                                ip.id = mov.product_id
                                        LEFT JOIN 
                                                mas_inventory_location AS loc
                                        ON 
                                                mov.location = loc.id	
                                        WHERE   
                                                mov.id <> "" 
                                        '. $str .' ' .  $str1 . ' ' . $str2 . '');
            
            if($product_id) {
                 $statement->bindValue('product_id', $product_id);
            }
            if($location_id) {
                 $statement->bindValue('location_id', $location_id);
            }
            if($product_category) {
                 $statement->bindValue('product_category', $product_category);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;

        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchProductCost($category_id,$product_name) {
         $db = Zend_Registry::get('db');
        try {
             $str='';
            $str1='';


            if($category_id){
                $str = " AND p.product_category_id=:category_id";
            }
            if($product_name){
                $str1 = " AND p.product_name =:product_name";
            }
            
            $statement =$db->prepare("SELECT 
                    p.id,
                      p.`product_name`,
                      h.`type`,
                      h.`order_id`,
                      DATE_FORMAT(h.`movement_date`, '%d %b %Y') AS movement_date,
                      h.`qty`,
                      h.`qty_after`,
                      pc.cost,
                      IF(h.`type`='Purchase Order' || h.`type`='Purchase Returned', h.total_amount,(pc.cost*h.`qty`)) AS totAmount,
                      (pc.cost*h.`qty_after`) AS total_cost
                    FROM
                      `tra_movement_history` AS h 
                      INNER JOIN `tra_inventory_product` AS p 
                        ON p.`id` = h.`product_id` 
                      LEFT JOIN 
                        (SELECT 
                          cost,
                          `product_id` 
                        FROM
                          `tra_inventory_product_cost` 
                        GROUP BY product_id) AS pc 
                        ON pc.product_id = p.`id`
                        where p.product_name <> '' ".$str." ".$str1."
                       " ); 
             if($category_id){
                $statement->bindValue('category_id', $category_id);
            }
            if($product_name){
                $statement->bindValue('product_name', $product_name);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sales by product and category id');
        }
    }
    
    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getLocationIdsForInventory($location_id,$category_id, $product_name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            
            if ($category_id) {
                $str = " AND p.product_category_id=:category_id";
            }
            if ($product_name) {
                $str1 = " AND p.product_name =:product_name";
            }
            if ($location_id) {
                $str2 = " AND pc.location_id =:location_id";
            }

            $sql = "SELECT 
                    distinct il.`id`,il.name as location_name
                  FROM
                `tra_inventory_product` AS p 
                INNER JOIN `mas_product_category` AS c ON c.id = p.product_category_id
                INNER JOIN `tra_inventory_product_quantity` AS pc ON pc.product_id = p.id
                INNER JOIN `mas_inventory_location` AS il ON il.id = pc.location_id
                
               WHERE p.product_name <> ''  " . $str . " " . $str1 . " " .  $str2  . "    
            ";
            $statement = $db->prepare($sql);
            if ($category_id) {
                $statement->bindValue('category_id', $category_id);
            }
            if ($product_name) {
                $statement->bindValue('product_name', $product_name);
            }
            if ($location_id) {
                $statement->bindValue('location_id', $location_id);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch category for inventory history');
        }
    }
}
?>
