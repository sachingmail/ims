<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_PricingCurrency extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $currency_name;
    protected $name;
    protected $default;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_pricing_currency(
                    id,
                    name,
                    currency_name,
                    `default`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :currency_name,
                    :default,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('currency_name', $this->getCurrency_name());
            $statement->bindValue('default', $this->getDefault());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
//            var_dump($statement);die;

            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_pricing_currency
                 SET
                    name = :name,
                    currency_name = :currency_name,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('currency_name', $this->getCurrency_name());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * set pricing currency as default pricing currency
     * @return 
     */
    public function MakeDefaultPricingCurrency($order_id) {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_pricing_currency
                 SET
                    `default` = :default,
                    updated = :updated,
                    updated_by = :updated_by
                 WHERE (1)
                    
            ');
            $statement->bindValue('default', 0);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE mas_pricing_currency
                 SET
                    `default` = :default,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('default', '1');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            if ($order_id) {
                $statement = $this->_db->prepare(
                        'UPDATE tra_sales_order_mas
                 SET
                    `pricing_currency_id` = :pricing_currency_id,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :order_id'
                );
                $statement->bindValue('order_id', $order_id);
                $statement->bindValue('pricing_currency_id', $this->getId());
                $statement->bindValue('updated', $this->getUpdated());
                $statement->bindValue('updated_by', $this->getUpdated_by());
                $statement->execute();

                $statement = $this->_db->prepare(
                        'SELECT det.id,det.product_id,det.quantity,det.discount,
                            cost.price,mas.pricing_currency_id
                        FROM 
                            tra_sales_order_det AS det
                        LEFT JOIN 
                            tra_sales_order_mas AS mas 
                        ON 
                            mas.id = det.order_id 
                        LEFT JOIN 
                            tra_inventory_product_cost AS cost 
                        ON
                            det.product_id = cost.product_id
                        WHERE  
                            cost.price_currency_id = :id and det.order_id = :order_id'
                );
                $statement->bindValue('id', $this->getId());
                $statement->bindValue('order_id', $order_id);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                $statement->closeCursor();

                return $resultSet;
            } else {
                $statement = $this->_db->prepare(
                        'SELECT det.id,det.product_id,det.quantity,det.discount,
                            cost.price
                        FROM 
                            tra_sales_order_det AS det
                        LEFT JOIN 
                            tra_inventory_product_cost AS cost 
                        ON
                            det.product_id = cost.product_id
                        WHERE  
                            cost.price_currency_id = :id and det.sessionid = :sessionid'
                );
                $statement->bindValue('id', $this->getId());
                $statement->bindValue('sessionid', session_id());
                $statement->execute();
                $resultSet = $statement->fetchAll();
                $statement->closeCursor();

                return $resultSet;
            }
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch pricing currency by currency id 
     * 
     * @access public
     * @return object
     */
    public function fetchPricingCurrencyById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_pricing_currency
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * 
     * 
     */
    public function deletePricingCurrency($tablename) {
        try {
            $statement = $this->_db->prepare(
                    'select `default` from mas_pricing_currency
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            if ($resultSet['default'] == 1) {
                $statement = $this->_db->prepare(
                        'DELETE FROM mas_pricing_currency
                    WHERE id= :id'
                );
                $statement->bindValue('id', $this->getId());
                $statement->execute();
                $statement = $this->_db->prepare(
                        "UPDATE mas_pricing_currency SET `default` = '1'
                     ORDER BY id ASC LIMIT 1"
                );
                $statement->execute();
            } else {
                $statement = $this->_db->prepare(
                        'DELETE FROM mas_pricing_currency
                    WHERE id= :id'
                );
                $statement->bindValue('id', $this->getId());
                $statement->execute();
                return $this;
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * 
     * 
     */
    public function fetchPriceById($id, $product_id) {
        try {
            $statement = $this->_db->prepare(
                    'select price from tra_inventory_product_cost
                  WHERE
                    product_id = :product_id and price_currency_id = :price_currency_id'
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('price_currency_id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * check if pricing currency is present in records or not
     * 
     */
    public function checkIfPricingCurrencyPresentInRecords() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_order_mas 
                    WHERE  
                            pricing_currency_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_order'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_quote_order_mas 
                    WHERE  
                            pricing_currency_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_quote'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            mas_customers 
                    WHERE  
                            pricing_currency_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['customers'] = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
             DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * @return type
     * all setters and getters
     */
    public function getId() {
        return $this->id;
    }

    public function getCurrency_name() {
        return $this->currency_name;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setCurrency_name($currency_name) {
        $this->currency_name = $currency_name;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    public function getDefault() {
        return $this->default;
    }

    public function setDefault($default) {
        $this->default = $default;
    }

}
