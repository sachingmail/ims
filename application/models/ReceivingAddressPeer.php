<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_ReceivingAddressPeer {

    static private $db;

    /**
     * class constructor
     * initializes $db
     */
    public function __construct() {
        if (!self::$db instanceof Zend_Db_Adapter_Pdo_Mysql) {
            self::$db = Zend_Registry::get('db');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllCountries() {
        try {
            $statement = self::$db->prepare('SELECT
					c.id,c.country_name 
                                        FROM mas_countries as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
//            var_dump($organizations);die;
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    public static function fetchAllStates() {
        try {
            $statement = self::$db->prepare('SELECT
					s.id,s.state_name 
                                        FROM mas_states as s
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
//            var_dump($organizations);die;
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public static function fetchAllReceivingAddress() {
        try {
            $statement = self::$db->prepare('SELECT a.zip_code,a.address_name,a.remarks,a.address_type,a.street,a.id,c.city_name,s.state_name,co.country_name,c.state_id,c.country_id
                                            FROM mas_cities AS c
                                            INNER JOIN mas_receiving_addresses AS a 
                                            ON a.city_id = c.id
                                            INNER JOIN mas_states AS s
                                            ON s.id = c.state_id
                                            INNER JOIN mas_countries AS co
                                            ON co.id = c.country_id
                                            ');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
//            var_dump($organizations);die;
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
   

}

?>
