<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_PricingCurrencyPeer extends DMC_Model_Abstract {

    /**
     * get all records form master table
     * fetch all currency from master tables
     * @return array
     */
    public static function fetchAllCurrency() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					c.id,c.currency_name,c.currency_code 
                                        FROM mas_currency as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form master table
     * fetch all pricing currency from master tables
     * @return array
     */
    public static function fetchAllPricingCurrency() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					p.id,p.currency_name,p.name,p.default
                                        FROM mas_pricing_currency as p
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /*
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validatePricingCurrency($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  name =:name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_pricing_currency
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    public static function getDefaultPricingCurrency() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					count(*) as tot from mas_pricing_currency
					');
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();
            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
}

?>
