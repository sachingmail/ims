<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_TaxingScheme extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $TaxingSchemeName;
    protected $TaxName;
    protected $TaxRate;
    protected $TaxOnShipping;
    protected $SecondaryTaxName;
    protected $SecondaryTaxRate;
    protected $SecondaryTaxOnShipping;
    protected $CompoundSecondary;
    protected $role_id;
    protected $showSecondaryTaxName;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_taxing_schemes(
                    id,
                    tax_scheme_name,
                    tax_name,
                    tax_rate,
                    tax_on_shipping,
                    secondary_tax_name,
                    secondary_tax_rate,
                    secondary_tax_on_shipping,
                    compound_secondary,
                    show_secondary_tax_rate,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :tax_scheme_name,
                    :tax_name,
                    :tax_rate,
                    :tax_on_shipping,
                    :secondary_tax_name,
                    :secondary_tax_rate,
                    :secondary_tax_on_shipping,
                    :compound_secondary,
                    :show_secondary_tax_rate,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('tax_scheme_name', $this->getTaxingSchemeName());
            $statement->bindValue('tax_name', $this->getTaxName());
            $statement->bindValue('tax_rate', $this->getTaxRate());
            $statement->bindValue('tax_on_shipping', $this->getTaxOnShipping());
            $statement->bindValue('secondary_tax_name', $this->getSecondaryTaxName());
            $statement->bindValue('secondary_tax_rate', $this->getSecondaryTaxRate());
            $statement->bindValue('secondary_tax_on_shipping', $this->getSecondaryTaxOnShipping());
            $statement->bindValue('compound_secondary', $this->getCompoundSecondary());
            $statement->bindValue('show_secondary_tax_rate', $this->getShowSecondaryTaxName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_taxing_schemes
                 SET
                     tax_scheme_name = :tax_scheme_name,
                     tax_name = :tax_name,
                     tax_rate = :tax_rate,
                     tax_on_shipping = :tax_on_shipping,
                     secondary_tax_name = :secondary_tax_name,
                     secondary_tax_rate = :secondary_tax_rate,
                     secondary_tax_on_shipping = :secondary_tax_on_shipping,
                     compound_secondary= :compound_secondary,
                     show_secondary_tax_rate=:show_secondary_tax_rate,
                     updated = :updated,
                     updated_by = :updated_by
                  WHERE
                     id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('tax_scheme_name', $this->getTaxingSchemeName());
            $statement->bindValue('tax_name', $this->getTaxName());
            $statement->bindValue('tax_rate', $this->getTaxRate());
            $statement->bindValue('tax_on_shipping', $this->getTaxOnShipping());
            $statement->bindValue('secondary_tax_name', $this->getSecondaryTaxName());
            $statement->bindValue('secondary_tax_rate', $this->getSecondaryTaxRate());
            $statement->bindValue('secondary_tax_on_shipping', $this->getSecondaryTaxOnShipping());
            $statement->bindValue('compound_secondary', $this->getCompoundSecondary());
            $statement->bindValue('show_secondary_tax_rate', $this->getShowSecondaryTaxName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch taxing scheme by id 
     * 
     * @access public
     * @return object
     */
    public function fetchTaxinSchemeById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_taxing_schemes
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * 
     */
    public function checkTaxingScehemePresentInRecords() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            mas_vendors 
                    WHERE  
                            taxing_schemes = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['vendors'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_quote_order_mas 
                    WHERE  
                            taxing_scheme_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_quote'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            mas_customers 
                    WHERE  
                            taxing_schemes = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['customers'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_purchase_order_mas 
                    WHERE  
                            taxing_scheme_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['purchase'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_order_mas 
                    WHERE  
                            taxing_scheme_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_order'] = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
             DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function getTaxingSchemeName() {
        return $this->TaxingSchemeName;
    }

    public function getTaxName() {
        return $this->TaxName;
    }

    public function getTaxRate() {
        return $this->TaxRate;
    }

    public function setTaxRate($TaxRate) {
        $this->TaxRate = $TaxRate;
    }

    public function getTaxOnShipping() {
        return $this->TaxOnShipping;
    }

    public function getSecondaryTaxName() {
        return $this->SecondaryTaxName;
    }

    public function getSecondaryTaxRate() {
        return $this->SecondaryTaxRate;
    }

    public function getSecondaryTaxOnShipping() {
        return $this->SecondaryTaxOnShipping;
    }

    public function getCompoundSecondary() {
        return $this->CompoundSecondary;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTaxingSchemeName($TaxingSchemeName) {
        $this->TaxingSchemeName = $TaxingSchemeName;
    }

    public function setTaxName($TaxName) {
        $this->TaxName = $TaxName;
    }

    public function setTaxOnShipping($TaxOnShipping) {
        $this->TaxOnShipping = $TaxOnShipping;
    }

    public function setSecondaryTaxName($SecondaryTaxName) {
        $this->SecondaryTaxName = $SecondaryTaxName;
    }

    public function setSecondaryTaxRate($SecondaryTaxRate) {
        $this->SecondaryTaxRate = $SecondaryTaxRate;
    }

    public function setSecondaryTaxOnShipping($SecondaryTaxOnShipping) {
        $this->SecondaryTaxOnShipping = $SecondaryTaxOnShipping;
    }

    public function setCompoundSecondary($CompoundSecondary) {
        $this->CompoundSecondary = $CompoundSecondary;
    }

    public function getShowSecondaryTaxName() {
        return $this->showSecondaryTaxName;
    }

    public function setShowSecondaryTaxName($showSecondaryTaxName) {
        $this->showSecondaryTaxName = $showSecondaryTaxName;
    }

}
