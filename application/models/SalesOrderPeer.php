<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_SalesOrderPeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllSalesOrder() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare(" SELECT 
                    mas.id,
                    DATE_FORMAT(mas.`order_date`,'%d %M %Y') AS so_date,
                    mas.`order_status`,
                    mas.`payment_status`,
                    mas.`po_ref`,
                    c.name AS customer_name,
                    mas.`order_id`,  
                    l.name AS location,
                    DATE_FORMAT(mas.req_ship_date,'%d %M %Y') AS ship_date,
                    DATE_FORMAT(mas.`due_date`,'%d %M %Y') AS due_date,
                    IFNULL(mas.`total_amount`,0.00) AS `total_amount`,
                    IFNULL(mas.`paid_amount`,0.00) AS paid_amount,
                    IFNULL(mas.`balance`,0.00) AS balance 
                  FROM
                    tra_sales_order_mas AS mas 
                    LEFT JOIN mas_customers AS c 
                      ON c.id = mas.customer_id
                    LEFT JOIN `mas_inventory_location` AS l 
                      ON l.id = mas.location_id ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $ordes = array();
            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchVendorOrders($vendor_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        id,
                                        po_id,
                                        po_date,
                                        CONCAT_WS(",", order_status, payment_status) AS order_status,
                                        total_amount,
                                        paid_amount,
                                        (total_amount - paid_amount) AS balance 
                                      FROM
                                        tra_purchase_order_mas where vendor_id =:vendor_id');
            $statement->bindValue('vendor_id', $vendor_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchSalesOrderProducts($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.location_id,
                                        det.sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.order_id=:order_id
                                       and det.item_status="Order"');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchSalesProducts($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        if(det.item_status="Returned",-det.quantity,det.quantity) as quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        if(det.item_status="Return","Return","Order") as orderType
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.item_status in("Order","Returned") and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchSalesOrderProductsBySession($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
    * get sub total by po id
    * @return array
    */
    public static function fetchSubTotalById($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                         sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.order_id=:order_id AND det.item_status = "Order"');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * get sub total by po id
     * @return array
     */
    public static function fetchSubTotalByIdRefund($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                SUM(det.sub_total) AS sub_total,ifnull(ret_sub_total,0.00) as ret_sub_total 
                              FROM
                                tra_sales_order_det det 
                                LEFT JOIN (
                                      SELECT 
                                SUM(sub_total) AS ret_sub_total ,order_id
                              FROM
                                tra_sales_order_det WHERE order_id =:order_id1
                                    AND item_status="Return"
                                ) AS det1
                                ON det.`order_id` = det1.order_id
                                  WHERE item_status="Order"
                                       AND det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('order_id1', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sub total of products');
        }
    }
    /**
    * get sub total by session id
    * @return array
    */
    public static function fetchSubTotalBySession($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['sub_total'];
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    
    /**
    * get product by session id
    * @return array
    */
    public static function fetchFulfillProductsBySessionId($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT ip.id,ip.product_name
                                        FROM `tra_inventory_product` AS ip 
                                        INNER JOIN `tra_sales_order_det` AS det 
                                        ON ip.id = det.`product_id`                                        
                                       where det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
//            var_dump($resultSet);die;
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get product by order id
    * @return array
    */
    public static function fetchFulfillProductsByOrderId($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT DISTINCT ip.id,ip.product_name
                                        FROM `tra_inventory_product` AS ip 
                                        INNER JOIN `tra_sales_order_det` AS det 
                                        ON ip.id = det.`product_id`                                        
                                       where det.order_id =:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
    * get product by session id
    * @return array
    */
    public static function fetchReturnProductsBySessionId($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT ip.id,ip.product_name
                                        FROM `tra_inventory_product` AS ip 
                                        INNER JOIN `tra_sales_order_det` AS det 
                                        ON ip.id = det.`product_id`                                        
                                       where det.sessionid =:session_id and det.item_status="Fulfilled"');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
//            var_dump($resultSet);die;
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get product by order id
    * @return array
    */
    public static function fetchReturnProductsByOrderId($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT DISTINCT ip.id,ip.product_name
                                        FROM `tra_inventory_product` AS ip 
                                        INNER JOIN `tra_sales_order_det` AS det 
                                        ON ip.id = det.`product_id`                                        
                                       where det.order_id =:order_id and det.item_status="Fulfilled"');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
    * get product price and vendor price
    * @return array
    */
    public static function fetchProductAndCostPrice($product_id,$customer_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT tipc.price,mpc.id,tipc.product_id 
                        FROM tra_inventory_product_cost AS tipc
                        INNER JOIN mas_pricing_currency AS mpc
                        ON mpc.id = tipc.price_currency_id
                        WHERE 
                        tipc.product_id = :product_id
                        AND 
                        mpc.default = :default 
            ');
            
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('default', '1');
            
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /*
     * fetch payments by session id
     * @return type
     * 
     */
    public function fetchSalesPaymentsBySession($sessionid) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'SELECT 
                        det.order_id,
                        det.id,
                        det.payment_type,
                        DATE_FORMAT(det.payment_date,"%d %M %Y") AS payment_date,
                        det.amount,
                        det.payment_method_id,
                        mas.name AS payment_method_name,
                        det.payment_ref,
                        det.payment_remarks
                        FROM tra_order_payments AS det
                        LEFT JOIN mas_payment_method AS mas
                        ON mas.id = payment_method_id
                    where 
                        det.sessionid =:sessionid and det.order_type="Sales"
                    '
            );

            $statement->bindValue('sessionid', $sessionid);
            $statement->execute();
            $resultSet = $statement->fetchAll();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /*
     * fetch payments by order id
     * @return type
     * 
     */
    
    public function fetchSalesPaymentsByOrderId($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'SELECT 
                        det.order_id,
                        det.id,
                        det.payment_type,
                        DATE_FORMAT(det.payment_date,"%d %M %Y") AS payment_date,
                        det.amount,
                        det.payment_method_id,
                        mas.name AS payment_method_name,
                        det.payment_ref,
                        det.payment_remarks
                        FROM tra_order_payments AS det
                        LEFT JOIN mas_payment_method AS mas
                        ON mas.id = det.payment_method_id
                    where 
                        det.order_id =:order_id and det.order_type="Sales"
                    '
            );

            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public static function getOrderStatus($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare(" SELECT 
                    order_status,
                    payment_status
                  FROM
                    tra_sales_order_mas 
                  where id=:id");
            $statement->bindValue('id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to order Status');
        }
    }
    /**
     * 
     * @param type $order_id
     * @param type $status
     * @param type $type
     * @return type
     * 
     */
    public static function fetchSalesOrderByStatus($order_id,$status,$type='O') {
         $db = Zend_Registry::get('db');
        try {
            if($type=='S'){
                $statement =$db->prepare('
                  SELECT 
                   det.id,
                   det.order_id,
                   det.product_id,
                   ip.product_name,
                   det.quantity,
                   det.unit_price,
                   det.discount,
                   det.sub_total,
                   det.item_status,
                   mas.location_id,
                   loc.name as location_name,
                   date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                   FROM 
                   tra_sales_order_det det
                   inner join tra_inventory_product ip
                       on ip.id = det.product_id
                   inner join tra_sales_order_mas mas 
                       on mas.id = det.order_id
                   LEFT JOIN mas_inventory_location as loc
                       on loc.id = det.location_id
                  where det.session=:order_id
                  AND det.item_status =:status
                   ');   
            } else {
                $statement =$db->prepare('SELECT 
                    det.id,
                    det.order_id,
                    det.product_id,
                    ip.product_name,
                    det.quantity,
                    det.unit_price,
                    det.discount,
                    det.sub_total,
                    det.item_status,
                    mas.location_id,
                    loc.name as location_name,
                    date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                    FROM 
                    tra_sales_order_det det
                    inner join tra_inventory_product ip
                        on ip.id = det.product_id
                    inner join tra_sales_order_mas mas 
                        on mas.id = det.order_id
                    LEFT JOIN mas_inventory_location as loc
                        on loc.id = det.location_id
                   where det.order_id=:order_id
                   AND det.item_status =:status
                ');
            }
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to Products Data according to status of the product');
        }
    }
     /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function countQty($order_id,$status) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        sum(quantity) as qty
                                       from tra_sales_order_det
                                       where order_id=:order_id and item_status=:status');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            
            return $resultSet['qty'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to get total quantity');
        }
    }
    /**
     * 
     * @param type $order_id
     * @return type
     */
    public static function fetchReturnSubTotalById($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                         sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where item_status="Returned" and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get sub total by session id
    * @return array
    */
    public static function fetchReturnSubTotalBySession($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where item_status="Returned" and det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['sub_total'];
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchReturnSalesProductsBySession($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        det.item_status,
                                        date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.sessionid =:session_id and det.item_status="Returned"');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchReturnSalesProducts($orderId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        det.item_status,
                                        date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.order_id =:orderId and (det.item_status="Returned" OR det.item_status="Discarded")');
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * @param type $id
     * @return type
     * 
     */
    public static function fetchSalesProductsByRecordId($id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.id=:id and det.item_status="Fulfilled"');
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch receive Product Data');
        }
    }
    
     /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchReturnedProductsByStatus($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.order_id=:order_id and (det.item_status="Returned" or det.item_status="Discarded")');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchProductsByStatus($order_id,$status) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.order_id=:order_id and det.item_status=:status');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchUnfulfilledtemByStatus() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT DISTINCT mas.order_id,mas.id,mas.order_status,
                                            mas.payment_status,mas.customer_name,mas.po_ref,
                                            loc.name as location_name,
                                            DATE_FORMAT(mas.`order_date`,"%d %M %Y") AS order_date,
                                            DATE_FORMAT(mas.`req_ship_date`,"%d %M %Y") AS req_ship_date,
                                            DATE_FORMAT(mas.`due_date`,"%d %M %Y") AS due_date,
                                            mas.total_amount,
                                            mas.paid_amount,(mas.total_amount - mas.paid_amount) AS balance
                                        FROM 
                                            tra_sales_order_mas AS mas
                                        LEFT JOIN 
                                            tra_sales_order_det AS det
                                        ON 
                                            det.order_id = mas.id
                                        LEFT JOIN 
                                            mas_inventory_location AS loc
                                        ON 
                                            loc.id = mas.location_id
                                        WHERE 
                                            mas.order_status = "Unfulfilled"');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * @return type
     * 
     */
    public static function fetchUnPaidtemByStatus() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT DISTINCT mas.order_id,mas.id,mas.order_status,
                                            mas.payment_status,mas.customer_name,mas.po_ref,
                                            loc.name as location_name,
                                            DATE_FORMAT(mas.`order_date`,"%d %M %Y") AS order_date,
                                            DATE_FORMAT(mas.`req_ship_date`,"%d %M %Y") AS req_ship_date,
                                            DATE_FORMAT(mas.`due_date`,"%d %M %Y") AS due_date,
                                            mas.total_amount,
                                            mas.paid_amount,(mas.total_amount - mas.paid_amount) AS balance
                                        FROM 
                                            tra_sales_order_mas AS mas
                                        LEFT JOIN 
                                            tra_sales_order_det AS det
                                        ON 
                                            det.order_id = mas.id
                                        LEFT JOIN 
                                            mas_inventory_location AS loc
                                        ON 
                                            loc.id = mas.location_id
                                        WHERE 
                                            mas.payment_status != "Paid"');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * @return type
     * 
     */
    public static function fetchUnInvoicedtemByStatus() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT DISTINCT mas.order_id,mas.id,mas.order_status,
                                            mas.payment_status,mas.customer_name,mas.po_ref,
                                            loc.name as location_name,
                                            DATE_FORMAT(mas.`order_date`,"%d %M %Y") AS order_date,
                                            DATE_FORMAT(mas.`req_ship_date`,"%d %M %Y") AS req_ship_date,
                                            DATE_FORMAT(mas.`due_date`,"%d %M %Y") AS due_date,
                                            mas.total_amount,
                                            mas.paid_amount,(mas.total_amount - mas.paid_amount) AS balance
                                        FROM 
                                            tra_sales_order_mas AS mas
                                        LEFT JOIN 
                                            tra_sales_order_det AS det
                                        ON 
                                            det.order_id = mas.id
                                        LEFT JOIN 
                                            mas_inventory_location AS loc
                                        ON 
                                            loc.id = mas.location_id
                                        WHERE 
                                            mas.payment_status = "Uninvoiced"');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    public function getPaidAmount($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT mas.last_pay_date,mas.payment_status,mas.paid_amount AS paid_amount,mas.total_amount AS order_total,(mas.total_amount - mas.paid_amount) AS balance
                                        FROM tra_sales_order_mas AS mas 
                                        LEFT JOIN tra_order_payments AS op
                                        ON mas.id = op.order_id
                                       WHERE mas.id =:order_id ');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
            
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    public function getPaidAmountBySessionId($sessionid) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT mas.payment_status,SUM(op.amount) AS paid_amount,mas.total_amount AS order_total,(mas.total_amount - SUM(op.amount)) AS balance
                                        FROM tra_order_payments AS op
                                        LEFT JOIN tra_sales_order_mas AS mas
                                        ON mas.id = op.order_id
                                       WHERE op.sessionid =:sessionid');
            $statement->bindValue('sessionid', $sessionid);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    public function fetchQuantity($product_id,$location_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT * FROM tra_inventory_product_quantity where 
                    product_id =:product_id and location_id =:location_id
                    '
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('location_id', $location_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public function fetchTotalAmount($order_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT 
                        total_amount 
                    FROM
                        tra_sales_order_mas
                    WHERE
                        id = :order_id
                    '
            );
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCancelledSalesOrder($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        det.item_status
                                        FROM 
                                        tra_sales_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.item_status in("Fulfilled","Restocked") and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    public static function fetchSoIdByOrderId($so_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT order_id FROM tra_sales_order_mas where 
                    id =:id 
                    '
            );
            $statement->bindValue('id', $so_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['order_id'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    } 
    
    public static function fetchProductSubTotal($so_id){
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT 
                        sub_total 
                    FROM 
                        tra_sales_order_det 
                    WHERE 
                        id =:id 
                    '
            );
            $statement->bindValue('id', $so_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     public static function fetchSubTotalByOrderId($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                SUM(det.sub_total) AS sub_total,ifnull(ret_sub_total,0.00) as ret_sub_total 
                              FROM
                                tra_sales_order_det det 
                                LEFT JOIN (
                                      SELECT 
                                SUM(sub_total) AS ret_sub_total ,order_id
                              FROM
                                tra_sales_order_det WHERE order_id =:order_id1
                                    AND item_status="Returned"
                                ) AS det1
                                ON det.`order_id` = det1.order_id
                                  WHERE item_status="Order"
                                       AND det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('order_id1', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sub total of products');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllSalesOrderByStatus($status) {
         $db = Zend_Registry::get('db');
        try {
            $str = '';
            if($status == "open") {
                $str = "WHERE mas.order_status = 'Unfulfilled' ";
            }
            
            if($status == "started") {
                $str = "WHERE mas.order_status = 'Started' ";
            }
            
            if($status == "Uninvoiced") {
                $str = "WHERE mas.payment_status = 'Uninvoiced' ";
            }
            
            if($status == "paid") {
                $str = "WHERE mas.payment_status = 'Paid' ";
            }
            
            $statement =$db->prepare(" SELECT 
                    mas.id,
                    DATE_FORMAT(mas.`order_date`,'%d %M %Y') AS so_date,
                    mas.`order_status`,
                    mas.`payment_status`,
                    mas.`po_ref`,
                    c.name AS customer_name,
                    mas.`order_id`,  
                    l.name AS location,
                    DATE_FORMAT(mas.req_ship_date,'%d %M %Y') AS ship_date,
                    DATE_FORMAT(mas.`due_date`,'%d %M %Y') AS due_date,
                    mas.`total_amount`,
                    mas.`paid_amount`,
                    (mas.total_amount-mas.paid_amount) AS balance 
                  FROM
                    tra_sales_order_mas AS mas 
                    LEFT JOIN mas_customers AS c 
                      ON c.id = mas.customer_id
                    LEFT JOIN `mas_inventory_location` AS l 
                      ON l.id = mas.location_id 
                      " . $str . "
                    ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $ordes = array();
            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
}

?>
