<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Inventory extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $location;
    protected $product_id;
    protected $newQuantity;
    protected $oldQuantity;
    protected $diff;
    protected $status;
    protected $invDate;
    protected $remarks;
    protected $toLocation;
    protected $transferDate;
    protected $sendDate;
    protected $receivedDate;
    protected $transferStockId;
    protected $reorderPoint;
    protected $vendorId;
    protected $reorderQuantity;
    protected $quantityAvailable;
    protected $itemStatus;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveAdjustStock() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->insertAdjustStock();
        } else {
            $this->updateAdjustStock();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertAdjustStock() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_adjust_stock(
                    id,
                    inventory_product_id,
                    new_quantity,
                    old_quantity,
                    difference,
                    inventory_date,
                    status,
                    inventory_location_id,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :inventory_product_id,
                    :new_quantity,
                    :old_quantity,
                    :difference,
                    :inventory_date,
                    :status,
                    :inventory_location_id,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('new_quantity', $this->getNewQuantity());
            $statement->bindValue('old_quantity', $this->getOldQuantity());
            $statement->bindValue('difference', $this->getDiff());
            $statement->bindValue('inventory_date', $this->getInvDate());
            $statement->bindValue('status', $this->getStatus());
            $statement->bindValue('inventory_location_id', $this->getLocation());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());


            //Update or insert Quantity in Inventory
            $validateQty = $this->validateProductInventory();
            if ($validateQty > 0) {
                $this->updateProductQuantity();
            } else {
                $this->insertProductQuantity();
            }

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate Adjust stock
     * 
     * @access public
     * @return object
     */
    public function updateAdjustStock() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_adjust_stock
                 SET
                    id= :id,
                    inventory_product_id= :inventory_product_id,
                    new_quantity =:new_quantity,
                    old_quantity =:old_quantity,
                    difference =:difference,
                    inventory_location_id= :inventory_location_id,
                    remarks =:remarks,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('new_quantity', $this->getNewQuantity());
            $statement->bindValue('old_quantity', $this->getOldQuantity());
            $statement->bindValue('difference', $this->getDiff());
            $statement->bindValue('inventory_location_id', $this->getLocation());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * inserts prduct in stock inventory
     * 
     * @access public
     * @return object
     */
    public function insertProductQuantity() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product_quantity(
                    id,
                    product_id,
                    quantity,
                    location_id
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :quantity,
                    :location_id
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $this->getNewQuantity());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate product quanity
     * 
     * @access public
     * @return object
     */
    public function updateProductQuantity() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    quantity= :quantity
                  WHERE
                    location_id = :location_id
                    AND product_id =:product_id
                    '
            );
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $this->getNewQuantity());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function cancelAdjustStock() {
        try {
            
           $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    quantity= :quantity
                  WHERE
                    location_id = :location_id
                    AND product_id =:product_id
                    '
            );
           
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $this->getNewQuantity());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();
            
             
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_adjust_stock
                 SET
                    status= :status,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('status', 'Cancelled');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * 
     * @return \Application_Model_Inventory
     * 
     */
    public function cancelReorderStock() {
        try {
            $statement = $this->_db->prepare(
                    'DELETE FROM tra_inventory_reorder_stock
                    where id =:id
            ');
            $statement->bindValue('id', $this->getId());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchAdjustStockById() {
        try {
            $statement = $this->_db->prepare(' Select
                    invs.id,
                    p.product_name,
                    invs.inventory_location_id,
                    invs.`new_quantity`, 
                    invs.`old_quantity`, 
                    invs.`difference`,
                    invs.remarks
                    FROM
                    `tra_inventory_adjust_stock` AS invs 
                    INNER JOIN `tra_inventory_product` AS p 
                      ON p.`id` = invs.`inventory_product_id` 
                  WHERE
                    invs.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch adjust stock data by id');
        }
    }
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchReorderStockById() {
        try {
            $statement = $this->_db->prepare(' Select
                    invs.id,
                invs.`product_id`,
                p.`product_name`,
                invs.`quantity_available`, 
                invs.`reorder_point`, 
                invs.`reorder_quantity`,
                invs.`vendor_id`,
                v.`name` as vendor_name,
                invs.reorder_status
                FROM
                `tra_inventory_reorder_stock` AS invs 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.`id` = invs.`product_id` 
                INNER JOIN `mas_vendors` AS v 
                  ON v.`id` = invs.`vendor_id` 
                  WHERE
                    invs.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch adjust stock data by id');
        }
    }

    /**
     * validate record
     * 
     * @access public
     * @return object
     */
    public function validateProductInventory() {
        try {
            $statement = $this->_db->prepare('Select 
                count(*) as totRecord
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :inventory_product_id 
                  AND location_id = :inventory_location_id  '
            );
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('inventory_location_id', $this->getLocation());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['totRecord'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total record by product and location id');
        }
    }

    /**
     * validate record
     * 
     * @access public
     * @return object
     */
    public function getProductDetails() {
        try {
            $statement = $this->_db->prepare('Select 
                    *
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :inventory_product_id 
                  AND location_id = :inventory_location_id'
            );
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('inventory_location_id', $this->getLocation());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total adjust stock record by product and location id');
        }
    }
    
    /**
     * validate record
     * 
     * @access public
     * @return object
     */
    public function getProductPrice() {
        try {
            $statement = $this->_db->prepare('Select 
                    *
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :inventory_product_id 
                  AND location_id = :inventory_location_id'
            );
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total adjust stock record by product and location id');
        }
    }

    /**
     * Get adjust stock record by location and product Id
     * 
     * @access public
     * @return object
     */
    public function getAdjustStockByLocationAndProductId() {
        try {
            $statement = $this->_db->prepare('Select 
                        *
                    FROM
                    tra_inventory_adjust_stock
                  WHERE
                    inventory_product_id = :inventory_product_id 
                  AND inventory_location_id = :inventory_location_id  '
            );
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('inventory_location_id', $this->getLocation());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total adjust stock record by product and location id');
        }
    }

    public function receiveStock() {
        //Update or insert Quantity in Inventory
        $validateQty = $this->validateDataByLocationAndProduct($this->getProduct_id(), $this->getToLocation());
        if ($validateQty > 0) {
            $this->updateTransferProductQuantity();
        } else {
            $this->insertTransferProductQuantity();
        }

        $this->updateReceiveStock();
    }

    /**
     * Get transfer stock record
     * 
     * @access public
     * @return object
     */
    public function getTransferStock() {
        try {
            $statement = $this->_db->prepare('Select 
                        *
                    FROM
                    tra_inventory_transfer_stock
                  WHERE
                    id = :id
                '
            );
            $statement->bindValue('id', $this->getTransferStockId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch transfer stock record tranfer stock id');
        }
    }

    /**
     * inserts a record into stock transfer table
     * 
     * @access public
     * @return object
     */
    public function insertTransferStock() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_transfer_stock(
                    id,
                    inventory_product_id,
                    from_location_id,
                    to_location_id,
                    quantity,
                    transfer_date,
                    status,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :inventory_product_id,
                    :from_location_id,
                    :to_location_id,
                    :quantity,
                    :transfer_date,
                    :status,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('inventory_product_id', $this->getProduct_id());
            $statement->bindValue('from_location_id', $this->getLocation());
            $statement->bindValue('to_location_id', $this->getToLocation());
            $statement->bindValue('quantity', $this->getNewQuantity());
            $statement->bindValue('transfer_date', $this->getTransferDate());
            $statement->bindValue('status', $this->getStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setTransferStockId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * inserts prduct in stock inventory
     * 
     * @access public
     * @return object
     */
    public function insertTransferProductQuantity() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product_quantity(
                    id,
                    product_id,
                    quantity,
                    location_id
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :quantity,
                    :location_id
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $this->getNewQuantity());
            $statement->bindValue('location_id', $this->getToLocation());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

     /**
    * udpate product quanity
    * 
    * @access public
    * @return object
    */
    public function updateTransferProductQuantity() {
        try {
            $qty_to = $this->getQuantityByLocation($this->getProduct_id(), $this->getToLocation());
            $qtyToLocation = $qty_to + $this->getNewQuantity();

            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    quantity= :quantity
                  WHERE
                    location_id = :location_id
                    AND product_id =:product_id
                    '
            );
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $qtyToLocation);
            $statement->bindValue('location_id', $this->getToLocation());
            $statement->execute();


            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
    * udpate product quanity
    * 
    * @access public
    * @return object
    */
    public function sendTransferProductQuantity() {
        try {
            $qty_from = $this->getQuantityByLocation($this->getProduct_id(), $this->getLocation());

            $qtyFromLocation = $qty_from - $this->getNewQuantity();
            
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    quantity= :quantity
                  WHERE
                    location_id = :location_id
                    AND product_id =:product_id
                    '
            );
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity', $qtyFromLocation);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public function getQuantityByLocation($productId, $locationId) {
        try {
            $statement = $this->_db->prepare('Select 
                    quantity
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :inventory_product_id 
                  AND location_id = :inventory_location_id'
            );
            $statement->bindValue('inventory_product_id', $productId);
            $statement->bindValue('inventory_location_id', $locationId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $qty = $resultSet['quantity'];

            return $qty;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function validateDataByLocationAndProduct($productId, $locationId) {
        try {
            $statement = $this->_db->prepare('Select 
                    count(*) as totCount
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :inventory_product_id 
                  AND location_id = :inventory_location_id'
            );
            $statement->bindValue('inventory_product_id', $productId);
            $statement->bindValue('inventory_location_id', $locationId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $qty = $resultSet['totCount'];

            return $qty;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function sendStock() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_transfer_stock
                 SET
                    status= :status,
                    send_date =:send_date,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getTransferStockId());
            $statement->bindValue('send_date', $this->getSendDate());
            $statement->bindValue('status', $this->getStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function updateReceiveStock() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_transfer_stock
                 SET
                    status= :status,
                    received_date =:received_date,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getTransferStockId());
            $statement->bindValue('received_date', date('Y-m-d'));
            $statement->bindValue('status', $this->getStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertReorderStock() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_reorder_stock(
                    id,
                    product_id,
                    quantity_available,
                    reorder_point,
                    reorder_quantity,
                    vendor_id,
                    reorder_status,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :quantity_available,
                    :reorder_point,
                    :reorder_quantity,
                    :vendor_id,
                    :reorder_status,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity_available', $this->getQuantityAvailable());
            $statement->bindValue('reorder_point', $this->getReorderPoint());
            $statement->bindValue('reorder_quantity', $this->getReorderQuantity());
            $statement->bindValue('vendor_id', $this->getVendorId());
            $statement->bindValue('reorder_status', $this->getItemStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());


            //Update or insert Quantity in Inventory
            $validateQty = $this->validateProductInventory();
            if ($validateQty > 0) {
                $this->updateProductQuantity();
            } else {
                $this->insertProductQuantity();
            }

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate Adjust stock
     * 
     * @access public
     * @return object
     */
    public function updateReorderStock() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_reorder_stock
                 SET
                    id= :id,
                    product_id= :product_id,
                    quantity_available =:quantity_available,
                    reorder_point =:reorder_point,
                    reorder_quantity =:reorder_quantity,
                    vendor_id= :vendor_id,
                    reorder_status =:reorder_status,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_id', $this->getProduct_id());
            $statement->bindValue('quantity_available', $this->getQuantityAvailable());
            $statement->bindValue('reorder_point', $this->getReorderPoint());
            $statement->bindValue('reorder_quantity', $this->getReorderQuantity());
            $statement->bindValue('vendor_id', $this->getVendorId());
            $statement->bindValue('reorder_status', $this->getItemStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public function getSuggestedProduct($product) {
        try{
            if($product == "out of stock"){
                $statement = $this->_db->prepare('
                        Select id from tra_inventory_product
                        ');
                $statement->execute();
                $resultSet = $statement->fetchAll();
                //var_dump($resultSet);die;
                $data = array();
                foreach ($resultSet as $key=>$value) {
//                    echo $key.'<br>';
//                    var_dump($value).'<br>'; 
                    $statement = $this->_db->prepare('SELECT mas.id,mas.product_name,mas.last_vendor_id,pv.vendor_id,mas.reorder_quantity,ven.name,mas.product_name,SUM(qua.quantity) AS quantity FROM 
                             tra_inventory_product AS mas
                            LEFT JOIN tra_inventory_product_quantity AS qua  
                            ON mas.id = qua.product_id
                             LEFT JOIN  mas_vendors_product AS pv
                            ON pv.product_id = mas.id
                            LEFT JOIN mas_vendors AS ven
                            ON mas.last_vendor_id = ven.id
                            WHERE 
                            qua.product_id =:product_id');
                    $statement->bindValue('product_id',$value['id']);
                    $statement->execute();
                    $result=$statement->fetch();
                    //var_dump($result);die;
                    if($result['quantity'] < 1) {
                        $data[$key]['product_id'] = $value['id'];
                        $data[$key]['quantity'] = $result['quantity'];
                        $data[$key]['product_name'] = $result['product_name'];
                        $data[$key]['vendor_id'] = $result['last_vendor_id'];
                        $data[$key]['reorder_quantity'] = $result['reorder_quantity'];
                        $data[$key]['vendor_name'] = $result['name'];
                    }
                    else {
                        $data[$key]['product_id'] = "";
                        $data[$key]['quantity'] = "";
                        $data[$key]['product_name'] = "";
                    }
                }
                return $data;
            }
            if($product == "bill of materials") {
                 $statement = $this->_db->prepare(' SELECT 
                                                        mas.id as product_id,
                                                        mas.product_name,
                                                        mas.last_vendor_id as vendor_id,
                                                        mas.reorder_quantity,
                                                        ven.name AS vendor_name,
                                                        SUM(qua.quantity) AS quantity
                                                    FROM 
                                                        tra_inventory_product_bill_of_materials AS bom
                                                    LEFT JOIN 
                                                        tra_inventory_product AS mas
                                                    ON 
                                                        mas.id = bom.product_id
                                                    LEFT JOIN 
                                                        mas_vendors AS ven
                                                    ON 
                                                        mas.last_vendor_id = ven.id
                                                    LEFT JOIN 
                                                        tra_inventory_product_quantity AS qua
                                                    ON 
                                                        qua.product_id = bom.product_id');
                    $statement->execute();
                    $result=$statement->fetchAll();
                    return $result;
            }
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * 
     * @return type
     * 
     */
    public function getReorderProductQuantity() {
        try {
            $statement = $this->_db->prepare('Select 
                    SUM(quantity) as quantity
                    FROM
                    tra_inventory_product_quantity
                  WHERE
                    product_id = :product_id '
            );
            $statement->bindValue('product_id',$this->getProduct_id());
            $statement->execute();
            $resultSet = $statement->fetch();
            $qty = $resultSet['quantity'];

            return $qty;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function getProduct_id() {
        return $this->product_id;
    }

    public function setProduct_id($product_id) {
        $this->product_id = $product_id;
    }

    public function getNewQuantity() {
        return $this->newQuantity;
    }

    public function setNewQuantity($newQuantity) {
        $this->newQuantity = $newQuantity;
    }

    public function getOldQuantity() {
        return $this->oldQuantity;
    }

    public function setOldQuantity($oldQuantity) {
        $this->oldQuantity = $oldQuantity;
    }

    public function getDiff() {
        return $this->diff;
    }

    public function setDiff($diff) {
        $this->diff = $diff;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getInvDate() {
        return $this->invDate;
    }

    public function setInvDate($invDate) {
        $this->invDate = $invDate;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function getToLocation() {
        return $this->toLocation;
    }

    public function setToLocation($toLocation) {
        $this->toLocation = $toLocation;
    }

    public function getTransferDate() {
        return $this->transferDate;
    }

    public function setTransferDate($transferDate) {
        $this->transferDate = $transferDate;
    }

    public function getSendDate() {
        return $this->sendDate;
    }

    public function setSendDate($sendDate) {
        $this->sendDate = $sendDate;
    }

    public function getReceivedDate() {
        return $this->receivedDate;
    }

    public function setReceivedDate($receivedDate) {
        $this->receivedDate = $receivedDate;
    }

    public function getTransferStockId() {
        return $this->transferStockId;
    }

    public function setTransferStockId($transferStockId) {
        $this->transferStockId = $transferStockId;
    }

    public function getReorderPoint() {
        return $this->reorderPoint;
    }

    public function getVendorId() {
        return $this->vendorId;
    }

    public function getReorderQuantity() {
        return $this->reorderQuantity;
    }

    public function getQuantityAvailable() {
        return $this->quantityAvailable;
    }

    public function setReorderPoint($reorderPoint) {
        $this->reorderPoint = $reorderPoint;
    }

    public function setVendorId($vendorId) {
        $this->vendorId = $vendorId;
    }

    public function setReorderQuantity($reorderQuantity) {
        $this->reorderQuantity = $reorderQuantity;
    }

    public function setQuantityAvailable($quantityAvailable) {
        $this->quantityAvailable = $quantityAvailable;
    }

    public function getItemStatus() {
        return $this->itemStatus;
    }

    public function setItemStatus($itemStatus) {
        $this->itemStatus = $itemStatus;
    }
    

}
