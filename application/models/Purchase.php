<?php

/**
 * Application_Model_Purchase
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Purchase extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $poId;
    protected $vendorId;
    protected $contactPerson;
    protected $phone;
    protected $address;
    protected $address1;
    protected $address2;
    protected $addressType;
    protected $paymentTerms;
    protected $vendorOrderNo;
    protected $location;
    protected $orderDate;
    protected $orderStatus;
    protected $paymentStatus;
    protected $isShipping;
    protected $sippingAddress;
    protected $dueDate;
    protected $taxScheme;
    protected $nonVendorCost;
    protected $Currency;
    protected $regShipDate;
    protected $remarks;
    protected $grandSubTotal;
    protected $freight;
    protected $tax1;
    protected $taxper1;
    protected $taxAmount1;
    protected $tax2;
    protected $taxPer2;
    protected $taxAmount2;
    protected $netSubTotal;
    protected $paidAmount;
    protected $netAmount;
    protected $balanceAmount;
    protected $productRecordId;
    protected $productId;
    protected $vendor_product_code;
    protected $quantity;
    protected $unitPrice;
    protected $discount;
    protected $productSubTotal;
    protected $sessionid;
    protected $carrierId;
    protected $vendorContact;
    protected $vendorPhone;
    protected $vendorAddress;
    protected $shipReq;
    protected $taxOnShipping;
    protected $secondaryTaxOnShipping;
    protected $compoundSecondary;
    protected $transactionDate;
    protected $receiveLocationId;
    protected $receiveRemark;
    protected $returnRemark;
    protected $unstockRemark;
    protected $productStatus;
    protected $returnSubtotal;
    protected $returnFee;
    protected $returnTotal;
    protected $lastPayDate;
    protected $parant_item_id;
    

    /**
     * get purchase order
     * 
     * @access public
     * @return object
     */
    public function fetchPurchaseOrder() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                     `id`,
                    `po_id`,
                    date_format(po_date,"%d/%m/%Y") as po_date,
                    `order_status`,
                    `payment_status`,
                    `vendor_id`,
                    vendor_contact,
                    vendor_phone,
                    address_type,
                    address,
                    address1,
                    address2,
                    `location_id`,
                    date_format(req_shipping_date,"%d/%m/%Y") as req_shipping_date,
                    `terms_id`,
                    `v_order`,
                    ship_req,
                    `ship_to_address`,
                    date_format(due_date,"%d/%m/%Y") as due_date,
                    `non_vendor_costs`,
                    `currency_id`,
                    `sub_total`,
                    `freight`,
                    `taxing_scheme_id`,
                    `tax1_name`,
                    `tax1_per`,
                    tax_on_shipping,
                    `tax1_amount`,
                    `tax2_name`,
                    `tax2_per`,
                    secondary_tax_on_shipping,
                    compound_secondary,
                    `tax2_amount`,
                    `total_amount`,
                    `paid_amount`,
                    `balance`,
                    `remarks`,
                    `receive_remark`,
                    `return_remark`,
                    `unstock_remark`,
                    `ret_subtotal`,
                    `ret_fee`,
                    `ret_total`
                    from tra_purchase_order_mas
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch purchase order');
        }
    }
    
    /**
     * get purchase order
     * 
     * @access public
     * @return object
     */
    public function fetchPurchaseOrderForPrint() {
        try {
            $statement = $this->_db->prepare('
                    SELECT 
                        m.id,
                        m.po_id,
                        DATE_FORMAT(m.po_date, "%d/%m/%Y") AS po_date,
                        m.order_status,
                        m.payment_status,
                        v.name AS vendor_name,
                        m.vendor_contact,
                        m.vendor_phone,
                        m.address_type,
                        m.address,
                        m.address1,
                        m.address2,
                        l.name AS location,
                        DATE_FORMAT(m.req_shipping_date, "%d/%m/%Y") AS req_shipping_date,
                        t.name AS term,
                        m.v_order,
                        m.ship_req,
                        m.ship_to_address,
                        DATE_FORMAT(due_date, "%d/%m/%Y") AS due_date,
                        m.non_vendor_costs,
                        m.currency_id,
                        m.sub_total,
                        m.freight,
                        m.taxing_scheme_id,
                        m.tax1_name,
                        m.tax1_per,
                        m.tax_on_shipping,
                        m.tax1_amount,
                        m.tax2_name,
                        m.tax2_per,
                        m.secondary_tax_on_shipping,
                        m.compound_secondary,
                        m.tax2_amount,
                        m.total_amount,
                        m.paid_amount,
                        m.balance,
                        m.remarks,
                        m.receive_remark,
                        m.return_remark,
                        m.unstock_remark,
                        m.ret_subtotal,
                        m.ret_fee,
                        m.ret_total ,
                        m.carrier_id,
                        c.carrier_name,
                        u.name as ordered_by
                      FROM
                        tra_purchase_order_mas AS m
                        LEFT JOIN mas_vendors AS v
                        ON v.id = m.vendor_id
                        LEFT JOIN mas_inventory_location AS l
                        ON l.id = m.location_id
                        LEFT JOIN mas_payment_terms AS t
                        ON t.id = m.terms_id
                        LEFT JOIN users AS u
                        ON u.id = m.created_by
                        LEFT JOIN mas_carriers AS c
                        ON c.id = m.carrier_id
                  WHERE
                    m.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch purchase order');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO `tra_purchase_order_mas`(
                    id,
                    po_id,
                    po_date,
                    order_status,
                    payment_status,
                    vendor_id,
                    vendor_contact,
                    vendor_phone,
                    address_type,
                    address,
                    address1,
                    address2,
                    location_id,
                    req_shipping_date,
                    terms_id,
                    v_order,
                    ship_req,
                    ship_to_address,
                    carrier_id,
                    due_date,
                    non_vendor_costs,
                    currency_id,
                    sub_total,
                    freight,
                    taxing_scheme_id,
                    tax1_name,
                    tax1_per,
                    tax1_amount,
                    tax2_name,
                    tax2_per,
                    tax2_amount,
                    tax_on_shipping,
                    secondary_tax_on_shipping,
                    compound_secondary,
                    total_amount,
                    paid_amount,
                    balance, 
                    remarks,
                    receive_remark,
                    return_remark,
                    unstock_remark,
                    ret_subtotal,
                    ret_fee,
                    ret_total,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :po_id,
                    :po_date,
                    :order_status,
                    :payment_status,
                    :vendor_id,
                    :vendor_contact,
                    :vendor_phone,
                    :address_type,
                    :address,
                    :address1,
                    :address2,
                    :location_id,
                    :req_shipping_date,
                    :terms_id,
                    :v_order,
                    :ship_req,
                    :ship_to_address,
                    :carrier_id,
                    :due_date,
                    :non_vendor_costs,
                    :currency_id,
                    :sub_total,
                    :freight,
                    :taxing_scheme_id,
                    :tax1_name,
                    :tax1_per,
                    :tax1_amount,
                    :tax2_name,
                    :tax2_per,
                    :tax2_amount,
                    :tax_on_shipping,
                    :secondary_tax_on_shipping,
                    :compound_secondary,
                    :total_amount,
                    :paid_amount,
                    :balance,
                    :remarks,
                    :receive_remark,
                    :return_remark,
                    :unstock_remark,
                    :ret_subtotal,
                    :ret_fee,
                    :ret_total,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('po_id', $this->getPoId());
            $statement->bindValue('po_date', $this->getOrderDate());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('payment_status', $this->getPaymentStatus());
            $statement->bindValue('vendor_id', $this->getVendorId());
            $statement->bindValue('vendor_contact', $this->getVendorContact());
            $statement->bindValue('vendor_phone', $this->getVendorPhone());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('address_type', $this->getAddressType());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('req_shipping_date', $this->getRegShipDate());
            $statement->bindValue('terms_id', $this->getPaymentTerms());
            $statement->bindValue('v_order', $this->getVendorOrderNo());
            $statement->bindValue('ship_to_address', $this->getSippingAddress());
            $statement->bindValue('carrier_id', $this->getCarrierId());
            $statement->bindValue('due_date', $this->getDueDate());
            $statement->bindValue('non_vendor_costs', $this->getNonVendorCost());
            $statement->bindValue('currency_id', $this->getCurrency());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('taxing_scheme_id', $this->getTaxScheme());
            $statement->bindValue('tax1_name', $this->getTax1());
            $statement->bindValue('tax1_per', $this->getTaxper1());
            $statement->bindValue('tax1_amount', $this->getTaxAmount1());
            $statement->bindValue('tax2_name', $this->getTax2());
            $statement->bindValue('tax2_per', $this->getTaxPer2());
            $statement->bindValue('tax2_amount', $this->getTaxAmount2());
            $statement->bindValue('tax_on_shipping', $this->getTaxOnShipping());
            $statement->bindValue('secondary_tax_on_shipping', $this->getSecondaryTaxOnShipping());
            $statement->bindValue('compound_secondary', $this->getCompoundSecondary());
            $statement->bindValue('ship_req', $this->getShipReq());
            $statement->bindValue('total_amount', $this->getNetAmount());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('receive_remark', $this->getReceiveRemark());
            $statement->bindValue('return_remark', $this->getReturnRemark());
            $statement->bindValue('unstock_remark', $this->getUnstockRemark());
            $statement->bindValue('ret_subtotal', $this->getReturnSubtotal());
            $statement->bindValue('ret_fee', $this->getReturnFee());
            $statement->bindValue('ret_total', $this->getReturnTotal());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate order
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    po_id=:po_id,
                    vendor_id=:vendor_id,
                    vendor_contact=:vendor_contact,
                    vendor_phone=:vendor_phone,
                    address=:address,
                    address1=:address1,
                    address2=:address2,
                    address_type=:address_type,
                    location_id=:location_id,
                    req_shipping_date =:req_shipping_date,
                    terms_id =:terms_id,
                    v_order=:v_order,
                    ship_req =:ship_req,
                    ship_to_address =:ship_to_address,
                    carrier_id=:carrier_id,
                    due_date=:due_date,
                    non_vendor_costs =:non_vendor_costs,
                    currency_id=:currency_id,
                    sub_total=:sub_total,
                    freight=:freight,
                    taxing_scheme_id=:taxing_scheme_id,
                    tax1_name=:tax1_name,
                    tax1_per=:tax1_per,
                    tax1_amount=:tax1_amount,
                    tax2_name=:tax2_name,
                    tax2_per=:tax2_per,
                    tax2_amount=:tax2_amount,
                    tax_on_shipping=:tax_on_shipping,
                    secondary_tax_on_shipping=:secondary_tax_on_shipping,
                    compound_secondary=:compound_secondary,
                    total_amount=:total_amount,
                    paid_amount= :paid_amount,
                    balance= :balance,
                    remarks=:remarks,
                    receive_remark=:receive_remark,
                    return_remark=:return_remark,
                    unstock_remark=:unstock_remark,
                    ret_subtotal=:ret_subtotal,
                    ret_fee=:ret_fee,
                    ret_total=:ret_total,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('po_id', $this->getPoId());
            $statement->bindValue('vendor_id', $this->getVendorId());
            $statement->bindValue('vendor_contact', $this->getVendorContact());
            $statement->bindValue('vendor_phone', $this->getVendorPhone());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('address_type', $this->getAddressType());
            $statement->bindValue('ship_req', $this->getShipReq());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('req_shipping_date', $this->getRegShipDate());
            $statement->bindValue('terms_id', $this->getPaymentTerms());
            $statement->bindValue('v_order', $this->getVendorOrderNo());
            $statement->bindValue('ship_to_address', $this->getSippingAddress());
            $statement->bindValue('ship_req', $this->getShipReq());
            $statement->bindValue('carrier_id', $this->getCarrierId());
            $statement->bindValue('due_date', $this->getDueDate());
            $statement->bindValue('non_vendor_costs', $this->getNonVendorCost());
            $statement->bindValue('currency_id', $this->getCurrency());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('taxing_scheme_id', $this->getTaxScheme());
            $statement->bindValue('tax1_name', $this->getTax1());
            $statement->bindValue('tax1_per', $this->getTaxper1());
            $statement->bindValue('tax1_amount', $this->getTaxAmount1());
            $statement->bindValue('tax2_name', $this->getTax2());
            $statement->bindValue('tax2_per', $this->getTaxPer2());
            $statement->bindValue('tax2_amount', $this->getTaxAmount2());
            $statement->bindValue('tax_on_shipping', $this->getTaxOnShipping());
            $statement->bindValue('secondary_tax_on_shipping', $this->getSecondaryTaxOnShipping());
            $statement->bindValue('compound_secondary', $this->getCompoundSecondary());
            $statement->bindValue('total_amount', $this->getNetAmount());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('receive_remark', $this->getReceiveRemark());
            $statement->bindValue('return_remark', $this->getReturnRemark());
            $statement->bindValue('unstock_remark', $this->getUnstockRemark());
            $statement->bindValue('ret_subtotal', $this->getReturnSubtotal());
            $statement->bindValue('ret_fee', $this->getReturnFee());
            $statement->bindValue('ret_total', $this->getReturnTotal());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updatePurchaseOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_det
                 SET
                    order_id= :order_id,
                    updated= :updated,
                    updated_by= :updated_by,
                    sessionid = ""
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fulfilledOrder() {
        try {
//            $statement = $this->_db->prepare(
//                 'UPDATE tra_purchase_order_det
//                 SET
//                    item_status= "Receive",
//                    updated= :updated,
//                    updated_by= :updated_by
//                  WHERE
//                    order_id = :order_id'
//            );
//            $statement->bindValue('order_id', $this->getId());
//            $statement->bindValue('updated', $this->getUpdated());
//            $statement->bindValue('updated_by', $this->getUpdated_by());
//            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();



            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function unfulfilledOrder() {
        try {
            $statement = $this->_db->prepare(
                    'delete from  tra_purchase_order_det
                  WHERE
                    order_id = :order_id AND item_status="Receive"'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Unfulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();



            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     /**
     * Delete Unstock Products
     * 
     * @access public
     * @return object
     */
    public function deleteUnstock() {
        try {
            $statement = $this->_db->prepare(
                    'delete from  tra_purchase_order_det
                  WHERE
                    parant_item_id = :parant_item_id AND item_status="Unstock"'
            );
            $statement->bindValue('parant_item_id', $this->getParant_item_id());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     /**
     * delete received products
     * 
     * @access public
     * @return object
     */
    public function deleteReceived() {
        try {
            $statement = $this->_db->prepare(
                    'delete from  tra_purchase_order_det
                  WHERE
                    parant_item_id = :parant_item_id AND item_status="Receive"'
            );
            $statement->bindValue('parant_item_id', $this->getParant_item_id());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function paymentStatusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    payment_status= :payment_status,
                    balance =:balance,
                    paid_amount=:paid_amount,
                    updated= :updated,
                    updated_by= :updated_by,
                    last_pay_date = :last_pay_date
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('payment_status', $this->getPaymentStatus());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('last_pay_date', $this->getLastPayDate());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function statusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    payment_status= :payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('payment_status', $this->getPaymentStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function getPaidPayment($id) {
        try {
            $statement = $this->_db->prepare(
                    'select  sum(amount) as amount 
                     from `tra_order_payments` 
                  where `order_id`= :order_id and `order_type`="Purchase"
               '
            );
            $statement->bindValue('order_id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['amount'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateTotalPayment() {
        try {
            $status ='';
            $amount = $this->getPaidPayment($this->getId());
//            echo $amount;
            $purchase = Application_Model_PurchasePeer::fetchSubTotalById($this->getId());
            
           //echo $amount.'############'.$purchase['total_amount'];exit;
            if ($amount > $purchase['sub_total']) {
                $status = 'Owing';
            } elseif ($amount < $purchase['sub_total']) {
                $status = 'Partial';
            } elseif ($amount == $purchase['sub_total']) {
                $status == 'Paid';
            }

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    paid_amount= :paid_amount,
                    payment_status= :payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('paid_amount', $amount);
            $statement->bindValue('payment_status', $status);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function orderStatusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= :order_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
    public function fetchProductId($name) {
        try {
            $statement = $this->_db->prepare(
                    'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updatePurchaseOrderId() {

        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    po_id =:po_id
                  WHERE
                    id = :id'
            );
            $statement->bindValue('po_id', $this->getPoId());
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProduct() {
        if ($this->getProductRecordId() == null || $this->getProductRecordId() < 1) {
            $this->insertProduct();
        } else {
            $this->updateProduct();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertProduct() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_purchase_order_det(
                    id,
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    `sessionid`,
                    transaction_date,
                    location_id,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :order_id,
                    :item_status,
                    :product_id,
                    :vendor_product_code,
                    :quantity,
                    :unit_price,
                    :discount,
                    :sub_total,
                    :sessionid,
                    :transaction_date,
                    :location_id,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('item_status', $this->getProductStatus());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('unit_price', $this->getUnitPrice());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('sub_total', $this->getProductSubTotal());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('transaction_date', $this->getTransactionDate());
            $statement->bindValue('location_id', $this->getReceiveLocationId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProduct() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_det
                 SET
                    product_id=:product_id,
                    vendor_product_code=:vendor_product_code,
                    quantity=:quantity,
                    unit_price=:unit_price,
                    discount=:discount,
                    sub_total=:sub_total,
                    location_id=:location_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('unit_price', $this->getUnitPrice());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('sub_total', $this->getProductSubTotal());
            $statement->bindValue('location_id', $this->getReceiveLocationId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */
    public function autoFillReceiveProducts() {
        try {
            $statement = $this->_db->prepare(
                    "delete from tra_purchase_order_det 
                    WHERE order_id = :order_id1 and item_status='Receive'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_purchase_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    `transaction_date`,
                    `location_id`,
                    parant_item_id,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Receive',
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_purchase_order_det 
                    WHERE order_id = :order_id and item_status= 'Order'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */
    public function autoFillReturnsProducts() {
        try {
            $statement = $this->_db->prepare(
                    "delete from tra_purchase_order_det 
                    WHERE order_id = :order_id1 and item_status='Return'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_purchase_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    `transaction_date`,
                    `location_id`,
                    parant_item_id,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Return',
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    location_id,
                    id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_purchase_order_det 
                    WHERE order_id = :order_id and item_status= 'Order'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Started",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */
    public function autoFillUnstockProducts() {
        try {

            $statement = $this->_db->prepare(
                    "delete from tra_purchase_order_det 
                    WHERE order_id = :order_id1 and item_status= 'Unstock'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_purchase_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    `transaction_date`,
                    `location_id`,
                    parant_item_id,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Unstock',
                    `product_id`,
                    `vendor_product_code`,
                    `quantity`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_purchase_order_det 
                    WHERE order_id = :order_id and item_status= 'Return'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function getProductById() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                    det.id,
                    det.order_id,
                    det.item_status,
                    det.product_id,
                    ip.product_name,
                    det.vendor_product_code,
                    det.quantity,
                    det.unit_price,
                    det.discount,
                    det.sub_total,
                    DATE_FORMAT(det.transaction_date,"%d/%m/%Y") as transaction_date,
                    det.location_id
                     from tra_purchase_order_det as det
                      INNER JOIN tra_inventory_product ip 
                    ON ip.id = det.product_id 
                    where det.id =:id
                    '
            );

            $statement->bindValue('id', $this->getProductRecordId());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function getProductByProductIdAndStatus() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                    quantity
                 from tra_purchase_order_det
                where order_id =:order_id and product_id =:product_id
                and item_status =:status
             ');

            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('status', $this->getProductStatus());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet['quantity'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function getReceivedProductQty() {
        try {
            $str = '';
            if($this->getProductRecordId()) {
                $str = 'AND  id <> :id';
            }
            $statement = $this->_db->prepare(
                    'select 
                    sum(quantity) as quantity
                 from tra_purchase_order_det
                where order_id =:order_id and product_id =:product_id
                and item_status =:status ' . $str . '
             ');
            
            if($this->getProductRecordId()) {
                $statement->bindValue('id', $this->getProductRecordId());
            }
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('status', $this->getProductStatus());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet['quantity'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function fetchPurchaseOrderDetails() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT * FROM tra_purchase_order_det 
                WHERE (item_status = "Order" OR item_status="Return") 
                AND order_id = :purchase_id    
             ');
            $statement->bindValue('purchase_id', $this->getPoId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function cancelPurchaseOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Cancelled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();
            return;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function reopenPurchaseOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_mas
                 SET
                    order_status= "Unfulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();
            return;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * 
     * @return type
     * 
     */
    public function updateProductAvgCost() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_cost
                 SET
                    cost= :cost,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    product_id = :product_id'
            );
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('cost', $this->getUnitPrice());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();
            return;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getPoId() {
        return $this->poId;
    }

    public function setPoId($poId) {
        $this->poId = $poId;
    }

    public function getVendorId() {
        return $this->vendorId;
    }

    public function setVendorId($vendorId) {
        $this->vendorId = $vendorId;
    }

    public function getContactPerson() {
        return $this->contactPerson;
    }

    public function setContactPerson($contactPerson) {
        $this->contactPerson = $contactPerson;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getAddressType() {
        return $this->addressType;
    }

    public function setAddressType($addressType) {
        $this->addressType = $addressType;
    }

    public function getPaymentTerms() {
        return $this->paymentTerms;
    }

    public function setPaymentTerms($paymentTerms) {
        $this->paymentTerms = $paymentTerms;
    }

    public function getVendorOrderNo() {
        return $this->vendorOrderNo;
    }

    public function setVendorOrderNo($vendorOrderNo) {
        $this->vendorOrderNo = $vendorOrderNo;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function getOrderDate() {
        return $this->orderDate;
    }

    public function setOrderDate($orderDate) {
        $this->orderDate = $orderDate;
    }

    public function getOrderStatus() {
        return $this->orderStatus;
    }

    public function setOrderStatus($orderStatus) {
        $this->orderStatus = $orderStatus;
    }

    public function getPaymentStatus() {
        return $this->paymentStatus;
    }

    public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }

    public function getSippingAddress() {
        return $this->sippingAddress;
    }

    public function setSippingAddress($sippingAddress) {
        $this->sippingAddress = $sippingAddress;
    }

    public function getDueDate() {
        return $this->dueDate;
    }

    public function setDueDate($dueDate) {
        $this->dueDate = $dueDate;
    }

    public function getTaxScheme() {
        return $this->taxScheme;
    }

    public function setTaxScheme($taxScheme) {
        $this->taxScheme = $taxScheme;
    }

    public function getNonVendorCost() {
        return $this->nonVendorCost;
    }

    public function setNonVendorCost($nonVendorCost) {
        $this->nonVendorCost = $nonVendorCost;
    }

    public function getCurrency() {
        return $this->Currency;
    }

    public function setCurrency($Currency) {
        $this->Currency = $Currency;
    }

    public function getRegShipDate() {
        return $this->regShipDate;
    }

    public function setRegShipDate($regShipDate) {
        $this->regShipDate = $regShipDate;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function getGrandSubTotal() {
        return $this->grandSubTotal;
    }

    public function setGrandSubTotal($grandSubTotal) {
        $this->grandSubTotal = $grandSubTotal;
    }

    public function getFreight() {
        return $this->freight;
    }

    public function setFreight($freight) {
        $this->freight = $freight;
    }

    public function getTax1() {
        return $this->tax1;
    }

    public function setTax1($tax1) {
        $this->tax1 = $tax1;
    }

    public function getTax2() {
        return $this->tax2;
    }

    public function setTax2($tax2) {
        $this->tax2 = $tax2;
    }

    public function getNetSubTotal() {
        return $this->netSubTotal;
    }

    public function setNetSubTotal($netSubTotal) {
        $this->netSubTotal = $netSubTotal;
    }

    public function getPaidAmount() {
        return $this->paidAmount;
    }

    public function setPaidAmount($paidAmount) {
        $this->paidAmount = $paidAmount;
    }

    public function getNetAmount() {
        return $this->netAmount;
    }

    public function setNetAmount($netAmount) {
        $this->netAmount = $netAmount;
    }

    public function getBalanceAmount() {
        return $this->balanceAmount;
    }

    public function setBalanceAmount($balanceAmount) {
        $this->balanceAmount = $balanceAmount;
    }

    public function getProductRecordId() {
        return $this->productRecordId;
    }

    public function setProductRecordId($productRecordId) {
        $this->productRecordId = $productRecordId;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
    }

    public function getVendor_product_code() {
        return $this->vendor_product_code;
    }

    public function setVendor_product_code($vendor_product_code) {
        $this->vendor_product_code = $vendor_product_code;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function getUnitPrice() {
        return $this->unitPrice;
    }

    public function setUnitPrice($unitPrice) {
        $this->unitPrice = $unitPrice;
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function setDiscount($discount) {
        $this->discount = $discount;
    }

    public function getProductSubTotal() {
        return $this->productSubTotal;
    }

    public function setProductSubTotal($productSubTotal) {
        $this->productSubTotal = $productSubTotal;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getTaxper1() {
        return $this->taxper1;
    }

    public function setTaxper1($taxper1) {
        $this->taxper1 = $taxper1;
    }

    public function getTaxAmount1() {
        return $this->taxAmount1;
    }

    public function setTaxAmount1($taxAmount1) {
        $this->taxAmount1 = $taxAmount1;
    }

    public function getTaxPer2() {
        return $this->taxPer2;
    }

    public function setTaxPer2($taxPer2) {
        $this->taxPer2 = $taxPer2;
    }

    public function getTaxAmount2() {
        return $this->taxAmount2;
    }

    public function setTaxAmount2($taxAmount2) {
        $this->taxAmount2 = $taxAmount2;
    }

    public function getIsShipping() {
        return $this->isShipping;
    }

    public function setIsShipping($isShipping) {
        $this->isShipping = $isShipping;
    }

    public function getCarrierId() {
        return $this->carrierId;
    }

    public function setCarrierId($carrierId) {
        $this->carrierId = $carrierId;
    }

    public function getShipReq() {
        return $this->shipReq;
    }

    public function setShipReq($shipReq) {
        $this->shipReq = $shipReq;
    }

    public function getTaxOnShipping() {
        return $this->taxOnShipping;
    }

    public function setTaxOnShipping($taxOnShipping) {
        $this->taxOnShipping = $taxOnShipping;
    }

    public function getSecondaryTaxOnShipping() {
        return $this->secondaryTaxOnShipping;
    }

    public function setSecondaryTaxOnShipping($secondaryTaxOnShipping) {
        $this->secondaryTaxOnShipping = $secondaryTaxOnShipping;
    }

    public function getCompoundSecondary() {
        return $this->compoundSecondary;
    }

    public function setCompoundSecondary($compoundSecondary) {
        $this->compoundSecondary = $compoundSecondary;
    }

    public function getVendorContact() {
        return $this->vendorContact;
    }

    public function setVendorContact($vendorContact) {
        $this->vendorContact = $vendorContact;
    }

    public function getVendorPhone() {
        return $this->vendorPhone;
    }

    public function setVendorPhone($vendorPhone) {
        $this->vendorPhone = $vendorPhone;
    }

    public function getVendorAddress() {
        return $this->vendorAddress;
    }

    public function setVendorAddress($vendorAddress) {
        $this->vendorAddress = $vendorAddress;
    }

    public function getTransactionDate() {
        return $this->transactionDate;
    }

    public function setTransactionDate($transactionDate) {
        $this->transactionDate = $transactionDate;
    }

    public function getReceiveLocationId() {
        return $this->receiveLocationId;
    }

    public function setReceiveLocationId($receiveLocationId) {
        $this->receiveLocationId = $receiveLocationId;
    }

    public function getReceiveRemark() {
        return $this->receiveRemark;
    }

    public function setReceiveRemark($receiveRemark) {
        $this->receiveRemark = $receiveRemark;
    }

    public function getReturnRemark() {
        return $this->returnRemark;
    }

    public function setReturnRemark($returnRemark) {
        $this->returnRemark = $returnRemark;
    }

    public function getProductStatus() {
        return $this->productStatus;
    }

    public function setProductStatus($productStatus) {
        $this->productStatus = $productStatus;
    }

    public function getReturnSubtotal() {
        return $this->returnSubtotal;
    }

    public function setReturnSubtotal($returnSubtotal) {
        $this->returnSubtotal = $returnSubtotal;
    }

    public function getReturnFee() {
        return $this->returnFee;
    }

    public function setReturnFee($returnFee) {
        $this->returnFee = $returnFee;
    }

    public function getReturnTotal() {
        return $this->returnTotal;
    }

    public function setReturnTotal($returnTotal) {
        $this->returnTotal = $returnTotal;
    }

    public function getUnstockRemark() {
        return $this->unstockRemark;
    }

    public function setUnstockRemark($unstockRemark) {
        $this->unstockRemark = $unstockRemark;
    }

    public function getLastPayDate() {
        return $this->lastPayDate;
    }

    public function setLastPayDate($lastPayDate) {
        $this->lastPayDate = $lastPayDate;
    }
    

    public function getParant_item_id() {
        return $this->parant_item_id;
    }

    public function setParant_item_id($parant_item_id) {
        $this->parant_item_id = $parant_item_id;
    }
    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }



}
