<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_CountryPeer {

    /**
     * get all records form master table
     * fetch all countries from master tables
     * @return array
     */
    public static function fetchAllCountries() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					c.id,c.country_name,c.country_code 
                                        FROM mas_countries as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateCountry($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  country_name =:country_name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_countries
                                        where country_name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('country_name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
