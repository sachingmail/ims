<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_CurrencyPeer {

    /**
     * get all records form mas_currency table
     * fetch all currencies from master tables
     * @return array
     */
    public static function fetchAllCurrency() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					c.id,c.currency_name,c.currency_code 
                                        FROM mas_currency as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateCurrency($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  currency_name =:currency_name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_currency
                                        where currency_name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('currency_name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateCurrencyCode($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  currency_code =:currency_code";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_currency
                                        where currency_code <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('currency_code', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
