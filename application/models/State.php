<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_State extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $country_id;
    protected $state_name;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_states(
                    id,
                    state_name,
                    country_id,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :state_name,
                    :country_id,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('state_name', $this->getState_name());
            $statement->bindValue('country_id', $this->getCountry_id());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_states
                 SET
                    state_name = :state_name,
                    country_id = :country_id,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('state_name', $this->getState_name());
            $statement->bindValue('country_id', $this->getCountry_id());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch state by id
     * 
     * @access public
     * @return object
     */
    public function fetchStateById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT s.state_name,s.id,c.country_name,s.country_id
                                            FROM mas_states AS s
                                            LEFT JOIN mas_countries AS c
                                            ON s.country_id = c.id
                    WHERE
                            s.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * check state is present in the user table
     * @return object
     * 
     */
    public function checkStateIsPresnt() {
         try {
            $statement = $this->_db->prepare(
                    'select * from mas_states
                    where
                    state_name =:state_name
                    and
                    country_id = :country_id
            ');
            $statement->bindValue('state_name', $this->getState_name());
            $statement->bindValue('country_id', $this->getCountry_id());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function getCountry_id() {
        return $this->country_id;
    }

    public function getState_name() {
        return $this->state_name;
    }

    public function setCountry_id($country_id) {
        $this->country_id = $country_id;
    }

    public function setState_name($state_name) {
        $this->state_name = $state_name;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

}
