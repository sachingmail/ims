<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_StatePeer {

    /**
     * get all records form master table
     * fetch all countries from master tables
     * @return array
     */
    public static function fetchAllCountries() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					id,country_name 
                                        FROM mas_countries order by country_name asc
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     *
     * fetch all states from master tables
     * @return array
     */
    public static function fetchAllState() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT s.state_name,s.id,c.country_name
                                            FROM mas_states AS s
                                            LEFT JOIN mas_countries AS c
                                            ON s.country_id = c.id
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
