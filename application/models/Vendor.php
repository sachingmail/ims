<?php

/**
 * Application_Model_Customer
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Vendor extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $name;
    protected $balance;
    protected $credit;
    protected $address_type;
    protected $address;
    protected $address1;
    protected $address2;
    protected $person_name;
    protected $phone;
    protected $fax;
    protected $email;
    protected $website;
    protected $currency;
    protected $payment_term;
    protected $tax_scheme;
    protected $carrier;
    protected $is_active;
    protected $remarks;
    protected $productRecordId;
    protected $productCode;
    protected $vendor_product_code;
    protected $product_cost;
    protected $sessionid;
    protected $paid_amount;
    protected $payment_status;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_vendors(
                    id,
                    `name`,
                    `balance`,
                    `credit`,
                    `address_type`,
                    `address`,
                    `address1`,
                    `address2`,
                    `contact_name`,
                    `contact_phone`,
                    `contact_fax`,
                    `contact_email`,
                    `contact_website`,
                    `currency_id`,
                    `payment_terms`,
                    `taxing_schemes`,
                    `carrier_id`,
                    `is_active`,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :balance,
                    :credit,
                    :address_type,
                    :address,
                    :address1,
                    :address2,
                    :contact_name,
                    :contact_phone,
                    :contact_fax,
                    :contact_email,
                    :contact_website,
                    :currency_id,
                    :payment_terms,
                    :taxing_schemes,
                    :carrier_id,
                    :is_active,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('balance', $this->getBalance());
            $statement->bindValue('credit', $this->getCredit());
            $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('contact_name', $this->getPerson_name());
            $statement->bindValue('contact_phone', $this->getPhone());
            $statement->bindValue('contact_fax', $this->getFax());
            $statement->bindValue('contact_email', $this->getEmail());
            $statement->bindValue('contact_website', $this->getWebsite());
            $statement->bindValue('currency_id', $this->getCurrency());
            $statement->bindValue('payment_terms', $this->getPayment_term());
            $statement->bindValue('taxing_schemes', $this->getTax_scheme());
            $statement->bindValue('carrier_id', $this->getCarrier());
            $statement->bindValue('is_active', $this->getIs_active());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors
                 SET
                    name= :name,
                    balance= :balance,
                    credit= :credit,
                    address_type= :address_type,
                    address= :address,
                    address1= :address1,
                    address2= :address2,
                    contact_name= :contact_name,
                    contact_phone= :contact_phone,
                    contact_fax= :contact_fax,
                    contact_email= :contact_email,
                    contact_website= :contact_website,
                    currency_id= :currency_id,
                    payment_terms= :payment_terms,
                    taxing_schemes= :taxing_schemes,
                    carrier_id= :carrier_id,
                    is_active= :is_active,
                    remarks= :remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('balance', $this->getBalance());
            $statement->bindValue('credit', $this->getCredit());
            $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('contact_name', $this->getPerson_name());
            $statement->bindValue('contact_phone', $this->getPhone());
            $statement->bindValue('contact_fax', $this->getFax());
            $statement->bindValue('contact_email', $this->getEmail());
            $statement->bindValue('contact_website', $this->getWebsite());
            $statement->bindValue('currency_id', $this->getCurrency());
            $statement->bindValue('payment_terms', $this->getPayment_term());
            $statement->bindValue('taxing_schemes', $this->getTax_scheme());
            $statement->bindValue('carrier_id', $this->getCarrier());
            $statement->bindValue('is_active', $this->getIs_active());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateBalance() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors
                 SET
                    balance= :balance
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('balance', $this->getBalance());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateCredit() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors
                 SET
                    credit= :credit
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('credit', $this->getCredit());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchVendorById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_vendors
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor data');
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchVendorProductById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                    vp.id,
                    vp.vendor_id,
                    ip.product_name,
                    vp.vendor_product_code,
                    vp.cost
                    FROM 
                    mas_vendors_product vp
                    inner join tra_inventory_product ip
                        on ip.id = vp.product_id
                   where 
                    vp.id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor product data');
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchVendorByProductId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                    vp.id,
                    v.name,
                    vp.vendor_product_code,
                    vp.cost
                    FROM 
                    mas_vendors_product vp
                    inner join mas_vendors v
                        on v.id = vp.vendor_id
                   where 
                    vp.id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor product data');
        }
    }

    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
    public function fetchProductId($name) {
        try {
            $statement = $this->_db->prepare(
                    'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProduct() {
        if ($this->getProductRecordId() == null || $this->getProductRecordId() < 1) {
            $this->insertProduct();
        } else {
            $this->updateProduct();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertProduct() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_vendors_product(
                    id,
                    `vendor_id`,
                    `product_id`,
                    `vendor_product_code`,
                    `cost`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :vendor_id,
                    :product_id,
                    :vendor_product_code,
                    :cost,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('cost', $this->getProduct_cost());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProduct() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    product_id =:product_id,
                    vendor_product_code = :vendor_product_code,
                    cost= :cost,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('cost', $this->getProduct_cost());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateVendorId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    vendor_id =:vendor_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    vendor_id = :vendor_id'
            );
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * Deactivate Vendor
     * 
     */
    public function DeactivateVendor() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors
                 SET
                    is_active = "0",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * Activate Vendor 
     * 
     */
    public function ActivateVendor() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors
                 SET
                    is_active = "1",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchVendorByName($name) {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_vendors
                  WHERE
                    name = :name'
            );
            $statement->bindValue('name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor data');
        }
    }

    public function checkProductCodeIsPresent() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_vendors_product
                  WHERE
                    vendor_id = :id and 
                    vendor_product_code = :vendor_product_code and
                    sessionid = :sessionid
                    '
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor data');
        }
    }

    public function checkProductNameIsPresent() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_vendors_product
                  WHERE
                    vendor_id = :id and 
                    product_id = :product_id and
                    sessionid = :sessionid
                    '
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor data');
        }
    }

    public function checkVendorProduct() {
        try {
            $statement = $this->_db->prepare(
                    'select count(*) as tot from mas_vendors_product
                  WHERE
                    vendor_id = :vendor_id and 
                    product_id = :product_id
                    '
            );
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate vendor data');
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function UpdatePaidAndBalance() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE 
                                        tra_purchase_order_mas
                             SET 	 
                                        paid_amount = :paid_amount,
                                        balance = :balance,
                                        payment_status = :payment_status
                             WHERE	
                                        id = :id
                            '
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('payment_status', $this->getPayment_status());
            $statement->bindValue('paid_amount', $this->getPaid_amount());
            $statement->bindValue('balance', $this->getBalance());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate vendor data');
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getBalance() {
        return $this->balance;
    }

    public function setBalance($balance) {
        $this->balance = $balance;
    }

    public function getCredit() {
        return $this->credit;
    }

    public function setCredit($credit) {
        $this->credit = $credit;
    }

    public function getAddress_type() {
        return $this->address_type;
    }

    public function setAddress_type($address_type) {
        $this->address_type = $address_type;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getPerson_name() {
        return $this->person_name;
    }

    public function setPerson_name($person_name) {
        $this->person_name = $person_name;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getFax() {
        return $this->fax;
    }

    public function setFax($fax) {
        $this->fax = $fax;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getWebsite() {
        return $this->website;
    }

    public function setWebsite($website) {
        $this->website = $website;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    public function getPayment_term() {
        return $this->payment_term;
    }

    public function setPayment_term($payment_term) {
        $this->payment_term = $payment_term;
    }

    public function getTax_scheme() {
        return $this->tax_scheme;
    }

    public function setTax_scheme($tax_scheme) {
        $this->tax_scheme = $tax_scheme;
    }

    public function getCarrier() {
        return $this->carrier;
    }

    public function setCarrier($carrier) {
        $this->carrier = $carrier;
    }

    public function getIs_active() {
        return $this->is_active;
    }

    public function setIs_active($is_active) {
        $this->is_active = $is_active;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function getProductCode() {
        return $this->productCode;
    }

    public function setProductCode($productCode) {
        $this->productCode = $productCode;
    }

    public function getVendor_product_code() {
        return $this->vendor_product_code;
    }

    public function setVendor_product_code($vendor_product_code) {
        $this->vendor_product_code = $vendor_product_code;
    }

    public function getProduct_cost() {
        return $this->product_cost;
    }

    public function setProduct_cost($product_cost) {
        $this->product_cost = $product_cost;
    }

    public function getProductRecordId() {
        return $this->productRecordId;
    }

    public function setProductRecordId($productRecordId) {
        $this->productRecordId = $productRecordId;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getPaid_amount() {
        return $this->paid_amount;
    }

    public function setPaid_amount($paid_amount) {
        $this->paid_amount = $paid_amount;
    }

    public function getPayment_status() {
        return $this->payment_status;
    }

    public function setPayment_status($payment_status) {
        $this->payment_status = $payment_status;
    }

    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }

}
