<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_Movement extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $productId;
    protected $type;
    protected $mDate;
    protected $orderId;
    protected $qtyBefore;
    protected $qty;
    protected $qtyAfter;
    protected $locationId;
    protected $total_amount;

    /**
     * inserts a record into table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_movement_history(
                    id,
                    product_id,
                    type,
                    movement_date,
                    location,
                    order_id,
                    qty_before,
                    qty,
                    qty_after,
                    total_amount
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :type,
                    :movement_date,
                    :location,
                    :order_id,
                    :qty_before,
                    :qty,
                    :qty_after,
                    :total_amount
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('type', $this->getType());
            $statement->bindValue('movement_date', $this->getMDate());
            $statement->bindValue('location', $this->getLocationId());
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('qty_before', $this->getQtyBefore());
            $statement->bindValue('qty', $this->getQty());
            $statement->bindValue('qty_after', $this->getQtyAfter());
            $statement->bindValue('total_amount',$this->getTotal_amount());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getMDate() {
        return $this->mDate;
    }

    public function setMDate($mDate) {
        $this->mDate = $mDate;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function getQtyBefore() {
        return $this->qtyBefore;
    }

    public function setQtyBefore($qtyBefore) {
        $this->qtyBefore = $qtyBefore;
    }

    public function getQty() {
        return $this->qty;
    }

    public function setQty($qty) {
        $this->qty = $qty;
    }

    public function getQtyAfter() {
        return $this->qtyAfter;
    }

    public function setQtyAfter($qtyAfter) {
        $this->qtyAfter = $qtyAfter;
    }

    public function getLocationId() {
        return $this->locationId;
    }

    public function setLocationId($locationId) {
        $this->locationId = $locationId;
    }

    public function getTotal_amount() {
        return $this->total_amount;
    }

    public function setTotal_amount($total_amount) {
        $this->total_amount = $total_amount;
    }

}
