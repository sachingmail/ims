<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_ReceivingAddress extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $ReceivingAddressName;
    protected $StreetName;
    protected $CityId;
    protected $ZipCode;
    protected $AddressType;
    protected $Remarks;
    protected $CityName;
    protected $CountryId;
    protected $StateId;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_receiving_addresses(
                    id,
                    address_name,
                    street,
                    city_id,
                    zip_code,
                    remarks,
                    address_type,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :address_name,
                    :street,
                    :city_id,
                    :zip_code,
                    :remarks,
                    :address_type,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('address_name', $this->getReceivingAddressName());
            $statement->bindValue('street', $this->getStreetName());
            $statement->bindValue('city_id', $this->getCityId());
            $statement->bindValue('zip_code', $this->getZipCode());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('address_type', $this->getAddressType());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_receiving_addresses
                 SET
                    address_name= :address_name,
                    street = :street,
                    city_id = :city_id,
                    zip_code = :zip_code,
                    remarks = :remarks,
                    address_type= :address_type, 
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('address_name', $this->getReceivingAddressName());
            $statement->bindValue('street', $this->getStreetName());
            $statement->bindValue('city_id', $this->getCityId());
            $statement->bindValue('zip_code', $this->getZipCode());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('address_type', $this->getAddressType());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch receiving address by id
     * 
     * @access public
     * @return object
     */
    public function fetchReceivingAddressById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT a.zip_code,a.address_name,a.remarks,a.address_type,a.street,a.id,c.city_name,s.state_name,co.country_name,c.state_id,c.country_id
                            FROM mas_cities AS c
                            INNER JOIN mas_receiving_addresses AS a 
                            ON a.city_id = c.id
                            INNER JOIN mas_states AS s
                            ON s.id = c.state_id
                            INNER JOIN mas_countries AS co
                            ON co.id = c.country_id
                    WHERE
                            a.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * fetch all states by country id 
     * @return type
     * 
     */
    public function getStatesByCountryId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT id,state_name
                        FROM mas_states
                    WHERE
                        country_id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * fetch country by country id
     * @return type
     */
    public function getCountryByCountryId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT c.id,c.country_name 
                            FROM mas_countries AS c
                            INNER JOIN mas_states AS s 
                            ON s.country_id = c.id
                     WHERE 
                            s.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * fetch all state and country by city name
     * @return type
     * 
     */
    public function getStateCountryByCityName() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT co.country_name,s.state_name,c.country_id,c.state_id
                            FROM mas_cities AS c
                            INNER JOIN mas_states AS s 
                            ON s.id =  c.state_id
                            INNER JOIN mas_countries AS co
                            ON co.id = c.country_id
                     WHERE 
                            c.city_name = :name'
            );
            $statement->bindValue('name', $this->getCityName());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * used to check whether city is present in table or not
     * @return type
     * 
     */
    public function checkCityExistence() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT * FROM mas_cities 
                     WHERE 
                            city_name = :name AND
                            country_id = :country_id AND
                            state_id = :state_id'
            );
            $statement->bindValue('name', $this->getCityName());
            $statement->bindValue('state_id', $this->getStateId());
            $statement->bindValue('country_id', $this->getCountryId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * used to check whether a address is present or not
     * @return type
     * 
     */
    public function checkAddressName() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT * FROM mas_receiving_addresses 
                     WHERE 
                            address_name = :address_name
                           
            ');
            $statement->bindValue('address_name', $this->getReceivingAddressName());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function getCityName() {
        return $this->CityName;
    }

    public function setCityName($CityName) {
        $this->CityName = $CityName;
    }

    public function getCountryId() {
        return $this->CountryId;
    }

    public function getStateId() {
        return $this->StateId;
    }

    public function setCountryId($CountryId) {
        $this->CountryId = $CountryId;
    }

    public function setStateId($StateId) {
        $this->StateId = $StateId;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    public function getReceivingAddressName() {
        return $this->ReceivingAddressName;
    }

    public function getStreetName() {
        return $this->StreetName;
    }

    public function setReceivingAddressName($ReceivingAddressName) {
        $this->ReceivingAddressName = $ReceivingAddressName;
    }

    public function setStreetName($StreetName) {
        $this->StreetName = $StreetName;
    }

    public function getCityId() {
        return $this->CityId;
    }

    public function getZipCode() {
        return $this->ZipCode;
    }

    public function getAddressType() {
        return $this->AddressType;
    }

    public function getRemarks() {
        return $this->Remarks;
    }

    public function setCityId($CityId) {
        $this->CityId = $CityId;
    }

    public function setZipCode($ZipCode) {
        $this->ZipCode = $ZipCode;
    }

    public function setAddressType($AddressType) {
        $this->AddressType = $AddressType;
    }

    public function setRemarks($Remarks) {
        $this->Remarks = $Remarks;
    }

}
