<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_CustomerPeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllCustomers() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id, `name`,`balance`,`credit`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`
                                        FROM mas_customers
                                        where is_active = "1"');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllCustomersList() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id, `name`,`balance`,`credit`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`
                                        FROM mas_customers');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCustomerOrders($customer_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        id,
                                        order_id,
                                        order_date,
                                        CONCAT_WS(",", order_status, payment_status) AS order_status,
                                        total_amount,
                                        paid_amount,
                                        (total_amount - paid_amount) AS balance 
                                      FROM
                                        tra_sales_order_mas where customer_id =:customer_id');
            $statement->bindValue('customer_id', $customer_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCustomerDetails($id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('
                SELECT 
                id,
                name,
                balance,
                credit,
                address,
                contact_name,
                contact_phone,
                contact_fax,
                contact_email,
                pricing_currency_id,
                payment_terms,
                taxing_schemes,
                default_location_id,
                contact_website 
              FROM
                mas_customers 
              WHERE id = :id ');
            
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validatePhone($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  contact_phone =:contact_phone";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_customers
                                        where contact_phone <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('contact_phone', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateEmail($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  contact_email =:contact_email";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_customers
                                        where contact_email <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('contact_email', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateName($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  name =:name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_customers
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
}

?>
