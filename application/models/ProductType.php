<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_ProductType extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $ProductName;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_product_type(
                    id,
                    name,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getProductName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_product_type
                 SET
                    name = :name,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getProductName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch product by id
     * 
     * @access public
     * @return object
     */
    public function fetchProductById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_product_type
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * check if product type is used in some records or not 
     * 
     */
    public function checkIfProductTypeIsPresentInRecords() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_inventory_product 
                    WHERE  
                            product_type_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function getProductName() {
        return $this->ProductName;
    }

    public function setProductName($ProductName) {
        $this->ProductName = $ProductName;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

}
