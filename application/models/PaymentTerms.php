<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_PaymentTerms extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $PaymentTerms;
    protected $days;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_payment_terms(
                    id,
                    name,
                    days,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :days,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getPaymentTerms());
            $statement->bindValue('days', $this->getDays());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_payment_terms
                 SET
                    name = :name,
                    days = :days,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getPaymentTerms());
            $statement->bindValue('days', $this->getDays());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch payment method by id from master table
     * 
     * @access public
     * @return object
     */
    public function fetchPaymentById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_payment_terms
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * check if Terms is present in records or not
     */
    public function checkIfTermsPresentInRecords() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            mas_vendors 
                    WHERE  
                            payment_terms = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['vendors'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_quote_order_mas 
                    WHERE  
                            terms_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_quote'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            mas_customers 
                    WHERE  
                            payment_terms = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['customers'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_purchase_order_mas 
                    WHERE  
                            terms_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['purchase'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_order_mas 
                    WHERE  
                            terms_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_order'] = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
             DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * @return type
     * all setters and getters 
     */
    public function getId() {
        return $this->id;
    }

    public function getPaymentTerms() {
        return $this->PaymentTerms;
    }

    public function setPaymentTerms($PaymentTerms) {
        $this->PaymentTerms = $PaymentTerms;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    public function getDays() {
        return $this->days;
    }

    public function setDays($days) {
        $this->days = $days;
    }

}
