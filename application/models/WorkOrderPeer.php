<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_WorkOrderPeer {

    /**
     * get all work orders
     * @return array
     */
    public static function fetchAllWorkOrder() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(" SELECT 
                mas.id,
                mas.`work_order_no`,
                DATE_FORMAT(mas.`scheduled_date`, '%d %M %Y') AS scheduled_date,
                mas.`order_status`,
                mas.`order_type`,
                l.name AS location
              FROM
                `tra_work_order_mas` AS mas 
               LEFT JOIN `mas_inventory_location` AS l 
                  ON l.id = mas.location_id  ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
    /**
     * get all Type of orders
     * @return array
     */
    public static function fetchOrderTypes() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(" SELECT 
                code as id, meaning as name
              FROM
                `lookup`
                  where type='ORDER_TYPE'");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch orderTypes');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchProductsBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                    det.id,
                    det.work_order_id,
                    ip.product_name,
                    det.`product_status`,
                    det.`quantity`,
                    DATE_FORMAT(det.date_scheduled,"%d/%m/%Y") AS date_scheduled
                  FROM
                    `tra_work_order_det` AS det 
                    INNER JOIN tra_inventory_product ip 
                      ON ip.id = det.product_id 
                  WHERE det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchProductsByOrderId($orderId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                    det.id,
                    det.work_order_id,
                    det.product_id,
                    ip.product_name,
                    det.`product_status`,
                    det.`quantity`,
                    DATE_FORMAT(det.date_scheduled,"%d/%m/%Y") AS date_scheduled
                  FROM
                    `tra_work_order_det` AS det 
                    INNER JOIN tra_inventory_product ip 
                      ON ip.id = det.product_id 
                  WHERE det.work_order_id =:orderId');
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * get total product count
     * @return array
     */
    public static function getTotalProductCount($orderId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                    count(*) as tot
                  FROM
                    `tra_work_order_det`
                  WHERE work_order_id =:orderId');
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get total product count
     * @return array
     */
    public static function getTotalCompletedProductCount($orderId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare("SELECT 
                    count(*) as tot
                  FROM
                    `tra_work_order_det`
                  WHERE work_order_id =:orderId 
                  AND product_status ='Fulfilled'
            ");
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchBOMProducts($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                b.id AS bomId,
                p.id,
                p.`product_name`,
                b.`quantity`,
                b.`quantity_used`,
                b.work_status,
                q.`quantity` AS avilQty 
              FROM
                `tra_work_order_bom` AS b 
                INNER JOIN `tra_work_order_det` AS d 
                  ON d.`id` = b.`wo_product_id` 
                INNER JOIN `tra_work_order_mas` AS m 
                  ON m.id = d.`work_order_id` 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.id = b.`bom_product_id` 
                LEFT JOIN `tra_inventory_product_quantity` AS q
                ON q.`product_id` = b.`bom_product_id` AND q.`location_id` = m.`location_id` 
            WHERE d.id =:id');
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchBOMByProducts($orderId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                    det.id,
                    det.work_order_id,
                    ip.product_name,
                    det.`product_status`,
                    det.`quantity`,
                    DATE_FORMAT(det.date_scheduled,"%d/%m/%Y") AS date_scheduled
                  FROM
                    `tra_work_order_det` AS det 
                    INNER JOIN tra_inventory_product ip 
                      ON ip.id = det.product_id 
                  WHERE det.work_order_id =:orderId');
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchBOMProductByRecordId($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                   bom_product_id,quantity
                  FROM
                    `tra_work_order_bom` 
                  WHERE id =:id');
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all work orders
     * @return array
     */
    public static function fetchAllWorkOrderByStatus($status) {
        $db = Zend_Registry::get('db');
        try {
            $str='';
            if($status == "open") {
                $str = 'WHERE mas,status = "open"';
            }
            if($status == "completed") {
                $str = 'WHERE mas,status = "completed"';
            }
            $statement = $db->prepare(" SELECT 
                mas.id,
                mas.`work_order_no`,
                DATE_FORMAT(mas.`scheduled_date`, '%d %M %Y') AS scheduled_date,
                mas.`order_status`,
                mas.`order_type`,
                l.name AS location
              FROM
                `tra_work_order_mas` AS mas 
               LEFT JOIN `mas_inventory_location` AS l 
                  ON l.id = mas.location_id 
                " . $str . "  
                ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
}

?>