<?php

/**
 * Application_Model_Customer
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_SalesQuote extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $phone;
    protected $customerId;
    protected $address_type;
    protected $customer_address;
    protected $address1;
    protected $address2;
    protected $customerPhone;
    protected $locationId;
    protected $contact;
    protected $orderId;
    protected $salesRepsId;
    protected $remarks;
    protected $freight;
    protected $totalAmount;
    protected $dueDate;
    protected $paymentStatus;
    protected $shippingAddress;
    protected $reqShippingDate;
    protected $paymentTermsId;
    protected $orderDate;
    protected $status;
    protected $po_ref;
    protected $tax1_name;
    protected $tax1_percent;
    protected $tax1_amount;
    protected $tax2_name;
    protected $tax2_percent;
    protected $tax2_amount;
    protected $taxingSchemeId;
    protected $pricingCurrencyId;
    protected $nonCustomerCosts;
    protected $sessionid;
    protected $SalesProductId;
    protected $salesQuantity;
    protected $salesDiscount;
    protected $salesUnitPrice;
    protected $salesTotal;
    protected $salesSubTotal;
    protected $addressId;
    protected $designation;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_sales_quote_order_mas(
                    id,
                    `customer_id`,
                    designation,
                    `address_type`,
                    `customer_address`,
                    `address1`,
                    `address2`,
                    `customer_phone`,
                    `order_date`,
                    `sales_rep_id`,
                    `location_id`,
                    `order_status`,
                    `terms_id`,
                    `po_ref`,
                    `shipping_address`,
                    `non_customer_costs`,
                    `taxing_scheme_id`,
                    `tax1_name`,
                    `tax1_per`,
                    `tax1_amount`,
                    `tax2_name`,
                    `tax2_per`,
                    `tax2_amount`,
                    `pricing_currency_id`,
                    `req_ship_date`,
                    `sub_total`,
                    `freight`,
                    `total_amount`,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :customer_id,
                    :designation,
                    :address_type,
                    :customer_address,
                    :address1,
                    :address2,
                    :customer_phone,
                    :order_date,
                    :sales_rep_id,
                    :location_id,
                    :order_status,
                    :terms_id,
                    :po_ref,
                    :shipping_address,
                    :non_customer_costs,
                    :taxing_scheme_id,
                    :tax1_name,
                    :tax1_per,
                    :tax1_amount,
                    :tax2_name,
                    :tax2_per,
                    :tax2_amount,
                    :pricing_currency_id,
                    :req_ship_date,
                    :sub_total,
                    :freight,
                    :total_amount,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('customer_id', $this->getCustomerId());
            $statement->bindValue('designation', $this->getDesignation());
            $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('customer_address', $this->getCustomerAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('customer_phone', $this->getCustomerPhone());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('sales_rep_id', $this->getSalesRepsId());
            $statement->bindValue('location_id', $this->getLocationId());
            $statement->bindValue('order_status', $this->getStatus());
            $statement->bindValue('terms_id', $this->getPaymentTermsId());
            $statement->bindValue('po_ref', $this->getPo_ref());
            $statement->bindValue('shipping_address', $this->getShippingAddress());
            $statement->bindValue('non_customer_costs', $this->getNonCustomerCosts());
            $statement->bindValue('taxing_scheme_id', $this->getTaxingSchemeId());
            $statement->bindValue('tax1_name', $this->getTax1_name());
            $statement->bindValue('tax1_per', $this->getTax1_percent());
            $statement->bindValue('tax1_amount', $this->getTax1_amount());
            $statement->bindValue('tax2_name', $this->getTax2_name());
            $statement->bindValue('tax2_per', $this->getTax2_percent());
            $statement->bindValue('tax2_amount', $this->getTax2_amount());
            $statement->bindValue('pricing_currency_id', $this->getPricingCurrencyId());
            $statement->bindValue('req_ship_date', $this->getReqShippingDate());
            $statement->bindValue('sub_total', $this->getSalesSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('total_amount', $this->getSalesTotal());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_quote_order_mas
                 SET
                    customer_id= :customer_id,
                    designation =:designation,
                    address_type= :address_type,
                    customer_address= :customer_address,
                    address1= :address1,
                    address2= :address2,
                    customer_phone= :customer_phone,
                    order_date= :order_date,
                    sales_rep_id= :sales_rep_id,
                    location_id= :location_id,
                    order_status= :order_status,
                    terms_id= :terms_id,
                    po_ref= :po_ref,
                    shipping_address= :shipping_address,
                    non_customer_costs= :non_customer_costs,
                    taxing_scheme_id= :taxing_scheme_id,
                    tax1_name= :tax1_name,
                    tax1_per= :tax1_per,
                    tax1_amount= :tax1_amount,
                    tax2_name= :tax2_name,
                    tax2_per= :tax2_per,
                    tax2_amount= :tax2_amount,
                    pricing_currency_id= :pricing_currency_id,
                    req_ship_date= :req_ship_date,
                    sub_total= :sub_total,
                    freight= :freight,
                    total_amount= :total_amount,
                    remarks= :remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('customer_id', $this->getCustomerId());
            $statement->bindValue('designation', $this->getDesignation());
           $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('customer_address', $this->getCustomerAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('customer_phone', $this->getCustomerPhone());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('sales_rep_id', $this->getSalesRepsId());
            $statement->bindValue('location_id', $this->getLocationId());
            $statement->bindValue('order_status', $this->getStatus());
            $statement->bindValue('terms_id', $this->getPaymentTermsId());
            $statement->bindValue('po_ref', $this->getPo_ref());
            $statement->bindValue('shipping_address', $this->getShippingAddress());
            $statement->bindValue('non_customer_costs', $this->getNonCustomerCosts());
            $statement->bindValue('taxing_scheme_id', $this->getTaxingSchemeId());
            $statement->bindValue('tax1_name', $this->getTax1_name());
            $statement->bindValue('tax1_per', $this->getTax1_percent());
            $statement->bindValue('tax1_amount', $this->getTax1_amount());
            $statement->bindValue('tax2_name', $this->getTax2_name());
            $statement->bindValue('tax2_per', $this->getTax2_percent());
            $statement->bindValue('tax2_amount', $this->getTax2_amount());
            $statement->bindValue('pricing_currency_id', $this->getPricingCurrencyId());
            $statement->bindValue('req_ship_date', $this->getReqShippingDate());
            $statement->bindValue('sub_total', $this->getSalesSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('total_amount', $this->getSalesTotal());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateOrderId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_quote_order_det
                 SET
                    order_id =:order_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_quote_order_det
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    order_id = :order_id'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateSalesQuoteOrderId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_quote_order_mas
                 SET
                    order_id =:order_id
                  WHERE
                    id = :id'
            );
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get sales quote by quote id
     * @return type\
     */
    public function fetchSalesQuoteByQuoteId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT tsqod.id,tsqod.order_id,tsqod.quantity,tsqod.product_id,
                                        tsqod.unit_price,tsqod.discount,
                                        tsqod.sub_total,tip.product_name
                                        FROM 
                                        tra_sales_quote_order_det AS tsqod
                                        LEFT JOIN 
                                        tra_inventory_product AS tip
                                        ON 
                                        tsqod.product_id = tip.id
                   where 
                    tsqod.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor product data');
        }
    }

    /**
     * get all sales quote data
     * @return type
     */
    public function fetchSalesQuoteByIdOrderMas() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT tsqom.id,tsqom.customer_id,tsqom.address_type,tsqom.customer_address,tsqom.address1,tsqom.address2,
                        tsqom.designation,
                  tsqom.customer_phone,tsqom.order_id,date_format(tsqom.order_date,"%d/%m/%Y") as order_date,
                  tsqom.sales_rep_id,tsqom.location_id,tsqom.order_status,tsqom.terms_id,
                  tsqom.po_ref,tsqom.shipping_address,tsqom.non_customer_costs,
                  tsqom.taxing_scheme_id,tsqom.tax1_name,tsqom.tax1_per,tsqom.tax1_amount,
                  tsqom.tax2_name,tsqom.tax2_per,tsqom.tax2_amount,tsqom.pricing_currency_id,
                  date_format(tsqom.req_ship_date,"%d/%m/%Y") as req_ship_date,tsqom.sub_total,tsqom.freight,
                    tsqom.total_amount,tsqom.remarks
                    FROM 
                    tra_sales_quote_order_mas AS tsqom
                   where 
                    tsqom.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor product data');
        }
    }

    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
    public function fetchProductId($name) {
        try {
            $statement = $this->_db->prepare(
                    'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveSalesItem() {
        if ($this->getOrderId() == null || $this->getOrderId() < 1) {
            $this->insertSalesItem();
        } else {
            $this->updateSalesItem();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertSalesItem() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_sales_quote_order_det(
                    id,
                    `order_id`,
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :order_id,
                    :product_id,
                    :quantity,
                    :unit_price,
                    :discount,
                    :sub_total,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getSalesProductId());
            $statement->bindValue('quantity', $this->getSalesQuantity());
            $statement->bindValue('unit_price', $this->getSalesUnitPrice());
            $statement->bindValue('discount', $this->getSalesDiscount());
            $statement->bindValue('sub_total', $this->getSalesTotal());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setOrderId($this->_db->lastInsertId());
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateSalesItem() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_quote_order_det
                 SET
                    order_id=:order_id,
                    product_id =:product_id,
                    quantity =:quantity,
                    unit_price=:unit_price,
                    discount=:discount,
                    sub_total=:sub_total,
                    updated=:updated,
                    updated_by=:updated_by
                  WHERE
                    id =:id'
            );
            $statement->bindValue('id', $this->getOrderId());
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getSalesProductId());
            $statement->bindValue('quantity', $this->getSalesQuantity());
            $statement->bindValue('unit_price', $this->getSalesUnitPrice());
            $statement->bindValue('discount', $this->getSalesDiscount());
            $statement->bindValue('sub_total', $this->getSalesTotal());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch sales product by session id
     * @param type $sesionId
     * @return type
     */
    public static function fetchSalesProductBySession($sesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT tsqod.id,tsqod.order_id,tsqod.quantity,tsqod.product_id,
                                        tsqod.unit_price,tsqod.discount,
                                        tsqod.sub_total,tip.product_name
                                        FROM 
                                        tra_sales_quote_order_det AS tsqod
                                        LEFT JOIN 
                                        tra_inventory_product AS tip
                                        ON 
                                        tsqod.product_id = tip.id
                                       where tsqod.sessionid =:session_id');
            $statement->bindValue('session_id', $sesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch sales product by order id
     * @param type $order_id
     * @return type
     */
    public static function fetchSalesProductsByOrderId($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT tsqod.id,tsqod.order_id,tsqod.quantity,tsqod.product_id,
                                        tsqod.unit_price,
                                        tsqod.discount,tsqod.sub_total,tip.product_name
                                        FROM 
                                        tra_sales_quote_order_det AS tsqod
                                        LEFT JOIN 
                                        tra_inventory_product AS tip
                                        ON 
                                        tsqod.product_id = tip.id
                                       WHERE
                                       tsqod.order_id =:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch customer details from masters
     * @return type
     */
    public function fetchCustomerDetails() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT cu.name,cu.address_type,cu.address,cu.address1,cu.address2,cu.city_id,cu.id,cu.designation,
                                        c.city_name,s.state_name,co.country_name,
                                        cu.zip_code,cu.contact_phone,cu.contact_name,
                                        cu.pricing_currency_id,cu.payment_terms,
                                        cu.taxing_schemes,cu.default_location_id,cu.sales_reps
                                        FROM mas_customers AS cu
                                        LEFT JOIN mas_cities AS c
                                        ON cu.city_id = c.id
                                        LEFT JOIN mas_states AS s 
                                        ON c.state_id = s.id
                                        LEFT JOIN mas_countries AS co
                                        ON c.country_id = co.id
                                        WHERE
                                        cu.id=:id');
            $statement->bindValue('id', $this->getCustomerId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * 
     */
    public function fetchSalesQuoteMasData() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                     select * from tra_sales_quote_order_mas
                     where id =:id
                    ');
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $result = $statement->fetch();
            return $result;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function fetchSalesQuoteDetData() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                     select * from tra_sales_quote_order_det
                     where order_id =:order_id
                    ');
            $statement->bindValue('order_id', $this->getId());
            $statement->execute();
            $result = $statement->fetchAll();
            return $result;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * 
     */
    public function UpdateSalesQuote() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                     UPDATE tra_sales_quote_order_mas
                     SET order_status = "Approved"
                     where id =:id
                    ');
            $statement->bindValue('id', $this->getId());
            $statement->execute();
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * 
     */
    public function CancelQuote() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                     UPDATE tra_sales_quote_order_mas
                     SET order_status =:status
                     where id =:id
                    ');
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('status', $this->getStatus());
            $statement->execute();
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get subtotal
     * @return type
     */
    public function fetchSubTotal() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('select sub_total from tra_sales_quote_order_det where sessionid=:session_id');
            $statement->bindValue('session_id', $this->getSessionid());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch subtotal by id
     * @return type
     */
    public function fetchSubTotalById() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('select sub_total from tra_sales_quote_order_det where order_id=:order_id');
            $statement->bindValue('order_id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get last order
     * @return type
     */
    public function fetchLastOrderId() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT order_id FROM tra_sales_quote_order_mas
                                        ORDER BY id DESC
                                        LIMIT 1;');
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch unit price by product id
     * @return type
     */
    public function fetchUnitPriceByProductId() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT tipc.price,mpc.id,tipc.product_id 
                        FROM tra_inventory_product_cost AS tipc
                        INNER JOIN mas_pricing_currency AS mpc
                        ON mpc.id = tipc.price_currency_id
                        WHERE 
                        tipc.product_id = :id
                        AND 
                        mpc.default = :default');
            $statement->bindValue('default', 1);
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function fetchSalesQuoteOrderForPrint() {
        try {
            $statement = $this->_db->prepare('
                    SELECT 
                        m.id,
                        m.order_id,
                        DATE_FORMAT(m.order_date, "%d/%m/%Y") AS so_date,
                        m.order_status,
                        m.payment_status,
                        c.name AS customer_name,
                        c.contact_name,
                        m.customer_phone,
                        m.customer_address,
                        l.name AS location,
                        DATE_FORMAT(m.req_ship_date, "%d/%m/%Y") AS req_shipping_date,
                        t.name AS term,
                        m.po_ref,
                        m.shipping_address,
                        DATE_FORMAT(due_date, "%d/%m/%Y") AS due_date,
                        m.non_customer_costs,
                        m.pricing_currency_id,
                        m.sub_total,
                        m.freight,
                        m.taxing_scheme_id,
                        m.tax1_name,
                        m.tax1_per,
                        m.tax1_amount,
                        m.tax2_name,
                        m.tax2_per,
                        m.tax2_amount,
                        m.total_amount,
                        m.remarks,
                        tax.tax_on_shipping,
                        tax.secondary_tax_on_shipping,
                        tax.compound_secondary
                      FROM
                        tra_sales_quote_order_mas AS m
                        LEFT JOIN mas_customers AS c
                        ON c.id = m.customer_id
                        LEFT JOIN mas_inventory_location AS l
                        ON l.id = m.location_id
                        LEFT JOIN mas_payment_terms AS t
                        ON t.id = m.terms_id
                        LEFT JOIN mas_taxing_schemes as tax
                        ON m.taxing_scheme_id = tax.id
                  WHERE
                    m.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch purchase order');
        }
    }

    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getCustomerId() {
        return $this->customerId;
    }

    public function getCustomerAddress() {
        return $this->customerAddress;
    }

    public function getCustomerPhone() {
        return $this->customerPhone;
    }

    public function getLocationId() {
        return $this->locationId;
    }

    public function getContact() {
        return $this->contact;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function getSalesRepsId() {
        return $this->salesRepsId;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function getFreight() {
        return $this->freight;
    }

    public function getTotalAmount() {
        return $this->totalAmount;
    }

    public function getDueDate() {
        return $this->dueDate;
    }

    public function getPaymentStatus() {
        return $this->paymentStatus;
    }

    public function getShippingAddress() {
        return $this->shippingAddress;
    }

    public function getReqShippingDate() {
        return $this->reqShippingDate;
    }

    public function getPaymentTermsId() {
        return $this->paymentTermsId;
    }

    public function getOrderDate() {
        return $this->orderDate;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getPo_ref() {
        return $this->po_ref;
    }

    public function getTax1_name() {
        return $this->tax1_name;
    }

    public function getTax1_percent() {
        return $this->tax1_percent;
    }

    public function getTax1_amount() {
        return $this->tax1_amount;
    }

    public function getTax2_name() {
        return $this->tax2_name;
    }

    public function getTax2_percent() {
        return $this->tax2_percent;
    }

    public function getTax2_amount() {
        return $this->tax2_amount;
    }

    public function getTaxingSchemeId() {
        return $this->taxingSchemeId;
    }

    public function getPricingCurrencyId() {
        return $this->pricingCurrencyId;
    }

    public function getNonCustomerCosts() {
        return $this->nonCustomerCosts;
    }

    public function getSalesProductId() {
        return $this->SalesProductId;
    }

    public function getSalesQuantity() {
        return $this->salesQuantity;
    }

    public function getSalesDiscount() {
        return $this->salesDiscount;
    }

    public function getSalesUnitPrice() {
        return $this->salesUnitPrice;
    }

    public function getSalesTotal() {
        return $this->salesTotal;
    }

    public function getSalesSubTotal() {
        return $this->salesSubTotal;
    }

    public function getAddressId() {
        return $this->addressId;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    public function setCustomerAddress($customerAddress) {
        $this->customerAddress = $customerAddress;
    }

    public function setCustomerPhone($customerPhone) {
        $this->customerPhone = $customerPhone;
    }

    public function setLocationId($locationId) {
        $this->locationId = $locationId;
    }

    public function setContact($contact) {
        $this->contact = $contact;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function setSalesRepsId($salesRepsId) {
        $this->salesRepsId = $salesRepsId;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function setFreight($freight) {
        $this->freight = $freight;
    }

    public function setTotalAmount($totalAmount) {
        $this->totalAmount = $totalAmount;
    }

    public function setDueDate($dueDate) {
        $this->dueDate = $dueDate;
    }

    public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }

    public function setShippingAddress($shippingAddress) {
        $this->shippingAddress = $shippingAddress;
    }

    public function setReqShippingDate($reqShippingDate) {
        $this->reqShippingDate = $reqShippingDate;
    }

    public function setPaymentTermsId($paymentTermsId) {
        $this->paymentTermsId = $paymentTermsId;
    }

    public function setOrderDate($orderDate) {
        $this->orderDate = $orderDate;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setPo_ref($po_ref) {
        $this->po_ref = $po_ref;
    }

    public function setTax1_name($tax1_name) {
        $this->tax1_name = $tax1_name;
    }

    public function setTax1_percent($tax1_percent) {
        $this->tax1_percent = $tax1_percent;
    }

    public function setTax1_amount($tax1_amount) {
        $this->tax1_amount = $tax1_amount;
    }

    public function setTax2_name($tax2_name) {
        $this->tax2_name = $tax2_name;
    }

    public function setTax2_percent($tax2_percent) {
        $this->tax2_percent = $tax2_percent;
    }

    public function setTax2_amount($tax2_amount) {
        $this->tax2_amount = $tax2_amount;
    }

    public function setTaxingSchemeId($taxingSchemeId) {
        $this->taxingSchemeId = $taxingSchemeId;
    }

    public function setPricingCurrencyId($pricingCurrencyId) {
        $this->pricingCurrencyId = $pricingCurrencyId;
    }

    public function setNonCustomerCosts($nonCustomerCosts) {
        $this->nonCustomerCosts = $nonCustomerCosts;
    }

    public function setSalesProductId($SalesProductId) {
        $this->SalesProductId = $SalesProductId;
    }

    public function setSalesQuantity($salesQuantity) {
        $this->salesQuantity = $salesQuantity;
    }

    public function setSalesDiscount($salesDiscount) {
        $this->salesDiscount = $salesDiscount;
    }

    public function setSalesUnitPrice($salesUnitPrice) {
        $this->salesUnitPrice = $salesUnitPrice;
    }

    public function setSalesTotal($salesTotal) {
        $this->salesTotal = $salesTotal;
    }

    public function setSalesSubTotal($salesSubTotal) {
        $this->salesSubTotal = $salesSubTotal;
    }

    public function setAddressId($addressId) {
        $this->addressId = $addressId;
    }

    public function getDesignation() {
        return $this->designation;
    }

    public function setDesignation($designation) {
        $this->designation = $designation;
    }
    
    public function getAddress_type() {
        return $this->address_type;
    }

    public function setAddress_type($address_type) {
        $this->address_type = $address_type;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }



}
