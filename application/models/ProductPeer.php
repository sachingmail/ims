<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_ProductPeer extends DMC_Model_Abstract {

    /**
     * get all records form roles table
     * @return array
     */
    public static function fetchAllProduct() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					id,product_name from tra_inventory_product
                                        where is_active = "1"
					order by product_name
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    
    public static function fetchAllBOMProduct() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					id,product_name from tra_inventory_product
                                        where is_active = "1" AND product_type_id = 1
					order by product_name
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    
    /**
     * get all records form roles table
     * @return array
     */
    public static function fetchAllProductInOrder($order_id,$session_id) {
        $db = Zend_Registry::get('db');
        try {
            if($order_id){
                $str = "AND det.order_id=:order_id";
            }
            if($session_id){
                $str = "AND det.sessionid=:session_id";
            }
            
            $statement = $db->prepare('SELECT 
                            DISTINCT p.product_name,
                            p.id
                          FROM
                          `tra_purchase_order_det` AS det
                          INNER JOIN tra_inventory_product AS p
                          ON p.`id` = det.`product_id`
                          '.$str.' 
                          ORDER BY product_name ');

            if($order_id){
                $statement->bindValue('order_id', $order_id);
            }
            if($session_id){
               $statement->bindValue('session_id', $session_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product in order');
        }
    }
    /**
     * get all records form sales table
     * @return array
     */
    public static function fetchAllSalesProductInOrder($order_id,$session_id) {
        $db = Zend_Registry::get('db');
        try {
            if($order_id){
                $str = "AND det.order_id=:order_id";
            }
            if($session_id){
                $str = "AND det.sessionid=:session_id";
            }
            
            $statement = $db->prepare('SELECT 
                            DISTINCT p.product_name,
                            p.id
                          FROM
                          `tra_sales_order_det` AS det
                          INNER JOIN tra_inventory_product AS p
                          ON p.`id` = det.`product_id`
                          '.$str.' 
                          ORDER BY product_name ');

            if($order_id){
                $statement->bindValue('order_id', $order_id);
            }
            if($session_id){
               $statement->bindValue('session_id', $session_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product in order');
        }
    }
    /**
     * get all records form sales table
     * @return array
     */
    public static function fetchAllSalesQuoteProductInOrder($order_id,$session_id) {
        $db = Zend_Registry::get('db');
        try {
            if($order_id){
                $str = "AND det.order_id=:order_id";
            }
            if($session_id){
                $str = "AND det.sessionid=:session_id";
            }
            
            $statement = $db->prepare('SELECT 
                            DISTINCT p.product_name,
                            p.id
                          FROM
                          `tra_sales_quote_order_det` AS det
                          INNER JOIN tra_inventory_product AS p
                          ON p.`id` = det.`product_id`
                          '.$str.' 
                          ORDER BY product_name ');

            if($order_id){
                $statement->bindValue('order_id', $order_id);
            }
            if($session_id){
               $statement->bindValue('session_id', $session_id);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product in order');
        }
    }
    
    /**
     * get all records form roles table
     * @return array
     */
    public static function fetchAllProductByCategory($cat_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
                    id,product_name from tra_inventory_product
                where product_category_id =:cat_id					
                order by product_name');
            $statement->bindValue('cat_id', $cat_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    /**
     * get all records form roles table
     * @return array
     */
    public static function fetchAllMovements($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                        p.`product_name`,
                        DATE_FORMAT(m.`movement_date`,"%d/%m/%Y") AS mdate,
                        m.`qty_before`,
                        m.`qty`,
                        m.`qty_after`,
                        l.`name` AS location_name,
                        m.`type`
                      FROM
                        `tra_movement_history` AS m 
                        INNER JOIN `tra_inventory_product` AS p 
                          ON p.`id` = m.product_id 
                        INNER JOIN `mas_inventory_location` AS l 
                          ON l.`id` = m.`location` 
                       WHERE m.`product_id` = :product_id');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * get record form product by ID
     * @return array
     */
    public static function fetchProductById($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT * from tra_inventory_product
					where id =:id');

            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    public static function fetchAllInvetoryProducts() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT p.id,p.product_name,pt.name AS product_type_name,pc.name AS product_category_name,p.remarks
                                        FROM tra_inventory_product AS p
                                        LEFT JOIN mas_product_type AS pt
                                        ON p.product_type_id = pt.id
                                        LEFT JOIN mas_product_category AS pc
                                        ON p.product_category_id = pc.id
                                    ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * get all product type from masters
     * @return type
     */
    public static function fetchAllProductType() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,`name` 
                                        FROM mas_product_type
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * fetch all product categories from master tables
     * @return type
     */
    public static function fetchAllProductCategory() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,`name`
                                       FROM mas_product_category
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
	
	/**
     * fetch all product categories from master tables
     * @return type
     */
    public static function fetchAllCategory() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,`name`
                                       FROM mas_product_category where parent_id=0
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
	
	public static function getCatRec($id){
		 $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,`name`
                                       FROM mas_product_category where parent_id=:id
                                        ');
										
			$statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
	}

    /**
     * fetch all units from master tables
     * @return type
     */
    public static function fetchAllUnits() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,unit_name AS `name` 
                                        FROM mas_units
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * fetch all inventory location
     * @return type
     */
    public static function fetchAllInvenotryLocation() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,name
                                        FROM mas_inventory_location
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * fetch all vendors from master tables
     * @return type
     */
    public static function fetchAllVendors() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,name
                                        FROM mas_vendors
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }

    /**
     * get all vendors by product id
     * @return array
     */
    public static function fetchVendorsByProductId($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        vp.id,
                                        v.name,
                                        vp.product_id,
                                        vp.vendor_product_code,
                                        vp.cost
                                        FROM 
                                        mas_vendors_product vp
                                        inner join mas_vendors v
                                            on v.id = vp.vendor_id
                                       where vp.product_id =:product_id');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors list by products');
        }
    }

    /**
     * fetch all vendors by session id
     * @return array
     */
    public static function fetchVendorsBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                                        SELECT 
                                        vp.id,
                                        v.name,
                                        vp.product_id,
                                        vp.vendor_product_code,
                                        vp.cost
                                        FROM 
                                        mas_vendors_product vp
                                        inner join mas_vendors v
                                            on v.id = vp.vendor_id
                                       where vp.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors using session');
        }
    }

    /**
     * get all materials by product id
     * @return array
     */
    public static function fetchMaterialsByProductId($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                bom.id,
                                bom.product_id,
                                p.product_name,
                                bom.quantity,
                                bom.raw_product_id,
                                bom.cost 
                              FROM
                                tra_inventory_product_bill_of_materials bom 
                                INNER JOIN tra_inventory_product p 
                                  ON p.id = bom.raw_product_id
                               where bom.product_id =:product_id');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors list by products');
        }
    }

    /**
     * fetch all inventory location by session id
     * @return array
     */
    public static function fetchInventoryLocationBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT ipq.id,ipq.product_id,ipq.location_id,il.name AS location_name,ipq.quantity
                                        FROM tra_inventory_product_quantity ipq
                                        INNER JOIN mas_inventory_location il
                                        ON il.id = ipq.location_id
                                        where ipq.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors using session');
        }
    }

    /**
     * fetch inventory location by product id(not by session id)
     * @param type $product_id
     * @return type
     */
    public static function fetchInventoryLocationByProductId($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT ipq.id,ipq.product_id,ipq.location_id,il.name AS location_name,ipq.quantity
                                        FROM tra_inventory_product_quantity ipq
                                        INNER JOIN mas_inventory_location il
                                        ON il.id = ipq.location_id
                                        where ipq.product_id =:product_id');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            
            $organizations = array();
            
            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors list by products');
        }
    }

    /**
     * get all materials by session id
     * @return array
     */
    public static function fetchMaterialsBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                bom.id,
                                bom.product_id,
                                p.product_name,
                                bom.quantity,
                                bom.raw_product_id,
                                bom.cost 
                              FROM
                                tra_inventory_product_bill_of_materials bom 
                                INNER JOIN tra_inventory_product p 
                                  ON p.id = bom.raw_product_id
                             where bom.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendors using session');
        }
    }

    /**
     * fetch all pricing curreny from master tables
     * @return type
     * 
     */
    public static function fetchAllPricingCurrency() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT *
                                        FROM mas_pricing_currency
                                        ');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    public static function fetchProductCostOnPricingCurrency($product_id,$pricing) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT DISTINCT
                                pc.`id`,pc.name,c.`price`,c.`markup`,c.`cost`,c.id,c.price_currency_id
                                FROM `mas_pricing_currency` AS pc
                                LEFT JOIN `tra_inventory_product_cost` AS c
                                ON c.`price_currency_id` = pc.`id` 
                                WHERE c.product_id = :product_id AND c.price_currency_id = :price_currency_id
                                        ');
            $statement->bindValue('product_id',$product_id);
            $statement->bindValue('price_currency_id',$pricing);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    public static function fetchProductCostId($product_id,$pricing) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id FROM 
                                        tra_inventory_product_cost
                                        WHERE
                                        price_currency_id = :price_currency_id
                                        AND 
                                        product_id =:product_id
                                        ');
            $statement->bindValue('product_id',$product_id);
            $statement->bindValue('price_currency_id',$pricing);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    
    public function fetchProductOrderHIstory($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT  mas.id, det.item_status,mas.order_id,mas.customer_name AS representative,DATE_FORMAT(mas.`order_date`,"%d %M %Y") AS order_date,
                    CONCAT(mas.order_status,",",mas.payment_status) AS order_status,(mas.total_amount-mas.paid_amount) AS sub_total,
                     det.quantity,

                    CASE mas.order_status
                            WHEN "Started" THEN "Sales Returned"
                            WHEN "Unfulfilled" THEN "Sales Order"
                            WHEN "Fulfilled" THEN "Sales Fulfilled"
                    END AS `type`
                    FROM tra_sales_order_mas AS mas
                    INNER JOIN tra_sales_order_det AS det
                    ON det.order_id = mas.id AND det.item_status = "Order" 
                     WHERE det.product_id =:product_id

                                        ');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $sales = $statement->fetchAll();
            
            $statement = $db->prepare('SELECT  mas.id, det.item_status,mas.po_id as order_id,mas.vendor_contact AS representative,DATE_FORMAT(mas.`po_date`,"%d %M %Y") AS order_date,
                    CONCAT(mas.order_status,",",mas.payment_status) AS order_status,(mas.total_amount-mas.paid_amount) AS sub_total,
                    det.quantity,

                    CASE mas.order_status
                            WHEN "Started" THEN "Purchase Returned"
                            WHEN "Unfulfilled" THEN "Purchase Order"
                            WHEN "Fulfilled" THEN "Purchase Fulfilled"
                    END AS `type`
                    FROM tra_purchase_order_mas AS mas
                    INNER JOIN tra_purchase_order_det AS det
                    ON det.order_id = mas.id AND det.item_status = "Order" 
                    WHERE det.product_id =:product_id');
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $purchase = $statement->fetchAll();
            
            $resultSet = array_merge($sales,$purchase);
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product');
        }
    }
    /**
     * 
     * @param type $id
     * @param type $name
     * @return type
     * 
     */
    public static function validateProduct($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  product_name =:product_name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM tra_inventory_product
                                        where product_name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('product_name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    public static function getProductCost($id) {
        $db = Zend_Registry::get('db');
        try {
            $sql = 'SELECT 
                        cost 
                      FROM
                        `tra_inventory_product_cost` 
                   where product_id=:id GROUP BY product_id';
            $statement = $db->prepare($sql);
            
            $statement->bindValue('id', $id);
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['cost'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product cost');
        }
    }
    
}

?>
