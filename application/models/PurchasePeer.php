<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_PurchasePeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function getOrderStatus($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(" SELECT 
                    order_status,
                    payment_status
                  FROM
                    tra_purchase_order_mas 
                  where id=:id");
            $statement->bindValue('id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to order Status');
        }
    }
    
    public static function getOrderData($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(" SELECT 
                    *
                  FROM
                    tra_purchase_order_mas 
                  where id=:id");
            $statement->bindValue('id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to order Status');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllPurchaseOrder() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(" SELECT 
                    mas.id,
                    mas.po_id,
                    DATE_FORMAT(mas.`po_date`,'%d %M %Y') AS po_date,
                    mas.`order_status`,
                    mas.`payment_status`,
                    v.name AS vendor_name,
                    mas.`v_order`,  
                    l.name AS location,
                    DATE_FORMAT(mas.req_shipping_date,'%d %M %Y') AS ship_date,
                    DATE_FORMAT(mas.`due_date`,'%d %M %Y') AS due_date,
                    mas.`total_amount`,
                    mas.`paid_amount`,
                    (mas.balance) AS balance 
                  FROM
                    tra_purchase_order_mas AS mas 
                    LEFT JOIN mas_vendors AS v 
                      ON v.id = mas.vendor_id 
                    LEFT JOIN `mas_inventory_location` AS l 
                      ON l.id = mas.location_id ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
     /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllOrderByStatus($orderStatus,$payStatus,$limit) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
            $str2 ='';
            if($orderStatus){
                $str = "AND mas.order_status=:order_status";
            }
            if($payStatus){
                $str1 = "AND mas.payment_status=:payment_status";
            }
            if($limit){
                $str2 = "limit ".$limit."";
            }
            $statement = $db->prepare(" SELECT 
                    mas.id,
                    mas.po_id,
                    DATE_FORMAT(mas.`po_date`,'%d %M %Y') AS po_date,
                    mas.`order_status`,
                    mas.`payment_status`,
                    v.name AS vendor_name,
                    mas.`v_order`,  
                    l.name AS location,
                    DATE_FORMAT(mas.req_shipping_date,'%d %M %Y') AS ship_date,
                    DATE_FORMAT(mas.`due_date`,'%d %M %Y') AS due_date,
                    mas.`total_amount`,
                    mas.`paid_amount`,
                    (mas.balance) AS balance 
                  FROM
                    tra_purchase_order_mas AS mas 
                    LEFT JOIN mas_vendors AS v 
                      ON v.id = mas.vendor_id 
                    LEFT JOIN `mas_inventory_location` AS l 
                      ON l.id = mas.location_id 
                    Where mas.po_id<>''   
                    ".$str." ".$str1." ".$str2." ");
            
            if($orderStatus){
                $statement->bindValue('order_status', $orderStatus);
            }
            if($payStatus){
                $statement->bindValue('payment_status', $payStatus);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchVendorOrders($vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        id,
                                        po_id,
                                        po_date,
                                        CONCAT_WS(",", order_status, payment_status) AS order_status,
                                        total_amount,
                                        paid_amount,
                                        (total_amount - paid_amount) AS balance 
                                      FROM
                                        tra_purchase_order_mas where vendor_id =:vendor_id');
            $statement->bindValue('vendor_id', $vendor_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseProductsByRecordId($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.id=:id and det.item_status="Receive"');
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch receive Product Data');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchProductsByStatus($order_id, $status) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.order_id=:order_id and det.item_status=:status');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function countQty($order_id, $status) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        sum(quantity) as qty
                                       from tra_purchase_order_det
                                       where order_id=:order_id and item_status=:status');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['qty'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to get total quantity');
        }
    }

    public static function countQtyCheck($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT tbl.product_id,SUM(tbl.qty) AS qty,SUM(tbl.qty2) AS qty2
                    FROM 
                    (SELECT 
                      product_id, quantity qty,"" AS qty2, item_status FROM `tra_purchase_order_det`
                    WHERE order_id = :orderid
                      AND item_status = "Order" 

                      UNION

                      SELECT 
                      product_id,"" AS qty, quantity qty2,item_status FROM `tra_purchase_order_det`
                    WHERE order_id = :orderid1
                      AND item_status = "Receive") AS tbl GROUP BY tbl.product_id');
            $statement->bindValue('orderid', $order_id);
            $statement->bindValue('orderid1', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['qty'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to get total quantity');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseOrderByStatus($order_id, $status, $type = 'O') {
        $db = Zend_Registry::get('db');
        try {
            if ($type == 'S') {
                $statement = $db->prepare('
                  SELECT 
                   det.id,
                   det.order_id,
                   det.product_id,
                   ip.product_name,
                   det.vendor_product_code,
                   det.quantity,
                   det.unit_price,
                   det.discount,
                   det.sub_total,
                   mas.location_id,
                   loc.name as location_name,
                   date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                   FROM 
                   tra_purchase_order_det det
                   inner join tra_inventory_product ip
                       on ip.id = det.product_id
                   inner join tra_purchase_order_mas mas 
                       on mas.id = det.order_id
                   LEFT JOIN mas_inventory_location as loc
                       on loc.id = det.location_id
                  where det.session=:order_id
                  AND det.item_status =:status
                   ');
            } else {
                $statement = $db->prepare('SELECT 
                    det.id,
                    det.order_id,
                    det.product_id,
                    ip.product_name,
                    det.vendor_product_code,
                    det.quantity,
                    det.unit_price,
                    det.discount,
                    det.sub_total,
                    mas.location_id,
                    loc.name as location_name,
                    date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                    FROM 
                    tra_purchase_order_det det
                    inner join tra_inventory_product ip
                        on ip.id = det.product_id
                    inner join tra_purchase_order_mas mas 
                        on mas.id = det.order_id
                    LEFT JOIN mas_inventory_location as loc
                        on loc.id = det.location_id
                   where det.order_id=:order_id
                   AND det.item_status =:status
                ');
            }
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('status', $status);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to Products Data according to status of the product');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseProducts($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        if(det.item_status="Return",-det.quantity,det.quantity) as quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        if(det.item_status="Return","Return","Order") as orderType
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.item_status in("Order","Return") and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseOrderProducts($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        det.item_status
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.item_status ="Order" and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function getProductByRecordId($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        det.item_status
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.parant_item_id=:id');
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCancelledPurchaseOrder($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        mas.location_id,
                                        det.item_status
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_purchase_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.item_status in("Receive","Unstock") and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchPurchaseProductsBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        if(det.item_status="Return",-det.quantity,det.quantity) as quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        if(det.item_status="Return","Return","Order") as orderType
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.item_status in("Order","Return") and det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchReturnPurchaseProductsBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchReturnPurchaseProducts($orderId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        ip.product_name,
                                        det.vendor_product_code,
                                        det.quantity,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        date_format(det.transaction_date,"%d/%m/%Y") as transaction_date
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where det.order_id =:orderId and det.item_status="Return"');
            $statement->bindValue('orderId', $orderId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get sub total by po id
     * @return array
     */
    public static function fetchSubTotalById($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                SUM(det.sub_total) AS sub_total,ifnull(ret_sub_total,0.00) as ret_sub_total 
                              FROM
                                tra_purchase_order_det det 
                                LEFT JOIN (
                                      SELECT 
                                SUM(sub_total) AS ret_sub_total ,order_id
                              FROM
                                tra_purchase_order_det WHERE order_id =:order_id1
                                    AND item_status="Return"
                                ) AS det1
                                ON det.`order_id` = det1.order_id
                                  WHERE item_status="Order"
                                       AND det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->bindValue('order_id1', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sub total of products');
        }
    }

    /**
     * get sub total by session id
     * @return array
     */
    public static function fetchSubTotalBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                SUM(det.sub_total) AS sub_total,ifnull(ret_sub_total,0.00) as ret_sub_total  
                              FROM
                                tra_purchase_order_det det 
                                LEFT JOIN (
                                      SELECT 
                                SUM(sub_total) AS ret_sub_total ,sessionid
                              FROM
                                tra_purchase_order_det WHERE sessionid =:session_id1 
                                    AND item_status="Return"
                                ) AS det1
                                ON det.`sessionid` = det1.sessionid
                                  WHERE item_status="Order"
                                       AND det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->bindValue('session_id1', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public static function fetchReturnSubTotalById($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                         sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where item_status="Return" and det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get sub total by session id
     * @return array
     */
    public static function fetchReturnSubTotalBySession($seesionId) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        sum(det.sub_total) as sub_total
                                        FROM 
                                        tra_purchase_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                       where item_status="Return" and det.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * get product price and vendor price
     * @return array
     */
    public static function fetchProductAndVendorPrice($product_id, $vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                        i.`product_id`,
                        i.cost AS product_price,
                        v.`cost` AS vendor_price, 
                        v.vendor_product_code
                      FROM
                        `tra_inventory_product_cost` AS i 
                        LEFT JOIN `mas_vendors_product` AS v 
                          ON v.product_id = i.`product_id` 
                          AND `vendor_id` = :vendor_id 
                      WHERE i.`product_id` = :product_id 
                      GROUP BY i.`cost` 
            ');

            $statement->bindValue('vendor_id', $vendor_id);
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * 
     */
    public function getPaidAmount($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                mas.last_pay_date,
                                mas.payment_status,
                                IFNULL(SUM(op.amount),0.00) AS paid_amount,
                                mas.total_amount AS order_total,
                                IFNULL(mas.balance,0.00) AS balance 
                              FROM
                              tra_purchase_order_mas AS mas 
                                LEFT JOIN tra_order_payments AS op 
                                  ON op.order_id  = mas.id AND op.order_type = "Purchase" 
                              WHERE mas.id = :order_id');
            $statement->bindValue("order_id", $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    public function getPaidAmountBySessionId($sessionid) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT mas.payment_status,SUM(op.amount) AS paid_amount,mas.total_amount AS order_total,(mas.total_amount - SUM(op.amount)) AS balance
                                        FROM tra_order_payments AS op
                                        LEFT JOIN tra_purchase_order_mas AS mas
                                        ON mas.id = op.order_id
                                       WHERE op.sessionid =:sessionid');
            $statement->bindValue('sessionid', $sessionid);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * 
     */
    public function fetchPurchasePaymentsBySession($sessionid) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'SELECT 
                        det.order_id,
                        det.id,
                        det.payment_type,
                        DATE_FORMAT(det.payment_date,"%d %M %Y") AS payment_date,
                        det.amount,
                        det.payment_method_id,
                        mas.name AS payment_method_name,
                        det.payment_ref,
                        det.payment_remarks
                        FROM tra_order_payments AS det
                        LEFT JOIN mas_payment_method AS mas
                        ON mas.id = payment_method_id
                    where 
                        det.sessionid =:sessionid and det.order_type="Purchase"
                    '
            );

            $statement->bindValue('sessionid', $sessionid);
            $statement->execute();
            $resultSet = $statement->fetchAll();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /*
     * fetch payments by order id
     * @return type
     * 
     */
    
    public function fetchPurchasePaymentsByOrderId($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'SELECT 
                        det.order_id,
                        det.id,
                        det.payment_type,
                        DATE_FORMAT(det.payment_date,"%d %M %Y") AS payment_date,
                        det.amount,
                        det.payment_method_id,
                        mas.name AS payment_method_name,
                        det.payment_ref,
                        det.payment_remarks
                        FROM tra_order_payments AS det
                        LEFT JOIN mas_payment_method AS mas
                        ON mas.id = det.payment_method_id
                    where 
                        det.order_id =:order_id and det.order_type="Purchase"
                    '
            );

            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public function fetchQuantity($product_id,$location_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT * FROM tra_inventory_product_quantity where 
                    product_id =:product_id and location_id =:location_id
                    '
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('location_id', $location_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    

    public static function updateLastVendorId($product_id,$vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    UPDATE 
                            tra_inventory_product 
                    SET 
                            last_vendor_id =:vendor_id 
                    WHERE 
                            id = :product_id	
                    '
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('vendor_id', $vendor_id);
            $statement->execute();
            return ;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
        
     public function getAvarageCost($product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '    
                    SELECT 
                      SUM(d.`quantity` * d.`unit_price`) AS totcost,
                      SUM(d.quantity) AS totqty 
                    FROM
                      `tra_purchase_order_det` AS d 
                      INNER JOIN `tra_purchase_order_mas` AS m 
                        ON m.`id` = d.`order_id` 
                    WHERE d.`product_id` = :product_id1 AND d.`item_status` = "Order"
                    AND m.order_status = "Fulfilled" 
                    '
            );
            $statement->bindValue('product_id1', $product_id);
            $statement->execute();
            $result= $statement->fetch();
            
            $statement = $db->prepare(
                    'SELECT 
                        p.id,
                        p.`product_name`,
                        IFNULL(SUM(pq.quantity), 0) AS qty,
                        IFNULL(pc.cost, 0.00) AS cost 
                      FROM
                        `tra_inventory_product_quantity` AS pq 
                        LEFT JOIN `tra_inventory_product` AS p 
                          ON p.`id` = pq.`product_id` 
                        LEFT JOIN 
                          (SELECT 
                            cost,
                            `product_id` 
                          FROM
                            `tra_inventory_product_cost` 
                          GROUP BY product_id) AS pc 
                          ON pc.product_id = pq.`product_id` 
                      WHERE pq.`product_id` = :product_id 
                      GROUP BY pq.`product_id`  
                    '
            );
            $statement->bindValue('product_id', $product_id);
            $statement->execute();
            $resultSet = $statement->fetch();

           
            
            $totalQty = $resultSet['qty'] + $result['totqty'];
            $totalCost = ($resultSet['qty']*$resultSet['cost']) + $result['totcost'];
            
            $avgCost = $totalCost / $totalQty ;

             
            return $avgCost;

        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public static function fetchPoIdByOrderId($po_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT po_id FROM tra_purchase_order_mas where 
                    id =:id 
                    '
            );
            $statement->bindValue('id', $po_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['po_id'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public static function fetchProductSubTotal($po_id){
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    '
                    SELECT 
                        sub_total 
                    FROM 
                        tra_purchase_order_det 
                    WHERE 
                        id =:id 
                    '
            );
            $statement->bindValue('id', $po_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['sub_total'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllPurchaseOrderByStatus($status) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if($status == "open") {
                $str = "WHERE mas.order_status = 'Unfulfilled' ";
            }
            
            if($status == "started") {
                $str = "WHERE mas.order_status = 'Started' ";
            }
            
            if($status == "unpaid") {
                $str = "WHERE mas.payment_status = 'Unpaid' ";
            }
            $statement = $db->prepare(" SELECT 
                    mas.id,
                    mas.po_id,
                    DATE_FORMAT(mas.`po_date`,'%d %M %Y') AS po_date,
                    mas.`order_status`,
                    mas.`payment_status`,
                    v.name AS vendor_name,
                    mas.`v_order`,  
                    l.name AS location,
                    DATE_FORMAT(mas.req_shipping_date,'%d %M %Y') AS ship_date,
                    DATE_FORMAT(mas.`due_date`,'%d %M %Y') AS due_date,
                    mas.`total_amount`,
                    mas.`paid_amount`,
                    (mas.balance) AS balance 
                  FROM
                    tra_purchase_order_mas AS mas 
                    LEFT JOIN mas_vendors AS v 
                      ON v.id = mas.vendor_id 
                    LEFT JOIN `mas_inventory_location` AS l 
                      ON l.id = mas.location_id 
                      " . $str . "
                    ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $ordes = array();

            if (count($resultSet)) {
                $ordes = $resultSet;
            }

            return $ordes;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
}

?>