<?php

/**
 * Application_Model_Timesheet
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Timesheet extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $empId;
    protected $timesheetDate;
    protected $jobNo;
    protected $hourlyRate;
    protected $ord;
    protected $ord15;
    protected $ord20;
    protected $ordTotal;
    protected $rateType;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_emp_timesheet(
                    id,
                    emp_id,
                    timesheet_date,
                    job_no,
                    hourly_rate,
                    total_ord,
                    total_1_5,
                    total_2_0,
                    sub_total,
                    sub_total,
                    rate_type,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :emp_id,
                    :timesheet_date,
                    :job_no,
                    :hourly_rate,
                    :total_ord,
                    :total_1_5,
                    :total_2_0,
                    :sub_total,
                    :rate_type,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('emp_id', $this->getEmpId());
            $statement->bindValue('timesheet_date', $this->getTimesheetDate());
            $statement->bindValue('job_no', $this->getJobNo());
            $statement->bindValue('hourly_rate', $this->getHourlyRate());
            $statement->bindValue('total_ord', $this->getOrd());
            $statement->bindValue('total_1_5', $this->getOrd15());
            $statement->bindValue('total_2_0', $this->getOrd20());
            $statement->bindValue('sub_total', $this->getOrdTotal());
            $statement->bindValue('rate_type', $this->getRateType());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_emp_timesheet
                 SET
                    emp_id=:emp_id,
                    timesheet_date=:timesheet_date,
                    job_no=:job_no,
                    hourly_rate=:hourly_rate,
                    total_ord =:total_ord,
                    total_1_5=:total_1_5,
                    total_2_0=:total_2_0,
                    sub_total=:sub_total,
                    rate_type=:rate_type,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('emp_id', $this->getEmpId());
            $statement->bindValue('timesheet_date', $this->getTimesheetDate());
            $statement->bindValue('job_no', $this->getJobNo());
            $statement->bindValue('hourly_rate', $this->getHourlyRate());
            $statement->bindValue('total_ord', $this->getOrd());
            $statement->bindValue('total_1_5', $this->getOrd15());
            $statement->bindValue('total_2_0', $this->getOrd20());
            $statement->bindValue('sub_total', $this->getOrdTotal());
            $statement->bindValue('rate_type', $this->getRateType());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch card by id
     * 
     * @access public
     * @return object
     */
    public function fetchTimesheetById() {
        try {
            $statement = $this->_db->prepare(
                    'select
                    id,
                    emp_id,
                    job_no,
                    hourly_rate,
                    DATE_FORMAT(`timesheet_date`,"%d/%m/%Y") AS timesheet_date,
                    `total_ord`,
                    `total_1_5`,
                    `total_2_0`,
                    `sub_total`,
                    rate_type
                  FROM tra_emp_timesheet
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch timesheet data');
        }
    }
    
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getEmpId() {
        return $this->empId;
    }

    public function setEmpId($empId) {
        $this->empId = $empId;
    }

    public function getTimesheetDate() {
        return $this->timesheetDate;
    }

    public function setTimesheetDate($timesheetDate) {
        $this->timesheetDate = $timesheetDate;
    }

    public function getJobNo() {
        return $this->jobNo;
    }

    public function setJobNo($jobNo) {
        $this->jobNo = $jobNo;
    }

    public function getHourlyRate() {
        return $this->hourlyRate;
    }

    public function setHourlyRate($hourlyRate) {
        $this->hourlyRate = $hourlyRate;
    }

    public function getOrd() {
        return $this->ord;
    }

    public function setOrd($ord) {
        $this->ord = $ord;
    }

    public function getOrd15() {
        return $this->ord15;
    }

    public function setOrd15($ord15) {
        $this->ord15 = $ord15;
    }

    public function getOrd20() {
        return $this->ord20;
    }

    public function setOrd20($ord20) {
        $this->ord20 = $ord20;
    }

    public function getOrdTotal() {
        return $this->ordTotal;
    }

    public function setOrdTotal($ordTotal) {
        $this->ordTotal = $ordTotal;
    }
    public function getRateType() {
        return $this->rateType;
    }

    public function setRateType($rateType) {
        $this->rateType = $rateType;
    }
}
