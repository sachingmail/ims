<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_ProductCategoryPeer {

    /**
     * get all records form master table
     * fetch all product categories from master tables
     * @return array
     */
    public static function fetchAllProductCategories() {
        $modelProduct = new Application_Model_ProductCategory();
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT name,id
                                        FROM mas_product_category 
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch all product parents 
     * used to fetch all parents id and name for grid
     * @return type
     */
    public static function fetchAllProductParents() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT a.id, a.name, b.Name AS parent_name,b.id AS parent_id
                                        FROM mas_product_category AS a 
                                        LEFT JOIN mas_product_category AS b 
                                        ON a.parent_id = b.id 
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /*
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateProductCategory($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  name =:name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_product_category
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
