<?php
/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Users extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $username;
    protected $password;
    protected $name;
    protected $email;
    protected $phone;
    protected $role_id;
    protected $isactive;
    

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                'INSERT INTO users(
                    id,
                    username,
                    password,
                    name,
                    email,
                    phone,
                    role_id,
                    is_active,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :username,
                    :password,
                    :name,
                    :email,
                    :phone,
                    :role_id,
                    :is_active,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('username', $this->getUsername());
            $statement->bindValue('password', $this->getPassword());
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('email', $this->getEmail());
            $statement->bindValue('phone', $this->getPhone());
            $statement->bindValue('role_id', $this->getRole_id());
            $statement->bindValue('is_active', $this->getIsactive());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            
            if($this->getPassword()!=''){
                $statement = $this->_db->prepare(
                 'UPDATE users
                 SET
                    username = :username,
                    password =:upassword,
                    name = :name,
                    email = :email,
                    phone = :phone,
                    role_id = :role_id,
                    is_active = :is_active,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
                );

                $statement->bindValue('upassword', $this->getPassword());
            } else {
                $statement = $this->_db->prepare(
                 'UPDATE users
                 SET
                    username = :username,
                    name = :name,
                    email = :email,
                    phone = :phone,
                    role_id = :role_id,
                    is_active = :is_active,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
                );
            }
            
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('username', $this->getUsername());
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('email', $this->getEmail());
            $statement->bindValue('phone', $this->getPhone());
            $statement->bindValue('role_id', $this->getRole_id());
            $statement->bindValue('is_active', $this->getIsactive());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updatePassword() {
        try {
            
                $statement = $this->_db->prepare(
                 'UPDATE users
                 SET
                    password =:upassword
                  WHERE
                    id = :id'
                );

            $statement->bindValue('id', $this->getId());
            $statement->bindValue('upassword', $this->getPassword());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchUserById() {
        try {
            $statement = $this->_db->prepare(
                 'select * from users
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    
    public function checkUsernameIsPresent() {
        try {
            $statement = $this->_db->prepare(
                 'select * from users
                  WHERE
                    username = :username'
            );
            $statement->bindValue('username', $this->getUsername());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    public function getIsactive() {
        return $this->isactive;
    }

    public function setIsactive($isactive) {
        $this->isactive = $isactive;
    }

}