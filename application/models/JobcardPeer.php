<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_JobcardPeer {

    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function fetchAllJobCard() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					distinct id,work_order_no
                                        FROM tra_work_order_mas
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch job card');
        }
    }
   
}

?>
