<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_UsersPeer {


    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllUsers() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					u.id,u.name,u.username,u.email,r.role_name 
                                        FROM users as u
					Left Join user_role_m as r on r.id = u.role_id
					order by u.id desc');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    public static function validateUsers($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  username =:username";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM `users`
                                        where username <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('username', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * @param type $id
     * @param type $name
     * @return type
     * 
     */
    public static function validateUsersEmail($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  email =:email";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM `users`
                                        where email <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('email', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * @param type $id
     * @param type $name
     * @return type
     * 
     */
    public static function validateUsersPhone($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  phone =:phone";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM `users`
                                        where phone <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('phone', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * 
     * @param type $id
     * @param type $name
     * @return type
     * 
     */
    public static function validateEmail($email) {
        $db = Zend_Registry::get('db');
        try {
            
            $sql = 'SELECT * 
                        FROM `users`
                    where email=:email';
            $statement = $db->prepare($sql);
            $statement->bindValue('email', $email);
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate email address');
        }
    }
    
    
    /**
     * 
     * @param type $id
     * @param type $name
     * @return type
     * 
     */
    public static function validateMd5Email($email) {
        $db = Zend_Registry::get('db');
        try {
            
            $sql = 'SELECT * 
                        FROM `users`
                    where md5(email)=:email';
            $statement = $db->prepare($sql);
            $statement->bindValue('email', $email);
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to validate email address');
        }
    }

}

?>
