<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_GenericPeer extends DMC_Model_Abstract {

    /**
     * get all records form roles table
     * @return array
     */
    public static function fetchRoles() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id,role_name from user_role_m
					order by role_name');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch roles');
        }
    }
    
    /**
    * get all records form roles table
    * @return array
    */
    public static function fetchStatus() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					code,meaning from lookup
                                        where type = "ACTIVE_STATUS"
					order by meaning');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch status');
        }
    }
}

?>
