<?php
/**
 * Application_Model_Customer
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Customer extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $name;
    protected $balance;
    protected $credit;
    protected $address_type;
    protected $address;
    protected $address1;
    protected $address2;
    protected $person_name;
    protected $phone;
    protected $fax;
    protected $email;
    protected $website;
    protected $pricing_currency;
    protected $discount;
    protected $payment_term;
    protected $tax_scheme;
    protected $tax_exempt;
    protected $location;
    protected $sales_reps;
    protected $carrier;
    protected $payment_method;
    protected $card_type;
    protected $card_no;
    protected $expiry_date;
    protected $security_code;
    protected $is_active;
    protected $remarks;
    protected $designation;
    protected $abnNo;
    

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                'INSERT INTO mas_customers(
                    id,
                    `name`,
                    `abn_no`,
                    `balance`,
                    `credit`,
                    `address_type`,
                    `address`,
                    `address1`,
                    `address2`,
                    `contact_name`,
                    designation,
                    `contact_phone`,
                    `contact_fax`,
                    `contact_email`,
                    `contact_website`,
                    `pricing_currency_id`,
                    `discount`,
                    `payment_terms`,
                    `taxing_schemes`,
                    `tax_exempt`,
                    `default_location_id`,
                    `sales_reps`,
                    `carrier_id`,
                    `payment_method`,
                    `card_type`,
                    `card_number`,
                    `card_expiry`,
                    `card_security_code`,
                    `is_active`,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :abn_no,
                    :balance,
                    :credit,
                    :address_type,
                    :address,
                    :address1,
                    :address2,
                    :contact_name,
                    :designation,
                    :contact_phone,
                    :contact_fax,
                    :contact_email,
                    :contact_website,
                    :pricing_currency_id,
                    :discount,
                    :payment_terms,
                    :taxing_schemes,
                    :tax_exempt,
                    :default_location_id,
                    :sales_reps,
                    :carrier_id,
                    :payment_method,
                    :card_type,
                    :card_number,
                    :card_expiry,
                    :card_security_code,
                    :is_active,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('abn_no', $this->getAbnNo());
            $statement->bindValue('balance', $this->getBalance());
            $statement->bindValue('credit', $this->getCredit());
            $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('contact_name', $this->getPerson_name());
            $statement->bindValue('designation', $this->getDesignation());
            $statement->bindValue('contact_phone', $this->getPhone());
            $statement->bindValue('contact_fax', $this->getFax());
            $statement->bindValue('contact_email', $this->getEmail());
            $statement->bindValue('contact_website', $this->getWebsite());
            $statement->bindValue('pricing_currency_id', $this->getPricing_currency());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('payment_terms', $this->getPayment_term());
            $statement->bindValue('taxing_schemes', $this->getTax_scheme());
            $statement->bindValue('tax_exempt', $this->getTax_exempt());
            $statement->bindValue('default_location_id', $this->getLocation());
            $statement->bindValue('sales_reps', $this->getSales_reps());
            $statement->bindValue('carrier_id', $this->getCarrier());
            $statement->bindValue('payment_method', $this->getPayment_method());
            $statement->bindValue('card_type', $this->getCard_type());
            $statement->bindValue('card_number', $this->getCard_no());
            $statement->bindValue('card_expiry', $this->getExpiry_date());
            $statement->bindValue('card_security_code', $this->getSecurity_code());
            $statement->bindValue('is_active', $this->getIs_active());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                 'UPDATE mas_customers
                 SET
                    name= :name,
                    abn_no = :abn_no,
                    balance= :balance,
                    credit= :credit,
                    address_type= :address_type,
                    address= :address,
                    address1= :address1,
                    address2= :address2,
                    contact_name= :contact_name,
                    designation = :designation,
                    contact_phone= :contact_phone,
                    contact_fax= :contact_fax,
                    contact_email= :contact_email,
                    contact_website= :contact_website,
                    pricing_currency_id= :pricing_currency_id,
                    discount= :discount,
                    payment_terms= :payment_terms,
                    taxing_schemes= :taxing_schemes,
                    tax_exempt= :tax_exempt,
                    default_location_id= :default_location_id,
                    sales_reps= :sales_reps,
                    carrier_id= :carrier_id,
                    payment_method= :payment_method,
                    card_type= :card_type,
                    card_number= :card_number,
                    card_expiry= :card_expiry,
                    card_security_code= :card_security_code,
                    is_active= :is_active,
                    remarks= :remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getName());
            $statement->bindValue('abn_no', $this->getAbnNo());
            $statement->bindValue('balance', $this->getBalance());
            $statement->bindValue('credit', $this->getCredit());
            $statement->bindValue('address_type', $this->getAddress_type());
            $statement->bindValue('address', $this->getAddress());
            $statement->bindValue('address1', $this->getAddress1());
            $statement->bindValue('address2', $this->getAddress2());
            $statement->bindValue('contact_name', $this->getPerson_name());
            $statement->bindValue('designation', $this->getDesignation());
            $statement->bindValue('contact_phone', $this->getPhone());
            $statement->bindValue('contact_fax', $this->getFax());
            $statement->bindValue('contact_email', $this->getEmail());
            $statement->bindValue('contact_website', $this->getWebsite());
            $statement->bindValue('pricing_currency_id', $this->getPricing_currency());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('payment_terms', $this->getPayment_term());
            $statement->bindValue('taxing_schemes', $this->getTax_scheme());
            $statement->bindValue('tax_exempt', $this->getTax_exempt());
            $statement->bindValue('default_location_id', $this->getLocation());
            $statement->bindValue('sales_reps', $this->getSales_reps());
            $statement->bindValue('carrier_id', $this->getCarrier());
            $statement->bindValue('payment_method', $this->getPayment_method());
            $statement->bindValue('card_type', $this->getCard_type());
            $statement->bindValue('card_number', $this->getCard_no());
            $statement->bindValue('card_expiry', $this->getExpiry_date());
            $statement->bindValue('card_security_code', $this->getSecurity_code());
            $statement->bindValue('is_active', $this->getIs_active());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchCustomerById() {
        try {
            $statement = $this->_db->prepare(
                 'select * from mas_customers
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer data');
        }
    }
    /**
     * Deactivate Customer
     * 
     */
    public function DeactivateCustomer() {
        try{
            $statement = $this->_db->prepare(
                 'UPDATE mas_customers
                 SET
                    is_active = "0",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
             DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }    
    /**
     * Activate Customer 
     * 
     */
    public function ActivateCustomer() {
        try{
            $statement = $this->_db->prepare(
                 'UPDATE mas_customers
                 SET
                    is_active = "1",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
             DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getBalance() {
        return $this->balance;
    }

    public function setBalance($balance) {
        $this->balance = $balance;
    }

    public function getCredit() {
        return $this->credit;
    }

    public function setCredit($credit) {
        $this->credit = $credit;
    }

    public function getAddress_type() {
        return $this->address_type;
    }

    public function setAddress_type($address_type) {
        $this->address_type = $address_type;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getPerson_name() {
        return $this->person_name;
    }

    public function setPerson_name($person_name) {
        $this->person_name = $person_name;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getFax() {
        return $this->fax;
    }

    public function setFax($fax) {
        $this->fax = $fax;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getWebsite() {
        return $this->website;
    }

    public function setWebsite($website) {
        $this->website = $website;
    }

    public function getPricing_currency() {
        return $this->pricing_currency;
    }

    public function setPricing_currency($pricing_currency) {
        $this->pricing_currency = $pricing_currency;
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function setDiscount($discount) {
        $this->discount = $discount;
    }

    public function getPayment_term() {
        return $this->payment_term;
    }

    public function setPayment_term($payment_term) {
        $this->payment_term = $payment_term;
    }

    public function getTax_scheme() {
        return $this->tax_scheme;
    }

    public function setTax_scheme($tax_scheme) {
        $this->tax_scheme = $tax_scheme;
    }

    public function getTax_exempt() {
        return $this->tax_exempt;
    }

    public function setTax_exempt($tax_exempt) {
        $this->tax_exempt = $tax_exempt;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function getSales_reps() {
        return $this->sales_reps;
    }

    public function setSales_reps($sales_reps) {
        $this->sales_reps = $sales_reps;
    }

    public function getCarrier() {
        return $this->carrier;
    }

    public function setCarrier($carrier) {
        $this->carrier = $carrier;
    }

    public function getPayment_method() {
        return $this->payment_method;
    }

    public function setPayment_method($payment_method) {
        $this->payment_method = $payment_method;
    }

    public function getCard_type() {
        return $this->card_type;
    }

    public function setCard_type($card_type) {
        $this->card_type = $card_type;
    }

    public function getCard_no() {
        return $this->card_no;
    }

    public function setCard_no($card_no) {
        $this->card_no = $card_no;
    }

    public function getExpiry_date() {
        return $this->expiry_date;
    }

    public function setExpiry_date($expiry_date) {
        $this->expiry_date = $expiry_date;
    }

    public function getSecurity_code() {
        return $this->security_code;
    }

    public function setSecurity_code($security_code) {
        $this->security_code = $security_code;
    }
    
    public function getIs_active() {
        return $this->is_active;
    }

    public function setIs_active($is_active) {
        $this->is_active = $is_active;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }
    public function getDesignation() {
        return $this->designation;
    }

    public function setDesignation($designation) {
        $this->designation = $designation;
    }

    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }

    public function getAbnNo() {
        return $this->abnNo;
    }

    public function setAbnNo($abnNo) {
        $this->abnNo = $abnNo;
    }

}