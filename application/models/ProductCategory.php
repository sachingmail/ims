<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_ProductCategory extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $ParentId;
    protected $ProductName;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_product_category(
                    id,
                    name,
                    parent_id,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :parent_id,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getProductName());
            $statement->bindValue('parent_id', $this->ParentId);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_product_category
                 SET
                    name = :name,
                    parent_id = :parent_id,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getProductName());
            $statement->bindValue('parent_id', $this->getParentId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch product by product id from master table
     * 
     * @access public
     * @return object
     */
    public function fetchProductById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT a.id, a.name, b.Name AS parent_name,b.id AS parent_id
                                        FROM mas_product_category AS a 
                                        LEFT JOIN mas_product_category AS b 
                                        ON a.parent_id = b.id 
                    WHERE
                            a.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * 
     * @return type
     * @return object
     * `fetch product name price from price currency id e
     */
    public function fetchProductNamePrice() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT p.product_name,c.price
                            FROM tra_inventory_product AS p
                            LEFT JOIN tra_inventory_product_cost AS c
                            ON p.id = c.product_id
                            LEFT JOIN mas_pricing_currency AS mpc
                            ON c.price_currency_id = mpc.id
                            LEFT JOIN mas_product_category AS pc
                            ON p.product_category_id = pc.id 
                    WHERE
                            pc.id = :id AND mpc.default = "1"'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * 
     * @return type
     * fetch product category name 
     */
    public function fetchProductCategoryEdit() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('
                                SELECT name,id
                                        FROM mas_product_category 
				WHERE 
                                  id !=:id AND parent_id != :id');
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
     * 
     * check if category is present in records or not
     */
    public function checkIfProductCategoryIsPresentInRecord() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_inventory_product 
                    WHERE  
                            product_category_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    public function getParentId() {
        return $this->ParentId;
    }

    public function getProductName() {
        return $this->ProductName;
    }

    public function setParentId($ParentId) {
        $this->ParentId = $ParentId;
    }

    public function setProductName($ProductName) {
        $this->ProductName = $ProductName;
    }

}
