<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_InventoryLocationPeer {

    /**
     * get all records form master table
     * fetch all locations from master tables
     * @return array
     */
    public static function fetchAllLocations() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					id,name
                                      FROM mas_inventory_location
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateInventoryLocation($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  name =:name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_inventory_location
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
