<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_VendorPeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllVendors() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id, `name`,`balance`,`credit`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`,is_active
                                        FROM mas_vendors');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllActiveVendors() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id, `name`,`balance`,`credit`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`
                                        FROM mas_vendors where is_active=1');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function getVendorBalance($vendorId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
                    sum(balance) as totBal from tra_purchase_order_mas 
                    where vendor_id =:vendor_id
		');
            $statement->bindValue('vendor_id', $vendorId);
            
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['totBal'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total balance');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function getVendorCredit($vendorId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
                    if(sum(balance)<0,sum(balance),"0.00") as totBal from tra_purchase_order_mas 
                    where vendor_id =:vendor_id
		');
            $statement->bindValue('vendor_id', $vendorId);
            
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet['totBal'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total balance');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchVendorDetails($id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('
                SELECT 
                id,
                name,
                balance,
                credit,
                address,
                contact_name,
                contact_phone,
                contact_fax,
                contact_email,
                contact_website,
                `carrier_id`,
                `taxing_schemes`,
                `currency_id`,
                address_type,
                address,
                address1,
                address2
              FROM
                mas_vendors 
              WHERE id = :id ');
            
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchVendorOrders($vendor_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        id,
                                        po_id,
                                        po_date,
                                        CONCAT_WS(",", order_status, payment_status) AS order_status,
                                        total_amount,
                                        paid_amount,
                                        (total_amount - paid_amount) AS balance 
                                      FROM
                                        tra_purchase_order_mas where vendor_id =:vendor_id');
            $statement->bindValue('vendor_id', $vendor_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchVendorProducts($vendor_id) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        vp.id,
                                        vp.vendor_id,
                                        ip.product_name,
                                        vp.vendor_product_code,
                                        vp.cost
                                        FROM 
                                        mas_vendors_product vp
                                        inner join tra_inventory_product ip
                                            on ip.id = vp.product_id
                                       where vp.vendor_id =:vendor_id');
            $statement->bindValue('vendor_id', $vendor_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    /**
    * get all records form case_stage_mas table
    * @return array
    */
    public static function fetchVendorProductsBySession($seesionId) {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                                        vp.id,
                                        vp.vendor_id,
                                        ip.product_name,
                                        vp.vendor_product_code,
                                        vp.cost
                                        FROM 
                                        mas_vendors_product vp
                                        inner join tra_inventory_product ip
                                            on ip.id = vp.product_id
                                       where vp.sessionid =:session_id');
            $statement->bindValue('session_id', $seesionId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validatePhone($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  contact_phone =:contact_phone";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_vendors
                                        where contact_phone <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('contact_phone', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateEmail($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  contact_email =:contact_email";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_vendors
                                        where contact_email <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('contact_email', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateName($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  name =:name";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_vendors
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('name', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    public static function fetchVendorPurchaseOrderById($vendor_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT id, 
                                            po_id,
                                            total_amount,
                                            paid_amount,
                                            (total_amount - paid_amount) AS balance 
                                      FROM   
                                            tra_purchase_order_mas 
                                      WHERE 
                                            vendor_id = :vendor_id 
                                      AND
                                            paid_amount <> total_amount
		');
            $statement->bindValue('vendor_id', $vendor_id);
            
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet ;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch total balance');
        }
    }
}

?>
