<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_CardTypePeer {

    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function fetchAllCards() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					c.id,c.card_type
                                        FROM mas_card_type as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
     /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateCardType($id,$name) {
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1 ='';
                
            if($id){
                $str = "AND  id <> :id";
            }
            if($name){
                $str1 = "AND  card_type =:card_type";
            }
            
            $sql = 'SELECT count(*) as tot
                                        FROM mas_card_type
                                        where card_type <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);
            
            if($id){
                $statement->bindValue('id', $id);
            }
            if($name){
                $statement->bindValue('card_type', $name);
            }
            
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
