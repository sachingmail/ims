<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_RolesPeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchAllRoles() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					id,role_name
                                        FROM user_role_m
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $roles = array();

            if (count($resultSet)) {
                $roles = $resultSet;
            }

            return $roles;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch roles');
        }
    }
    
    /**
     * get all screens from menu table
     * @return array
     */
    public static function fetchAllScreens() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT
					*
                                        FROM user_menu_m
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $roles = array();

            if (count($resultSet)) {
                $roles = $resultSet;
            }

            return $roles;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch menus');
        }
    }

    /**
     * get all screens from menu table
     * @return array
     */
    public static function fetchAllPermissionsByRole($roleId) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('select
                            a.screen_id,
                            b.screen_name,
                            a.view
                            from user_roles_rights as a
                            inner join
                            user_menu_m as b
                            on 
                            b.id = a.screen_id where a.role_id= :roleId order by a.screen_id
			');
            $statement->bindValue('roleId', $roleId);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $roles = array();

            if (count($resultSet)) {
                $roles = $resultSet;
            }

            return $roles;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch menus');
        }
    }
        

}

?>
