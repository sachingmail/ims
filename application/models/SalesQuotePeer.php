<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_SalesQuotePeer {

    /**
     * get all records from master tables
     * fetch all sales quotes from master tables
     * @return array
     */
    public static function fetchAllSalesQuote() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT id,order_id,order_status FROM tra_sales_quote_order_mas');
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Vendors');
        }
    }

    /**
     * fetch product by name
     * @return array
     */
    public function fetchProductById($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'select * from tra_inventory_product
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch components data');
        }
    }
    /**
     * 
     * @param type $order_id
     * @return type
     * 
     */
    public static function fetchSubTotalByOrderId($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                SUM(sub_total) AS sub_total 
                              FROM
                                tra_sales_quote_order_det 
                                  WHERE order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch sub total of products');
        }
    }
    
    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchSalesProducts($order_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                                        det.id,
                                        det.order_id,
                                        det.product_id,
                                        ip.product_name,
                                        det.unit_price,
                                        det.discount,
                                        det.sub_total,
                                        det.quantity,
                                        mas.location_id
                                        FROM 
                                        tra_sales_quote_order_det det
                                        inner join tra_inventory_product ip
                                            on ip.id = det.product_id
                                        inner join tra_sales_quote_order_mas mas 
                                        on mas.id = det.order_id
                                       where det.order_id=:order_id');
            $statement->bindValue('order_id', $order_id);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
}

?>
