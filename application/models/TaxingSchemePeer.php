<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_TaxingSchemePeer {

    /**
     * get all records form master table
     * fetch all taxing scheme from master tables
     * @return array
     */
    public static function fetchAllTaxingSchemes() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					*
                                        FROM mas_taxing_schemes
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * 
     * fetch all taxing scheme by id from master tables
     * @return array
     */
    public static function getTaxingSchemeById($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                    'select * from mas_taxing_schemes
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

}

?>
