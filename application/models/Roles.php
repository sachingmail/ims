<?php
/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Roles extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $name;
    protected $screen_id;
    protected $view_per;
    protected $add_per;
    protected $edit_per;
    protected $delete_per;



    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                'INSERT INTO user_role_m(
                    id,
                    role_name,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :role_name,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('role_name', $this->getName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                 'UPDATE user_role_m
                 SET
                    role_name = :role_name,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('role_name', $this->getName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchRoleById() {
        try {
            $statement = $this->_db->prepare(
                 'select * from user_role_m
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch role data');
        }
    }
    
    public function insertPermissions() {
        try {
            $statement = $this->_db->prepare(
                'INSERT INTO user_roles_rights(
                    id,
                    role_id,
                    screen_id,
                    `view`
                    )
                    VALUES(
                    :id,
                    :role_id,
                    :screen_id,
                    :view
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('role_id', $this->getId());
            $statement->bindValue('screen_id', $this->getScreen_id());
            $statement->bindValue('view', $this->getView_per());
//            $statement->bindValue('add', $this->getAdd_per());
//            $statement->bindValue('edit', $this->getEdit_per());
//            $statement->bindValue('delete', $this->getDelete_per());
            $statement->execute();
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    public function deletePermissions() {
        try {
            $statement = $this->_db->prepare('
                delete from user_roles_rights
                where role_id = :role_id');

            $statement->bindValue('role_id', $this->getId());
            $statement->execute();
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage() . ' - Unable to delete Roles');
        }
        return $this;
    }
        
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    
    public function getScreen_id() {
        return $this->screen_id;
    }

    public function setScreen_id($screen_id) {
        $this->screen_id = $screen_id;
    }

    public function getView_per() {
        return $this->view_per;
    }

    public function setView_per($view_per) {
        $this->view_per = $view_per;
    }

    public function getAdd_per() {
        return $this->add_per;
    }

    public function setAdd_per($add_per) {
        $this->add_per = $add_per;
    }

    public function getEdit_per() {
        return $this->edit_per;
    }

    public function setEdit_per($edit_per) {
        $this->edit_per = $edit_per;
    }

    public function getDelete_per() {
        return $this->delete_per;
    }

    public function setDelete_per($delete_per) {
        $this->delete_per = $delete_per;
    }



}