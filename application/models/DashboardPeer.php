<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_DashboardPeer {

    /**
     * 
     * 
     */
    public function fetchOutstandingList() {
        $db = Zend_Registry::get('db');
        try {
            $sql = "select(
                    SELECT
                    COUNT(*)  
                    FROM 
                    tra_sales_order_mas) AS sales_count,
                    (
                    SELECT 
                    COUNT(*)
                    FROM 
                    tra_purchase_order_mas )AS purchase_count ,
                    (SELECT 
                        COUNT(invs.id)
                      FROM
                        `tra_inventory_reorder_stock` AS invs 
                        INNER JOIN `tra_inventory_product` AS p 
                          ON p.`id` = invs.`product_id` 
                        INNER JOIN `mas_vendors` AS v 
                          ON v.`id` = invs.`vendor_id`)AS reorder_count,
                    (SELECT COUNT(*) 
                FROM inventory_work_order)AS work_count
                    ";

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    /**
     * fetch Sales Details
     * 
     */
    public function getSalesDetails() {
        
                $db = Zend_Registry::get('db');
        try {
            $sql = 'SELECT  (SELECT COUNT(*) FROM tra_sales_order_mas WHERE order_status = "Unfulfilled") AS `open`,
                            (SELECT COUNT(*) FROM tra_sales_order_mas WHERE order_status = "Started") AS started,
                            (SELECT COUNT(*) FROM tra_sales_order_mas WHERE payment_status = "Uninvoiced") AS Uninvoiced,
                            (SELECT COUNT(*) FROM tra_sales_order_mas WHERE payment_status = "Paid") AS paid,
                            (SELECT COUNT(*) FROM tra_sales_quote_order_mas) AS sales_quote
                    ';

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    /**
     * fetch Purchase Details
     * 
     */
    public function getPurchaseDetails() {
        
                $db = Zend_Registry::get('db');
        try {
            $sql = 'SELECT  (SELECT COUNT(*) FROM tra_purchase_order_mas WHERE order_status = "Unfulfilled") AS `open`,
                            (SELECT COUNT(*) FROM tra_purchase_order_mas WHERE order_status = "Started") AS started,
                            (SELECT COUNT(*) FROM tra_purchase_order_mas WHERE payment_status = "Unpaid") AS unpaid
                    ';

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    /**
     * fetch Purchase Details
     * 
     */
    public function fetchReorderDetails() {
        
                $db = Zend_Registry::get('db');
        try {
            $sql = 'SELECT 
                        COUNT(invs.id) AS `reorder_orders`
                      FROM
                        `tra_inventory_reorder_stock` AS invs 
                        INNER JOIN `tra_inventory_product` AS p 
                          ON p.`id` = invs.`product_id` 
                        INNER JOIN `mas_vendors` AS v 
                          ON v.`id` = invs.`vendor_id`
                    ';

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    /**
     * fetch Purchase Details
     * 
     */
    public function getWorkOrderDetails() {
        
                $db = Zend_Registry::get('db');
        try {
            $sql = 'SELECT (SELECT COUNT(*) FROM inventory_work_order WHERE status = "open") AS `open`,
                    (SELECT COUNT(*) FROM inventory_work_order WHERE status = "completed") AS completed
                    ';

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    
    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function fetchSalesOrders($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = 'DATE_FORMAT(`order_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%b-%Y")';
            }
            if($group == "Year") {
                $str1 = 'DATE_FORMAT(`order_date`,"%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%Y")';
            }
            if($group == "Week") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%u")';
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = 'DATE_FORMAT(`order_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%b-%Y")';
            }
            if ($from) {
                $str = 'AND order_date = :from';
            }
            if ($to) {
                $str = 'AND order_date = :to';
            }
            if ($from != '' && $to != '') {
                $str = 'AND order_date BETWEEN :from AND :to';
            }
            $statement = $db->prepare('SELECT
                                            COUNT(order_date),
                                            (SELECT DATE_FORMAT(`order_date`,"%d-%m-%Y") FROM tra_sales_order_mas ORDER BY order_date ASC LIMIT 1) AS `from`,
                                            (SELECT DATE_FORMAT(`order_date`,"%d-%m-%Y") FROM tra_sales_order_mas ORDER BY order_date DESC LIMIT 1) AS `to`,
                                            SUM(total_amount) AS total_amount,
                                            ' . $str1 . '
                                       FROM 
                                            tra_sales_order_mas
                                       WHERE 
                                            order_status != "Cancelled" 
                                            ' . $str . '
                                        GROUP BY ' . $str2 . '   
                                            ORDER BY MAX(DATE_FORMAT(`order_date`,"%Y-%m-%d")) ASC 
					');

            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Employee');
        }
    }
    
    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function fetchSalesCompleted($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = 'DATE_FORMAT(`order_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%b-%Y")';
            }
            if($group == "Year") {
                $str1 = 'DATE_FORMAT(`order_date`,"%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%Y")';
            }
            if($group == "Week") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%u")';
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = 'DATE_FORMAT(`order_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%b-%Y")';
            }
            if ($from) {
                $str = 'AND order_date = :from';
            }
            if ($to) {
                $str = 'AND order_date = :to';
            }
            if ($from != '' && $to != '') {
                $str = 'AND order_date BETWEEN :from AND :to';
            }
            $statement = $db->prepare('SELECT COUNT(order_date),
                                            (SELECT DATE_FORMAT(`order_date`,"%d-%m-%Y") FROM tra_sales_order_mas ORDER BY order_date ASC LIMIT 1) AS `from`,
                                            (SELECT DATE_FORMAT(`order_date`,"%d-%m-%Y") FROM tra_sales_order_mas ORDER BY order_date DESC LIMIT 1) AS `to`,
                                            order_status,
                                            SUM(total_amount) AS total_amount,
                                            ' .$str1 . '
                                       FROM 
                                            tra_sales_order_mas
                                       WHERE 
                                            order_status = "Fulfilled"
                                            ' . $str . '
                                         GROUP BY ' . $str2 . '  
                                             ORDER BY MAX(DATE_FORMAT(`order_date`,"%Y-%m-%d")) ASC
					');

            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Employee');
        }
    }

    /**
     * 
     * @param type $from
     * @param type $to
     * @return type
     * 
     */
    public static function fetchPurchaseOrders($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = 'DATE_FORMAT(`po_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%b-%Y")';
            }
            if($group == "Year") {
                $str1 = 'DATE_FORMAT(`po_date`,"%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%Y")';
            }
            if($group == "Week") {
                $str1 = 'DATE_FORMAT(`po_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%u")';
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`po_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = 'DATE_FORMAT(`po_date`,"%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%b-%Y")';
            }
            if ($from) {
                $str = 'AND po_date = :from';
            }
            if ($to) {
                $str = 'AND po_date = :to';
            }
            if ($from != '' && $to != '') {
                $str = 'AND po_date BETWEEN :from AND :to';
            }
            $statement = $db->prepare('SELECT 
                                            COUNT(po_date) as count,
                                            (SELECT DATE_FORMAT(`po_date`,"%d-%m-%Y") FROM tra_purchase_order_mas ORDER BY po_date ASC LIMIT 1) AS `from`,
                                            (SELECT DATE_FORMAT(`po_date`,"%d-%m-%Y") FROM tra_purchase_order_mas ORDER BY po_date DESC LIMIT 1) AS `to`,
                                            order_status,
                                            SUM(total_amount) as total_amount,
                                            ' . $str1 . '
                                       FROM 
                                            tra_purchase_order_mas
                                       WHERE 
                                            order_status = "Fulfilled" 
                                            ' . $str . '
                                           GROUP BY ' . $str2 .'
                                           ORDER BY MAX(DATE_FORMAT(`po_date`,"%Y-%m-%d")) ASC   

					');

            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Employee');
        }
    }

    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function validateEmployee($id, $name) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';

            if ($id) {
                $str = "AND  id <> :id";
            }
            if ($name) {
                $str1 = "AND  name =:name";
            }

            $sql = 'SELECT count(*) as tot
                                        FROM mas_employee
                                        where name <> "" ' . $str . ' ' . $str1 . '';
            $statement = $db->prepare($sql);

            if ($id) {
                $statement->bindValue('id', $id);
            }
            if ($name) {
                $statement->bindValue('name', $name);
            }

            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            return $resultSet['tot'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to Validate Employee');
        }
    }

    /**
     * get all records form master table
     * fetch all sales representative from master tables
     * @return array
     */
    public static function getSalesOrderProfit($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = "DATE_FORMAT(`order_date`,'%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%b-%Y')";
            }
            if($group == "Year") {
                $str1 = "DATE_FORMAT(`order_date`,'%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%Y')";
            }
            if($group == "Week") {
                $str1 = "DATE_FORMAT(`order_date`,'%d-%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%u')";
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = 'DATE_FORMAT(`order_date`,"%b-%Y") AS order_date';
                $str2 = "DATE_FORMAT(`order_date`,'%b-%Y')";
            }
            if ($from) {
                $str = 'AND mas.order_date = :from';
            }
            if ($to) {
                $str = 'AND mas.order_date = :to';
            }
            if ($from != '' && $to != '') {
                $str = 'AND mas.order_date BETWEEN :from AND :to';
            }
            $sql = "SELECT 
                    COUNT(order_date),	
                    (SELECT DATE_FORMAT(`order_date`,'%d-%m-%Y') FROM tra_sales_order_mas ORDER BY order_date ASC LIMIT 1) AS `from`,
                    (SELECT DATE_FORMAT(`order_date`,'%d-%m-%Y') FROM tra_sales_order_mas ORDER BY order_date DESC LIMIT 1) AS `to`,
                    " . $str1.  ",
                    (det.sub_total-(pc.`cost`*det.quantity)) AS profit
                  FROM
                    tra_sales_order_det AS det 
                    INNER JOIN tra_sales_order_mas AS mas 
                      ON mas.id = det.order_id 
                    INNER JOIN tra_inventory_product ip 
                      ON ip.id = det.product_id 
                    LEFT JOIN (
                    SELECT cost,`product_id`  
                    FROM `tra_inventory_product_cost`
                      GROUP BY product_id) AS pc
                     ON pc.product_id = det.`product_id`
                  WHERE det.item_status = 'Order' " . $str . "
                      GROUP BY " . $str2 . "
                      ORDER BY MAX(DATE_FORMAT(`order_date`,'%Y-%m-%d')) ASC    

            ";

            $statement = $db->prepare($sql);
            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }

    /**
     * 
     * @param type $from
     * @param type $to
     * @return type
     * Get all Customer Payments due
     */
    public function getCustomerPayments($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = "DATE_FORMAT(`order_date`,'%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%b-%Y')";
            }
            if($group == "Year") {
                $str1 = "DATE_FORMAT(`order_date`,'%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%Y')";
            }
            if($group == "Week") {
                $str1 = "DATE_FORMAT(`order_date`,'%d-%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%u')";
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`order_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = "DATE_FORMAT(`order_date`,'%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`order_date`,'%b-%Y')";
            }
            if ($from) {
                $str = "AND order_date = :from";
            }
            if ($to) {
                $str = "AND order_date = :to";
            }
            if ($from != '' && $to != '') {
                $str = "AND order_date BETWEEN :from AND :to";
            }
            $sql = "SELECT 
                        COUNT(DATE_FORMAT(order_date, '%M')),	
                        IFNULL(SUM(balance),'0.00') AS total_amount,
                        " . $str1 . "
                      FROM
                        `tra_sales_order_mas`
                        WHERE payment_status NOT IN('Owing') 
                        " . $str . "
                      GROUP BY " .$str2 . "
                          ORDER BY MAX(DATE_FORMAT(`order_date`,'%Y-%m-%d')) ASC 
                        ";

            $statement = $db->prepare($sql);
            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $customers = array();

            if (count($resultSet)) {
                $customers = $resultSet;
            }
            return $customers;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }

    /**
     * 
     * @param type $from
     * @param type $to
     * @return type
     * Get all Vendor Payments due
     */
    public function getVendorPayments($group,$from, $to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            $str1 = '';
            $str2 = '';
            if($group == "Month") {
                $str1 = "DATE_FORMAT(`po_date`,'%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`po_date`,'%b-%Y')";
            }
            if($group == "Year") {
                $str1 = "DATE_FORMAT(`po_date`,'%Y') AS order_date";
                $str2 = "DATE_FORMAT(`po_date`,'%Y')";
            }
            if($group == "Week") {
                $str1 = "DATE_FORMAT(`po_date`,'%d-%b-%Y') AS order_date";
                $str2 = "DATE_FORMAT(`po_date`,'%u')";
            }
            if($group == "Day") {
                $str1 = 'DATE_FORMAT(`po_date`,"%d-%b-%Y") AS order_date';
                $str2 = 'DATE_FORMAT(`po_date`,"%d-%b-%Y")';
            }
            if($group == "") {
                $str1 = 'DATE_FORMAT(`po_date`,"%b-%Y") AS order_date';
                 $str2 = "DATE_FORMAT(`po_date`,'%b-%Y')";
            }
            if ($from) {
                $str = "AND po_date = :from";
            }
            if ($to) {
                $str = "AND po_date = :to";
            }
            if ($from != '' && $to != '') {
                $str = "AND po_date BETWEEN :from AND :to";
            }
            $sql = "SELECT 
                        COUNT(DATE_FORMAT(po_date, '%M')),	
                        IFNULL(SUM(balance),'0.00') AS total_amount,
                        " . $str1 . "
                      FROM
                        `tra_purchase_order_mas`
                        WHERE payment_status NOT IN('Owing')
                        " . $str . "
                      GROUP BY " . $str2 . " 
                          ORDER BY MAX(DATE_FORMAT(`po_date`,'%Y-%m-%d')) ASC 
                        ";

            $statement = $db->prepare($sql);
            if ($from) {
                $statement->bindValue('from', $from);
            }
            if ($to) {
                $statement->bindValue('to', $to);
            }
            if ($from != '' && $to != '') {
                $statement->bindValue('from', $from);
                $statement->bindValue('to', $to);
            }

            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $vendors = array();

            if (count($resultSet)) {
                $vendors = $resultSet;
            }
            return $vendors;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }

    public static function fetchTopList() {
        $db = Zend_Registry::get('db');
        try {
            $sql = "SELECT 
                        * 
                    FROM 
                        mas_top_list   
                    ";

            $statement = $db->prepare($sql);
            $statement->execute();
            $resultSet = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch customer order profit report');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchSalesOrdersWithHighestSales($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = "";
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE order_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE order_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE order_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                            id,
                                            order_id as name,
                                            total_amount
                                    FROM 
                                            tra_sales_order_mas
                                    " . $str . "
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE order_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE order_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE order_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("  SELECT 
                                            id,
                                            order_id as name,
                                            total_amount
                                    FROM 
                                            tra_sales_order_mas
                                    " . $str . "
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchSalesOrdersWithHighestNonInvoicedTotal($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = "";
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "AND order_date = :date_from";
                }
                if ($date_to) {
                    $str = "AND order_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "AND order_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                            id,
                                            order_id as name,
                                            total_amount
                                    FROM 
                                            tra_sales_order_mas
                                    WHERE
                                            payment_status = 'Uninvoiced'
                                    " . $str . "
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "AND order_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "AND order_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "AND order_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("  SELECT 
                                            id,
                                            order_id as name,
                                            total_amount
                                    FROM 
                                            tra_sales_order_mas
                                    WHERE
                                        payment_status = 'Uninvoiced'
                                    " . $str . "
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchCustomerWithHighestSales($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = "";
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE order_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE order_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE order_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                mas.customer_id,
                                                SUM(mas.total_amount) AS total_amount,
                                                c.name AS `name`
                                           FROM 
                                                tra_sales_order_mas AS mas
                                            LEFT JOIN 
                                                mas_customers AS c
                                            ON 
                                                c.id = mas.customer_id
                                    " . $str . "
                                    GROUP BY 
                                                mas.customer_id    
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE order_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE order_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE order_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("  SELECT 
                                                mas.customer_id,
                                                SUM(mas.total_amount) AS total_amount,
                                                c.name AS `name`
                                           FROM 
                                                tra_sales_order_mas AS mas
                                            LEFT JOIN 
                                                mas_customers AS c
                                            ON 
                                                c.id = mas.customer_id
                                    " . $str . "
                                    GROUP BY 
                                                mas.customer_id    
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchProductWithHighestQuantityPurchased($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = "";
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                $str = "";
                if ($date_from) {
                    $str = "WHERE mas.po_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE mas.po_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE mas.po_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                SUM(det.quantity) AS total_amount,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_purchase_order_det AS det
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_purchase_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE mas.po_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE mas.po_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE mas.po_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("SELECT 
                                                    det.product_id,
                                                    SUM(det.quantity) AS total_amount,
                                                    ip.product_name AS `name`
                                               FROM 
                                                    tra_purchase_order_det AS det
                                               LEFT JOIN 
                                                    tra_purchase_order_mas AS mas
                                               ON 
                                                    det.product_id = mas.id
                                               LEFT JOIN 
                                                    tra_inventory_product AS ip
                                               ON 
                                                    det.product_id = ip.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchProductWithHighestQuantitySold($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE mas.order_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE mas.order_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE mas.order_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                SUM(det.quantity) AS total_amount,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_sales_order_det AS det
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_sales_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE mas.order_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE mas.order_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE mas.order_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("SELECT 
                                                    det.product_id,
                                                    SUM(det.quantity) AS total_amount,
                                                    ip.product_name AS `name`
                                               FROM 
                                                    tra_sales_order_det AS det
                                               LEFT JOIN 
                                                    tra_sales_order_mas AS mas
                                               ON 
                                                    det.product_id = mas.id
                                               LEFT JOIN 
                                                    tra_inventory_product AS ip
                                               ON 
                                                    det.product_id = ip.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchProductWithHighestValueSold($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE mas.order_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE mas.order_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE mas.order_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                SUM(mas.total_amount) AS `total_amount`,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_sales_order_det AS det
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_sales_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE mas.order_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE mas.order_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE mas.order_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("SELECT 
                                                    det.product_id,
                                                    SUM(mas.total_amount) AS `total_amount`,
                                                    ip.product_name AS `name`
                                               FROM 
                                                    tra_sales_order_det AS det
                                               LEFT JOIN 
                                                    tra_sales_order_mas AS mas
                                               ON 
                                                    det.product_id = mas.id
                                               LEFT JOIN 
                                                    tra_inventory_product AS ip
                                               ON 
                                                    det.product_id = ip.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchProductWithHighestValuePurchased($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE mas.po_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE mas.po_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE mas.po_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                SUM(mas.total_amount) AS `total_amount`,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_purchase_order_det AS det
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_purchase_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE mas.po_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE mas.po_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE mas.po_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("SELECT 
                                                    det.product_id,
                                                    SUM(mas.total_amount) AS `total_amount`,
                                                    ip.product_name AS `name`
                                               FROM 
                                                    tra_purchase_order_det AS det
                                               LEFT JOIN 
                                                    tra_purchase_order_mas AS mas
                                               ON 
                                                    det.product_id = mas.id
                                               LEFT JOIN 
                                                    tra_inventory_product AS ip
                                               ON 
                                                    det.product_id = ip.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }
    /**
     * 
     * @param type $top_or_bottom
     * @param type $limit
     * @param type $date_from
     * @param type $date_to
     * @return type
     * 
     */
    public static function fetchProductWithHighestInventoryValue($top_or_bottom, $limit, $date_from, $date_to) {
        $db = Zend_Registry::get('db');
        try {
            $str = '';
            if ($limit == "" || $limit == "0" || $limit < 0) {
                $limit = "5";
            }
            $limit = intval($limit);
            if ($top_or_bottom == "Top") {
                if ($date_from) {
                    $str = "WHERE mas.po_date = :date_from";
                }
                if ($date_to) {
                    $str = "WHERE mas.po_date = :date_to";
                }
                if ($date_from != "" && $date_to != "") {
                    $str = "WHERE mas.po_date between :date_from AND :date_to";
                }
                $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                IFNULL(cost.cost * SUM(qua.quantity),'0.00') AS total_amount,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_purchase_order_det AS det
                                           LEFT JOIN 
						tra_inventory_product_cost AS cost
					   ON 
						cost.product_id = det.product_id
					   LEFT JOIN 
						tra_inventory_product_quantity AS qua
					   ON 
						cost.product_id = qua.product_id
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_purchase_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            cost.cost * SUM(qua.quantity) DESC LIMIT :limit   
                                    ");
                if ($date_from) {
                    $statement->bindValue("date_from", $date_from);
                }
                if ($date_to) {
                    $statement->bindValue("date_to", $date_from);
                }
                if ($date_from != "" && $date_to != "") {
                    $statement->bindValue("date_from", $date_from);
                    $statement->bindValue("date_to", $date_to);
                }
                 //$statement->_bindParam('limit', 5);
                 $statement->bindValue("limit",$limit,PDO::PARAM_INT);
                $statement->execute();
                $resultSet = $statement->fetchAll();
                return $resultSet;
            }
            if ($top_or_bottom == "Bottom") {
                $str = "";
                if ($limit == "" || $limit == "0" || $limit < 0) {
                    $limit = "5";
                }
                $limit = intval($limit);
                if ($top_or_bottom == "Bottom") {
                    if ($date_from) {
                        $str = "WHERE mas.po_date = :date_from";
                    }
                    if ($date_to) {
                        $str = "WHERE mas.po_date = :date_to";
                    }
                    if ($date_from != "" && $date_to != "") {
                        $str = "WHERE mas.po_date between :date_from AND :date_to";
                    }
                    $statement = $db->prepare("SELECT 
                                                det.product_id,
                                                IFNULL(cost.cost * SUM(qua.quantity),'0.00') AS total_amount,
                                                ip.product_name AS `name`
                                           FROM 
                                                tra_purchase_order_det AS det
                                           LEFT JOIN 
						tra_inventory_product_cost AS cost
					   ON 
						cost.product_id = det.product_id
					   LEFT JOIN 
						tra_inventory_product_quantity AS qua
					   ON 
						cost.product_id = qua.product_id
                                           LEFT JOIN 
                                                tra_inventory_product AS ip
                                           ON 
                                                det.product_id = ip.id
                                           LEFT JOIN 
                                                tra_purchase_order_mas AS mas
                                           ON 
                                                det.product_id = mas.id
                                    " . $str . "
                                    GROUP BY 
                                                det.product_id  
                                    ORDER BY 
                                            total_amount ASC LIMIT :limit   
                                                        ");

                    if ($date_from) {
                        $statement->bindValue("date_from", $date_from);
                    }
                    if ($date_to) {
                        $statement->bindValue("date_to", $date_from);
                    }
                    if ($date_from != "" && $date_to != "") {
                        $statement->bindValue("date_from", $date_from);
                        $statement->bindValue("date_to", $date_to);
                    }
                    $statement->bindValue("limit", $limit,PDO::PARAM_INT);
                    $statement->execute();
                    $resultSet = $statement->fetchAll();
                    return $resultSet;
                }
            }
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch');
        }
    }

}

?>
