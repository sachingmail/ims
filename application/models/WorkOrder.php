<?php

/**
 * Application_Model_WorkOrder
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_WorkOrder extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $orderId;
    protected $location;
    protected $orderDate;
    protected $orderStatus;
    protected $orderType;
    protected $remarks;
    protected $productRecordId;
    protected $productId;
    protected $quantity;
    protected $sessionid;
    protected $dateScheduled;
    protected $description;
    protected $productStatus;
    protected $status;
    



    /**
     * get purchase order
     * 
     * @access public
     * @return object
     */
    public function fetchWorkOrder() {
        try {
            $statement = $this->_db->prepare(
                'select 
                   m.`id`,
                   m.`work_order_no`,
                    date_format(m.scheduled_date,"%d/%m/%Y") as scheduled_date,
                    date_format(m.created,"%d/%m/%Y") as created,
                    date_format(m.updated,"%d/%m/%Y") as updated,
                    date_format(m.date_issued,"%d/%m/%Y") as date_issued,
                    date_format(m.date_fulfilled,"%d/%m/%Y") as date_fulfilled,
                   m.`location_id`,
                   m.`order_type`,
                   m.`order_status`,
                   m.remarks,
                   u.username as updated_by
                    from tra_work_order_mas as m
                    left join users as u
                    on u.id = m.updated_by                    
                  WHERE
                    m.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch work order');
        }
    }
    
    
    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO `tra_work_order_mas`(
                    id,
                    work_order_no,
                    scheduled_date,
                    location_id,
                    order_type,
                    order_status,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :work_order_no,
                    :scheduled_date,
                    :location_id,
                    :order_type,
                    :order_status,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('work_order_no', $this->getOrderId());
            $statement->bindValue('scheduled_date', $this->getOrderDate());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('order_type', $this->getOrderType());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate order
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_mas
                 SET
                    location_id=:location_id,
                    order_type =:order_type,
                    remarks =:remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('order_type', $this->getOrderType());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

     /**
     * udpate Order No
     * 
     * @access public
     * @return object
     */
    public function updateWorkOrderId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_mas
                 SET
                    work_order_no= :work_order_no,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('work_order_no', $this->getOrderId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            
            
            

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate Order No in details table
     * 
     * @access public
     * @return object
     */
    public function updateWorkOrderDet() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_det
                 SET
                    work_order_id= :work_order_id,
                    sessionid = "",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('work_order_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function statusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_det
                 SET
                    product_status= :product_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('product_status', $this->getProductStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function finishStatusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_bom
                 SET
                    work_status= :status,
                    `quantity_used`=`quantity`,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    wo_product_id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('status', 'Finished');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateBomStatus($id,$status) {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_bom
                 SET
                    work_status= :status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $id);
            $statement->bindValue('status', $status);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            
            if($status == 'Committed' || $status =='Finished') {
                $statement = $this->_db->prepare(
                    'UPDATE `tra_work_order_bom` 
                        SET
                       `quantity_used`=`quantity`
                    WHERE
                    id = :id1'
                );
                $statement->bindValue('id1', $id);
                $statement->execute();
            }
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function voidBomStatus($id,$status) {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_bom
                 SET
                    work_status= :status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $id);
            $statement->bindValue('status', $status);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            
            $statement = $this->_db->prepare(
                    'UPDATE `tra_work_order_bom` 
                        SET
                       `quantity_used`= 0
                    WHERE
                    id = :id1'
                );
                $statement->bindValue('id1', $id);
                $statement->execute();
            
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function deleteBOM() {
        try {
            $statement = $this->_db->prepare(
                    'delete from tra_work_order_bom
                  WHERE
                    wo_product_id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->execute();
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    
    /**
     * Issue Order
     * 
     * @access public
     * @return object
     */
    public function issueOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_mas
                 SET
                    order_status= :order_status,
                    date_issued= :date_issued,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('date_issued', $this->getOrderDate());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

   /**
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */
    public function saveBOMProduct($recordId,$product_id,$qty) {
        try {
            
            $statement = $this->_db->prepare(
                    "INSERT INTO tra_work_order_bom (
                    `wo_product_id`,
                    `bom_product_id`,
                    `quantity`,
                    work_status,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    :recordId,
                    `raw_product_id`,
                    `quantity`* :qty,
                    'Entered',
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_inventory_product_bill_of_materials 
                    WHERE product_id = :product_id
               "
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('recordId', $recordId);
            $statement->bindValue('qty', $qty);
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */
    public function getBOMProduct($recordId,$product_id,$qty) {
        try {
            
            $statement = $this->_db->prepare(
                    "INSERT INTO tra_work_order_bom (
                    `wo_product_id`,
                    `bom_product_id`,
                    `quantity`,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    :recordId,
                    `raw_product_id`,
                    `quantity`* :qty,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_inventory_product_bill_of_materials 
                    WHERE product_id = :product_id
               "
            );
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('recordId', $recordId);
            $statement->bindValue('qty', $qty);
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
     /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProduct() {
        if ($this->getProductRecordId() == null || $this->getProductRecordId() < 1) {
            $this->insertProduct();
        } else {
            $this->updateProduct();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertProduct() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_work_order_det(
                    `id`,
                    `work_order_id`,
                    `product_id`,
                    `quantity`,
                    `description`,
                    `date_scheduled`,
                    `product_status`,
                    `sessionid`,
                     updated,
                     updated_by,
                     created,
                     created_by
                    )
                    VALUES(
                    :id,
                    :work_order_id,
                    :product_id,
                    :quantity,
                    :description,
                    :date_scheduled,
                    :product_status,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('work_order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('product_status', $this->getProductStatus());
            $statement->bindValue('description', '');
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('date_scheduled', $this->getDateScheduled());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setProductRecordId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProduct() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_purchase_order_det
                 SET
                    product_id=:product_id,
                    vendor_product_code=:vendor_product_code,
                    quantity=:quantity,
                    unit_price=:unit_price,
                    discount=:discount,
                    sub_total=:sub_total,
                    location_id=:location_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('unit_price', $this->getUnitPrice());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('sub_total', $this->getProductSubTotal());
            $statement->bindValue('location_id', $this->getReceiveLocationId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate master status
     * 
     * @access public
     * @return object
     */
    public function updateStatusMasterData() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_work_order_mas
                 SET
                    order_status= :order_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            
            if($this->getOrderStatus()=='Fulfilled'){
                $statement = $this->_db->prepare(
                        'UPDATE tra_work_order_mas
                     SET
                        date_fulfilled= :date_fulfilled
                      WHERE
                        id = :id'
                );
                $statement->bindValue('id', $this->getId());
                $statement->bindValue('date_fulfilled', date('Y-m-d'));
                $statement->execute();
            }

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }


    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function getOrderDate() {
        return $this->orderDate;
    }

    public function setOrderDate($orderDate) {
        $this->orderDate = $orderDate;
    }

    public function getOrderStatus() {
        return $this->orderStatus;
    }

    public function setOrderStatus($orderStatus) {
        $this->orderStatus = $orderStatus;
    }

    public function getOrderType() {
        return $this->orderType;
    }

    public function setOrderType($orderType) {
        $this->orderType = $orderType;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function getProductRecordId() {
        return $this->productRecordId;
    }

    public function setProductRecordId($productRecordId) {
        $this->productRecordId = $productRecordId;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }
    public function getDateScheduled() {
        return $this->dateScheduled;
    }

    public function setDateScheduled($dateScheduled) {
        $this->dateScheduled = $dateScheduled;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    public function getProductStatus() {
        return $this->productStatus;
    }

    public function setProductStatus($productStatus) {
        $this->productStatus = $productStatus;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }







}
