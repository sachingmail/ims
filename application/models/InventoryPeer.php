<?php

/**
 *
 * @package IMS
 * @subpackage Inventory
 */
class Application_Model_InventoryPeer {

    /**
     * get all current stock data 
     * @return array
     */
    public static function getCurrentStock() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT 
                p.id,
                p.`product_name`,
                pc.`name` as category,
                v.name as last_vendor_name,
                l.`name` as location,
                q.`quantity` 
                FROM
                `tra_inventory_product` AS p 
                INNER JOIN `tra_inventory_product_quantity` AS q 
                  ON q.`product_id` = p.`id` 
                INNER JOIN `mas_inventory_location` AS l 
                  ON l.`id` = q.`location_id` 
                INNER JOIN `mas_product_category` AS pc 
                  ON pc.`id` = p.`product_category_id` 
               LEFT JOIN `mas_vendors` AS v 
                    ON v.`id` = p.`last_vendor_id`
	    ');
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
            
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
     public static function fetchProductId($name) {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare(
                 'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }
    
    /**
    * get all adjust stock data 
    * @return array
    */
    public static function getAdjustStock() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                invs.id,
                p.`product_name`,
                l.`name` as location,
                invs.`new_quantity`, 
                invs.`old_quantity`, 
                invs.`difference`, 
                date_format(invs.inventory_date,'%d %b %Y') as invDate, 
                invs.status,
                invs.remarks
                FROM
                `tra_inventory_adjust_stock` AS invs 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.`id` = invs.`inventory_product_id` 
                INNER JOIN `mas_inventory_location` AS l 
                  ON l.`id` = invs.`inventory_location_id` 
	    ");
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all adjust stock data 
    * @return array
    */
    public static function getAdjustStockById($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                *
                FROM
                tra_inventory_adjust_stock
                where id =:id
	    ");
             $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all adjust stock data 
    * @return array
    */
    public static function getReorderStock() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                invs.id,
                invs.`product_id`,
                p.`product_name`,
                invs.`quantity_available`, 
                invs.`reorder_point`, 
                invs.`reorder_quantity`,
                invs.`vendor_id`,
                v.`name` as vendor_name,
                invs.reorder_status
                FROM
                `tra_inventory_reorder_stock` AS invs 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.`id` = invs.`product_id` 
                INNER JOIN `mas_vendors` AS v 
                  ON v.`id` = invs.`vendor_id` 
	    ");
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all transfer stock data 
    * @return array
    */
    public static function getTransferStock() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                trf.id,
                p.`product_name`,
                l.`name` as fromlocation,
                l1.`name` as tolocation,
                trf.quantity, 
                date_format(trf.transfer_date,'%d %b %Y') as trasferDate, 
                date_format(trf.send_date,'%d %b %Y') as sendDate, 
                date_format(trf.received_date,'%d %b %Y') as receivedDate, 
                trf.status
                FROM
                `tra_inventory_transfer_stock` AS trf 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.`id` = trf.`inventory_product_id` 
                INNER JOIN `mas_inventory_location` AS l 
                  ON l.`id` = trf.`from_location_id` 
                INNER JOIN `mas_inventory_location` AS l1 
                  ON l1.`id` = trf.`to_location_id` 
	    ");
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get transfer stock data by id
    * @return array
    */
    public static function getTransferStockById($id) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                trf.id,
                p.`product_name`,
                l.`name` as fromlocation,
                l1.`name` as tolocation,
                trf.quantity, 
                date_format(trf.transfer_date,'%d %b %Y') as trasferDate, 
                date_format(trf.send_date,'%d %b %Y') as sendDate, 
                date_format(trf.received_date,'%d %b %Y') as receivedDate, 
                trf.status,
                trf.`inventory_product_id`,
                trf.`from_location_id` 
                FROM
                `tra_inventory_transfer_stock` AS trf 
                INNER JOIN `tra_inventory_product` AS p 
                  ON p.`id` = trf.`inventory_product_id` 
                INNER JOIN `mas_inventory_location` AS l 
                  ON l.`id` = trf.`from_location_id` 
                INNER JOIN `mas_inventory_location` AS l1 
                  ON l1.`id` = trf.`to_location_id` 
                WHERE trf.id =:id 
	    ");
            $statement->bindValue('id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
    
    /**
    * get all adjust stock data 
    * @return array
    */
    public static function getProductStockByLocation($location_id,$product_id) {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT 
                    * 
                  FROM
                    tra_inventory_product_quantity 
                  WHERE `location_id`= :location_id AND `product_id`=:product_id
                ");
            
            $statement->bindValue('product_id', $product_id);
            $statement->bindValue('location_id', $location_id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch stock quantity');
        }
    }
    /**
     * 
     * 
     */
    public static function fetchMovementHistory() {
        $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare("SELECT his.id,his.product_id,ip.product_name,his.type,his.movement_date,his.location,his.order_id,il.name as location_name,
                                        his.qty_before,his.qty,his.qty_after
                                FROM tra_movement_history AS his
                                LEFT JOIN tra_inventory_product AS ip 
                                        ON ip.id = his.product_id
                                LEFT JOIN mas_inventory_location AS il 
                                        ON il.id = his.location
 
                  
                ");
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch stock quantity');
        }
    }
    

}

?>
