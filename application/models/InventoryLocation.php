<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_InventoryLocation extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $LocationName;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_inventory_location(
                    id,
                    name,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :name,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('name', $this->getLocationName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_inventory_location
                 SET
                    name = :name,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('name', $this->getLocationName());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch location by location id
     * 
     * @access public
     * @return object
     */
    public function fetchLocationById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_inventory_location
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * 
     */
    public function checkIfLocationIsPresentInRecords() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_inventory_product 
                    WHERE  
                            storage_location = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['inventory_product'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_inventory_product_quantity 
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['product_quantity'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_purchase_order_det 
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['purchase_det'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_purchase_order_mas 
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['purchase_mas'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_order_mas 
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_mas'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_order_det
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_det'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'SELECT 
                            * 
                    FROM 
                            tra_sales_quote_order_mas 
                    WHERE  
                            location_id = :id
                    	'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['sales_quote_mas'] = $statement->fetchAll();
            return $resultSet;
        } catch (Exception $e) {
             DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * @return type
     * all setters and getters
     */
    public function getId() {
        return $this->id;
    }

    public function getLocationName() {
        return $this->LocationName;
    }

    public function setLocationName($LocationName) {
        $this->LocationName = $LocationName;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

}
