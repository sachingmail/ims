<?php

/**
 *
 * @package IMS
 * @subpackage Vendor
 */
class Application_Model_SettingPeer {

    /**
     * get all records form case_stage_mas table
     * @return array
     */
    public static function fetchCompanyDetails() {
         $db = Zend_Registry::get('db');
        try {
            $statement =$db->prepare('SELECT * from company_details
                                       ');
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }

            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch company details');
        }
    }
   
    
}

?>
