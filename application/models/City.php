<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_City extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $CityName;
    protected $CountryId;
    protected $StateId;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_cities(
                    id,
                    city_name,
                    state_id,
                    country_id,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :city_name,
                    :state_id,
                    :country_id,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('city_name', $this->getCityName());
            $statement->bindValue('state_id', $this->getStateId());
            $statement->bindValue('country_id', $this->getCountryId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_cities
                 SET
                    city_name= :city_name,
                    state_id = :state_id,
                    country_id =:country_id,
                    updated =:updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('city_name', $this->getCityName());
            $statement->bindValue('state_id', $this->getStateId());
            $statement->bindValue('country_id', $this->getCountryId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch city by id from master table
     * 
     * @access public
     * @return object
     */
    public function fetchCityById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT c.city_name,s.state_name,co.country_name,c.state_id,c.country_id
                                    FROM mas_cities AS c
                                    LEFT JOIN mas_states AS s 
                                    ON c.state_id = s.id
                                    LEFT JOIN mas_countries AS co
                                    ON c.country_id = co.id
                    WHERE
                            c.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * fetch state by id from master table
     * 
     * @access public
     * @return object
     */
    public function getStatesByCountryId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT id,state_name
                        FROM mas_states
                    WHERE
                        country_id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }

    /**
     * fetch country by country id from master table
     * 
     * @access public
     * @return object
     */
    public function getCountryByCountryId() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT c.id,c.country_name 
                            FROM mas_countries AS c
                            INNER JOIN mas_states AS s 
                            ON s.country_id = c.id
                     WHERE 
                            s.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * 
     */
    public function checkCityIsPresent() {
        try{
            $statement = $this->_db->prepare(
                    'SELECT * from mas_cities
                     WHERE 
                            city_name = :city_name
                            and
                            state_id = :state_id
                            and 
                            country_id = :country_id'
            );
            $statement->bindValue('city_name', $this->getCityName());
            $statement->bindValue('state_id', $this->getStateId());
            $statement->bindValue('country_id', $this->getCountryId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
                DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * setter and getters
     * @return type
     * 
     */
    public function getId() {
        return $this->id;
    }

    public function getCityName() {
        return $this->CityName;
    }

    public function setCityName($CityName) {
        $this->CityName = $CityName;
    }

    public function getCountryId() {
        return $this->CountryId;
    }

    public function getStateId() {
        return $this->StateId;
    }

    public function setCountryId($CountryId) {
        $this->CountryId = $CountryId;
    }

    public function setStateId($StateId) {
        $this->StateId = $StateId;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

}
