<?php

/**
 * Application_Model_OrderPayments
 * 
 * The class is object representation of payment table
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_OrderPayments extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $orderId;
    protected $sessionid;
    protected $paymentDate;
    protected $paymentType;
    protected $paymentMethodId;
    protected $paymentRef;
    protected $paymentRemarks;
    protected $payAmount;
    protected $paymentId;
    protected $orderType;
    protected $currencyId;

    /**
     * inserts a record into purchase Payment Table
     * 
     * @access public
     * @return object
     */
    public function insertPayment() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_order_payments(
                    id,
                    order_id,
                    order_type,
                    payment_date,
                    payment_type,
                    payment_method_id,
                    payment_ref,
                    payment_remarks,
                    amount,
                    currency_id,
                    sessionid,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :order_id,
                    :order_type,
                    :payment_date,
                    :payment_type,
                    :payment_method_id,
                    :payment_ref,
                    :payment_remarks,
                    :amount,
                    :currency_id,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('payment_date', $this->getPaymentDate());
            $statement->bindValue('payment_type', $this->getPaymentType());
            $statement->bindValue('order_type', $this->getOrderType());
            $statement->bindValue('payment_method_id', $this->getPaymentMethodId());
            $statement->bindValue('payment_ref', $this->getPaymentRef());
            $statement->bindValue('payment_remarks', $this->getPaymentRemarks());
            $statement->bindValue('amount', $this->getPayAmount());
            $statement->bindValue('currency_id', $this->getCurrencyId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());
//            $statement = $this->_db->prepare("");
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate order
     * 
     * @access public
     * @return object
     */
    public function updatePayment() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_order_payments
                 SET
                    order_id=:order_id,
                    payment_type=:payment_type,
                    payment_method_id=:payment_method_id,
                    payment_ref=:payment_ref,
                    payment_remarks=:payment_remarks,
                    amount=:amount,
                    currency_id=:currency_id,
                    sessionid=:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('payment_type', $this->getPaymentType());
            $statement->bindValue('payment_method_id', $this->getPaymentMethodId());
            $statement->bindValue('payment_ref', $this->getPaymentRef());
            $statement->bindValue('payment_remarks', $this->getPaymentRemarks());
            $statement->bindValue('amount', $this->getPayAmount());
            $statement->bindValue('currency_id', $this->getCurrencyId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * use to fetch payment by id
     * @return 
     */
    public function fetchSalesPurchaseOrderPaymentById() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                    det.id,
                    det.order_id,
                    det.order_type,
                    date_format(det.payment_date,"%d/%m/%Y") as payment_date,
                    mas.name,
                    det.payment_type,
                    det.payment_method_id,
                    det.payment_ref,
                    det.payment_remarks,
                    det.amount
                     from tra_order_payments as det
                      INNER JOIN mas_payment_method mas 
                    ON mas.id = det.payment_method_id 
                    where det.id =:id and det.order_type= :order_type 
                    '
            );

            $statement->bindValue('id', $this->getPaymentId());
            $statement->bindValue('order_type', $this->getOrderType());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function deleteOrderPayment() {
        try {
            $statement = $this->_db->prepare('
                    DELETE FROM tra_order_payments 
                    WHERE 
                    `order_id`= :order_id AND
                    order_type = :order_type 
                    ');
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('order_type', $this->getOrderType());
            $statement->execute();
        } catch (Exception $ex) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    // all getters and setters
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getPaymentDate() {
        return $this->paymentDate;
    }

    public function setPaymentDate($paymentDate) {
        $this->paymentDate = $paymentDate;
    }

    public function getPaymentType() {
        return $this->paymentType;
    }

    public function setPaymentType($paymentType) {
        $this->paymentType = $paymentType;
    }

    public function getPaymentMethodId() {
        return $this->paymentMethodId;
    }

    public function setPaymentMethodId($paymentMethodId) {
        $this->paymentMethodId = $paymentMethodId;
    }

    public function getPaymentRef() {
        return $this->paymentRef;
    }

    public function setPaymentRef($paymentRef) {
        $this->paymentRef = $paymentRef;
    }

    public function getPaymentRemarks() {
        return $this->paymentRemarks;
    }

    public function setPaymentRemarks($paymentRemarks) {
        $this->paymentRemarks = $paymentRemarks;
    }

    public function getPayAmount() {
        return $this->payAmount;
    }

    public function setPayAmount($payAmount) {
        $this->payAmount = $payAmount;
    }

    public function getPaymentId() {
        return $this->paymentId;
    }

    public function setPaymentId($paymentId) {
        $this->paymentId = $paymentId;
    }

    public function getOrderType() {
        return $this->orderType;
    }

    public function setOrderType($orderType) {
        $this->orderType = $orderType;
    }

    public function getCurrencyId() {
        return $this->currencyId;
    }

    public function setCurrencyId($currencyId) {
        $this->currencyId = $currencyId;
    }

}
