<?php

/**
 * Application_Model_Customer
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 */
class Application_Model_Product extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $ProductName;
    protected $ProductTypeId;
    protected $ProdcutCategoryId;
    protected $ProductImage;
    protected $Barcode;
    protected $ReorderPoint;
    protected $ReorderQuantity;
    protected $StorageLocation;
    protected $LastVendor;
    protected $Length;
    protected $Width;
    protected $Height;
    protected $Weight;
    protected $StandardUoM;
    protected $SalesUom;
    protected $PurchasingUom;
    protected $Remarks;
    protected $sessionid;
    protected $vendor_id;
    protected $bom_id;
    protected $component_name;
    protected $quantity;
    protected $cost;
    protected $LocationId;
    protected $LocationQuantity;
    protected $LocationName;
    protected $ProductId;
    protected $PricingName;
    protected $PricingCurrencyId;
    protected $Markup;
    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product(
                    id,
                    product_name,
                    product_type_id,
                    product_category_id,
                    product_image,
                    bar_code,
                    reorder_point,
                    reorder_quantity,
                    storage_location,
                    last_vendor_id,
                    length,
                    width,
                    height,
                    weight,
                    standard_uom_id,
                    sales_uom_id,
                    purchasing_uom_id,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :product_name,
                    :product_type_id,
                    :product_category_id,
                    :product_image,
                    :bar_code,
                    :reorder_point,
                    :reorder_quantity,
                    :storage_location,
                    :last_vendor_id,
                    :length,
                    :width,
                    :height,
                    :weight,
                    :standard_uom_id,
                    :sales_uom_id,
                    :purchasing_uom_id,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_name', $this->getProductName());
            $statement->bindValue('product_type_id', $this->getProductTypeId());
            $statement->bindValue('product_category_id', $this->getProdcutCategoryId());
            $statement->bindValue('product_image', $this->getProductImage());
            $statement->bindValue('bar_code', $this->getBarcode());
            $statement->bindValue('reorder_point', $this->getReorderPoint());
            $statement->bindValue('reorder_quantity', $this->getReorderQuantity());
            $statement->bindValue('storage_location', $this->getStorageLocation());
            $statement->bindValue('last_vendor_id', $this->getLastVendor());
            $statement->bindValue('length', $this->getLength());
            $statement->bindValue('width', $this->getWidth());
            $statement->bindValue('height', $this->getHeight());
            $statement->bindValue('weight', $this->getWeight());
            $statement->bindValue('standard_uom_id', $this->getStandardUoM());
            $statement->bindValue('sales_uom_id', $this->getSalesUom());
            $statement->bindValue('purchasing_uom_id', $this->getPurchasingUom());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product
                 SET
                    product_name= :product_name,
                    product_type_id= :product_type_id,
                    product_category_id= :product_category_id,
                    bar_code= :bar_code,
                    reorder_point= :reorder_point,
                    reorder_quantity= :reorder_quantity,
                    storage_location= :storage_location,
                    last_vendor_id= :last_vendor_id,
                    length= :length,
                    width= :width,
                    height= :height,
                    weight= :weight,
                    standard_uom_id= :standard_uom_id,
                    sales_uom_id= :sales_uom_id,
                    purchasing_uom_id= :purchasing_uom_id,
                    remarks= :remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_name', $this->getProductName());
            $statement->bindValue('product_type_id', $this->getProductTypeId());
            $statement->bindValue('product_category_id', $this->getProdcutCategoryId());
            $statement->bindValue('bar_code', $this->getBarcode());
            $statement->bindValue('reorder_point', $this->getReorderPoint());
            $statement->bindValue('reorder_quantity', $this->getReorderQuantity());
            $statement->bindValue('storage_location', $this->getStorageLocation());
            $statement->bindValue('last_vendor_id', $this->getLastVendor());
            $statement->bindValue('length', $this->getLength());
            $statement->bindValue('width', $this->getWidth());
            $statement->bindValue('height', $this->getHeight());
            $statement->bindValue('weight', $this->getWeight());
            $statement->bindValue('standard_uom_id', $this->getStandardUoM());
            $statement->bindValue('sales_uom_id', $this->getSalesUom());
            $statement->bindValue('purchasing_uom_id', $this->getPurchasingUom());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch vendor by vendor id from master tables
     * 
     * @access public
     * @return object
     */
    public function fetchVendorById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_vendors
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch vendor data');
        }
    }

    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
    public function fetchProductId($name) {
        try {
            $statement = $this->_db->prepare(
                    'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProduct() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->insertProduct();
        } else {
            $this->updateProduct();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertProduct() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_vendors_product(
                    id,
                    `vendor_id`,
                    `product_id`,
                    `vendor_product_code`,
                    `cost`,
                    `sessionid`
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :vendor_id,
                    :product_id,
                    :vendor_product_code,
                    :cost,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('cost', $this->getProduct_cost());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProduct() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    product_id =:product_id,
                    vendor_product_code = :vendor_product_code,
                    cost= :cost,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_id', $this->getProductCode());
            $statement->bindValue('vendor_product_code', $this->getVendor_product_code());
            $statement->bindValue('cost', $this->getProduct_cost());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateVendorId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    vendor_id =:vendor_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('vendor_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get inventory product
     * @return type
     */
    public function fetchInvenotryProductById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT p.id,p.product_name,p.product_type_id,pt.name AS product_type,
                    p.product_category_id,pc.name AS product_category,p.bar_code,
                    p.reorder_point,p.reorder_quantity,p.storage_location as storage_location_id,
                    l.name AS storage_location,p.last_vendor_id,v.name AS last_vendor,p.length,
                    p.width,p.height,p.weight,p.standard_uom_id,p.sales_uom_id,
                    p.purchasing_uom_id,p.remarks,p.product_image,p.is_active
                    FROM tra_inventory_product AS p
                    LEFT JOIN  mas_product_type AS pt
                    ON p.product_type_id = pt.id
                    LEFT JOIN mas_product_category AS pc 
                    ON p.product_category_id = pc.id
                    LEFT JOIN mas_inventory_location AS l
                    ON p.storage_location = l.id
                    LEFT JOIN mas_vendors AS v
                    ON p.last_vendor_id = v.id
                  WHERE
                        p.id=:id
            ');
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get product cost 
     * @return type
     */
    public function fetchProductCostById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT ipc.id,ipc.price_currency_id,ipc.price,ipc.markup,ipc.cost,pc.name AS price_currency_name
                    FROM tra_inventory_product_cost AS ipc
                    INNER JOIN mas_pricing_currency AS pc 
                    ON ipc.price_currency_id = pc.id
                    WHERE 
                    ipc.product_id = :id

            ');
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetchAll();
            //var_dump($resultSet);die;
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProductId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    product_id =:product_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE mas_vendors_product
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    product_id = :product_id'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveComponent() {
        if ($this->getBom_id() == null || $this->getBom_id() < 1) {
            $this->insertComponent();
        } else {
            $this->updateComponent();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertComponent() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product_bill_of_materials(
                    id,
                    `product_id`,
                    `raw_product_id`,
                    `quantity`,
                    `cost`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :raw_product_id,
                    :quantity,
                    :cost,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('raw_product_id', $this->getComponent_name());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('cost', $this->getCost());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setBom_id($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateComponent() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_bill_of_materials
                 SET
                    raw_product_id =:raw_product_id,
                    quantity = :quantity,
                    cost= :cost,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getBom_id());
            $statement->bindValue('raw_product_id', $this->getComponent_name());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('cost', $this->getCost());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProductIdBOM() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_bill_of_materials
                 SET
                    product_id =:product_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_bill_of_materials
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    product_id = :product_id'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProductIdipq() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    product_id =:product_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by 
                  WHERE
                    product_id = :product_id'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProductCost() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_cost
                 SET
                    product_id =:product_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_cost
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    product_id = :product_id'
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get component name 
     * 
     * @access public
     * @return object
     */
    public function fetchComponentByName($name) {
        try {
            $statement = $this->_db->prepare(
                    'select * from tra_inventory_product
                  WHERE
                    product_name = :name'
            );
            $statement->bindValue('name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch components data');
        }
    }

    /**
     * get component by id
     * 
     * @access public
     * @return object
     */
    public function fetchComponentDataById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                    bom.id,
                    bom.product_id,
                    p.product_name,
                    bom.quantity,
                    bom.raw_product_id,
                    bom.cost 
                  FROM
                    tra_inventory_product_bill_of_materials bom 
                    INNER JOIN tra_inventory_product p 
                      ON p.id = bom.raw_product_id
                   where 
                    bom.id = :id'
            );
            $statement->bindValue('id', $this->getBom_id());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch material product data');
        }
    }

    /**
     * get inventory location from master
     * @param type $name
     * @return type
     */
    public function fetchInventoryLocationByName($name) {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_inventory_location
                  WHERE
                    name = :name'
            );
            $statement->bindValue('name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch components data');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveInventoryLocation() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->insertInventoryLocation();
        } else {
            $this->updateInventoryLocation();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertInventoryLocation() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product_quantity(
                    id,
                    `product_id`,
                    `location_id`,
                    `quantity`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :location_id,
                    :quantity,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('location_id', $this->getLocationId());
            $statement->bindValue('quantity', $this->getLocationQuantity());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setBom_id($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateInventoryLocation() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_quantity
                 SET
                    product_id =:product_id,
                    location_id =:location_id,
                    quantity = :quantity,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('location_id', $this->getLocationId());
            $statement->bindValue('quantity', $this->getLocationQuantity());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch inventory location by id
     * @return type
     * 
     */
    public function fetchInventoryLocationDataById() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT ipq.id,ipq.product_id,ipq.location_id,ipq.quantity,il.name as location_name
                        FROM tra_inventory_product_quantity ipq
                        INNER JOIN mas_inventory_location il
                        ON ipq.location_id = il.id
                   where 
                        ipq.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch material product data');
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProductCost() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->insertInventoryProductCost();
        } else {
            $this->updateInventoryProductCost();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertInventoryProductCost() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_inventory_product_cost(
                    id,
                    `product_id`,
                    `price_currency_id`,
                    `price`,
                    `markup`,
                    `cost`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :product_id,
                    :price_currency_id,
                    :price,
                    :markup,
                    :cost,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('price_currency_id', $this->getPricingCurrencyId());
            $statement->bindValue('price', $this->getPricingName());
            $statement->bindValue('markup', $this->getMarkup());
            $statement->bindValue('cost', $this->getCost());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setBom_id($this->_db->lastInsertId());
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * Deactivate Product
     * 
     */
    public function DeactivateProduct() {
        try{
            $statement = $this->_db->prepare(
                 'UPDATE tra_inventory_product
                 SET
                    is_active = "0",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
             DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }    
    /**
     * Activate Product 
     * 
     */
    public function ActivateProduct() {
        try{
            $statement = $this->_db->prepare(
                 'UPDATE tra_inventory_product
                 SET
                    is_active = "1",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
             DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateInventoryProductCost() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product_cost
                  SET
                    price= :price,
                    markup= :markup,
                    cost = :cost,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    product_id = :product_id 
                  AND
                    price_currency_id  =:price_currency_id
                    '
            );
            $statement->bindValue('product_id', $this->getId());
            $statement->bindValue('price', $this->getPricingName());
            $statement->bindValue('price_currency_id', $this->getPricingCurrencyId());
            $statement->bindValue('markup', $this->getMarkup());
            $statement->bindValue('cost', $this->getCost());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateFileName() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_inventory_product
                 SET
                    product_image =:product_image
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('product_image', $this->getProductImage());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getProductName() {
        return $this->ProductName;
    }

    public function getProductTypeId() {
        return $this->ProductTypeId;
    }

    public function getProdcutCategoryId() {
        return $this->ProdcutCategoryId;
    }

    public function getProductImage() {
        return $this->ProductImage;
    }

    public function getBarcode() {
        return $this->Barcode;
    }

    public function getReorderPoint() {
        return $this->ReorderPoint;
    }

    public function getStorageLocation() {
        return $this->StorageLocation;
    }

    public function getLastVendor() {
        return $this->LastVendor;
    }

    public function getLength() {
        return $this->Length;
    }

    public function getWidth() {
        return $this->Width;
    }

    public function getHeight() {
        return $this->Height;
    }

    public function getWeight() {
        return $this->Weight;
    }

    public function getStandardUoM() {
        return $this->StandardUoM;
    }

    public function getSalesUom() {
        return $this->SalesUom;
    }

    public function getPurchasingUom() {
        return $this->PurchasingUom;
    }

    public function getRemarks() {
        return $this->Remarks;
    }

    public function setProductName($ProductName) {
        $this->ProductName = $ProductName;
    }

    public function setProductTypeId($ProductTypeId) {
        $this->ProductTypeId = $ProductTypeId;
    }

    public function setProdcutCategoryId($ProdcutCategoryId) {
        $this->ProdcutCategoryId = $ProdcutCategoryId;
    }

    public function setProductImage($ProductImage) {
        $this->ProductImage = $ProductImage;
    }

    public function setBarcode($Barcode) {
        $this->Barcode = $Barcode;
    }

    public function setReorderPoint($ReorderPoint) {
        $this->ReorderPoint = $ReorderPoint;
    }

    public function setStorageLocation($StorageLocation) {
        $this->StorageLocation = $StorageLocation;
    }

    public function setLastVendor($LastVendor) {
        $this->LastVendor = $LastVendor;
    }

    public function setLength($Length) {
        $this->Length = $Length;
    }

    public function setWidth($Width) {
        $this->Width = $Width;
    }

    public function setHeight($Height) {
        $this->Height = $Height;
    }

    public function setWeight($Weight) {
        $this->Weight = $Weight;
    }

    public function setStandardUoM($StandardUoM) {
        $this->StandardUoM = $StandardUoM;
    }

    public function setSalesUom($SalesUom) {
        $this->SalesUom = $SalesUom;
    }

    public function setPurchasingUom($PurchasingUom) {
        $this->PurchasingUom = $PurchasingUom;
    }

    public function setRemarks($Remarks) {
        $this->Remarks = $Remarks;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getReorderQuantity() {
        return $this->ReorderQuantity;
    }

    public function setReorderQuantity($ReorderQuantity) {
        $this->ReorderQuantity = $ReorderQuantity;
    }

    public function getVendor_id() {
        return $this->vendor_id;
    }

    public function setVendor_id($vendor_id) {
        $this->vendor_id = $vendor_id;
    }

    public function getComponent_name() {
        return $this->component_name;
    }

    public function setComponent_name($component_name) {
        $this->component_name = $component_name;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function getCost() {
        return $this->cost;
    }

    public function setCost($cost) {
        $this->cost = $cost;
    }

    public function getBom_id() {
        return $this->bom_id;
    }

    public function setBom_id($bom_id) {
        $this->bom_id = $bom_id;
    }

    public function getLocationId() {
        return $this->LocationId;
    }

    public function getLocationQuantity() {
        return $this->LocationQuantity;
    }

    public function getLocationName() {
        return $this->LocationName;
    }

    public function setLocationId($LocationId) {
        $this->LocationId = $LocationId;
    }

    public function setLocationQuantity($LocationQuantity) {
        $this->LocationQuantity = $LocationQuantity;
    }

    public function setLocationName($LocationName) {
        $this->LocationName = $LocationName;
    }

    public function getProductId() {
        return $this->ProductId;
    }

    public function setProductId($ProductId) {
        $this->ProductId = $ProductId;
    }

    public function getMarkup() {
        return $this->Markup;
    }

    public function setMarkup($Markup) {
        $this->Markup = $Markup;
    }

    public function getPricingName() {
        return $this->PricingName;
    }

    public function getPricingCurrencyId() {
        return $this->PricingCurrencyId;
    }

    public function setPricingName($PricingName) {
        $this->PricingName = $PricingName;
    }

    public function setPricingCurrencyId($PricingCurrencyId) {
        $this->PricingCurrencyId = $PricingCurrencyId;
    }

}
