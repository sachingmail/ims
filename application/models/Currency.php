<?php

/**
 * Application_Model_Merger
 * 
 * The class is object representation of users table
 * Allows to work with create,update abd delete users table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel <ankitg1@damcogroup.com>
 */
class Application_Model_Currency extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $currency_name;
    protected $currency_code;
    protected $role_id;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO mas_currency(
                    id,
                    currency_name,
                    currency_code,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :currency_name,
                    :currency_code,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('currency_name', $this->getCurrency_name());
            $statement->bindValue('currency_code', $this->getCurrency_code());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE mas_currency
                 SET
                    currency_name = :currency_name,
                    currency_code = :currency_code,
                    updated = :updated,
                    updated_by = :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('currency_name', $this->getCurrency_name());
            $statement->bindValue('currency_code', $this->getCurrency_code());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * fetch currency by id
     * 
     * @access public
     * @return object
     */
    public function fetchCurrencyById() {
        try {
            $statement = $this->_db->prepare(
                    'select * from mas_currency
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    /**
     * 
     * @return type
     * 
     */
    public function checkIfCurrencyIsPresentInRecords() {
        try{
            $statement = $this->_db->prepare(
                    'select * from mas_vendors
                  WHERE
                    currency_id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['vendors'] = $statement->fetchAll();
            
            $statement = $this->_db->prepare(
                    'select * from tra_purchase_order_mas
                  WHERE
                    currency_id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet['purchase'] = $statement->fetchAll();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    public function getId() {
        return $this->id;
    }

    public function getCurrency_name() {
        return $this->currency_name;
    }

    public function getCurrency_code() {
        return $this->currency_code;
    }

    public function setCurrency_name($currency_name) {
        $this->currency_name = $currency_name;
    }

    public function setCurrency_code($currency_code) {
        $this->currency_code = $currency_code;
    }

    public function getRole_id() {
        return $this->role_id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

}
