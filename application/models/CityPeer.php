<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_CityPeer {

    /**
     * get all records form mas_cities table
     * fetch all cities from master tables
     * @return array
     */
    public static function fetchAllCountries() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					c.id,c.country_name 
                                        FROM mas_countries as c
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch all states from mas states 
     * @return type
     */
    public static function fetchAllStates() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT
					s.id,s.state_name 
                                        FROM mas_states as s
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

    /**
     * fetch all cities from mas cities
     * @return type
     */
    public static function fetchAllCities() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT c.id,c.city_name,s.state_name,co.country_name,c.state_id,c.country_id
                                            FROM mas_cities AS c
                                            LEFT JOIN mas_states AS s 
                                            ON c.state_id = s.id
                                            LEFT JOIN mas_countries AS co
                                            ON c.country_id = co.id
					');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }

}

?>
