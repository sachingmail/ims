<?php

/**
 *
 * @package application
 * @subpackage Projects Details
 */
class Application_Model_TimesheetPeer {

    /**
     * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function fetchAllTimesheet() {
        $db = Zend_Registry::get('db');
        try {
            $statement = $db->prepare('SELECT 
                t.id,
                t.emp_id,
                t.job_no,
                t.hourly_rate,
                e.name AS empname,
                DATE_FORMAT(t.`timesheet_date`,"%d/%m/%Y") AS timesheet_date,
                t.`total_ord`,
                t.`total_1_5`,
                t.`total_2_0`,
                t.`sub_total` 
              FROM
                `tra_emp_timesheet` AS t 
                INNER JOIN `mas_employee` AS e 
                  ON e.`id` = t.`emp_id` 
            ');
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch Users');
        }
    }
       
    /**
    * 
    * 
    */
    public function checkRecordExist($id,$tDate,$empId) {
        $db = Zend_Registry::get('db');
        try{
            $str ='';
            if($id){
                $str = "AND id!= :id";
            }
            $statement = $db->prepare(
                    'SELECT count(*) as tot
                        FROM tra_emp_timesheet 
                    WHERE  timesheet_date = :tDate AND emp_id=:emp_id '.$str.''
            );
            if($id){
                $statement->bindValue('id', $id);
            }
            
            $statement->bindValue('tDate', $tDate);
            $statement->bindValue('emp_id', $empId);
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['tot'];
        } catch (Exception $e) {
             DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch user data');
        }
    }
    
    /**
   * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function getTimesheetSummary($date_from,$date_to,$emp_id) {
        
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1='';
            if($date_from <> '' && $date_to ==''){
                $str = "AND t.timesheet_date = :dateFrom";
            }
            if($date_from && $date_to){
                $str = "AND (t.timesheet_date between :dateFrom and :dateTo)";
            }
            
            if($emp_id){
                $str1 = "AND t.emp_id = :empId";
            }
            
       
            $statement = $db->prepare('SELECT 
                t.id,
                t.emp_id,
                e.name AS empname,
                DATE_FORMAT(t.`timesheet_date`,"%d/%m/%Y") AS timesheet_date,
                sum(t.`total_ord`) as total_ord,
                sum(t.`total_1_5`) as total_1_5,
                sum(t.`total_2_0`) as total_2_0,
                sum(t.`sub_total`) as sub_total 
              FROM
                `tra_emp_timesheet` AS t 
                INNER JOIN `mas_employee` AS e 
                  ON e.`id` = t.`emp_id` 
                WHERE t.emp_id <> ""  '.$str.' '.$str1.'
                GROUP BY t.timesheet_date
            ');
            
            if($date_from != ''  && $date_to ==''){
                $statement->bindValue('dateFrom', $date_from);
            }
            if($date_from != ''  && $date_to !=''){
                $statement->bindValue('dateFrom', $date_from);
                $statement->bindValue('dateTo', $date_to);
            }            
            if($emp_id){
                $statement->bindValue('empId', $emp_id);
            }            
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch timesheet summary');
        }
    }
    
    /**
   * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function getEmployeeForTimesheet($date_from,$date_to,$emp_id) {
        
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1='';
            if($date_from <> '' && $date_to ==''){
                $str = "AND t.timesheet_date = :dateFrom";
            }
            if($date_from && $date_to){
                $str = "AND (t.timesheet_date between :dateFrom and :dateTo)";
            }
            
            if($emp_id){
                $str1 = "AND t.emp_id = :empId";
            }
            
       
            $statement = $db->prepare('SELECT 
                DISTINCT t.emp_id,
                e.name AS empname
                FROM `tra_emp_timesheet` AS t 
                INNER JOIN `mas_employee` AS e 
                  ON e.`id` = t.`emp_id` 
                WHERE t.emp_id <> ""  '.$str.' '.$str1.'
            ');
            
            if($date_from != ''  && $date_to ==''){
                $statement->bindValue('dateFrom', $date_from);
            }
            if($date_from != ''  && $date_to !=''){
                $statement->bindValue('dateFrom', $date_from);
                $statement->bindValue('dateTo', $date_to);
            }            
            if($emp_id){
                $statement->bindValue('empId', $emp_id);
            }            
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch timesheet summary');
        }
    }
    
    /**
   * @return array
     * static function
     * used to fetch all cards from user table
     */
    public static function getEmployeeTimesheetDates($date_from,$date_to,$emp_id) {
        
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            $str1='';
            if($date_from <> '' && $date_to ==''){
                $str = "AND t.timesheet_date = :dateFrom";
            }
            if($date_from && $date_to){
                $str = "AND (t.timesheet_date between :dateFrom and :dateTo)";
            }
            
            if($emp_id){
                $str1 = "AND t.emp_id = :empId";
            }

            $statement = $db->prepare('SELECT 
                t.id,
                t.emp_id,
                e.name AS empname,
                DATE_FORMAT(t.`timesheet_date`,"%d/%m/%Y") AS timesheet_date,
                sum(t.`total_ord`) as total_ord,
                sum(t.`total_1_5`) as total_1_5,
                sum(t.`total_2_0`) as total_2_0,
                sum(t.`sub_total`) as sub_total 
              FROM
                `tra_emp_timesheet` AS t 
                INNER JOIN `mas_employee` AS e 
                  ON e.`id` = t.`emp_id` 
                WHERE t.emp_id <> ""  '.$str.' '.$str1.'
                GROUP BY t.timesheet_date
            ');
            
            if($date_from != ''  && $date_to ==''){
                $statement->bindValue('dateFrom', $date_from);
            }
            if($date_from != ''  && $date_to !=''){
                $statement->bindValue('dateFrom', $date_from);
                $statement->bindValue('dateTo', $date_to);
            }            
            if($emp_id){
                $statement->bindValue('empId', $emp_id);
            }            
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch timesheet summary');
        }
    }
    
    public static function getEmployeeTimesheetDetails($dDate,$emp_id) {
        
        $db = Zend_Registry::get('db');
        try {
            $str ='';
            
            if($emp_id){
                $str = "AND t.emp_id = :empId";
            }
            
       
            $statement = $db->prepare('SELECT
                t.job_no,
                t.hourly_rate,
                t.`total_ord`,
                t.`total_1_5`,
                t.`total_2_0`,
                t.`sub_total` 
              FROM
                `tra_emp_timesheet` AS t 
                WHERE t.timesheet_date =:dDate '.$str.'
            ');
            
            
            $statement->bindValue('dDate', $dDate); 
            
            if($emp_id){
                $statement->bindValue('empId', $emp_id);
            }            
            
            $statement->execute();
            $resultSet = $statement->fetchAll();

            $statement->closeCursor();

            $organizations = array();

            if (count($resultSet)) {
                $organizations = $resultSet;
            }
            return $organizations;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch timesheet details');
        }
    }

}

?>
