<?php

/**
 * Application_Model_Purchase
 * 
 * The class is object representation of customer table
 * Allows to work with create,update abd delete customer table in object manner
 * 
 * @category Model
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @author Ankit Goel<ankitg1@damcogroup.com>
 */
class Application_Model_SalesOrder extends DMC_Model_Abstract {

    protected $_db;
    protected $id;
    protected $salesOrderId;
    protected $itemStatus;
    protected $customerId;
    protected $contactPerson;
    protected $phone;
    protected $address;
    protected $paymentTerms;
    protected $location;
    protected $orderDate;
    protected $salesRepsId;
    protected $orderStatus;
    protected $po_Ref;
    protected $orderId;
    protected $paymentStatus;
    protected $person_name;
    protected $sippingAddress;
    protected $dueDate;
    protected $taxScheme;
    protected $transactionDate;
    protected $nonCustomerCost;
    protected $pricingCurrency;
    protected $regShipDate;
    protected $grandSubTotal;
    protected $freight;
    protected $tax1;
    protected $taxper1;
    protected $taxAmount1;
    protected $tax2;
    protected $shipReq;
    protected $taxPer2;
    protected $taxAmount2;
    protected $netSubTotal;
    protected $paidAmount;
    protected $netAmount;
    protected $balanceAmount;
    protected $productRecordId;
    protected $remarks;
    protected $productId;
    protected $fulfilledRemarks;
    protected $returnRemarks;
    protected $restockRemarks;
    protected $receiveLocationId;
    protected $quantity;
    protected $unitPrice;
    protected $discount;
    protected $sessionid;
    protected $lastPayDate;

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function save() {
        if ($this->getId() == null || $this->getId() < 1) {
            $this->saveInsert();
        } else {
            $this->saveUpdate();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function saveInsert() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_sales_order_mas(
                    id,
                    `customer_id`,
                    `customer_name`,
                    `customer_address`,
                    `customer_phone`,
                    `order_date`,
                    `sales_rep_id`,
                    `location_id`,
                    `order_status`,
                    `payment_status`,
                    `terms_id`,
                    `po_ref`,
                    `ship_req`,
                    `shipping_address`,
                    `non_customer_costs`,
                    `due_date`,
                    `taxing_scheme_id`,
                    `tax1_name`,
                    `tax1_per`,
                    `tax1_amount`,
                    `tax2_name`,
                    `tax2_per`,
                    `tax2_amount`,
                    `pricing_currency_id`,
                    `req_ship_date`,
                    `sub_total`,
                    `freight`,
                    `total_amount`,
                    `paid_amount`,
                    `balance`,
                    `fulfilled_remarks`,
                    `return_remarks`,
                    `restock_remarks`,
                    remarks,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :customer_id,
                    :customer_name,
                    :customer_address,
                    :customer_phone,
                    :order_date,
                    :sales_rep_id,
                    :location_id,
                    :order_status,
                    :payment_status,
                    :terms_id,
                    :po_ref,
                    :ship_req,
                    :shipping_address,
                    :non_customer_costs,
                    :due_date,
                    :taxing_scheme_id,
                    :tax1_name,
                    :tax1_per,
                    :tax1_amount,
                    :tax2_name,
                    :tax2_per,
                    :tax2_amount,
                    :pricing_currency_id,
                    :req_ship_date,
                    :sub_total,
                    :freight,
                    :total_amount,
                    :paid_amount,
                    :balance,
                    :fulfilled_remarks,
                    :return_remarks,
                    :restock_remarks,
                    :remarks,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('customer_id', $this->getCustomerId());
            $statement->bindValue('customer_name', $this->getPerson_name());
            $statement->bindValue('customer_address', $this->getAddress());
            $statement->bindValue('customer_phone', $this->getPhone());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('sales_rep_id', $this->getSalesRepsId());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('order_status', 'Unfulfilled');
            $statement->bindValue('payment_status', 'Uninvoiced');
            $statement->bindValue('terms_id', $this->getPaymentTerms());
            $statement->bindValue('po_ref', $this->getPo_Ref());
            $statement->bindValue('ship_req', $this->getShipReq());
            $statement->bindValue('shipping_address', $this->getSippingAddress());
            $statement->bindValue('non_customer_costs', $this->getNonCustomerCost());
            $statement->bindValue('due_date', $this->getDueDate());
            $statement->bindValue('taxing_scheme_id', $this->getTaxScheme());
            $statement->bindValue('tax1_name', $this->getTax1());
            $statement->bindValue('tax1_per', $this->getTaxper1());
            $statement->bindValue('tax1_amount', $this->getTaxAmount1());
            $statement->bindValue('tax2_name', $this->getTax2());
            $statement->bindValue('tax2_per', $this->getTaxPer2());
            $statement->bindValue('tax2_amount', $this->getTaxAmount2());
            $statement->bindValue('pricing_currency_id', $this->getPricingCurrency());
            $statement->bindValue('req_ship_date', $this->getRegShipDate());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('total_amount', $this->getGrandSubTotal());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('fulfilled_remarks', $this->getFulfilledRemarks());
            $statement->bindValue('return_remarks', $this->getReturnRemarks());
            $statement->bindValue('restock_remarks', $this->getRestockRemarks());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function saveUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    customer_id= :customer_id,
                    customer_name= :customer_name,
                    customer_address= :customer_address,
                    customer_phone= :customer_phone,
                    order_date= :order_date,
                    sales_rep_id= :sales_rep_id,
                    location_id= :location_id,
                    terms_id= :terms_id,
                    po_ref= :po_ref,
                    ship_req= :ship_req,
                    shipping_address= :shipping_address,
                    non_customer_costs= :non_customer_costs,
                    due_date= :due_date,
                    taxing_scheme_id= :taxing_scheme_id,
                    tax1_name= :tax1_name,
                    tax1_per= :tax1_per,
                    tax1_amount= :tax1_amount,
                    tax2_name= :tax2_name,
                    tax2_per= :tax2_per,
                    tax2_amount= :tax2_amount,
                    pricing_currency_id= :pricing_currency_id,
                    req_ship_date= :req_ship_date,
                    sub_total= :sub_total,
                    freight= :freight,
                    total_amount= :total_amount,
                    paid_amount= :paid_amount,
                    balance= :balance,
                    fulfilled_remarks= :fulfilled_remarks,
                    return_remarks= :return_remarks,
                    restock_remarks= :restock_remarks,
                    remarks= :remarks,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('customer_id', $this->getCustomerId());
            $statement->bindValue('customer_name', $this->getPerson_name());
            $statement->bindValue('customer_address', $this->getAddress());
            $statement->bindValue('customer_phone', $this->getPhone());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('sales_rep_id', $this->getSalesRepsId());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('terms_id', $this->getPaymentTerms());
            $statement->bindValue('po_ref', $this->getPo_Ref());
            $statement->bindValue('ship_req', $this->getShipReq());
            $statement->bindValue('shipping_address', $this->getSippingAddress());
            $statement->bindValue('non_customer_costs', $this->getNonCustomerCost());
            $statement->bindValue('due_date', $this->getDueDate());
            $statement->bindValue('taxing_scheme_id', $this->getTaxScheme());
            $statement->bindValue('tax1_name', $this->getTax1());
            $statement->bindValue('tax1_per', $this->getTaxper1());
            $statement->bindValue('tax1_amount', $this->getTaxAmount1());
            $statement->bindValue('tax2_name', $this->getTax2());
            $statement->bindValue('tax2_per', $this->getTaxPer2());
            $statement->bindValue('tax2_amount', $this->getTaxAmount2());
            $statement->bindValue('pricing_currency_id', $this->getPricingCurrency());
            $statement->bindValue('req_ship_date', $this->getRegShipDate());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('freight', $this->getFreight());
            $statement->bindValue('total_amount', $this->getGrandSubTotal());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('fulfilled_remarks', $this->getFulfilledRemarks());
            $statement->bindValue('return_remarks', $this->getReturnRemarks());
            $statement->bindValue('restock_remarks', $this->getRestockRemarks());
            $statement->bindValue('remarks', $this->getRemarks());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get product Id
     * 
     * @access public
     * @return object
     */
    public function fetchProductId($name) {
        try {
            $statement = $this->_db->prepare(
                    'select id from tra_inventory_product
                  WHERE
                    product_name = :product_name'
            );
            $statement->bindValue('product_name', $name);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['id'];
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch product id');
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateSalesOrderId() {

        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_id =:order_id
                  WHERE
                    id = :id'
            );
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * update/inserts records
     * 
     * @access public
     * @return object
     */
    public function saveProduct() {
        if ($this->getProductRecordId() == null || $this->getProductRecordId() < 1) {
            $this->insertProduct();
        } else {
            $this->updateProduct();
        }
        return $this;
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function insertProduct() {
        try {
            $statement = $this->_db->prepare(
                    'INSERT INTO tra_sales_order_det(
                    id,
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `location_id`,
                    `sub_total`,
                    `transaction_date`,
                    `sessionid`,
                    updated,
                    updated_by,
                    created,
                    created_by
                    )
                    VALUES(
                    :id,
                    :order_id,
                    :item_status,
                    :product_id,
                    :quantity,
                    :unit_price,
                    :discount,
                    :location_id,
                    :sub_total,
                    :transaction_date,
                    :sessionid,
                    :updated,
                    :updated_by,
                    :created,
                    :created_by
                    )'
            );

            $statement->bindValue('id', null);
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('item_status', $this->getItemStatus());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('unit_price', $this->getUnitPrice());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('transaction_date', $this->getTransactionDate());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->bindValue('created', $this->getCreated());
            $statement->bindValue('created_by', $this->getCreated_by());
            $statement->execute();
            $this->setId($this->_db->lastInsertId());

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateProduct() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_det
                 SET
                    product_id=:product_id,
                    quantity=:quantity,
                    unit_price=:unit_price,
                    discount=:discount,
                    location_id=:location_id,
                    sub_total=:sub_total,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('quantity', $this->getQuantity());
            $statement->bindValue('unit_price', $this->getUnitPrice());
            $statement->bindValue('discount', $this->getDiscount());
            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('sub_total', $this->getNetSubTotal());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateOrderDetId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_det
                 SET
                    order_id =:order_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_det
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    order_id = :order_id'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateSalesOrderPaymentId() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_order_payments
                 SET
                    order_id =:order_id,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    sessionid = :sessionid'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', $this->getSessionid());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            $statement = $this->_db->prepare(
                    'UPDATE tra_order_payments
                 SET
                    sessionid =:sessionid,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    order_id = :order_id'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            $statement = $this->_db->prepare(
                    ' UPDATE 
                        tra_sales_order_mas 
                       SET 
                            paid_amount = (SELECT SUM(amount) FROM tra_order_payments WHERE order_id =:order_id),
                           updated= :updated,
                           updated_by= :updated_by
                  WHERE
                    id = :order_id'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function updatePaidAmount() {
        try {
            $statement = $this->_db->prepare(
                    ' UPDATE 
                        tra_sales_order_mas 
                       SET 
                            paid_amount = (SELECT SUM(amount) FROM tra_order_payments WHERE order_id =:order_id),
                           updated= :updated,
                           updated_by= :updated_by
                  WHERE
                    id = :order_id AND order_type = "Sales"'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('sessionid', '');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();
            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * get Sales order
     * 
     * @access public
     * @return object
     */
    public function fetchSalesOrder() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                     `id`,
                    `order_id`,
                    `customer_name`,
                    `customer_phone`,
                    `customer_address`,
                    date_format(order_date,"%d/%m/%Y") as so_date,
                    `order_status`,
                    `payment_status`,
                    `customer_id`,
                    `location_id`,
                    date_format(req_ship_date,"%d/%m/%Y") as req_ship_date,
                    `terms_id`,
                    `po_ref`,
                    `shipping_address`,
                    date_format(due_date,"%d/%m/%Y") as due_date,
                    `non_customer_costs`,
                    `pricing_currency_id`,
                    `sub_total`,
                    `freight`,
                    `taxing_scheme_id`,
                    `tax1_name`,
                    `tax1_per`,
                    `tax1_amount`,
                    `tax2_name`,
                    `tax2_per`,
                    `tax2_amount`,
                    `total_amount`,
                    `paid_amount`,
                    (total_amount - paid_amount) AS balance,
                    `fulfilled_remarks`,
                    `return_remarks`,
                    `restock_remarks`,
                    `remarks` 
            from tra_sales_order_mas
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            // var_dump($resultSet);die;

            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch purchase order');
        }
    }

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function getProductById() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                    det.id,
                    det.order_id,
                    det.item_status,
                    det.product_id,
                    ip.product_name,
                    det.quantity,
                    det.unit_price,
                    det.discount,
                    det.sub_total,
                    det.location_id,
                    DATE_FORMAT(det.transaction_date,"%d/%m/%Y") as transaction_date
                     from tra_sales_order_det as det
                      INNER JOIN tra_inventory_product ip 
                    ON ip.id = det.product_id 
                    where det.id =:id
                    '
            );
            $statement->bindValue('id', $this->getProductRecordId());
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fulfilledOrder() {
        try {
//            $statement = $this->_db->prepare(
//                    'UPDATE tra_sales_order_det
//                 SET
//                    item_status= "Fulfilled",
//                    updated= :updated,
//                    updated_by= :updated_by
//                  WHERE
//                    order_id = :order_id'
//            );
//            $statement->bindValue('order_id', $this->getId());
//            $statement->bindValue('updated', $this->getUpdated());
//            $statement->bindValue('updated_by', $this->getUpdated_by());
//            $statement->execute();
//
//            $statement = $this->_db->prepare(
//                    'UPDATE tra_sales_order_mas
//                 SET
//                    order_status= "Fulfilled",
//                    updated= :updated,
//                    updated_by= :updated_by
//                  WHERE
//                    id = :id'
//            );
//            $statement->bindValue('id', $this->getId());
//            $statement->bindValue('updated', $this->getUpdated());
//            $statement->bindValue('updated_by', $this->getUpdated_by());
//            $statement->execute();
            $statement = $this->_db->prepare(
                    "delete from tra_sales_order_det 
                    WHERE order_id = :order_id1 and item_status= 'Fulfilled'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_sales_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `quantity`,
                    `transaction_date`,
                    `location_id`,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Fulfilled',
                    `product_id`,
                    `quantity`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_sales_order_det 
                    WHERE order_id = :order_id and item_status= 'Returned'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();


            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function unfulfilledOrder() {
        try {
            $statement = $this->_db->prepare(
                    'delete from tra_sales_order_det 
                    WHERE order_id = :order_id and item_status= "Fulfilled"'
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Unfulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();



            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /*
     * autoFillReceiveProducts
     * 
     * @access public
     * @return object
     */

    public function autoFillRestockProducts() {
        try {

            $statement = $this->_db->prepare(
                    "delete from tra_sales_order_det 
                    WHERE order_id = :order_id1 and item_status= 'Restocked'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_sales_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `quantity`,
                    `transaction_date`,
                    `location_id`,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Restocked',
                    `product_id`,
                    `quantity`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_sales_order_det 
                    WHERE order_id = :order_id and item_status= 'Returned'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * 
     */
    public function autoFillReturnProducts() {
        try {

            $statement = $this->_db->prepare(
                    "delete from tra_sales_order_det 
                    WHERE order_id = :order_id1 and item_status= 'Returned'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_sales_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    `transaction_date`,
                    `location_id`,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Returned',
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_sales_order_det 
                    WHERE order_id = :order_id and item_status= 'Fulfilled'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Started",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * update merger
     * mark order as invoiced
     * @return 
     */
    public function markSalesOrderInvoiced() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_date = :order_date,
                    payment_status=:payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('payment_status', 'Invoiced');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * update merger
     * mark order as invoiced
     * @return 
     */
    public function markSalesOrderUninvoiced() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_date = :order_date,
                    payment_status=:payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_date', $this->getOrderDate());
            $statement->bindValue('payment_status', 'Uninvoiced');
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * check whether order id is present or not
     * 
     */
    public function chekOrderIdPresent() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT 
                         order_id
                     FROM 
                         tra_sales_order_mas
                    WHERE
                        order_id = :order_id
                    '
            );
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function getProductByProductIdAndStatus() {
        try {
            $statement = $this->_db->prepare(
                    'select 
                    quantity
                 from tra_sales_order_det
                where order_id =:order_id and product_id =:product_id
                and item_status =:status
             ');

            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('status', $this->getItemStatus());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet['quantity'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return \Application_Model_SalesOrder
     * 
     */
    public function autoFillFulfillProducts() {
        try {
            $statement = $this->_db->prepare(
                    "delete from tra_sales_order_det 
                    WHERE order_id = :order_id1 and item_status= 'Fulfilled'
               "
            );
            $statement->bindValue('order_id1', $this->getId());
            $statement->execute();

            $statement = $this->_db->prepare(
                    "INSERT INTO tra_sales_order_det (
                    `order_id`,
                    `item_status`,
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    `transaction_date`,
                    `location_id`,
                    `updated`,
                    `updated_by`,
                    `created`,
                    `created_by`
                  ) 
                  SELECT 
                    `order_id`,
                    'Fulfilled',
                    `product_id`,
                    `quantity`,
                    `unit_price`,
                    `discount`,
                    `sub_total`,
                    DATE_FORMAT(NOW(),'%Y-%m-%d'),
                    :location_id,
                    NOW(),
                    :updated,
                    NOW(),
                    :created  
                    FROM tra_sales_order_det 
                    WHERE order_id = :order_id and item_status= 'Order'
               "
            );
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('updated', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('created', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->bindValue('location_id', $this->getLocation());
            $statement->execute();

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Fulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();

            $statement = $this->_db->prepare('
                    Select id,
                           order_id,
                           product_id,
                           quantity,
                           location_id
                    from
                        tra_sales_order_det
                    where 
                        order_id =:order_id and item_status = "Order"
                    ');
            $statement->bindValue('order_id', $this->getId());
            $statement->execute();
            $result = $statement->fetchAll();
            return $result;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function statusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    payment_status= :payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('payment_status', $this->getPaymentStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function paymentStatusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    payment_status= :payment_status,
                    balance =:balance,
                    paid_amount=:paid_amount,
                    last_pay_date= :last_pay_date,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('payment_status', $this->getPaymentStatus());
            $statement->bindValue('balance', $this->getBalanceAmount());
            $statement->bindValue('paid_amount', $this->getPaidAmount());
            $statement->bindValue('last_pay_date', $this->getLastPayDate());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function orderStatusUpdate() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= :order_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('order_status', $this->getOrderStatus());
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    public function getProductPresentAtLocation() {
        try {
            $statement = $this->_db->prepare(
                    'SELECT SUM(quantity) AS quantity FROM `tra_inventory_product_quantity` 
                        WHERE location_id = :location_id AND product_id = :product_id
             ');

            $statement->bindValue('location_id', $this->getLocation());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet['quantity'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * 
     */
    public function getFulfilProductDetails() {
        try {
            $statement = $this->_db->prepare('
                    select quantity,location_id
                        from tra_sales_order_det
                    where 
                        product_id =:product_id
                    and 
                        order_id =:order_id
                    ');
            $statement->bindValue('order_id', $this->getOrderId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->execute();
            $resultSet = $statement->fetch();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * 
     */

    /**
     * inserts a record into user table
     * 
     * @access public
     * @return object
     */
    public function getFulfilledProductQty() {
        try {
            $str = '';
            if ($this->getProductRecordId()) {
                $str = 'AND  id <> :id';
            }
            $statement = $this->_db->prepare(
                    'select 
                    sum(quantity) as quantity
                 from tra_sales_order_det
                where order_id =:order_id and product_id =:product_id
                and item_status =:status ' . $str . '
             ');

            if ($this->getProductRecordId()) {
                $statement->bindValue('id', $this->getProductRecordId());
            }
            $statement->bindValue('order_id', $this->getId());
            $statement->bindValue('product_id', $this->getProductId());
            $statement->bindValue('status', $this->getItemStatus());
            $statement->execute();
            $resultSet = $statement->fetch();

            return $resultSet['quantity'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function getPaidPayment($id) {
        try {
            $statement = $this->_db->prepare(
                    'select  sum(amount) as amount 
                     from `tra_order_payments` 
                  where `order_id`= :order_id and `order_type`="Sales"
               '
            );
            $statement->bindValue('order_id', $id);
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['amount'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }
    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function fetchCreatedDate() {
        try {
            $statement = $this->_db->prepare(
                    '   SELECT 
                                DATE_FORMAT(created,"%Y-%m-%d") AS created  
                        FROM 
                                tra_sales_order_mas
                        WHERE 
                                id = :id 
                                       '
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet['created'];
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * udpate merger
     * 
     * @access public
     * @return object
     */
    public function updateTotalPayment() {
        try {
            $status = '';
            $amount = $this->getPaidPayment($this->getId());
//            echo $amount;
            $sales = Application_Model_SalesOrderPeer::fetchSubTotalByIdRefund($this->getId());

            //echo $amount.'############'.$sales['total_amount'];exit;
            if ($amount > $sales['sub_total']) {
                $status = 'Owing';
            } elseif ($amount < $sales['sub_total']) {
                $status = 'Partial';
            } elseif ($amount == $sales['sub_total']) {
                $status == 'Paid';
            }

            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    paid_amount= :paid_amount,
                    payment_status= :payment_status,
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('paid_amount', $amount);
            $statement->bindValue('payment_status', $status);
            $statement->bindValue('updated', $this->getUpdated());
            $statement->bindValue('updated_by', $this->getUpdated_by());
            $statement->execute();

            return $this;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function fetchSalesOrderForPrint() {
        try {
            $statement = $this->_db->prepare('
                    SELECT 
                        m.id,
                        m.order_id,
                        DATE_FORMAT(m.order_date, "%d-%m-%Y") AS so_date,
                        m.order_status,
                        m.payment_status,
                        c.name AS customer_name,
                        c.contact_name,
                        m.customer_phone,
                        m.customer_address,
                        l.name AS location,
                        DATE_FORMAT(m.req_ship_date, "%d/%m/%Y") AS req_shipping_date,
                        t.id as term_id,
                        t.name AS term,
                        m.po_ref,
                        m.ship_req,
                        m.shipping_address,
                        DATE_FORMAT(due_date, "%d-%m-%Y") AS due_date,
                        m.non_customer_costs,
                        m.pricing_currency_id,
                        m.sub_total,
                        m.freight,
                        m.taxing_scheme_id,
                        m.tax1_name,
                        m.tax1_per,
                        m.tax1_amount,
                        m.tax2_name,
                        m.tax2_per,
                        m.tax2_amount,
                        m.total_amount,
                        m.paid_amount,
                        m.balance,
                        m.remarks,
                        m.fulfilled_remarks,
                        m.return_remarks,
                        m.restock_remarks,
                        tax.tax_on_shipping,
                        tax.secondary_tax_on_shipping,
                        tax.compound_secondary
                      FROM
                        tra_sales_order_mas AS m
                        LEFT JOIN mas_customers AS c
                        ON c.id = m.customer_id
                        LEFT JOIN mas_inventory_location AS l
                        ON l.id = m.location_id
                        LEFT JOIN mas_payment_terms AS t
                        ON t.id = m.terms_id
                        LEFT JOIN mas_taxing_schemes as tax
                        ON m.taxing_scheme_id = tax.id
                  WHERE
                    m.id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->execute();
            $resultSet = $statement->fetch();
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            DMC_Exception::processError(
                    Zend_Log::ERR, $e->getMessage() . ' - Unable to fetch purchase order');
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function cancelSalesOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Cancelled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();
            return;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * 
     * @return type
     * 
     */
    public function reopenSalesOrder() {
        try {
            $statement = $this->_db->prepare(
                    'UPDATE tra_sales_order_mas
                 SET
                    order_status= "Unfulfilled",
                    updated= :updated,
                    updated_by= :updated_by
                  WHERE
                    id = :id'
            );
            $statement->bindValue('id', $this->getId());
            $statement->bindValue('updated', date('Y-m-d'));
            $statement->bindValue('updated_by', Zend_Auth::getInstance()->getIdentity()->id);
            $statement->execute();
            return;
        } catch (Exception $e) {
            DMC_Exception::processError(Zend_Log::ERR, $e->getMessage());
        }
    }

    /**
     * all setters and getters
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getSalesOrderId() {
        return $this->salesOrderId;
    }

    public function getCustomerId() {
        return $this->customerId;
    }

    public function getContactPerson() {
        return $this->contactPerson;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getPaymentTerms() {
        return $this->paymentTerms;
    }

    public function getLocation() {
        return $this->location;
    }

    public function getOrderDate() {
        return $this->orderDate;
    }

    public function getOrderStatus() {
        return $this->orderStatus;
    }

    public function getPo_Ref() {
        return $this->po_Ref;
    }

    public function getPaymentStatus() {
        return $this->paymentStatus;
    }

    public function getSippingAddress() {
        return $this->sippingAddress;
    }

    public function getDueDate() {
        return $this->dueDate;
    }

    public function getTaxScheme() {
        return $this->taxScheme;
    }

    public function getNonCustomerCost() {
        return $this->nonCustomerCost;
    }

    public function getPricingCurrency() {
        return $this->pricingCurrency;
    }

    public function getRegShipDate() {
        return $this->regShipDate;
    }

    public function getRemarks() {
        return $this->remarks;
    }

    public function getGrandSubTotal() {
        return $this->grandSubTotal;
    }

    public function getFreight() {
        return $this->freight;
    }

    public function getTax1() {
        return $this->tax1;
    }

    public function getTaxper1() {
        return $this->taxper1;
    }

    public function getTaxAmount1() {
        return $this->taxAmount1;
    }

    public function getTax2() {
        return $this->tax2;
    }

    public function getTaxPer2() {
        return $this->taxPer2;
    }

    public function getTaxAmount2() {
        return $this->taxAmount2;
    }

    public function getNetSubTotal() {
        return $this->netSubTotal;
    }

    public function getPaidAmount() {
        return $this->paidAmount;
    }

    public function getNetAmount() {
        return $this->netAmount;
    }

    public function getBalanceAmount() {
        return $this->balanceAmount;
    }

    public function getProductRecordId() {
        return $this->productRecordId;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getUnitPrice() {
        return $this->unitPrice;
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function getSessionid() {
        return $this->sessionid;
    }

    public function setSalesOrderId($salesOrderId) {
        $this->salesOrderId = $salesOrderId;
    }

    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    public function setContactPerson($contactPerson) {
        $this->contactPerson = $contactPerson;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setPaymentTerms($paymentTerms) {
        $this->paymentTerms = $paymentTerms;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function setOrderDate($orderDate) {
        $this->orderDate = $orderDate;
    }

    public function setOrderStatus($orderStatus) {
        $this->orderStatus = $orderStatus;
    }

    public function setPo_Ref($po_Ref) {
        $this->po_Ref = $po_Ref;
    }

    public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;
    }

    public function setSippingAddress($sippingAddress) {
        $this->sippingAddress = $sippingAddress;
    }

    public function setDueDate($dueDate) {
        $this->dueDate = $dueDate;
    }

    public function setTaxScheme($taxScheme) {
        $this->taxScheme = $taxScheme;
    }

    public function setNonCustomerCost($nonCustomerCost) {
        $this->nonCustomerCost = $nonCustomerCost;
    }

    public function setPricingCurrency($pricingCurrency) {
        $this->pricingCurrency = $pricingCurrency;
    }

    public function setRegShipDate($regShipDate) {
        $this->regShipDate = $regShipDate;
    }

    public function setGrandSubTotal($grandSubTotal) {
        $this->grandSubTotal = $grandSubTotal;
    }

    public function setFreight($freight) {
        $this->freight = $freight;
    }

    public function setTax1($tax1) {
        $this->tax1 = $tax1;
    }

    public function setTaxper1($taxper1) {
        $this->taxper1 = $taxper1;
    }

    public function setTaxAmount1($taxAmount1) {
        $this->taxAmount1 = $taxAmount1;
    }

    public function setTax2($tax2) {
        $this->tax2 = $tax2;
    }

    public function setTaxPer2($taxPer2) {
        $this->taxPer2 = $taxPer2;
    }

    public function setTaxAmount2($taxAmount2) {
        $this->taxAmount2 = $taxAmount2;
    }

    public function setNetSubTotal($netSubTotal) {
        $this->netSubTotal = $netSubTotal;
    }

    public function setPaidAmount($paidAmount) {
        $this->paidAmount = $paidAmount;
    }

    public function setNetAmount($netAmount) {
        $this->netAmount = $netAmount;
    }

    public function setBalanceAmount($balanceAmount) {
        $this->balanceAmount = $balanceAmount;
    }

    public function setProductRecordId($productRecordId) {
        $this->productRecordId = $productRecordId;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function setUnitPrice($unitPrice) {
        $this->unitPrice = $unitPrice;
    }

    public function setDiscount($discount) {
        $this->discount = $discount;
    }

    public function setSessionid($sessionid) {
        $this->sessionid = $sessionid;
    }

    public function getPerson_name() {
        return $this->person_name;
    }

    public function setPerson_name($person_name) {
        $this->person_name = $person_name;
    }

    public function getSalesRepsId() {
        return $this->salesRepsId;
    }

    public function setSalesRepsId($salesRepsId) {
        $this->salesRepsId = $salesRepsId;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function getTransactionDate() {
        return $this->transactionDate;
    }

    public function setTransactionDate($transactionDate) {
        $this->transactionDate = $transactionDate;
    }

    public function getItemStatus() {
        return $this->itemStatus;
    }

    public function setItemStatus($itemStatus) {
        $this->itemStatus = $itemStatus;
    }

    public function getReceiveLocationId() {
        return $this->receiveLocationId;
    }

    public function setReceiveLocationId($receiveLocationId) {
        $this->receiveLocationId = $receiveLocationId;
    }

    public function getFulfilledRemarks() {
        return $this->fulfilledRemarks;
    }

    public function getReturnRemarks() {
        return $this->returnRemarks;
    }

    public function getRestockRemarks() {
        return $this->restockRemarks;
    }

    public function setFulfilledRemarks($fulfilledRemarks) {
        $this->fulfilledRemarks = $fulfilledRemarks;
    }

    public function setReturnRemarks($returnRemarks) {
        $this->returnRemarks = $returnRemarks;
    }

    public function setRestockRemarks($restockRemarks) {
        $this->restockRemarks = $restockRemarks;
    }

    public function getLastPayDate() {
        return $this->lastPayDate;
    }

    public function setLastPayDate($lastPayDate) {
        $this->lastPayDate = $lastPayDate;
    }

    public function getShipReq() {
        return $this->shipReq;
    }

    public function setShipReq($shipReq) {
        $this->shipReq = $shipReq;
    }

}
