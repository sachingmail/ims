<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_ReceivingAddress extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');

        
        $address = new Zend_Form_Element_Text('address_name');
        $address->setLabel('Address Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Address Name'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
                ->setOrder('0')
        ;
        
        $street = new Zend_Form_Element_Text('street');
        $street->setLabel('Street Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Street Name'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
                ->setOrder('1')
        ;
        $city = new Zend_Form_Element_Text('city_name');
        $city->setLabel('City Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter City Name'))
                ->setAttrib('class', 'form-control')
                ->setAttrib('id', 'city')
                ->addFilter('StringTrim')
                ->setOrder('2')
        ;
        
        $state = new Zend_Form_Element_Select('state');
        $state->setLabel('State Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select data from the drop down List'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control','id'=> 'state'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setOrder('4')
                ;
        
        $country = new Zend_Form_Element_Select('country');
        $country->setLabel('Country Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select data from the drop down List'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control','id'=>'country'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setOrder('3')
                ;
        
        $zipcode = new Zend_Form_Element_Text('zip_code');
        $zipcode->setLabel('Zip Code')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid Zip Code'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
                ->setOrder('5')
        ;
        
        $type = new Zend_Form_Element_Select('address_type');
        $type->setLabel('Address Type')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select data from the drop down List'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setOrder('6');
        
        $remarks= new Zend_Form_Element_Text('remarks');
        $remarks->setLabel('Remarks')
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setOrder('7')
                ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $address,
            $street,
            $city,
            $state,
            $country,
            $zipcode,
            $type,
            $remarks,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
