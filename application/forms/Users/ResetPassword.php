<?php
/**
 * Form used for change password
 *
 * @category Form
 * @package  IMS
 * @author   Amit kumar<amitk@damcogroup.com>
 * @link     Application_Form_Users_ResetPassword
 */

class Application_Form_Users_ResetPassword extends Application_Form_BaseForm
{

    /**
     * adds and intializes form elements
     * 
     * @access public
     * 
     * @return object
     */
    public function init()
    {

        parent::init();

        $userid = new Zend_Form_Element_Hidden('userid');

        $passwordInput = new Zend_Form_Element_Password('password');
        $passwordInput->setRequired(true);
        $validatorNotEmpty = new Zend_Validate_NotEmpty();
        $validatorNotEmpty->setMessage('New Password cannot be blank');

        $passwordInput->setLabel('Password')
           ->addFilters(array('StringTrim', 'StripTags'))
           ->setAttribs(array('class' => 'form-control'))
           ->addValidator($validatorNotEmpty, $breakChainOnFailure = true);
          ;


        $confirmpassword = new Zend_Form_Element_Password('confirmpassword');
        $confirmpassword->setRequired(true);
        $validatorNotEmpty = new Zend_Validate_NotEmpty();
        $validatorNotEmpty->setMessage('Confirm Password cannot be blank');
        $identical = new Zend_Validate_Identical();
        $identical->setMessage("New password and confirm password should be same.");
        $identical->setToken('password');

        $confirmpassword->setLabel('Confirm password')
            ->addFilters(array('StringTrim', 'StripTags'))
            ->setAttribs(array('class' => 'form-control'))
            ->addValidator($validatorNotEmpty, $breakChainOnFailure = true)
            ->addValidator($identical);
        ;

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
            ->setAttrib('type', 'submit')
            ->setLabel('Change Password')
            ->setAttrib('class', 'btn')
            ->setOrder(101);

        $this->addElements(array($userid,
            $passwordInput,
            $confirmpassword,
            $submit
        ));
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return array
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        return $valid;
    }

}
?>

