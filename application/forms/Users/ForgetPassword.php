<?php
/**
 * Form used for forget password
 *
 * @category Form
 * @package  IMS
 * @author   Ami tkumar<amitk@damcogroup.com>
 * @link     Application_Form_Users_ForgetPassword
 */
class Application_Form_Users_ForgetPassword extends Application_Form_BaseForm
{

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return object
     */
    public function init()
    {

        parent::init();

        $email = new Zend_Form_Element_Text('email');
        $validatorNotEmpty = new Zend_Validate_NotEmpty();
        $validatorNotEmpty->setMessage('Email cannot be blank');
        $email->setRequired(true)
            ->setLabel('Email')
            ->setAttrib('class', 'form-control')
            ->addFilters(array('StringTrim', 'StripTags'))
                //  ->getDecorator('Errors')
            ->addValidator($validatorNotEmpty, $breakChainOnFailure = true)
            ->addValidator('EmailAddress')
            ->setErrorMessages(array('messages' => 'Email cannot be blank. Please enter valid email address.'));

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
            ->setAttrib('type', 'submit')
            ->setLabel('Reset Password')
            ->setAttrib('class', 'btn')
            ->setOrder(101);

        $this->addElements(array(
            $email,
            $submit
        ));
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * @access public
     * @return array
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        return $valid;
    }

}
?>

