<?php
/**
 * Application_Form_Users_Add
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_User_Add
 */
class Application_Form_Users_Add extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->setMethod('POST');
        $this->setAction('/users');
        $id = new Zend_Form_Element_Hidden('id');

        $username = new Zend_Form_Element_Text('uname');
        $username->setLabel('User Name')
            ->setRequired(true)
            ->addValidator('Alnum', true, array('allowWhiteSpace' => 'false', 'messages' => 'Please Enter valid user name'))
            ->setAttribs(array('placeholder' => 'Please enter username','class' => 'form-control','autocomplete'=>'off'));
        
        $password = new Zend_Form_Element_Password('upassword');
        $password->setLabel('Password')
            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control','autocomplete'=>'off'))
            ;
        
        $cpassword = new Zend_Form_Element_Password('cpassword');
        $cpassword->setLabel('Confirm Password')
            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control'))
            ;
        
        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Person Name')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->addValidator('Alnum', true, array('allowWhiteSpace' => 'false', 'messages' => 'Please Enter valid name'))
            ->setAttribs(array('placeholder' => 'Please enter name'));
        
        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email Address')
            ->addValidator('regex',false,array('pattern'=>'/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/','messages' =>'Email address should be in a format "xyz@gmail.com"'))    
            ->addDecorator('Label', array('for' => 'exampleInputEmail1'))
            ->setRequired(true)
            //->addValidator('EmailAddress',FALSE,array('messages'=>'Email address should be in a format "xyz@gmail.com"') )
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter email'))
           ;
        
        $phone = new Zend_Form_Element_Text('phoneno');
        $phone->setLabel('Phone No')
            ->setRequired(false)
            ->addValidator('Digits', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
            ->addValidator('StringLength', true, array('min' =>7,'max' => 15, 'messages' => 'Min Length is 7 and Max length is 15'))
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter phone no'));

        $roleSelect = new Zend_Form_Element_Select('role_id');
        $roleSelect->setLabel('Role Name')
             ->setRequired(true)
             ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
    ;
        
        $isactive = new Zend_Form_Element_Select('isactive');
        $isactive->setLabel('Is Active')
                ->setRequired(true)
             ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'));

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
            ->setAttrib('type', 'submit')
            ->setLabel('Save')
            ->setAttrib('class', 'btn btn-success')
            ->setOrder(101);

        $this->addElements(
            array(
                $id,
                $username,
                $password,
                $cpassword,
                $name,
                $phone,
                $email,
                $roleSelect,
                $isactive,
                $submit
            )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        
        if($vd['upassword']){
            if($vd['upassword']!=$vd['cpassword']){
                $this->upassword->addError('Password and confirm password should be same');
                $valid = FALSE;
            }
        }
        $validateRecord = Application_Model_UsersPeer::validateUsers($vd['id'], $vd['uname']);
        if($validateRecord>0){
            $this->uname->addError('User name already present in our record, Please enter diffirent name.');
            $valid = FALSE;
        }
        return $valid;
    }

}

?>
