<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_City extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $city = new Zend_Form_Element_Text('city_name');
        $city->setLabel('City Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid City Name'))
                ->setRequired(TRUE)
                ->addValidator('Alnum', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->setAttribs(array('class' => 'form-control custom-select'))
                ->addFilter('StringTrim')
        ;
        
        $state = new Zend_Form_Element_Select('state');
        $state->setLabel('State Name')
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control custom-select'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'));
        
        $country = new Zend_Form_Element_Select('country');
        $country->setLabel('Country Name')
                ->setAttrib('id','country')
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control custom-select'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'));
       
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $city,
            $state,
            $country,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
