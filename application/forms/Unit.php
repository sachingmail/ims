<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Unit extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('unit_name');
        $name->setLabel('Unit Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Unit Name'))
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['unit_name']) {
            $validateRecord = Application_Model_UnitPeer::validateUnit($vd['id'], $vd['unit_name']);
            if ($validateRecord > 0) {
                $this->unit_name->addError('Unit already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        return $valid;
    }

}
