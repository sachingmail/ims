<?php

/**
 * Application_Form_Customer
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_Customer
 */
class Application_Form_Customer extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
        $this->setAction('/customer/add');
        $id = new Zend_Form_Element_Hidden('id');

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Customer Name'))
                ->setRequired(true)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 100, 'messages' => 'Max length is 100'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter name'));

        $balance = new Zend_Form_Element_Text('balance');
        $balance->setLabel('Balance')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length of this field can be 15'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => '0.00'));

        $credit = new Zend_Form_Element_Text('credit');
        $credit->setLabel('Credit')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Customer Credit'))
                ->setRequired(false)
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => '0.00'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->addValidator('StringLength', true, array('max' => 10, 'messages' => 'Max length of this field can be 15'))
        ;

        $contact_name = new Zend_Form_Element_Text('contact_name');
        $contact_name->setLabel('Name')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 30, 'messages' => 'Max length is 30'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter Name'));
        
        $abn_no = new Zend_Form_Element_Text('abn_no');
        $abn_no->setLabel('ABN No')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 50, 'messages' => 'Max length is 50'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter ABN No'));

         $des = new Zend_Form_Element_Text('des');
        $des->setLabel('Designation')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ;
        
        $address = new Zend_Form_Element_Textarea('address');
        $address->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control'))
        ;
        $address1 = new Zend_Form_Element_Textarea('address1');
        $address1->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;
        
        $address2 = new Zend_Form_Element_Textarea('address2');
        $address2->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;

        $address_type = new Zend_Form_Element_Select('address_type');
        $typeData = array(
            'Bussiness Address' => 'Bussiness Address',
            'Billing Address' => 'Billing Address',
            'Shipping Address' => 'Shipping Address'
        );
        $address_type->setRequired(true)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Customers.ChangeAddress()'))
                ->addFilter('StringTrim')
                ->setMultiOptions($typeData)
        ;


        $phone = new Zend_Form_Element_Text('phoneno');
        $phone->setLabel('Phone No')
                ->setRequired(false)
                ->addValidator('regex', false, array('pattern' => "/^\d+$/", 'messages' => 'Only Numeric Field Allowed'))
                ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length is 15'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter phone no'));


        $fax = new Zend_Form_Element_Text('fax');
        $fax->setLabel('Fax')
                ->setRequired(false)
                ->addValidator('regex', false, array('pattern' => "/^\d+$/", 'messages' => 'Only Numeric Field Allowed'))
                ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length is 15'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter Fax'));



        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email')
                ->setRequired(false)
                ->addValidator('EmailAddress',false,array('messages'=>'Email Address Should be in format like xyz@gmail.com'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter email'));


        $website = new Zend_Form_Element_Text('website');
        $website->setLabel('Website')
                ->setRequired(false)
                //->addValidator('regex', false, array('pattern' => "/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/", 'messages' => 'Format Should be http://www.google.com'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter website'));

        $pricingCurrency = Application_Model_PricingCurrencyPeer::fetchAllPricingCurrency();
        $pricingCurrencyData = array();
        $pricingCurrencyData[''] = 'Please Select';
        foreach ($pricingCurrency as $key => $val) {
            $pricingCurrencyData[$val['id']] = $val['name'];
        }

        $pricing_currency = new Zend_Form_Element_Select('pricing_currency');
        $pricing_currency->setLabel('Pricing / Currency')
                // ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($pricingCurrencyData);
        ;

        $discount = new Zend_Form_Element_Text('discount');
        $discount->setLabel('Discount')
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => '0.00'));

        $terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $termsData = array();
        $termsData[''] = 'Please Select';
        foreach ($terms as $key => $val) {
            $termsData[$val['id']] = $val['name'];
        }

        $payment_terms = new Zend_Form_Element_Select('payment_terms');
        $payment_terms->setLabel('Payment Terms')
                // ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($termsData);

        $taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $taxingSchemeData = array();
        $taxingSchemeData[''] = 'Please Select';
        foreach ($taxingScheme as $key => $val) {
            $taxingSchemeData[$val['id']] = $val['tax_scheme_name'];
        }

        $taxing_scheme = new Zend_Form_Element_Select('taxing_scheme');
        $taxing_scheme->setLabel('Taxing Scheme')
                //    ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($taxingSchemeData);


        $tax_exempt = new Zend_Form_Element_Text('tax_exempt');
        $tax_exempt->setLabel('Tax Exempt#')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => ''));

        $locationRaw = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $locationData = array();
        $locationData[''] = 'Please Select';
        foreach ($locationRaw as $key => $val) {
            $locationData[$val['id']] = $val['name'];
        }

        $location = new Zend_Form_Element_Select('location');
        $location->setLabel('Default Location')
                //    ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($locationData);

        $salesReps = Application_Model_SaleRepsPeer::fetchAllReps();
        $salesRepsData = array();
        $salesRepsData[''] = 'Please Select';
        foreach ($salesReps as $key => $val) {
            $salesRepsData[$val['id']] = $val['name'];
        }
        $sales_reps = new Zend_Form_Element_Select('sales_reps');
        $sales_reps->setLabel('Default Sales Reps')
                //  ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($salesRepsData);

        $carrierRaw = Application_Model_CarrierPeer::fetchAllCarriers();
        $carrierData = array();
        $carrierData[''] = 'Please Select';
        foreach ($carrierRaw as $key => $val) {
            $carrierData[$val['id']] = $val['carrier_name'];
        }
        $carrier = new Zend_Form_Element_Select('carrier');
        $carrier->setLabel('Carrier')
                //  ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($carrierData);

        $paymentm = Application_Model_PaymentMethodPeer::fetchAllpayments();
        $paymentmData = array();
        $paymentmData[''] = 'Please Select';
        foreach ($paymentm as $key => $val) {
            $paymentmData[$val['id']] = $val['name'];
        }

        $payment_method = new Zend_Form_Element_Select('payment_method');
        $payment_method->setLabel('Payment Method')
                //  ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($paymentmData);

        $cardType = Application_Model_CardTypePeer::fetchAllCards();
        $cardTypeData = array();
        $cardTypeData[''] = 'Please Select';
        foreach ($cardType as $key => $val) {
            $cardTypeData[$val['id']] = $val['card_type'];
        }

        $card_type = new Zend_Form_Element_Select('card_type');
        $card_type->setLabel('Card Type')
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setMultiOptions($cardTypeData);

        $card_no = new Zend_Form_Element_Text('card_no');
        $card_no->setLabel('Card Number')
                ->setRequired(false)
                ->addValidator('Digits', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Enter Card Number'));

        $expiration_date = new Zend_Form_Element_Text('expiration_date');
        $expiration_date->setLabel('Expiration Date')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Expiration Date'));

        $card_security_code = new Zend_Form_Element_Text('card_security_code');
        $card_security_code->setLabel('Card Security Code')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('Digits', true, array('allowWhiteSpace' => 'true', 'Messages' => 'No Special Characters Allowed.'))
                ->addValidator('StringLength', true, array('max' => 5, 'messages' => 'Max length is 5'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Card Security Code'));

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(
                array(
                    $id,
                    $name,
                    $abn_no,
                    $balance,
                    $credit,
                    $address_type,
                    $address,
                    $address1,
                    $address2,
                    $contact_name,
                    $phone,
                    $fax,
                    $email,
                    $website,
                    $pricing_currency,
                    $discount,
                    $payment_terms,
                    $taxing_scheme,
                    $tax_exempt,
                    $location,
                    $sales_reps,
                    $carrier,
                    $payment_method,
                    $card_type,
                    $card_no,
                    $expiration_date,
                    $card_security_code,
                    $des,
                    $submit
                )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['phoneno']) {
            $validatePhone = Application_Model_CustomerPeer::validatePhone($vd['id'], $vd['phoneno']);
            if ($validatePhone > 0) {
                $this->phoneno->addError('Phone Number already present in our record, Please enter diffirent Number.');
                $valid = FALSE;
            }
        }
        if ($vd['name']) {
            $validateName = Application_Model_CustomerPeer::validateName($vd['id'], $vd['name']);
            if ($validateName > 0) {
                $this->name->addError('Name already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        if ($vd['email']) {
            $validateEmail = Application_Model_CustomerPeer::validateEmail($vd['id'], $vd['email']);
            if ($validateEmail > 0) {
                $this->email->addError('Email already present in our record, Please enter diffirent Email.');
                $valid = FALSE;
            }
        }

        if ($vd['discount'] != '') {
            if (($vd['discount'] < 0) || ($vd['discount'] > 100)) {
                $this->discount->addError('Discount should be between 0 to 100.');
            }
        }
        if($vd['website']) { 
                $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
                $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
                $regex .= "([a-z0-9-.]*)\.([a-z]{2,4})"; // Host or IP
                $regex .= "(\:[0-9]{2,5})?"; // Port
                $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
                $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
                $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
               if(!preg_match("/^$regex$/",$vd['website'],$m)) { 
                $this->website->addError('Not a valid Website. Please enter a Valid Website Name');
                $valid = FALSE;
                }
        }
        return $valid;
    }

}

?>
