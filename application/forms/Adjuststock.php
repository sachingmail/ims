<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Adjuststock extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);
        
        
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $productArr[''] = 'Please Select';
        foreach ($product as $key => $val) {
             $productArr[$val['id']] = $val['product_name'];
        }
        
        $name = new Zend_Form_Element_Select('name');
        $name->setLabel('Product Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please select product name'))
                ->setAttribs(array('class' => 'form-control','onchange'=>'Inventory.refreshQty();'))
                ->addFilter('StringTrim')
                ->setMultiOptions($productArr)
        ;
        
        $locationData = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $locArr = array();
        $locArr[''] = 'Please Select';
        foreach ($locationData as $key => $val) {
                $locArr[$val['id']] = $val['name'];
        }
        
        $defaultLocation= new Zend_Form_Element_Select('location');
        $defaultLocation->setLabel('Location')
            ->setRequired(true)
            ->addValidator('NotEmpty', true, array('messages' => 'Please select valid location name'))
            ->setAttribs(array('class' => 'form-control','onchange'=>'Inventory.refreshQty();'))
            ->addFilter('StringTrim')
            ->setMultiOptions($locArr);
        
        $newQty = new Zend_Form_Element_Text('newqty');
        $newQty->setLabel('New Quanity')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid quantity'))
                ->setAttrib('class', 'form-control')
                ->setAttrib('onchange','Inventory.calcDiff();')
                ->addFilter('StringTrim')
                ->addValidator(
        		'regex',
        		false,
        		array(
                            'pattern' => '/^\d+\.?\d{0,0}$/', // Numeric up to 2 decimal places
                            'messages' => array(
                                            'regexNotMatch' => "Please enter valid quantity"
                            )
        		)
        )
        ;
        $oldQty = new Zend_Form_Element_Text('oldqty');
        $oldQty->setLabel('Old Quanity')
                ->setRequired(FALSE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid quantity'))
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->addFilter('StringTrim')
        ;
        
        $diff = new Zend_Form_Element_Text('diff');
        $diff->setLabel('Difference')
                ->setRequired(FALSE)
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->addFilter('StringTrim')
        ;  
         $remarks = new Zend_Form_Element_Textarea('remarks');
        $remarks->setLabel('Remarks')
                ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control'))
        ;  

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $id,
            $name,
            $defaultLocation,
            $newQty,
            $oldQty,
            $diff,
            $remarks,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
