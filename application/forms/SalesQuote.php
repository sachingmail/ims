<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_SalesQuote extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
        
        
        $customerFetch = Application_Model_CustomerPeer::fetchAllCustomers();
        $customerData = array();
        $customerData[''] = 'Please Select';
        foreach ($customerFetch as $key => $val) {
                $customerData[$val['id']] = $val['name'];
        }
        $customer= new Zend_Form_Element_Select('customer');
        $customer->setLabel('Customer')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select Customer Name'))
                ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select','id'=>'customer'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($customerData)
                ;
        
        $des = new Zend_Form_Element_Text('des');
        $des->setLabel('Designation')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ;
        
        $contact = new Zend_Form_Element_Text('contact');
        $contact->setLabel('Contact')
            ->addValidator('NotEmpty', true, array('messages' => 'Please enter contact'))
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'contact'))
            ;
        
        $phone = new Zend_Form_Element_Text('phone');
        $phone->setLabel('Phone')
            ->setRequired(false)
            ->addValidator('Digits', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
            ->addValidator('StringLength', true, array('min'=>'7','max' => 15, 'messages' => 'Max length is 15'))
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'phone_no'))
            ->setAttribs(array('placeholder' => 'Please enter phone no'));
        
        $address = new Zend_Form_Element_Textarea('address');
        $address->setRequired(false)
            //->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
            ->addValidator('StringLength', true, array('max' => 200, 'messages' => 'Max length is 200'))
            ->setLabel('Billing Address')    
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control','id'=>'address'))
            ;
        
        
        $address1 = new Zend_Form_Element_Textarea('address1');
        $address1->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;
        
        $address2 = new Zend_Form_Element_Textarea('address2');
        $address2->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;

        $address_type = new Zend_Form_Element_Select('address_type');
        $typeData = array(
            'Bussiness Address' => 'Bussiness Address',
            'Billing Address' => 'Billing Address',
            'Shipping Address' => 'Shipping Address'
        );
        $address_type->setRequired(true)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Customers.ChangeAddress()'))
                ->addFilter('StringTrim')
                ->setMultiOptions($typeData)
        ;
        
        $shipping_address = new Zend_Form_Element_Textarea('shipping_address');
        $shipping_address->setRequired(false)
            ->setLabel('Shipping Address')    
            ->addValidator('StringLength', true, array('max' => 200, 'messages' => 'Max length is 200'))    
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control','id'=>'shipping_address'))
            ;
        $paymentTerms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $paymentTermsData = array();
        $paymentTermsData[''] = 'Please Select';
        foreach ($paymentTerms as $key => $val) {
                $paymentTermsData[$val['id']] = $val['name'];
        }
        
        $payment_terms= new Zend_Form_Element_Select('payment_terms_id');
        $payment_terms->setLabel('Terms')
            //    ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select' ))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($paymentTermsData);
        
        $taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $taxingSchemeData = array();
        $taxingSchemeData[''] = 'Please Select';
        foreach ($taxingScheme as $key => $val) {
                $taxingSchemeData[$val['id']] = $val['tax_scheme_name'];
        }
        
        $taxing_scheme= new Zend_Form_Element_Select('taxing_scheme');
        $taxing_scheme->setLabel('Tax Code')
            //    ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select','id'=>'taxing_scheme'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($taxingSchemeData);
        
         $pricingCurrency = Application_Model_PricingCurrencyPeer::fetchAllPricingCurrency();
        $pricingCurrencyData = array();
        $pricingCurrencyData[''] = 'Please Select';
        foreach ($pricingCurrency as $key => $val) {
                $pricingCurrencyData[$val['id']] = $val['name'];
        }
        
        $pricing_currency = new Zend_Form_Element_Select('pricing_currency');
        $pricing_currency->setLabel('Pricing / Currency')
            // ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($pricingCurrencyData);
         ;
        
        $locationRaw = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $locationData = array();
        $locationData[''] = 'Please Select';
        foreach ($locationRaw as $key => $val) {
                $locationData[$val['id']] = $val['name'];
        }
        
        
        $location= new Zend_Form_Element_Select('location');
        $location->setLabel('Location')
            //    ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($locationData);
        
        $productRaw = Application_Model_ProductPeer::fetchAllProduct();
        $productData = array();
        $productData[''] = 'Please Select';
        foreach ($productRaw as $key => $val) {
                $productData[$val['id']] = $val['product_name'];
        }
        
        $product= new Zend_Form_Element_Select('sales_item_id');
        $product->setLabel('Item')
            //    ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
            ->setAttribs(array('id'=>'sales_item_id'))
            ->setMultiOptions($productData);
        
        $salesReps = Application_Model_SaleRepsPeer::fetchAllReps();
        $salesRepsData = array();
        $salesRepsData[''] = 'Please Select';
        foreach ($salesReps as $key => $val) {
                $salesRepsData[$val['id']] = $val['name'];
        }
        $sales_reps= new Zend_Form_Element_Select('sales_reps');
        $sales_reps->setLabel('Sales Reps')
              //  ->setRequired(true)
             ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => 'Please select'))
            ->setMultiOptions($salesRepsData);
        
        $quote = new Zend_Form_Element_Text('quote');
        $quote->setLabel('Quote #')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','readonly'=>'true','title' => 'Quote Id will be generated automatically'))
                ;
        $p_o = new Zend_Form_Element_Text('p_o');
        $p_o->setLabel('P.O.#')
            ->setRequired(false)
            ->addValidator('Alnum', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
            ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length is 15'))
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
                ;
        
        $quoteDate = new Zend_Form_Element_Text('quote_date');
        $quoteDate->setLabel('Quote Date')
                ->setRequired(False)
                ->setValue(date('d/m/Y'))
                ->setAttribs(array('class'=>'form-control','id'=>'quote_date'))
                ;
        $reqShipDate = new Zend_Form_Element_Text('req_ship_date');
        $reqShipDate->setLabel('Req. Ship Date')
                ->setRequired(False)
                ->setAttribs(array('class'=>'form-control','id'=>'req_ship_date'))
                ;
        $status = new Zend_Form_Element_Text('status');
        $status->setLabel('Status')
                ->setAttribs(array('class'=>'form-control','id'=>'status','readonly'=>'true'))
                ->setRequired(false)
                ->setValue('Quote')
                ;
        $item = new Zend_Form_Element_Text('item');
        $item->setLabel('Item')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Item Name'));
        
        $quantity = new Zend_Form_Element_Text('quantity');
        $quantity->setLabel('Quantity')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Quantity'));
        
        $unit_price = new Zend_Form_Element_Text('unit_price');
        $unit_price->setLabel('Unit Price')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Unit Price'));
        
        $discount = new Zend_Form_Element_Text('discount');
        $discount->setLabel('Discount')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Discount'));
        
        $non_customer_cost= new Zend_Form_Element_Text('non_customer_cost');
        $non_customer_cost->setLabel('Non Customer Costs')
             ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setAttribs(array('placeholder' => ''))
           ;
//            ->setMultiOptions($locationData)
                ;
        
        $sub_total = new Zend_Form_Element_Text('sub_total');
        $sub_total->setLabel('Sub Total')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'sub_total_1','readonly'=>'true'))
            ;
        $freight = new Zend_Form_Element_Text('freight');
        $freight->setLabel('Freight')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'freight'))
            ;
        $total = new Zend_Form_Element_Text('total');
        $total->setLabel('Total')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','readonly'=>'true','id'=>'total'))
            ;
        $tax1 = new Zend_Form_Element_Text('tax1_name');
        $tax1->setLabel('Tax 1')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'tax1_name'))
            ;
        $tax2 = new Zend_Form_Element_Text('tax2_name');
        $tax2->setLabel('Tax 2')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'tax2_name'))
            ;
        $remarks = new Zend_Form_Element_Textarea('remarks');
        $remarks->setLabel('Remarks')
                ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control'))
            ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttribs(array('class'=> 'btn btn-success','readonly'=>'true'))
                ->setOrder(101);

        $this->addElements(array(
            $customer,
            $contact,
            $phone,
            $address_type,
            $address,
            $address1,
            $address2,
            $quote,
            $quoteDate,
            $taxing_scheme,
            $pricing_currency,
            $sales_reps,
            $quantity,
            $discount,
            $shipping_address,
            $p_o,
            $freight,
            $total,
            $item,
            $payment_terms,
            $sub_total,
            $status,
            $reqShipDate,
            $location,
            $sub_total,
            $tax1,
            $tax2,
            $unit_price,
            $remarks,
            $non_customer_cost,    
            $id,
            $product,
            $des,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
