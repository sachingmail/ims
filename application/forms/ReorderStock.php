<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_ReorderStock extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);
        
        
        $productRaw = Application_Model_ProductPeer::fetchAllProduct();
        $productData = array();
        $productData[''] = 'Please Select';
        foreach ($productRaw as $key => $val) {
                $productData[$val['id']] = $val['product_name'];
        }
        
        $product= new Zend_Form_Element_Select('item_id');
        $product->setLabel('Product Name')
                ->setRequired(true)
             ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setAttribs(array('id'=>'item_id'))
            ->setMultiOptions($productData);  
        
        $vendorRaw = Application_Model_VendorPeer::fetchAllVendors();
        $vendorData = array();
        $vendorData[''] = 'Please Select';
        foreach ($vendorRaw as $key => $val) {
                $vendorData[$val['id']] = $val['name'];
        }
        
        $vendor= new Zend_Form_Element_Select('vendor_id');
        $vendor->setLabel('Vendor')
                ->setRequired(true)
             ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setAttribs(array('id'=>'item_id'))
            ->setMultiOptions($vendorData);        
        
        $newQty = new Zend_Form_Element_Text('newqty');
        $newQty->setLabel('Quantity Available')
                ->setRequired(FALSE)
//                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid quantity'))
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('name', 'newqty[]')
                ->setAttrib('id', 'newqty0')
                ->addFilter('StringTrim')
        ;
        $suggested_quantity = new Zend_Form_Element_Text('suggested_quantity');
        $suggested_quantity->setLabel('Suggested Quanity')
                ->setRequired(false)
                ->setAttribs(array('class'=>'form-control','name'=>'suggested_quantity[]'))
                ->addFilter('StringTrim')
        ;
        $reorder_point = new Zend_Form_Element_Text('reorder_point');
        $reorder_point->setLabel('Reorder Point')
                ->setRequired(FALSE)
//                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid quantity'))
                ->setAttrib('name', 'reorder_point[]')
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->addFilter('StringTrim')
        ;
        
        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Add Product')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);
        $reset = new Zend_Form_Element_Button('resetbtn');
        $reset->setAttrib('id', 'reset')
                ->setAttrib('type', 'reset')
                ->setLabel('Reset')
                ->setAttrib('class', 'btn btn-warning')
                ->setOrder(101);

        $this->addElements(array(
            $id,
            $product,
            $vendor,
            $newQty,
            $suggested_quantity,
            $reorder_point,
            $submit,
            $reset
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
