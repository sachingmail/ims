<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_ProductCategory extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Product Category Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Product Category Name'))
                ->setRequired(TRUE)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $parent = new Zend_Form_Element_Select('parent');
        $parent->setLabel('Parent Name')
                ->setAttribs(array('class' => 'form-control custom-select'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'));
       
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $parent,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        $validateRecord = Application_Model_ProductCategoryPeer::validateProductCategory($vd['id'], $vd['name']);
        if($validateRecord>0){
            $this->name->addError('Product Category already present in our record, Please enter diffirent name.');
            $valid = FALSE;
        }

        return $valid;
    }

}
