<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Carrier extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('carrier_name');
        $name->setLabel('Carrier Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Carrier Name'))
                ->setRequired(TRUE)
                ->addValidator('Alnum', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['carrier_name']) {
            $validateRecord = Application_Model_CarrierPeer::validateCarier($vd['id'], $vd['carrier_name']);
            if ($validateRecord > 0) {
                $this->carrier_name->addError('Carrier name already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        return $valid;
    }

}
