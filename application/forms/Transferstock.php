<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Transferstock extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');

        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);
        
        $trasfer_date = new Zend_Form_Element_Text('transfer_date');
        $trasfer_date->setLabel('Date')
                ->setRequired(FALSE)
                ->setValue(date('d/m/Y'))
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->addFilter('StringTrim')
        ;  
        
        $send_date = new Zend_Form_Element_Text('send_date');
        $send_date->setLabel('Send Date')
                ->setRequired(FALSE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;  
        
        $received_date = new Zend_Form_Element_Text('received_date');
        $received_date->setLabel('Receive Date')
                ->setRequired(FALSE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ; 
        
        $status = new Zend_Form_Element_Text('status');
        $status->setLabel('Status')
                ->setRequired(FALSE)
                ->setValue('Open')
                ->setAttrib('class', 'form-control')
                ->setAttrib('readonly', 'readonly')
                ->addFilter('StringTrim')
        ;
        
        $product = Application_Model_ProductPeer::fetchAllProduct();
        $productArr = array();
        $productArr[''] = 'Please Select';
        foreach ($product as $key => $val) {
             $productArr[$val['id']] = $val['product_name'];
        }
        
        $name = new Zend_Form_Element_Select('name');
        $name->setLabel('Product Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please select product name'))
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setMultiOptions($productArr)
        ;
        
        $locationData = Application_Model_InventoryLocationPeer::fetchAllLocations();
        $locArr = array();
        $locArr[''] = 'Please Select';
        foreach ($locationData as $key => $val) {
                $locArr[$val['id']] = $val['name'];
        }
        
        $fromLocation= new Zend_Form_Element_Select('location');
        $fromLocation->setLabel('From Location')
            ->setRequired(true)
            ->addValidator('NotEmpty', true, array('messages' => 'Please select from location name'))
            ->setAttribs(array('class' => 'form-control','onchange'=>'Inventory.getCurrentQtyByLocation();'))
            ->addFilter('StringTrim')
            ->setMultiOptions($locArr);
        
        $tolocation= new Zend_Form_Element_Select('tolocation');
        $tolocation->setLabel('To Location')
            ->setRequired(true)
            ->addValidator('NotEmpty', true, array('messages' => 'Please select to location name'))
            ->setAttribs(array('class' => 'form-control'))
            ->addFilter('StringTrim')
            ->setMultiOptions($locArr);
        
        $newQty = new Zend_Form_Element_Text('qty');
        $newQty->setLabel('Quanity')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter quantity'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
                ->setValue(0)
                ->addValidator(
        		'regex',
        		false,
        		array(
                            'pattern' => '/^\d+\.?\d{0,0}$/', // Numeric up to 2 decimal places
                            'messages' => array(
                                            'regexNotMatch' => "Please enter valid quantity"
                            )
        		)
        )
        ;
                
        $currentQty = new Zend_Form_Element_Hidden('currentqty');
        $currentQty->setLabel('Quanity')
                ->setRequired(FALSE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        
        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $id,
            $name,
            $fromLocation,
            $tolocation,
            $newQty,
            $trasfer_date,
            $send_date,
            $received_date,
            $status,
            $currentQty,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();  
        if($vd['tolocation']){
            if($vd['location'] == $vd['tolocation']){
                $this->tolocation->addError('To location should not be same as from location');
                $valid = FALSE;
            }
        }
        
        if($vd['qty'] < 1){
            $this->qty->addError('Quantity should not be less than 1');
            $valid = FALSE;
        }
        
        if($vd['qty'] > 0){
            if($vd['qty'] > $vd['currentqty']){
                $this->qty->addError('Quantity is not available for transfer in this location');
                $valid = FALSE;
            }
        }
        
        return $valid;
    }

}
