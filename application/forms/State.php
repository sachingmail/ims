<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_State extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('state_name');
        $name->setLabel('State Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter State Name'))
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $country = new Zend_Form_Element_Select('country');
        $country->setLabel('Country Name')
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control custom-select'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'));
       
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $country,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
