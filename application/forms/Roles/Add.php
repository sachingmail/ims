<?php
/**
 * Application_Form_Roles_Add
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_Roles_Add
 */
class Application_Form_Roles_Add extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->setMethod('POST');
        $this->setAction('/roles');
        $id = new Zend_Form_Element_Hidden('id');

        $rolename = new Zend_Form_Element_Text('rolename');
        $rolename->setLabel('Role Name')
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter role name'));
        
        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
            ->setAttrib('type', 'submit')
            ->setLabel('Add Role')
            ->setAttrib('class', 'btn btn-success')
            ->setOrder(101);

        $this->addElements(
            array(
                $id,
                $rolename,
                $submit
            )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        return $valid;
    }

}

?>
