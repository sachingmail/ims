<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_PaymentTerms extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Payment Term Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter valid Payment Term Name'))
                ->setRequired(TRUE)
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'Only Alphabets are allowed'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $days = new Zend_Form_Element_Text('days');
        $days->setLabel('Number Of Days')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Number Of Days'))
                ->addValidator('Digits', true, array('allowWhiteSpace' => 'false', 'messages' => 'Only Numerical Values are allowed'))
                ->addValidator('StringLength', true, array('max' => 3, 'messages' => 'Max length is 3'))
                ->setRequired(TRUE)
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $days,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['days'] && is_numeric($vd['days']) ) {
            if ($vd['days'] == 0 || $vd['days'] < 0) {
                $this->days->addError('Days should be greater than 0');
                $valid = FALSE;
            }
        }
        if ($vd['name']) {
            $validateRecord = Application_Model_PaymentTermsPeer::validatePaymentTerm($vd['id'], $vd['name']);
            if ($validateRecord > 0) {
                $this->name->addError('Payment Term already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        return $valid;
    }

}
