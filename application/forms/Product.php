<?php
/**
 * Application_Form_Vendor
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_Vendor
 */
class Application_Form_Product extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->setMethod('POST');
        $this->setAction('/product/add');
        $this->setEnctype('multipart/form-data');
        $id = new Zend_Form_Element_Hidden('id');

        $product_name = new Zend_Form_Element_Text('product_name');
        $product_name->setLabel('Item Name/Code')
            ->addValidator('NotEmpty', true, array('messages' => 'Please enter Product Name'))
            ->setRequired(true)
            ->addValidator('Alnum', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
            ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
            ->setAttribs(array('class' => 'form-control','id' => "product_name",'tabindex' => '1'))
            ->setAttribs(array('placeholder' => 'Please enter Product name'))
            ->addFilter('StringTrim');
        
        $product_type = new Zend_Form_Element_Select('product_type');
        $product_type->setLabel('Product Type')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select Product Type'))
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control custom-select','tabindex' => '2'))
                ->addFilter('StringTrim')
         ;
        
        $product_category = new Zend_Form_Element_Select('product_category');
        $product_category->setLabel('Product Category')
                ->addValidator('NotEmpty', true, array('messages' => 'Please select product category'))
                ->setRequired(true)
            ->setAttribs(array('class' => 'form-control custom-select','tabindex' => '3'))
            ->addFilter('StringTrim')
         ;
        

        $picture = new Zend_Form_Element_Textarea('picture');
        $picture->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control'))
            ;
        
        $browse= new Zend_Form_Element_File('browse');
        $browse->setLabel('Upload Image')
			 ->setRequired(FALSE)
			 //->addValidator('NotEmpty', true, array('messages'=>'Please upload a valid file'))
			 ->setAttribs(array('id'=>'imgInp'))
			 //->setDestination(path)
			->addValidator('Extension', FALSE,array('jpg','png','jpeg'))
			 //->addValidator('Size', false, array('max' => 200000))
//                         ->addFilter('StringTrim')
             ;


        $barcode = new Zend_Form_Element_Text('bar_code');
        $barcode->setLabel('Barcode')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
                ;
        
        $reorder_point = new Zend_Form_Element_Text('reorder_point');
        $reorder_point->setLabel('Reorder Point')
//            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => '0'))
           ;
        
        $reorder_quantity = new Zend_Form_Element_Text('reorder_quantity');
        $reorder_quantity->setLabel('Reorder Quantity')
//            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => '0'))
           ;
        
        $storage_location = new Zend_Form_Element_Select('storage_location');
        $storage_location->setLabel('Default Location')
//            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
         ;
        
        $last_vendor = new Zend_Form_Element_Select('last_vendor');
        $last_vendor->setLabel('Last Vendor')
//            ->setRequired(true)
            ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
        ;
        
        $length = new Zend_Form_Element_Text('length');
        $length->setLabel('Length')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Length'))
                ;
        $width = new Zend_Form_Element_Text('width');
        $width->setLabel('Width')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Width'))
                ;
        
        $height = new Zend_Form_Element_Text('height');
        $height->setLabel('Height')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Height'))
                ;
        $weight = new Zend_Form_Element_Text('weight');
        $weight->setLabel('Weight')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control'))
            ->setAttribs(array('placeholder' => 'Please enter Weight'))
                ;
        
        $standard_uom = new Zend_Form_Element_Select('standard_uom_id');
        $standard_uom->setLabel('Standard UoM')
            ->setRequired(false)
            ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
        ;
        $sales_uom  = new Zend_Form_Element_Select('sales_uom_id');
        $sales_uom ->setLabel('Sales UoM')
            ->setRequired(false)
            ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
        ;
        $purchasing_uom = new Zend_Form_Element_Select('purchasing_uom_id');
        $purchasing_uom->setLabel('Purchasing UoM')
            ->setRequired(false)
            ->setAttribs(array('class' => 'form-control custom-select'))
            ->addFilter('StringTrim')
        ;

        $normal = new Zend_Form_Element_Text('normal');
        $normal->setLabel('Normal')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'normal'))
                ;
        $markup = new Zend_Form_Element_Text('markup');
        $markup->setLabel('Markup')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'markup','readonly'=>'true'))
            ->setAttribs(array('placeholder' => 'Please enter Weight'))
                ;
        
        $cost = new Zend_Form_Element_Text('cost');
        $cost->setLabel('Cost')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'cost'))
                ;
        
        $remarks = new Zend_Form_Element_Textarea('remarks');
        $remarks->setLabel('Remarks')
                ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttrib('cols', '40')
            ->setAttrib('rows', '4')
            ->setAttribs(array('class' => 'form-control'))
            ;
        
        $upload = new Zend_Form_Element_Button('upload');
        $upload->setAttrib('id', 'upload')
            ->setAttrib('type', 'button')
            ->setLabel('Save')
            ->setAttrib('class', 'btn btn-success')
            ->setOrder(101);
        
        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
            ->setAttrib('type', 'submit')
            ->setLabel('Save')
            ->setAttrib('class', 'btn btn-success')
            ->setOrder(101);

        $this->addElements(
            array(
                $id,
                $product_name,
                $product_type,
                $product_category,
                $picture,
                $barcode,
                $reorder_point,
                $reorder_quantity,
                $storage_location,
                $last_vendor,
                $length,
                $width,
                $height,
                $weight,
                //$browse,
                $standard_uom,
                $sales_uom,
                $purchasing_uom,
                $remarks,
                 $submit
            )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
    public function isValid($data)
    {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if($vd['product_name'] != '') {
            $name = Application_Model_ProductPeer::validateProduct($vd['id'], $vd['product_name']);
            if($name > 0) {
                $this->product_name->addError('Product Already Present In our record. Please Enter a different Product.');
                $valid = False;
            }
        }
//        if($vd['browse'] != '') {
//            $imageFileType = $vd['browse']['name'];
//            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
//                $this->browse->addError('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
//            $valid = FALSE;
//        }
//        }
        return $valid;
    

    }
}
?>
