<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Timesheet extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
        
        $id = new Zend_Form_Element_Hidden('id');

        $empData = Application_Model_EmployeePeer::fetchAllEmployee();
        $empArr = array();
        $empArr[''] = 'Please Select';
        foreach ($empData as $key => $val) {
            $empArr[$val['id']] = $val['name'];
        }

        $empName = new Zend_Form_Element_Select('emp_name');
        $empName->setLabel('Employee Name')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please select employee name'))
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($empArr);
        
        $timesheet_date = new Zend_Form_Element_Text('timesheet_date');
        $timesheet_date->setLabel('Timesheet Date')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter date'))
                ->setAttribs(array('class' => 'form-control', 'readonly' => 'readonly'))
                ->setAttribs(array('placeholder' => 'Please Add Date'));
        
        $jobData = Application_Model_JobcardPeer::fetchAllJobCard();
        $jobcardArr = array();
        $jobcardArr[''] = 'Please Select';
        foreach ($jobData as $key => $val) {
            $jobcardArr[$val['id']] = $val['work_order_no'];
        }

        $jobcard = new Zend_Form_Element_Select('job_card');
        $jobcard->setLabel('Jobcard No')
                ->setRequired(TRUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Please select job card no'))
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($jobcardArr);
        
        $hourly_rate = new Zend_Form_Element_Text('hourly_rate');
        $hourly_rate->setLabel('Hourly rate')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                  )
                )
         ->setAttribs(array('placeholder' => '0.00'));
        
        $ord = new Zend_Form_Element_Text('ord');
        $ord->setLabel('Ord')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Timesheet.CalcTotal()'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                  )
                )
         ->setAttribs(array('placeholder' => '0.00'));
        
        $ord_15 = new Zend_Form_Element_Text('ord_15');
        $ord_15->setLabel('1.5')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Timesheet.CalcTotal()'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                  )
                )
         ->setAttribs(array('placeholder' => '0.00'));
        
        $ord_20 = new Zend_Form_Element_Text('ord_20');
        $ord_20->setLabel('2.0')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Timesheet.CalcTotal()'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                  )
                )
         ->setAttribs(array('placeholder' => '0.00'));

        $total = new Zend_Form_Element_Text('total');
        $total->setLabel('Total')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control', 'readonly' => 'readonly'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                  )
                )
         ->setAttribs(array('placeholder' => '0.00'));
        
        $rate_type = new Zend_Form_Element_Select('rate_type');
        $rateData = array(
            'Onsite Day Rate' => 'Onsite Day Rate',
            'Onsite Night Rate' => 'Onsite Night Rate'
        );
        $rate_type->setLabel('Rate Type')
                ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setMultiOptions($rateData)
        ;
        

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $id,
            $empName,
            $hourly_rate,
            $jobcard,
            $timesheet_date,
            $ord,
            $ord_15,
            $ord_20,
            $total,
            $rate_type,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        
//        if ($vd['emp_name'] && $vd['timesheet_date']) {
//            $validateRecord = Application_Model_TimesheetPeer::checkRecordExist($vd['id'],$vd['emp_name'],$vd['timesheet_date']);
//            if ($validateRecord > 0) {
//                $this->emp_name->addError('Timesheet with same date is already present in our record.');
//                $valid = FALSE;
//            }
//        }
        return $valid;
    }

}
