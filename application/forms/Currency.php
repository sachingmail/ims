<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Currency extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('currency_name');
        $name->setLabel('Currency Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Currency Name'))
                ->setRequired(TRUE)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'Only Alphabets are allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $code = new Zend_Form_Element_Text('currency_code');
        $code->setLabel('Currency Code')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Currency Code'))
                ->setRequired(TRUE)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'Only Alphabets are allowed'))
                ->addValidator('StringLength', true, array('max' => 3, 'messages' => 'Max length is 3'))
//                ->addValidator('regex',false,array('pattern'=>'/[A-Z]{2}/','messages' =>'Only 2 Capital Alphabets'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $code,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['currency_name']) {
            $validateRecord = Application_Model_CurrencyPeer::validateCurrency($vd['id'], $vd['currency_name']);
            if ($validateRecord > 0) {
                $this->currency_name->addError('Currency already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        if ($vd['currency_code']) {
            $validateCurrencyCode = Application_Model_CurrencyPeer::validateCurrencyCode($vd['id'], $vd['currency_code']);
            if ($validateCurrencyCode > 0) {
                $this->currency_code->addError('Currency Code already present in our record, Please enter diffirent Code.');
                $valid = FALSE;
            }
        }
        return $valid;
    }

}
