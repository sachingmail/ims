<?php

/**
 * Application_Form_Users_Add
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_User_Add
 */
class Application_Form_TaxingScheme extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
//        $this->setAction('/taxing-scheme/index');

        $id = new Zend_Form_Element_Hidden('id');
        $id->setAttrib('id', 'id')
            ->setOrder(102);
        $schemename = new Zend_Form_Element_Text('tax_scheme_name');
        $schemename->setLabel('Taxing Scheme Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Tax Scheme Name'))
                //->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
                ->addValidator('StringLength', true, array('max' => 50, 'messages' => 'Max length is 50'))
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter name'));

        $taxname = new Zend_Form_Element_Text('tax_name');
        $taxname->setLabel('Tax Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Tax Name'))
                //->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter name'));

        $taxrate = new Zend_Form_Element_Text('tax_rate');
        $taxrate->setLabel('Tax Rate')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Tax Rate'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->addValidator('StringLength', true, array('max' => 6, 'messages' => 'Length Could be Maximum Upto 6 places including decimal'))
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                    ;

        $taxonshipping = new Zend_Form_Element_Checkbox('tax_on_shipping');
        $taxonshipping->setLabel('Tax On Shipping')
                ->setAttribs(array('id' => 'tax_on_shipping'));

        $showSecondaryTaxName = new Zend_Form_Element_Checkbox('show_secondary_tax_name');
        $showSecondaryTaxName->setLabel('Show Secondary Tax Rates')
                ->setAttribs(array('id' => 'show_secondary_tax_name'));

        $secondaryTaxName = new Zend_Form_Element_Text('secondary_tax_name');
        $secondaryTaxName->setLabel('Secondary Tax Name')
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters and Numbers Allowed'))
                ->addValidator('StringLength', true, array('max' => 20, 'messages' => 'Max length is 20'))
                ->setAttribs(array('class' => 'form-control', 'id' => 'secondary_tax_name'))
                ->addFilter('StringTrim');

        $secondaryTaxRate = new Zend_Form_Element_Text('secondary_tax_rate');
        $secondaryTaxRate->setLabel('Secondary Tax Rate')
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->addValidator('StringLength', true, array('max' => 6, 'messages' => 'Length Could be Maximum Upto 6 places including decimal'))
                ->setAttribs(array('class' => 'form-control', 'id' => 'secondary_tax_rate'))
                ->addFilter('StringTrim');
        
        $secondarytaxonshipping= new Zend_Form_Element_Checkbox('secondary_tax_on_shipping');
        $secondarytaxonshipping->setLabel('Secondary Tax On Shipping')
                ->setAttribs(array('id' => 'secondary_tax_on_shipping'));

        $compoundSecondary= new Zend_Form_Element_Checkbox('compound_secondary');
        $compoundSecondary->setLabel('Compound Secondary')
                ->setAttribs(array('id' => 'compound_secondary'));
        
        
        
        $subTotal = new Zend_Form_Element_Text('sub_total');
        $subTotal->setLabel('Sub Total')
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control','id'=>'sub_total','readonly'=> 'true'))
                ->setValue('100.00');
        
        $freight = new Zend_Form_Element_Text('freight');
        $freight->setLabel('Freight')
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control','id'=>'freight','readonly'=> 'true'))
                ->setValue('20.00');
        
        $Tax1 = new Zend_Form_Element_Text('tax_1');
        $Tax1->setLabel('Tax 1')
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control','id'=>'tax_1','readonly'=> 'true'));
        
        $Tax2 = new Zend_Form_Element_Text('tax_2');
        $Tax2->setLabel('Tax 2')
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control','id'=>'tax_2','readonly'=> 'true'));

        
        $Total = new Zend_Form_Element_Text('total');
        $Total->setLabel('Total')
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control','id'=>'total','readonly'=>'true'))
                ;        
        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(
                array(
                    $id,
                    $schemename,
                    $taxname,
                    $taxrate,
                    $taxonshipping,  
                    $showSecondaryTaxName,
                    $secondaryTaxName,
                    $secondaryTaxRate,
                    $secondarytaxonshipping,
                    $compoundSecondary,
                    $subTotal,
                    $freight,
                    $Tax1,
                    $Tax2,
                    $Total,
                    $submit
                )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
//    public function isValid($data) {
//        $valid = parent::isValid($data);
//        $vd = $this->getValues();
//
//        if ($vd['upassword'] != $vd['cpassword']) {
//            $this->upassword->addError('Password and confirm password should be same');
//            $valid = FALSE;
//        }
//        return $valid;
//    }

}

?>
