<?php

/**
 * company Add form
 *
 * @package company
 */
class Application_Form_Signin extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {

        parent::init();
        $this->setMethod('POST');
        $this->setAction('/login');
        $usernameInput = new Zend_Form_Element_Text('loginname');
        $usernameInput->setRequired(true)
                ->setLabel('Username')
                ->addValidator('NotEmpty', true, array('messages'=>'Please enter valid Username'))
//  ->addDecorator('Label', array('class' => 'sr-only'))
                ->addFilters(array('StringTrim', 'StripTags'))
                ->setAttribs(array('class' => 'form-control'));
//  ->getDecorator('Errors');

        $passwordInput = new Zend_Form_Element_Password('loginpassword');
        $passwordInput->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages'=>'Please enter valid Password'))
                ->setLabel('Password')
//  ->addDecorator('Label', array('class' => 'control-label'))
                ->addFilters(array('StringTrim', 'StripTags'))
                ->setAttribs(array('class' => 'form-control'));
//  ->getDecorator('Errors');

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Sign In')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $usernameInput,
            $passwordInput,
            $submit
        ));
    }

    /**
     * override base class method to provide custom validation on search form
     * @param array $data posted form data
     * @access public
     */
    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
?>