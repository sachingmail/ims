<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_SalesPurchasePayment extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
        
        $paymentMethods = Application_Model_PaymentMethodPeer::fetchAllpayments();
        $paymentMethodsData = array();
        $paymentMethodsData[''] = 'Please Select';
        foreach ($paymentMethods as $key => $val) {
                $paymentMethodsData[$val['id']] = $val['name'];
        }
        
        $payment_method= new Zend_Form_Element_Select('payment_method_id');
        $payment_method->setLabel('Payment Method')
             ->setAttribs(array('class' => 'form-control custom-select','id'=>'payment_method_id'))
            ->addFilter('StringTrim')
            ->setMultiOptions($paymentMethodsData);
             ;
        $payment_ref = new Zend_Form_Element_Text('payment_ref');
        $payment_ref->setLabel('Ref #')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'payment_ref'))
                ;
        $payment_type = new Zend_Form_Element_Text('payment_type');
        $payment_type ->setLabel('payment_type')
                      ->setAttribs(array('class'=>'form-control','id'=>'payment_type','readonly'=>'true'));
        
        $payment_date = new Zend_Form_Element_Text('payment_date');
        $payment_date->setLabel('Payment Date')
                ->setRequired(false)
                ->setValue(date('d/m/Y'))
                ->setAttribs(array('readonly'=>'readonly','class'=>'form-control','id'=>'payment_date'))
                ;
        
        $payment_remarks = new Zend_Form_Element_Text('payment_remarks');
        $payment_remarks->setLabel('Remarks')
                ->setRequired(FALSE)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'payment_remarks'))
            ;
        $payment_amount = new Zend_Form_Element_Text('payment_amount');
        $payment_amount->setLabel('Amount')
            ->setRequired(false)
            ->addFilter('StringTrim')
            ->setAttribs(array('class' => 'form-control','id'=>'payment_amount'))
                ;
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttribs(array('class'=> 'btn btn-success','readonly'=>'true'))
                ->setOrder(101);

        $this->addElements(array(
            $payment_method,
            $payment_ref,
            $payment_type,
            $payment_date,
            $payment_remarks,
            $payment_amount,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();


        return $valid;
    }

}
