<?php

/**
 * Application_Form_Vendor
 * 
 * @category Form
 * @package  IMS
 * @author   Amit Kumar <amitk@damcogroup.com>
 * @link     Application_Form_Vendor
 */
class Application_Form_Vendor extends Application_Form_BaseForm {

    /**
     * adds and intializes form elements
     * 
     * @access public
     * @return void
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');
        $this->setAction('/vendor/add');
        $id = new Zend_Form_Element_Hidden('id');

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Vendor Name'))
                ->setRequired(true)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'titles' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 100, 'messages' => 'Max length is 100'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter name'));

        $balance = new Zend_Form_Element_Text('balance');
        $balance->setLabel('Balance')
                ->setRequired(FALSE)
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control', 'readonly' => 'readonly'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->setAttribs(array('placeholder' => '0.00'));

        $credit = new Zend_Form_Element_Text('credit');
        $credit->setLabel('Credit')
                ->setRequired(FALSE)
                ->setAttribs(array('class' => 'form-control', 'readonly' => 'readonly'))
                ->addValidator(
                        'regex', false, array(
                    'pattern' => '/^\d+\.?\d{0,2}$/', // Numeric up to 2 decimal places
                    'messages' => array(
                        'regexNotMatch' => "This must be a numerical value up to two decimal places"
                    )
                        )
                )
                ->setAttribs(array('placeholder' => '0.00'))
        ;

        $contact_name = new Zend_Form_Element_Text('contact_name');
        $contact_name->setLabel('Name')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'No Special Characters Allowed'))
                ->addValidator('StringLength', true, array('max' => 30, 'messages' => 'Max length is 30'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter Name'));

        $address = new Zend_Form_Element_Textarea('address');
        $address->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                // ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 20'))
                ->setAttribs(array('class' => 'form-control'))
        ;

        $address1 = new Zend_Form_Element_Textarea('address1');
        $address1->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;
        
        $address2 = new Zend_Form_Element_Textarea('address2');
        $address2->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator('StringLength', true, array('max' => 150, 'messages' => 'Max length is 150'))
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control dnone'))
        ;

        $address_type = new Zend_Form_Element_Select('address_type');
        $typeData = array(
            'Bussiness Address' => 'Bussiness Address',
            'Billing Address' => 'Billing Address',
            'Shipping Address' => 'Shipping Address'
        );
        $address_type->setRequired(true)
                ->setAttribs(array('class' => 'form-control','onchange'=>'Customers.ChangeAddress()'))
                ->addFilter('StringTrim')
                ->setMultiOptions($typeData)
        ;


        $phone = new Zend_Form_Element_Text('phoneno');
        $phone->setLabel('Phone No')
                ->setRequired(false)
                ->addValidator('regex', false, array('pattern' => "/^\d+$/", 'messages' => 'Only Numeric Field Allowed'))
                ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length is 15'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter phone no'));


        $fax = new Zend_Form_Element_Text('fax');
        $fax->setLabel('Fax')
                ->setRequired(false)
                ->addValidator('regex', false, array('pattern' => "/^\d+$/", 'messages' => 'Only Numeric Field Allowed'))
                ->addValidator('StringLength', true, array('max' => 15, 'messages' => 'Max length is 15'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter Fax'));



        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email')
                ->setRequired(false)
                ->addValidator('EmailAddress',false,array('messages'=>'Email Address Should be in format like xyz@gmail.com'))
                ->addFilter('StringTrim')
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter email'));


        $website = new Zend_Form_Element_Text('website');
        $website->setLabel('Website')
                ->setRequired(false)
                ->addFilter('StringTrim')
                //->addValidator('regex', false, array('pattern' => "/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/", 'messages' => 'Format Should be http://www.google.com'))
                ->setAttribs(array('class' => 'form-control'))
                ->setAttribs(array('placeholder' => 'Please enter website'));

        $terms = Application_Model_PaymentTermsPeer::fetchAllPaymentsTerms();
        $termsData = array();
        $termsData[''] = 'Please Select';
        foreach ($terms as $key => $val) {
            $termsData[$val['id']] = $val['name'];
        }

        $payment_terms = new Zend_Form_Element_Select('payment_terms');
        $payment_terms->setLabel('Payment Terms')
                // ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($termsData);

        $taxingScheme = Application_Model_TaxingSchemePeer::fetchAllTaxingSchemes();
        $taxingSchemeData = array();
        $taxingSchemeData[''] = 'Please Select';
        foreach ($taxingScheme as $key => $val) {
            $taxingSchemeData[$val['id']] = $val['tax_scheme_name'];
        }

        $taxing_scheme = new Zend_Form_Element_Select('taxing_scheme');
        $taxing_scheme->setLabel('Taxing Scheme')
                //    ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($taxingSchemeData);

        $carrierRaw = Application_Model_CarrierPeer::fetchAllCarriers();
        $carrierData = array();
        $carrierData[''] = 'Please Select';
        foreach ($carrierRaw as $key => $val) {
            $carrierData[$val['id']] = $val['carrier_name'];
        }
        $carrier = new Zend_Form_Element_Select('carrier');
        $carrier->setLabel('Carrier')
                //  ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($carrierData);
        $currencyRawData = Application_Model_CurrencyPeer::fetchAllCurrency();
        $currencyData = array();
        $currencyData[''] = 'Please Select';
        foreach ($currencyRawData as $key => $val) {
            $currencyData[$val['id']] = $val['currency_name'];
        }

        $currency = new Zend_Form_Element_Select('currency');
        $currency->setLabel('Currency')
                // ->setRequired(true)
                ->setAttribs(array('class' => 'form-control'))
                ->addFilter('StringTrim')
                ->setAttribs(array('placeholder' => 'Please select'))
                ->setMultiOptions($currencyData);
        ;

        $remarks = new Zend_Form_Element_Textarea('remarks');
        $remarks->setLabel('Remarks')
                ->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttrib('cols', '40')
                ->setAttrib('rows', '4')
                ->setAttribs(array('class' => 'form-control'))
        ;

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(
                array(
                    $id,
                    $name,
                    $balance,
                    $credit,
                    $address_type,
                    $address,
                    $address1,
                    $address2,
                    $contact_name,
                    $phone,
                    $fax,
                    $email,
                    $website,
                    $payment_terms,
                    $taxing_scheme,
                    $carrier,
                    $currency,
                    $remarks,
                    $submit
                )
        );
    }

    /**
     * override base class method to provide custom validation on search form
     * 
     * @param array $data posted form data
     * 
     * @access public
     * @return bool returns form valid status
     */
    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['phoneno']) {
            $validatePhone = Application_Model_VendorPeer::validatePhone($vd['id'], $vd['phoneno']);
            if ($validatePhone > 0) {
                $this->phoneno->addError('Phone Number already present in our record, Please enter diffirent Number.');
                $valid = FALSE;
            }
        }
        if ($vd['name']) {
            $validateName = Application_Model_VendorPeer::validateName($vd['id'], $vd['name']);
            if ($validateName > 0) {
                $this->name->addError('Name already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        if ($vd['email']) {
            $validateEmail = Application_Model_VendorPeer::validateEmail($vd['id'], $vd['email']);
            if ($validateEmail > 0) {
                $this->email->addError('Email already present in our record, Please enter diffirent Email.');
                $valid = FALSE;
            }
        }
        if($vd['website']) { 
                $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
                $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
                $regex .= "([a-z0-9-.]*)\.([a-z]{2,4})"; // Host or IP
                $regex .= "(\:[0-9]{2,5})?"; // Port
                $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
                $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
                $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
               if(!preg_match("/^$regex$/",$vd['website'],$m)) { 
                $this->website->addError('Not a valid Website. Please enter a Valid Website Name');
                $valid = FALSE;
                }
        }
        return $valid;
    }

}

?>
