<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Form_Country extends Zend_Form {

    /**
     * adds and intializes form elements
     * @access public
     */
    public function init() {
        parent::init();
        $this->setMethod('POST');


        $name = new Zend_Form_Element_Text('country_name');
        $name->setLabel('Country Name')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Country Name'))
                ->setRequired(TRUE)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'Only Alphabets are allowed'))
                ->addValidator('StringLength', true, array('max' => 30, 'messages' => 'Max length is 30'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $code = new Zend_Form_Element_Text('country_code');
        $code->setLabel('Country Code')
                ->addValidator('NotEmpty', true, array('messages' => 'Please enter Country Country Code'))
                ->setRequired(TRUE)
                ->addValidator('Alpha', true, array('allowWhiteSpace' => 'true', 'messages' => 'Only Alphabets are allowed'))
                ->addValidator('StringLength', true, array('max' => 3, 'messages' => 'Max length is 3'))
//                ->addValidator('regex',false,array('pattern'=>'/[A-Z]{2}/','messages' =>'Only 2 Capital Alphabets'))
                ->setAttrib('class', 'form-control')
                ->addFilter('StringTrim')
        ;
        $id = new Zend_Form_Element_Hidden('id');
        $id->setOrder(102);

        $submit = new Zend_Form_Element_Button('submitbtn');
        $submit->setAttrib('id', 'submit')
                ->setAttrib('type', 'submit')
                ->setLabel('Save')
                ->setAttrib('class', 'btn btn-success')
                ->setOrder(101);

        $this->addElements(array(
            $name,
            $code,
            $id,
            $submit
        ));
    }

    public function isValid($data) {
        $valid = parent::isValid($data);
        $vd = $this->getValues();
        if ($vd['country_name']) {
            $validateRecord = Application_Model_CountryPeer::validateCountry($vd['id'], $vd['country_name']);
            if ($validateRecord > 0) {
                $this->country_name->addError('Country already present in our record, Please enter diffirent name.');
                $valid = FALSE;
            }
        }
        return $valid;
    }

}
