<?php
/**
 * @author Amit Kumar <amitk@damcogroup.com> * 
 * @package IMS *
 */

class Application_View_Helper_General extends Zend_View_Helper_Abstract
{   
    public function general(){
        return $this;
    }
    
    public function ValidateNullDate($date) {
        if($date=='0000-00-00' || $date=='00/00/0000' || $date=='01/01/1970' || $date=='1970-01-01') {
           $retDate ='';
        } else {
            $retDate = $date;
        }
        return $retDate;
    }
}
