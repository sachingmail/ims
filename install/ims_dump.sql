/*
SQLyog Community v8.3 
MySQL - 5.5.20-log : Database - ims_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ims_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `company_details` */

DROP TABLE IF EXISTS `company_details`;

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `misc_info` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp866;

/*Data for the table `company_details` */

/*Table structure for table `inventory_work_order` */

DROP TABLE IF EXISTS `inventory_work_order`;

CREATE TABLE `inventory_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assembled_by` varchar(255) DEFAULT NULL,
  `inventory_location_id` int(11) NOT NULL,
  `other_cost` decimal(20,2) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `finshed_date` datetime DEFAULT NULL,
  `status` enum('open','completed') DEFAULT NULL,
  `inventory_product_id` int(11) NOT NULL,
  `item` varchar(255) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `component_structure` varchar(255) DEFAULT NULL,
  `remarks` text,
  `pick_item` varchar(255) DEFAULT NULL,
  `pick_quantity` int(11) NOT NULL,
  `pick_location_id` int(11) NOT NULL,
  `pick_remarks` varchar(255) DEFAULT NULL,
  `put_away_item` varchar(255) DEFAULT NULL,
  `put_away_quantity` int(11) DEFAULT NULL,
  `put_away_location_id` int(11) DEFAULT NULL,
  `put_away_remarks` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inventory_work_order` */

/*Table structure for table `lookup` */

DROP TABLE IF EXISTS `lookup`;

CREATE TABLE `lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `meaning` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `lookup` */

insert  into `lookup`(`id`,`type`,`code`,`meaning`) values (1,'ACTIVE_STATUS','1','Active'),(2,'ACTIVE_STATUS','0','Inactive');

/*Table structure for table `mas_address_type` */

DROP TABLE IF EXISTS `mas_address_type`;

CREATE TABLE `mas_address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mas_address_type` */

/*Table structure for table `mas_card_type` */

DROP TABLE IF EXISTS `mas_card_type`;

CREATE TABLE `mas_card_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_type` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mas_card_type` */

insert  into `mas_card_type`(`id`,`card_type`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Master Card','2015-02-16 04:09:45',1,'2015-02-04 05:34:11',1),(2,'Debit Card','2015-02-16 04:10:02',1,'2015-02-16 04:10:02',1);

/*Table structure for table `mas_carriers` */

DROP TABLE IF EXISTS `mas_carriers`;

CREATE TABLE `mas_carriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_name` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mas_carriers` */

insert  into `mas_carriers`(`id`,`carrier_name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Blue Dart','2015-02-11 04:14:32',1,'2015-02-11 04:14:15',1);

/*Table structure for table `mas_cities` */

DROP TABLE IF EXISTS `mas_cities`;

CREATE TABLE `mas_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `mas_cities` */

insert  into `mas_cities`(`id`,`city_name`,`state_id`,`country_id`,`updated`,`updated_by`,`created`,`created_by`) values (4,'Faridabad',5,105,'2015-02-16 05:49:32',1,'2015-02-16 05:49:32',1),(26,'XYZ',1,105,'2015-02-26 05:00:28',1,'2015-02-26 05:00:28',1);

/*Table structure for table `mas_countries` */

DROP TABLE IF EXISTS `mas_countries`;

CREATE TABLE `mas_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;

/*Data for the table `mas_countries` */

insert  into `mas_countries`(`id`,`country_name`,`country_code`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Andorra','AD','2015-02-04 05:42:24',1,NULL,NULL),(2,'United Arab Emirates','AE',NULL,NULL,NULL,NULL),(3,'Afghanistan','AF',NULL,NULL,NULL,NULL),(4,'Antigua and Barbuda','AG',NULL,NULL,NULL,NULL),(5,'Anguilla','AI',NULL,NULL,NULL,NULL),(6,'Albania','AL',NULL,NULL,NULL,NULL),(7,'Armenia','AM',NULL,NULL,NULL,NULL),(8,'Angola','AO',NULL,NULL,NULL,NULL),(9,'Antarctica','AQ',NULL,NULL,NULL,NULL),(10,'Argentina','AR',NULL,NULL,NULL,NULL),(11,'American Samoa','AS',NULL,NULL,NULL,NULL),(12,'Austria','AT',NULL,NULL,NULL,NULL),(13,'Australia','AU',NULL,NULL,NULL,NULL),(14,'Aruba','AW',NULL,NULL,NULL,NULL),(15,'Åand','AX',NULL,NULL,NULL,NULL),(16,'Azerbaijan','AZ',NULL,NULL,NULL,NULL),(17,'Bosnia and Herzegovina','BA',NULL,NULL,NULL,NULL),(18,'Barbados','BB',NULL,NULL,NULL,NULL),(19,'Bangladesh','BD',NULL,NULL,NULL,NULL),(20,'Belgium','BE',NULL,NULL,NULL,NULL),(21,'Burkina Faso','BF',NULL,NULL,NULL,NULL),(22,'Bulgaria','BG',NULL,NULL,NULL,NULL),(23,'Bahrain','BH',NULL,NULL,NULL,NULL),(24,'Burundi','BI',NULL,NULL,NULL,NULL),(25,'Benin','BJ',NULL,NULL,NULL,NULL),(26,'Saint Barthémy','BL',NULL,NULL,NULL,NULL),(27,'Bermuda','BM',NULL,NULL,NULL,NULL),(28,'Brunei','BN',NULL,NULL,NULL,NULL),(29,'Bolivia','BO',NULL,NULL,NULL,NULL),(30,'Bonaire','BQ',NULL,NULL,NULL,NULL),(31,'Brazil','BR',NULL,NULL,NULL,NULL),(32,'Bahamas','BS',NULL,NULL,NULL,NULL),(33,'Bhutan','BT',NULL,NULL,NULL,NULL),(34,'Bouvet Island','BV',NULL,NULL,NULL,NULL),(35,'Botswana','BW',NULL,NULL,NULL,NULL),(36,'Belarus','BY',NULL,NULL,NULL,NULL),(37,'Belize','BZ',NULL,NULL,NULL,NULL),(38,'Canada','CA',NULL,NULL,NULL,NULL),(39,'Cocos [Keeling] Islands','CC',NULL,NULL,NULL,NULL),(40,'Democratic Republic of the Congo','CD',NULL,NULL,NULL,NULL),(41,'Central African Republic','CF',NULL,NULL,NULL,NULL),(42,'Republic of the Congo','CG',NULL,NULL,NULL,NULL),(43,'Switzerland','CH',NULL,NULL,NULL,NULL),(44,'Ivory Coast','CI',NULL,NULL,NULL,NULL),(45,'Cook Islands','CK',NULL,NULL,NULL,NULL),(46,'Chile','CL',NULL,NULL,NULL,NULL),(47,'Cameroon','CM',NULL,NULL,NULL,NULL),(48,'China','CN',NULL,NULL,NULL,NULL),(49,'Colombia','CO',NULL,NULL,NULL,NULL),(50,'Costa Rica','CR',NULL,NULL,NULL,NULL),(51,'Cuba','CU',NULL,NULL,NULL,NULL),(52,'Cape Verde','CV',NULL,NULL,NULL,NULL),(53,'Curacao','CW',NULL,NULL,NULL,NULL),(54,'Christmas Island','CX',NULL,NULL,NULL,NULL),(55,'Cyprus','CY',NULL,NULL,NULL,NULL),(56,'Czech Republic','CZ',NULL,NULL,NULL,NULL),(57,'Germany','DE',NULL,NULL,NULL,NULL),(58,'Djibouti','DJ',NULL,NULL,NULL,NULL),(59,'Denmark','DK',NULL,NULL,NULL,NULL),(60,'Dominica','DM',NULL,NULL,NULL,NULL),(61,'Dominican Republic','DO',NULL,NULL,NULL,NULL),(62,'Algeria','DZ',NULL,NULL,NULL,NULL),(63,'Ecuador','EC',NULL,NULL,NULL,NULL),(64,'Estonia','EE',NULL,NULL,NULL,NULL),(65,'Egypt','EG',NULL,NULL,NULL,NULL),(66,'Western Sahara','EH',NULL,NULL,NULL,NULL),(67,'Eritrea','ER',NULL,NULL,NULL,NULL),(68,'Spain','ES',NULL,NULL,NULL,NULL),(69,'Ethiopia','ET',NULL,NULL,NULL,NULL),(70,'Finland','FI',NULL,NULL,NULL,NULL),(71,'Fiji','FJ',NULL,NULL,NULL,NULL),(72,'Falkland Islands','FK',NULL,NULL,NULL,NULL),(73,'Micronesia','FM',NULL,NULL,NULL,NULL),(74,'Faroe Islands','FO',NULL,NULL,NULL,NULL),(75,'France','FR',NULL,NULL,NULL,NULL),(76,'Gabon','GA',NULL,NULL,NULL,NULL),(77,'United Kingdom','GB',NULL,NULL,NULL,NULL),(78,'Grenada','GD',NULL,NULL,NULL,NULL),(79,'Georgia','GE',NULL,NULL,NULL,NULL),(80,'French Guiana','GF',NULL,NULL,NULL,NULL),(81,'Guernsey','GG',NULL,NULL,NULL,NULL),(82,'Ghana','GH',NULL,NULL,NULL,NULL),(83,'Gibraltar','GI',NULL,NULL,NULL,NULL),(84,'Greenland','GL',NULL,NULL,NULL,NULL),(85,'Gambia','GM',NULL,NULL,NULL,NULL),(86,'Guinea','GN',NULL,NULL,NULL,NULL),(87,'Guadeloupe','GP',NULL,NULL,NULL,NULL),(88,'Equatorial Guinea','GQ',NULL,NULL,NULL,NULL),(89,'Greece','GR',NULL,NULL,NULL,NULL),(90,'South Georgia and the South Sandwich Islands','GS',NULL,NULL,NULL,NULL),(91,'Guatemala','GT',NULL,NULL,NULL,NULL),(92,'Guam','GU',NULL,NULL,NULL,NULL),(93,'Guinea-Bissau','GW',NULL,NULL,NULL,NULL),(94,'Guyana','GY',NULL,NULL,NULL,NULL),(95,'Hong Kong','HK',NULL,NULL,NULL,NULL),(96,'Heard Island and McDonald Islands','HM',NULL,NULL,NULL,NULL),(97,'Honduras','HN',NULL,NULL,NULL,NULL),(98,'Croatia','HR',NULL,NULL,NULL,NULL),(99,'Haiti','HT',NULL,NULL,NULL,NULL),(100,'Hungary','HU',NULL,NULL,NULL,NULL),(101,'Indonesia','ID',NULL,NULL,NULL,NULL),(102,'Ireland','IE',NULL,NULL,NULL,NULL),(103,'Israel','IL',NULL,NULL,NULL,NULL),(104,'Isle of Man','IM',NULL,NULL,NULL,NULL),(105,'India','IN',NULL,NULL,NULL,NULL),(106,'British Indian Ocean Territory','IO',NULL,NULL,NULL,NULL),(107,'Iraq','IQ',NULL,NULL,NULL,NULL),(108,'Iran','IR',NULL,NULL,NULL,NULL),(109,'Iceland','IS',NULL,NULL,NULL,NULL),(110,'Italy','IT',NULL,NULL,NULL,NULL),(111,'Jersey','JE',NULL,NULL,NULL,NULL),(112,'Jamaica','JM',NULL,NULL,NULL,NULL),(113,'Jordan','JO',NULL,NULL,NULL,NULL),(114,'Japan','JP',NULL,NULL,NULL,NULL),(115,'Kenya','KE',NULL,NULL,NULL,NULL),(116,'Kyrgyzstan','KG',NULL,NULL,NULL,NULL),(117,'Cambodia','KH',NULL,NULL,NULL,NULL),(118,'Kiribati','KI',NULL,NULL,NULL,NULL),(119,'Comoros','KM',NULL,NULL,NULL,NULL),(120,'Saint Kitts and Nevis','KN',NULL,NULL,NULL,NULL),(121,'North Korea','KP',NULL,NULL,NULL,NULL),(122,'South Korea','KR',NULL,NULL,NULL,NULL),(123,'Kuwait','KW',NULL,NULL,NULL,NULL),(124,'Cayman Islands','KY',NULL,NULL,NULL,NULL),(125,'Kazakhstan','KZ',NULL,NULL,NULL,NULL),(126,'Laos','LA',NULL,NULL,NULL,NULL),(127,'Lebanon','LB',NULL,NULL,NULL,NULL),(128,'Saint Lucia','LC',NULL,NULL,NULL,NULL),(129,'Liechtenstein','LI',NULL,NULL,NULL,NULL),(130,'Sri Lanka','LK',NULL,NULL,NULL,NULL),(131,'Liberia','LR',NULL,NULL,NULL,NULL),(132,'Lesotho','LS',NULL,NULL,NULL,NULL),(133,'Lithuania','LT',NULL,NULL,NULL,NULL),(134,'Luxembourg','LU',NULL,NULL,NULL,NULL),(135,'Latvia','LV',NULL,NULL,NULL,NULL),(136,'Libya','LY',NULL,NULL,NULL,NULL),(137,'Morocco','MA',NULL,NULL,NULL,NULL),(138,'Monaco','MC',NULL,NULL,NULL,NULL),(139,'Moldova','MD',NULL,NULL,NULL,NULL),(140,'Montenegro','ME',NULL,NULL,NULL,NULL),(141,'Saint Martin','MF',NULL,NULL,NULL,NULL),(142,'Madagascar','MG',NULL,NULL,NULL,NULL),(143,'Marshall Islands','MH',NULL,NULL,NULL,NULL),(144,'Macedonia','MK',NULL,NULL,NULL,NULL),(145,'Mali','ML',NULL,NULL,NULL,NULL),(146,'Myanmar [Burma]','MM',NULL,NULL,NULL,NULL),(147,'Mongolia','MN',NULL,NULL,NULL,NULL),(148,'Macao','MO',NULL,NULL,NULL,NULL),(149,'Northern Mariana Islands','MP',NULL,NULL,NULL,NULL),(150,'Martinique','MQ',NULL,NULL,NULL,NULL),(151,'Mauritania','MR',NULL,NULL,NULL,NULL),(152,'Montserrat','MS',NULL,NULL,NULL,NULL),(153,'Malta','MT',NULL,NULL,NULL,NULL),(154,'Mauritius','MU',NULL,NULL,NULL,NULL),(155,'Maldives','MV',NULL,NULL,NULL,NULL),(156,'Malawi','MW',NULL,NULL,NULL,NULL),(157,'Mexico','MX',NULL,NULL,NULL,NULL),(158,'Malaysia','MY',NULL,NULL,NULL,NULL),(159,'Mozambique','MZ',NULL,NULL,NULL,NULL),(160,'Namibia','NA',NULL,NULL,NULL,NULL),(161,'New Caledonia','NC',NULL,NULL,NULL,NULL),(162,'Niger','NE',NULL,NULL,NULL,NULL),(163,'Norfolk Island','NF',NULL,NULL,NULL,NULL),(164,'Nigeria','NG',NULL,NULL,NULL,NULL),(165,'Nicaragua','NI',NULL,NULL,NULL,NULL),(166,'Netherlands','NL',NULL,NULL,NULL,NULL),(167,'Norway','NO',NULL,NULL,NULL,NULL),(168,'Nepal','NP',NULL,NULL,NULL,NULL),(169,'Nauru','NR',NULL,NULL,NULL,NULL),(170,'Niue','NU',NULL,NULL,NULL,NULL),(171,'New Zealand','NZ',NULL,NULL,NULL,NULL),(172,'Oman','OM',NULL,NULL,NULL,NULL),(173,'Panama','PA',NULL,NULL,NULL,NULL),(174,'Peru','PE',NULL,NULL,NULL,NULL),(175,'French Polynesia','PF',NULL,NULL,NULL,NULL),(176,'Papua New Guinea','PG',NULL,NULL,NULL,NULL),(177,'Philippines','PH',NULL,NULL,NULL,NULL),(178,'Pakistan','PK',NULL,NULL,NULL,NULL),(179,'Poland','PL',NULL,NULL,NULL,NULL),(180,'Saint Pierre and Miquelon','PM',NULL,NULL,NULL,NULL),(181,'Pitcairn Islands','PN',NULL,NULL,NULL,NULL),(182,'Puerto Rico','PR',NULL,NULL,NULL,NULL),(183,'Palestine','PS',NULL,NULL,NULL,NULL),(184,'Portugal','PT',NULL,NULL,NULL,NULL),(185,'Palau','PW',NULL,NULL,NULL,NULL),(186,'Paraguay','PY',NULL,NULL,NULL,NULL),(187,'Qatar','QA',NULL,NULL,NULL,NULL),(188,'Réion','RE',NULL,NULL,NULL,NULL),(189,'Romania','RO',NULL,NULL,NULL,NULL),(190,'Serbia','RS',NULL,NULL,NULL,NULL),(191,'Russia','RU',NULL,NULL,NULL,NULL),(192,'Rwanda','RW',NULL,NULL,NULL,NULL),(193,'Saudi Arabia','SA',NULL,NULL,NULL,NULL),(194,'Solomon Islands','SB',NULL,NULL,NULL,NULL),(195,'Seychelles','SC',NULL,NULL,NULL,NULL),(196,'Sudan','SD',NULL,NULL,NULL,NULL),(197,'Sweden','SE',NULL,NULL,NULL,NULL),(198,'Singapore','SG',NULL,NULL,NULL,NULL),(199,'Saint Helena','SH',NULL,NULL,NULL,NULL),(200,'Slovenia','SI',NULL,NULL,NULL,NULL),(201,'Svalbard and Jan Mayen','SJ',NULL,NULL,NULL,NULL),(202,'Slovakia','SK',NULL,NULL,NULL,NULL),(203,'Sierra Leone','SL',NULL,NULL,NULL,NULL),(204,'San Marino','SM',NULL,NULL,NULL,NULL),(205,'Senegal','SN',NULL,NULL,NULL,NULL),(206,'Somalia','SO',NULL,NULL,NULL,NULL),(207,'Suriname','SR',NULL,NULL,NULL,NULL),(208,'South Sudan','SS',NULL,NULL,NULL,NULL),(209,'SãToménd Príipe','ST',NULL,NULL,NULL,NULL),(210,'El Salvador','SV',NULL,NULL,NULL,NULL),(211,'Sint Maarten','SX',NULL,NULL,NULL,NULL),(212,'Syria','SY',NULL,NULL,NULL,NULL),(213,'Swaziland','SZ',NULL,NULL,NULL,NULL),(214,'Turks and Caicos Islands','TC',NULL,NULL,NULL,NULL),(215,'Chad','TD',NULL,NULL,NULL,NULL),(216,'French Southern Territories','TF',NULL,NULL,NULL,NULL),(217,'Togo','TG',NULL,NULL,NULL,NULL),(218,'Thailand','TH',NULL,NULL,NULL,NULL),(219,'Tajikistan','TJ',NULL,NULL,NULL,NULL),(220,'Tokelau','TK',NULL,NULL,NULL,NULL),(221,'East Timor','TL',NULL,NULL,NULL,NULL),(222,'Turkmenistan','TM',NULL,NULL,NULL,NULL),(223,'Tunisia','TN',NULL,NULL,NULL,NULL),(224,'Tonga','TO',NULL,NULL,NULL,NULL),(225,'Turkey','TR',NULL,NULL,NULL,NULL),(226,'Trinidad and Tobago','TT',NULL,NULL,NULL,NULL),(227,'Tuvalu','TV',NULL,NULL,NULL,NULL),(228,'Taiwan','TW',NULL,NULL,NULL,NULL),(229,'Tanzania','TZ',NULL,NULL,NULL,NULL),(230,'Ukraine','UA',NULL,NULL,NULL,NULL),(231,'Uganda','UG',NULL,NULL,NULL,NULL),(232,'U.S. Minor Outlying Islands','UM',NULL,NULL,NULL,NULL),(233,'United States','US',NULL,NULL,NULL,NULL),(234,'Uruguay','UY',NULL,NULL,NULL,NULL),(235,'Uzbekistan','UZ',NULL,NULL,NULL,NULL),(236,'Vatican City','VA',NULL,NULL,NULL,NULL),(237,'Saint Vincent and the Grenadines','VC',NULL,NULL,NULL,NULL),(238,'Venezuela','VE',NULL,NULL,NULL,NULL),(239,'British Virgin Islands','VG',NULL,NULL,NULL,NULL),(240,'U.S. Virgin Islands','VI',NULL,NULL,NULL,NULL),(241,'Vietnam','VN',NULL,NULL,NULL,NULL),(242,'Vanuatu','VU',NULL,NULL,NULL,NULL),(243,'Wallis and Futuna','WF',NULL,NULL,NULL,NULL),(244,'Samoa','WS',NULL,NULL,NULL,NULL),(245,'Kosovo','XK',NULL,NULL,NULL,NULL),(246,'Yemen','YE',NULL,NULL,NULL,NULL),(247,'Mayotte','YT',NULL,NULL,NULL,NULL),(248,'South Africa','ZA',NULL,NULL,NULL,NULL),(249,'Zambia','ZM',NULL,NULL,NULL,NULL),(250,'Zimbabwe','ZW',NULL,NULL,NULL,NULL);

/*Table structure for table `mas_currency` */

DROP TABLE IF EXISTS `mas_currency`;

CREATE TABLE `mas_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(64) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;

/*Data for the table `mas_currency` */

insert  into `mas_currency`(`id`,`currency_name`,`currency_code`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Andorran Peseta','ADP',NULL,NULL,NULL,NULL),(2,'United Arab Emirates Dirham','AED',NULL,NULL,NULL,NULL),(3,'Afghanistan Afghani','AFA',NULL,NULL,NULL,NULL),(4,'Albanian Lek','ALL',NULL,NULL,NULL,NULL),(5,'Netherlands Antillian Guilder','ANG',NULL,NULL,NULL,NULL),(6,'Angolan Kwanza','AOK',NULL,NULL,NULL,NULL),(7,'Argentine Peso','ARS',NULL,NULL,NULL,NULL),(9,'Australian Dollar','AUD',NULL,NULL,NULL,NULL),(10,'Aruban Florin','AWG',NULL,NULL,NULL,NULL),(11,'Barbados Dollar','BBD',NULL,NULL,NULL,NULL),(12,'Bangladeshi Taka','BDT',NULL,NULL,NULL,NULL),(14,'Bulgarian Lev','BGN',NULL,NULL,NULL,NULL),(15,'Bahraini Dinar','BHD',NULL,NULL,NULL,NULL),(16,'Burundi Franc','BIF',NULL,NULL,NULL,NULL),(17,'Bermudian Dollar','BMD',NULL,NULL,NULL,NULL),(18,'Brunei Dollar','BND',NULL,NULL,NULL,NULL),(19,'Bolivian Boliviano','BOB',NULL,NULL,NULL,NULL),(20,'Brazilian Real','BRL',NULL,NULL,NULL,NULL),(21,'Bahamian Dollar','BSD',NULL,NULL,NULL,NULL),(22,'Bhutan Ngultrum','BTN',NULL,NULL,NULL,NULL),(23,'Burma Kyat','BUK',NULL,NULL,NULL,NULL),(24,'Botswanian Pula','BWP',NULL,NULL,NULL,NULL),(25,'Belize Dollar','BZD',NULL,NULL,NULL,NULL),(26,'Canadian Dollar','CAD',NULL,NULL,NULL,NULL),(27,'Swiss Franc','CHF',NULL,NULL,NULL,NULL),(28,'Chilean Unidades de Fomento','CLF',NULL,NULL,NULL,NULL),(29,'Chilean Peso','CLP',NULL,NULL,NULL,NULL),(30,'Yuan (Chinese) Renminbi','CNY',NULL,NULL,NULL,NULL),(31,'Colombian Peso','COP',NULL,NULL,NULL,NULL),(32,'Costa Rican Colon','CRC',NULL,NULL,NULL,NULL),(33,'Czech Republic Koruna','CZK',NULL,NULL,NULL,NULL),(34,'Cuban Peso','CUP',NULL,NULL,NULL,NULL),(35,'Cape Verde Escudo','CVE',NULL,NULL,NULL,NULL),(36,'Cyprus Pound','CYP',NULL,NULL,NULL,NULL),(40,'Danish Krone','DKK',NULL,NULL,NULL,NULL),(41,'Dominican Peso','DOP',NULL,NULL,NULL,NULL),(42,'Algerian Dinar','DZD',NULL,NULL,NULL,NULL),(43,'Ecuador Sucre','ECS',NULL,NULL,NULL,NULL),(44,'Egyptian Pound','EGP',NULL,NULL,NULL,NULL),(45,'Estonian Kroon (EEK)','EEK',NULL,NULL,NULL,NULL),(46,'Ethiopian Birr','ETB',NULL,NULL,NULL,NULL),(47,'Euro','EUR',NULL,NULL,NULL,NULL),(49,'Fiji Dollar','FJD',NULL,NULL,NULL,NULL),(50,'Falkland Islands Pound','FKP',NULL,NULL,NULL,NULL),(52,'British Pound','GBP',NULL,NULL,NULL,NULL),(53,'Ghanaian Cedi','GHC',NULL,NULL,NULL,NULL),(54,'Gibraltar Pound','GIP',NULL,NULL,NULL,NULL),(55,'Gambian Dalasi','GMD',NULL,NULL,NULL,NULL),(56,'Guinea Franc','GNF',NULL,NULL,NULL,NULL),(58,'Guatemalan Quetzal','GTQ',NULL,NULL,NULL,NULL),(59,'Guinea-Bissau Peso','GWP',NULL,NULL,NULL,NULL),(60,'Guyanan Dollar','GYD',NULL,NULL,NULL,NULL),(61,'Hong Kong Dollar','HKD',NULL,NULL,NULL,NULL),(62,'Honduran Lempira','HNL',NULL,NULL,NULL,NULL),(63,'Haitian Gourde','HTG',NULL,NULL,NULL,NULL),(64,'Hungarian Forint','HUF',NULL,NULL,NULL,NULL),(65,'Indonesian Rupiah','IDR',NULL,NULL,NULL,NULL),(66,'Irish Punt','IEP',NULL,NULL,NULL,NULL),(67,'Israeli Shekel','ILS',NULL,NULL,NULL,NULL),(68,'Indian Rupee','INR',NULL,NULL,NULL,NULL),(69,'Iraqi Dinar','IQD',NULL,NULL,NULL,NULL),(70,'Iranian Rial','IRR',NULL,NULL,NULL,NULL),(73,'Jamaican Dollar','JMD',NULL,NULL,NULL,NULL),(74,'Jordanian Dinar','JOD',NULL,NULL,NULL,NULL),(75,'Japanese Yen','JPY',NULL,NULL,NULL,NULL),(76,'Kenyan Schilling','KES',NULL,NULL,NULL,NULL),(77,'Kampuchean (Cambodian) Riel','KHR',NULL,NULL,NULL,NULL),(78,'Comoros Franc','KMF',NULL,NULL,NULL,NULL),(79,'North Korean Won','KPW',NULL,NULL,NULL,NULL),(80,'(South) Korean Won','KRW',NULL,NULL,NULL,NULL),(81,'Kuwaiti Dinar','KWD',NULL,NULL,NULL,NULL),(82,'Cayman Islands Dollar','KYD',NULL,NULL,NULL,NULL),(83,'Lao Kip','LAK',NULL,NULL,NULL,NULL),(84,'Lebanese Pound','LBP',NULL,NULL,NULL,NULL),(85,'Sri Lanka Rupee','LKR',NULL,NULL,NULL,NULL),(86,'Liberian Dollar','LRD',NULL,NULL,NULL,NULL),(87,'Lesotho Loti','LSL',NULL,NULL,NULL,NULL),(89,'Libyan Dinar','LYD',NULL,NULL,NULL,NULL),(90,'Moroccan Dirham','MAD',NULL,NULL,NULL,NULL),(91,'Malagasy Franc','MGF',NULL,NULL,NULL,NULL),(92,'Mongolian Tugrik','MNT',NULL,NULL,NULL,NULL),(93,'Macau Pataca','MOP',NULL,NULL,NULL,NULL),(94,'Mauritanian Ouguiya','MRO',NULL,NULL,NULL,NULL),(95,'Maltese Lira','MTL',NULL,NULL,NULL,NULL),(96,'Mauritius Rupee','MUR',NULL,NULL,NULL,NULL),(97,'Maldive Rufiyaa','MVR',NULL,NULL,NULL,NULL),(98,'Malawi Kwacha','MWK',NULL,NULL,NULL,NULL),(99,'Mexican Peso','MXP',NULL,NULL,NULL,NULL),(100,'Malaysian Ringgit','MYR',NULL,NULL,NULL,NULL),(101,'Mozambique Metical','MZM',NULL,NULL,NULL,NULL),(102,'Namibian Dollar','NAD',NULL,NULL,NULL,NULL),(103,'Nigerian Naira','NGN',NULL,NULL,NULL,NULL),(104,'Nicaraguan Cordoba','NIO',NULL,NULL,NULL,NULL),(105,'Norwegian Kroner','NOK',NULL,NULL,NULL,NULL),(106,'Nepalese Rupee','NPR',NULL,NULL,NULL,NULL),(107,'New Zealand Dollar','NZD',NULL,NULL,NULL,NULL),(108,'Omani Rial','OMR',NULL,NULL,NULL,NULL),(109,'Panamanian Balboa','PAB',NULL,NULL,NULL,NULL),(110,'Peruvian Nuevo Sol','PEN',NULL,NULL,NULL,NULL),(111,'Papua New Guinea Kina','PGK',NULL,NULL,NULL,NULL),(112,'Philippine Peso','PHP',NULL,NULL,NULL,NULL),(113,'Pakistan Rupee','PKR',NULL,NULL,NULL,NULL),(114,'Polish Zloty','PLN',NULL,NULL,NULL,NULL),(116,'Paraguay Guarani','PYG',NULL,NULL,NULL,NULL),(117,'Qatari Rial','QAR',NULL,NULL,NULL,NULL),(118,'Romanian Leu','RON',NULL,NULL,NULL,NULL),(119,'Rwanda Franc','RWF',NULL,NULL,NULL,NULL),(120,'Saudi Arabian Riyal','SAR',NULL,NULL,NULL,NULL),(121,'Solomon Islands Dollar','SBD',NULL,NULL,NULL,NULL),(122,'Seychelles Rupee','SCR',NULL,NULL,NULL,NULL),(123,'Sudanese Pound','SDP',NULL,NULL,NULL,NULL),(124,'Swedish Krona','SEK',NULL,NULL,NULL,NULL),(125,'Singapore Dollar','SGD',NULL,NULL,NULL,NULL),(126,'St. Helena Pound','SHP',NULL,NULL,NULL,NULL),(127,'Sierra Leone Leone','SLL',NULL,NULL,NULL,NULL),(128,'Somali Schilling','SOS',NULL,NULL,NULL,NULL),(129,'Suriname Guilder','SRG',NULL,NULL,NULL,NULL),(130,'Sao Tome and Principe Dobra','STD',NULL,NULL,NULL,NULL),(131,'Russian Ruble','RUB',NULL,NULL,NULL,NULL),(132,'El Salvador Colon','SVC',NULL,NULL,NULL,NULL),(133,'Syrian Potmd','SYP',NULL,NULL,NULL,NULL),(134,'Swaziland Lilangeni','SZL',NULL,NULL,NULL,NULL),(135,'Thai Baht','THB',NULL,NULL,NULL,NULL),(136,'Tunisian Dinar','TND',NULL,NULL,NULL,NULL),(137,'Tongan Paanga','TOP',NULL,NULL,NULL,NULL),(138,'East Timor Escudo','TPE',NULL,NULL,NULL,NULL),(139,'Turkish Lira','TRY',NULL,NULL,NULL,NULL),(140,'Trinidad and Tobago Dollar','TTD',NULL,NULL,NULL,NULL),(141,'Taiwan Dollar','TWD',NULL,NULL,NULL,NULL),(142,'Tanzanian Schilling','TZS',NULL,NULL,NULL,NULL),(143,'Uganda Shilling','UGX',NULL,NULL,NULL,NULL),(144,'US Dollar','USD',NULL,NULL,NULL,NULL),(145,'Uruguayan Peso','UYU',NULL,NULL,NULL,NULL),(146,'Venezualan Bolivar','VEF',NULL,NULL,NULL,NULL),(147,'Vietnamese Dong','VND',NULL,NULL,NULL,NULL),(148,'Vanuatu Vatu','VUV',NULL,NULL,NULL,NULL),(149,'Samoan Tala','WST',NULL,NULL,NULL,NULL),(150,'CommunautÃ© FinanciÃ¨re Africaine BEAC, Francs','XAF',NULL,NULL,NULL,NULL),(151,'Silver, Ounces','XAG',NULL,NULL,NULL,NULL),(152,'Gold, Ounces','XAU',NULL,NULL,NULL,NULL),(153,'East Caribbean Dollar','XCD',NULL,NULL,NULL,NULL),(154,'International Monetary Fund (IMF) Special Drawing Rights','XDR',NULL,NULL,NULL,NULL),(155,'CommunautÃ© FinanciÃ¨re Africaine BCEAO - Francs','XOF',NULL,NULL,NULL,NULL),(156,'Palladium Ounces','XPD',NULL,NULL,NULL,NULL),(157,'Comptoirs FranÃ§ais du Pacifique Francs','XPF',NULL,NULL,NULL,NULL),(158,'Platinum, Ounces','XPT',NULL,NULL,NULL,NULL),(159,'Democratic Yemeni Dinar','YDD',NULL,NULL,NULL,NULL),(160,'Yemeni Rial','YER',NULL,NULL,NULL,NULL),(161,'New Yugoslavia Dinar','YUD',NULL,NULL,NULL,NULL),(162,'South African Rand','ZAR',NULL,NULL,NULL,NULL),(163,'Zambian Kwacha','ZMK',NULL,NULL,NULL,NULL),(164,'Zaire Zaire','ZRZ',NULL,NULL,NULL,NULL),(165,'Zimbabwe Dollar','ZWD',NULL,NULL,NULL,NULL),(166,'Slovak Koruna','SKK',NULL,NULL,NULL,NULL),(167,'Armenian Dram','AMD',NULL,NULL,NULL,NULL);

/*Table structure for table `mas_customers` */

DROP TABLE IF EXISTS `mas_customers`;

CREATE TABLE `mas_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `balance` decimal(20,2) DEFAULT '0.00',
  `credit` decimal(20,2) DEFAULT '0.00',
  `address_type` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `contact_fax` varchar(50) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `contact_website` varchar(50) DEFAULT NULL,
  `pricing_currency_id` int(11) DEFAULT NULL,
  `discount` decimal(20,2) DEFAULT NULL,
  `payment_terms` int(11) DEFAULT NULL,
  `taxing_schemes` int(11) DEFAULT NULL,
  `tax_exempt` decimal(20,2) DEFAULT NULL,
  `default_location_id` int(11) DEFAULT NULL,
  `sales_reps` int(11) DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `payment_method` int(11) DEFAULT NULL,
  `card_type` int(11) DEFAULT NULL,
  `card_number` bigint(20) DEFAULT NULL,
  `card_expiry` varchar(50) DEFAULT NULL,
  `card_security_code` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mas_customers` */

insert  into `mas_customers`(`id`,`name`,`balance`,`credit`,`address_type`,`address`,`city_id`,`zip_code`,`contact_name`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`,`pricing_currency_id`,`discount`,`payment_terms`,`taxing_schemes`,`tax_exempt`,`default_location_id`,`sales_reps`,`carrier_id`,`payment_method`,`card_type`,`card_number`,`card_expiry`,`card_security_code`,`is_active`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (2,'Tiwari MIlls','2500.00','3333333.00','Bussiness Address','108 HSIDC\r\nIndustrial Complex sector 86 faridabad',2,NULL,'Amit kumar','97148996066','65852585','amitk@dmacom.com','dasd.com',0,'10.00',1,1,'11.00',0,0,0,0,0,0,'',0,1,'','2015-02-09 08:19:09',1,'2015-02-06 09:04:00',1);

/*Table structure for table `mas_inventory_location` */

DROP TABLE IF EXISTS `mas_inventory_location`;

CREATE TABLE `mas_inventory_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mas_inventory_location` */

insert  into `mas_inventory_location`(`id`,`name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Damco Faridabad','2015-02-12 11:44:31',1,'2015-02-12 11:44:31',1),(2,'Damco Noida','2015-02-18 04:12:55',1,'2015-02-18 04:12:55',1);

/*Table structure for table `mas_payment_method` */

DROP TABLE IF EXISTS `mas_payment_method`;

CREATE TABLE `mas_payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mas_payment_method` */

insert  into `mas_payment_method`(`id`,`name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Cash','2015-02-16 05:43:25',1,'2015-02-16 05:43:25',1);

/*Table structure for table `mas_payment_terms` */

DROP TABLE IF EXISTS `mas_payment_terms`;

CREATE TABLE `mas_payment_terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mas_payment_terms` */

insert  into `mas_payment_terms`(`id`,`name`,`days`,`updated`,`updated_by`,`created`,`created_by`) values (1,'asdasdasdaasd',10,'2015-02-16 05:44:06',1,NULL,NULL);

/*Table structure for table `mas_pricing_currency` */

DROP TABLE IF EXISTS `mas_pricing_currency`;

CREATE TABLE `mas_pricing_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `currency_name` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mas_pricing_currency` */

insert  into `mas_pricing_currency`(`id`,`name`,`currency_name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Normal','Indian Rupee','2015-02-11 04:16:47',1,'2015-02-11 04:16:47',1),(2,'Normal2','US Dollar','2015-02-18 10:05:03',1,'2015-02-18 10:05:03',1),(6,'Vat','Indian Rupee','2015-02-20 07:19:31',1,'2015-02-20 07:19:31',1);

/*Table structure for table `mas_product_category` */

DROP TABLE IF EXISTS `mas_product_category`;

CREATE TABLE `mas_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mas_product_category` */

insert  into `mas_product_category`(`id`,`name`,`parent_id`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Blue Chip 2',0,'2015-02-11 13:42:20',1,'2015-02-11 04:34:11',1),(2,'Class Diagrams',1,'2015-02-11 13:25:15',1,'2015-02-11 04:35:29',1),(3,'Erd',2,'2015-02-16 10:30:00',1,'2015-02-11 04:35:42',1),(4,'Controllers',3,'2015-02-16 10:03:58',1,'2015-02-11 04:36:05',1),(5,'Models',2,'2015-02-16 10:03:26',1,'2015-02-11 04:36:13',1);

/*Table structure for table `mas_product_type` */

DROP TABLE IF EXISTS `mas_product_type`;

CREATE TABLE `mas_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mas_product_type` */

insert  into `mas_product_type`(`id`,`name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Stock Product','2015-02-11 12:42:10',1,'2015-02-11 12:42:10',1),(2,'Serialized Product','2015-02-16 04:08:56',1,'2015-02-16 04:08:56',1);

/*Table structure for table `mas_receiving_addresses` */

DROP TABLE IF EXISTS `mas_receiving_addresses`;

CREATE TABLE `mas_receiving_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_name` varchar(255) NOT NULL,
  `street` varchar(512) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `remarks` text,
  `address_type` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `mas_receiving_addresses` */

insert  into `mas_receiving_addresses`(`id`,`address_name`,`street`,`city_id`,`zip_code`,`remarks`,`address_type`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Damco','HSIDC',4,'121008','asdfdasdaaaaaaaaaaaaaaaaaaaaaaaaa','Residential','2015-02-16 05:51:18',1,'2015-02-16 05:50:13',1),(27,'Ashish','Anand Niketan',26,'11212','','0','2015-02-26 05:00:28',1,'2015-02-26 05:00:28',1);

/*Table structure for table `mas_sales_reps` */

DROP TABLE IF EXISTS `mas_sales_reps`;

CREATE TABLE `mas_sales_reps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mas_sales_reps` */

insert  into `mas_sales_reps`(`id`,`name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Amit','2015-02-16 05:54:50',1,'2015-02-11 04:14:58',1);

/*Table structure for table `mas_states` */

DROP TABLE IF EXISTS `mas_states`;

CREATE TABLE `mas_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mas_states` */

insert  into `mas_states`(`id`,`state_name`,`country_id`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Delhi',105,'2015-02-11 04:05:06',1,'2015-02-11 04:05:06',1),(2,'Perth',13,'2015-02-11 04:05:29',1,'2015-02-11 04:05:29',1),(3,'Adelaide',13,'2015-02-16 04:11:21',1,'2015-02-16 04:11:21',1),(4,'Sydney',13,'2015-02-16 04:11:39',1,'2015-02-16 04:11:39',1),(5,'Haryanaaaaa',105,'2015-02-16 05:33:48',1,'2015-02-16 05:28:19',1);

/*Table structure for table `mas_taxing_schemes` */

DROP TABLE IF EXISTS `mas_taxing_schemes`;

CREATE TABLE `mas_taxing_schemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_scheme_name` varchar(255) DEFAULT NULL,
  `tax_name` varchar(255) DEFAULT NULL,
  `tax_rate` decimal(20,2) DEFAULT '0.00',
  `tax_on_shipping` tinyint(4) DEFAULT NULL,
  `secondary_tax_name` varchar(255) DEFAULT NULL,
  `secondary_tax_rate` decimal(20,2) DEFAULT '0.00',
  `secondary_tax_on_shipping` tinyint(4) DEFAULT NULL,
  `compound_secondary` tinyint(4) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mas_taxing_schemes` */

insert  into `mas_taxing_schemes`(`id`,`tax_scheme_name`,`tax_name`,`tax_rate`,`tax_on_shipping`,`secondary_tax_name`,`secondary_tax_rate`,`secondary_tax_on_shipping`,`compound_secondary`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Tax1','Vat','12.00',1,'service','2.00',0,0,'2015-02-18 05:11:55',1,'2015-02-18 05:11:55',1);

/*Table structure for table `mas_units` */

DROP TABLE IF EXISTS `mas_units`;

CREATE TABLE `mas_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mas_units` */

insert  into `mas_units`(`id`,`unit_name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'pcs','2015-02-12 10:22:04',1,'2015-02-12 10:22:04',1),(2,'cases','2015-02-12 10:22:33',1,'2015-02-12 10:22:33',1),(3,'pa.','2015-02-12 10:22:39',1,'2015-02-12 10:22:39',1),(4,'packs','2015-02-12 10:22:47',1,'2015-02-12 10:22:47',1);

/*Table structure for table `mas_vendors` */

DROP TABLE IF EXISTS `mas_vendors`;

CREATE TABLE `mas_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `balance` decimal(20,2) DEFAULT '0.00',
  `credit` decimal(20,2) DEFAULT '0.00',
  `address` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `address_type` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `contact_fax` varchar(50) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `contact_website` varchar(50) DEFAULT NULL,
  `payment_terms` int(11) DEFAULT NULL,
  `taxing_schemes` int(11) DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mas_vendors` */

insert  into `mas_vendors`(`id`,`name`,`balance`,`credit`,`address`,`city_id`,`zip_code`,`address_type`,`contact_name`,`contact_phone`,`contact_fax`,`contact_email`,`contact_website`,`payment_terms`,`taxing_schemes`,`carrier_id`,`currency_id`,`is_active`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Amit kumar','12000.00','2000.00','sdfsdf\r\nsdf\r\nsd\r\nfsd\r\nf',NULL,NULL,'Bussiness Address','Amit kumar','252525','525252','amitk@damcogroup.com','14525252',1,1,0,0,1,'','2015-02-09 12:45:11',1,'2015-02-09 12:45:11',1),(2,'ankit','0.00','12.00','',NULL,NULL,'Bussiness Address','','','','','',0,0,0,0,1,'','2015-02-12 11:48:53',1,'2015-02-12 11:48:53',1);

/*Table structure for table `mas_vendors_product` */

DROP TABLE IF EXISTS `mas_vendors_product`;

CREATE TABLE `mas_vendors_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `vendor_product_code` varchar(50) DEFAULT NULL,
  `cost` double(20,2) DEFAULT '0.00',
  `sessionid` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mas_vendors_product` */

insert  into `mas_vendors_product`(`id`,`vendor_id`,`product_id`,`vendor_product_code`,`cost`,`sessionid`,`updated`,`updated_by`,`created`,`created_by`) values (1,2,0,'111',22343.00,'909q6e140885m3ta9hpdeskcd2','2015-02-17 06:46:06',1,'2015-02-17 06:46:06',1),(2,2,0,'12334',1111.00,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 11:10:59',1,'2015-02-17 10:32:26',1),(3,NULL,0,'aaa',1223.00,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 11:28:59',1,'2015-02-17 11:28:59',1),(4,0,1,'kkkkk',120.00,'67t91lsre53shu7od3he1urrk4','2015-02-25 11:23:22',1,'2015-02-25 05:16:23',1);

/*Table structure for table `session` */

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `session` */

insert  into `session`(`id`,`modified`,`lifetime`,`data`) values ('1knnka5uk8fhf9i2t9ch146m03',1425041021,1440,'Default|a:1:{s:12:\"authRedirect\";s:16:\"/sales-quote/add\";}'),('5apqjp01ves8mq3d5od876tfr2',1425041022,1440,''),('9mdp21k7dboma3trbekk9quag4',1425041022,1440,'Default|a:1:{s:12:\"authRedirect\";s:29:\"/sales-quote/add?status=added\";}'),('ahmi0arb9ljqroki9i3u7egov4',1425039918,1440,'Default|a:1:{s:12:\"authRedirect\";s:21:\"/sales-quote/add/id/5\";}'),('cfh771rmpjlnt9aqvh4hk4ejv0',1425041022,1440,'Default|a:1:{s:12:\"authRedirect\";s:12:\"/sales-quote\";}'),('on3cqeljdlf385bipbfi7b0qb0',1425041023,1440,''),('s00aon5qu6k947csf1ncbv78t1',1425041021,1440,''),('ttn7955olmo328n5smpuseho35',1425043852,1440,'');

/*Table structure for table `tra_inventory_adjust_stock` */

DROP TABLE IF EXISTS `tra_inventory_adjust_stock`;

CREATE TABLE `tra_inventory_adjust_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_quantity` int(11) NOT NULL,
  `old_quantity` int(11) NOT NULL,
  `difference` int(11) NOT NULL,
  `inventory_date` date DEFAULT NULL,
  `status` enum('Unsaved','Saved','Completed','Pending') DEFAULT NULL,
  `inventory_product_id` int(11) DEFAULT NULL,
  `inventory_location_id` int(11) DEFAULT NULL,
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_adjust_stock` */

/*Table structure for table `tra_inventory_product` */

DROP TABLE IF EXISTS `tra_inventory_product`;

CREATE TABLE `tra_inventory_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `bar_code` varchar(255) DEFAULT NULL,
  `reorder_point` varchar(255) DEFAULT NULL,
  `reorder_quantity` int(11) DEFAULT NULL,
  `storage_location` int(11) DEFAULT NULL,
  `last_vendor_id` int(11) DEFAULT NULL,
  `length` decimal(20,2) DEFAULT '0.00',
  `width` decimal(20,2) DEFAULT '0.00',
  `height` decimal(20,2) DEFAULT '0.00',
  `weight` decimal(20,2) DEFAULT '0.00',
  `standard_uom_id` int(11) DEFAULT NULL,
  `sales_uom_id` int(11) DEFAULT NULL,
  `purchasing_uom_id` int(11) DEFAULT NULL,
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_product` */

insert  into `tra_inventory_product`(`id`,`code`,`product_name`,`product_type_id`,`product_category_id`,`product_image`,`bar_code`,`reorder_point`,`reorder_quantity`,`storage_location`,`last_vendor_id`,`length`,`width`,`height`,`weight`,`standard_uom_id`,`sales_uom_id`,`purchasing_uom_id`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (1,'AA','AAAAAA',1,4,'','##235555453','2',1,1,2,'11.00','11.00','122.00','11.00',1,2,3,'asdasd','2015-02-16 05:57:42',1,NULL,NULL),(3,NULL,'asdasd',1,2,'','','11',11,1,2,'11.00','0.00','0.00','0.00',2,4,2,'asdasdasdasdasdasdasd','2015-02-13 06:00:15',1,'2015-02-12 13:55:12',1),(7,NULL,'Item4',1,2,'','','',0,0,0,'11.00','13.00','12.00','14.00',1,3,1,'asdfdadas','2015-02-23 06:20:15',1,'2015-02-20 10:47:51',1);

/*Table structure for table `tra_inventory_product_bill_of_materials` */

DROP TABLE IF EXISTS `tra_inventory_product_bill_of_materials`;

CREATE TABLE `tra_inventory_product_bill_of_materials` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `raw_product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cost` double(20,2) DEFAULT '0.00',
  `sessionid` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_product_bill_of_materials` */

insert  into `tra_inventory_product_bill_of_materials`(`id`,`product_id`,`raw_product_id`,`quantity`,`cost`,`sessionid`,`updated`,`updated_by`,`created`,`created_by`) values (2,0,3,0,1111.00,'909q6e140885m3ta9hpdeskcd2','2015-02-17 06:45:02',1,'2015-02-17 06:45:02',1),(3,0,NULL,0,0.00,'909q6e140885m3ta9hpdeskcd2','2015-02-17 06:58:13',1,'2015-02-17 06:58:13',1),(4,0,NULL,NULL,NULL,'909q6e140885m3ta9hpdeskcd2','2015-02-17 08:55:05',1,'2015-02-17 08:55:05',1),(5,0,NULL,NULL,NULL,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 09:49:55',1,'2015-02-17 09:49:55',1),(6,0,NULL,NULL,NULL,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 09:51:27',1,'2015-02-17 09:51:27',1),(7,0,NULL,NULL,NULL,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 09:51:50',1,'2015-02-17 09:51:50',1),(8,0,NULL,NULL,NULL,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 09:52:59',1,'2015-02-17 09:52:59',1),(9,0,1,12,111.00,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 10:32:14',1,'2015-02-17 10:32:14',1),(10,0,NULL,6,77.00,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 11:29:53',1,'2015-02-17 11:29:53',1),(11,0,3,121,1111.00,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 11:56:02',1,'2015-02-17 11:56:02',1),(12,0,7,1111,121.00,'67t91lsre53shu7od3he1urrk4','2015-02-25 11:27:26',1,'2015-02-25 11:26:53',1);

/*Table structure for table `tra_inventory_product_cost` */

DROP TABLE IF EXISTS `tra_inventory_product_cost`;

CREATE TABLE `tra_inventory_product_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `price_currency_id` int(11) DEFAULT NULL,
  `price` decimal(20,2) DEFAULT '0.00',
  `markup` decimal(20,2) DEFAULT '0.00',
  `cost` decimal(20,2) DEFAULT '0.00',
  `sessionid` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_product_cost` */

insert  into `tra_inventory_product_cost`(`id`,`product_id`,`price_currency_id`,`price`,`markup`,`cost`,`sessionid`,`updated`,`updated_by`,`created`,`created_by`) values (1,1,NULL,'222.00','0.00','0.00',NULL,NULL,NULL,NULL,NULL),(11,7,1,'121.00','21.00','100.00','0','2015-02-20 10:47:51',1,'2015-02-20 10:47:51',1),(12,7,2,'122.00','22.00','100.00','0','2015-02-20 10:47:51',1,'2015-02-20 10:47:51',1),(13,7,6,'123.00','23.00','100.00','0','2015-02-20 10:47:51',1,'2015-02-20 10:47:51',1),(14,NULL,1,'121.00','21.00','100.00','22','2015-02-23 06:20:15',1,'2015-02-23 06:20:15',1),(15,NULL,2,'122.00','22.00','100.00','22','2015-02-23 06:20:15',1,'2015-02-23 06:20:15',1),(16,NULL,6,'123.00','23.00','100.00','22','2015-02-23 06:20:15',1,'2015-02-23 06:20:15',1);

/*Table structure for table `tra_inventory_product_quantity` */

DROP TABLE IF EXISTS `tra_inventory_product_quantity`;

CREATE TABLE `tra_inventory_product_quantity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `sessionid` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_product_quantity` */

insert  into `tra_inventory_product_quantity`(`id`,`product_id`,`location_id`,`quantity`,`unit_id`,`sessionid`,`updated`,`updated_by`,`created`,`created_by`) values (22,0,1,111,NULL,'t40n3i139c5ci3b5472nbmjdl7','2015-02-17 13:16:15',1,'2015-02-17 13:16:15',1),(23,0,1,123,NULL,'ng6fj1t4amv8ad1irnt8kpjqg6','2015-02-18 04:12:19',1,'2015-02-18 04:12:19',1),(24,0,NULL,111,NULL,'ng6fj1t4amv8ad1irnt8kpjqg6','2015-02-18 04:12:31',1,'2015-02-18 04:12:31',1),(25,0,2,111,NULL,'67t91lsre53shu7od3he1urrk4','2015-02-25 11:58:22',1,'2015-02-25 11:27:39',1);

/*Table structure for table `tra_inventory_product_vendors` */

DROP TABLE IF EXISTS `tra_inventory_product_vendors`;

CREATE TABLE `tra_inventory_product_vendors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `price` decimal(20,2) DEFAULT '0.00',
  `vendor_product_code` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_inventory_product_vendors` */

/*Table structure for table `tra_purchase_order_det` */

DROP TABLE IF EXISTS `tra_purchase_order_det`;

CREATE TABLE `tra_purchase_order_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_status` enum('Not Received','Received') DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `vendor_product_code` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `discount` decimal(20,2) DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT NULL,
  `sessionid` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_purchase_order_det` */

/*Table structure for table `tra_purchase_order_mas` */

DROP TABLE IF EXISTS `tra_purchase_order_mas`;

CREATE TABLE `tra_purchase_order_mas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `po_id` varchar(50) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `order_status` enum('Unfulfilled','Fulfilled','Started') NOT NULL,
  `payment_status` enum('Paid','Unpaid') DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `req_shipping_date` date DEFAULT NULL,
  `terms_id` int(11) DEFAULT NULL,
  `v_order` varchar(255) DEFAULT NULL,
  `ship_to_address` varchar(500) DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `non_vendor_costs` decimal(20,2) DEFAULT '0.00',
  `currency_id` int(11) DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT '0.00',
  `freight` decimal(20,2) DEFAULT '0.00',
  `taxing_scheme_id` int(11) DEFAULT NULL,
  `tax1_name` varchar(50) DEFAULT NULL,
  `tax1_per` decimal(10,2) DEFAULT '0.00',
  `tax1_amount` decimal(20,2) DEFAULT '0.00',
  `tax2_name` varchar(50) DEFAULT NULL,
  `tax2_per` decimal(10,2) DEFAULT '0.00',
  `tax2_amount` decimal(20,2) DEFAULT '0.00',
  `total_amount` decimal(20,2) DEFAULT '0.00',
  `paid_amount` decimal(20,2) DEFAULT '0.00',
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`order_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tra_purchase_order_mas` */

insert  into `tra_purchase_order_mas`(`id`,`po_id`,`po_date`,`order_status`,`payment_status`,`vendor_id`,`location_id`,`req_shipping_date`,`terms_id`,`v_order`,`ship_to_address`,`carrier_id`,`due_date`,`non_vendor_costs`,`currency_id`,`sub_total`,`freight`,`taxing_scheme_id`,`tax1_name`,`tax1_per`,`tax1_amount`,`tax2_name`,`tax2_per`,`tax2_amount`,`total_amount`,`paid_amount`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (1,'PO-001','2015-02-08','Fulfilled','Paid',1,1,NULL,1,NULL,NULL,1,NULL,'0.00',1,'1000.00','0.00',NULL,NULL,'0.00','0.00',NULL,'0.00','0.00','1200.00','1200.00',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tra_purchase_order_payments` */

DROP TABLE IF EXISTS `tra_purchase_order_payments`;

CREATE TABLE `tra_purchase_order_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `payment_ref` varchar(255) DEFAULT NULL,
  `payment_remarks` varchar(500) DEFAULT NULL,
  `amount` decimal(20,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_purchase_order_payments` */

/*Table structure for table `tra_sales_order_det` */

DROP TABLE IF EXISTS `tra_sales_order_det`;

CREATE TABLE `tra_sales_order_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_status` enum('Not Received','Received') DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `discount` decimal(20,2) DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `box_number` int(20) DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `discarded` tinyint(4) DEFAULT NULL,
  `restock_date` date DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_sales_order_det` */

/*Table structure for table `tra_sales_order_mas` */

DROP TABLE IF EXISTS `tra_sales_order_mas`;

CREATE TABLE `tra_sales_order_mas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `sales_rep_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `order_status` enum('Unfulfilled','Fulfilled','Started') DEFAULT NULL,
  `payment_status` enum('Invoiced','Uninvoiced','Paid') DEFAULT NULL,
  `terms_id` int(11) DEFAULT NULL,
  `po_ref` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(500) DEFAULT NULL,
  `non_customer_costs` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `taxing_scheme_id` int(11) DEFAULT NULL,
  `tax1_name` varchar(50) DEFAULT NULL,
  `tax1_per` decimal(10,2) DEFAULT '0.00',
  `tax1_amount` decimal(20,2) DEFAULT '0.00',
  `tax2_name` varchar(50) DEFAULT NULL,
  `tax2_per` decimal(10,2) DEFAULT '0.00',
  `tax2_amount` decimal(20,2) DEFAULT '0.00',
  `pricing_currency_id` int(11) DEFAULT NULL,
  `req_ship_date` date DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT '0.00',
  `freight` decimal(20,2) DEFAULT '0.00',
  `total_amount` decimal(20,2) DEFAULT '0.00',
  `paid_amount` decimal(20,2) DEFAULT '0.00',
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tra_sales_order_mas` */

insert  into `tra_sales_order_mas`(`id`,`customer_id`,`order_id`,`order_date`,`sales_rep_id`,`location_id`,`order_status`,`payment_status`,`terms_id`,`po_ref`,`shipping_address`,`non_customer_costs`,`due_date`,`taxing_scheme_id`,`tax1_name`,`tax1_per`,`tax1_amount`,`tax2_name`,`tax2_per`,`tax2_amount`,`pricing_currency_id`,`req_ship_date`,`sub_total`,`freight`,`total_amount`,`paid_amount`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (1,2,'001','2015-02-08 14:09:19',1,1,'Fulfilled','Paid',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0.00','0.00',NULL,'0.00','0.00',NULL,NULL,'0.00','0.00','1000.00','500.00',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tra_sales_order_payments` */

DROP TABLE IF EXISTS `tra_sales_order_payments`;

CREATE TABLE `tra_sales_order_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `payment_ref` varchar(255) DEFAULT NULL,
  `paymnet_remarks` text,
  `amount` decimal(20,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tra_sales_order_payments` */

/*Table structure for table `tra_sales_quote_order_det` */

DROP TABLE IF EXISTS `tra_sales_quote_order_det`;

CREATE TABLE `tra_sales_quote_order_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_status` enum('Not Received','Received') DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `discount` decimal(20,2) DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT NULL,
  `sessionid` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `tra_sales_quote_order_det` */

insert  into `tra_sales_quote_order_det`(`id`,`order_id`,`item_status`,`product_id`,`quantity`,`unit_price`,`discount`,`sub_total`,`sessionid`,`updated`,`updated_by`,`created`,`created_by`) values (1,1,NULL,1,11,123,'12.00','111.00',NULL,NULL,NULL,NULL,NULL),(28,0,NULL,3,10,11,'12.00','96.80','o5jmrhde6g034r935pjjbhocn7','2015-02-26 07:28:30',1,'2015-02-26 07:28:30',1),(29,0,NULL,3,112,11,'12.00','1084.16','o5jmrhde6g034r935pjjbhocn7','2015-02-26 09:30:10',1,'2015-02-26 09:30:10',1),(46,0,NULL,3,11,12,'11.00','117.48','mmkc59b7qckito1lnudh6qvvc2','2015-02-26 12:12:37',1,'2015-02-26 12:12:37',1),(47,0,NULL,3,112,11,'12.00','1084.16','uh76sokc6eakhardt2eeka6390','2015-02-27 05:19:26',1,'2015-02-27 05:19:26',1),(48,5,NULL,3,11,112,'12.00','1084.16','','2015-02-27 10:09:39',1,'2015-02-27 10:01:59',1),(49,0,NULL,3,12,11,'12.00','116.16','ttn7955olmo328n5smpuseho35','2015-02-27 12:33:50',1,'2015-02-27 12:33:50',1);

/*Table structure for table `tra_sales_quote_order_mas` */

DROP TABLE IF EXISTS `tra_sales_quote_order_mas`;

CREATE TABLE `tra_sales_quote_order_mas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_address` varchar(500) DEFAULT NULL,
  `customer_phone` bigint(20) DEFAULT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `sales_rep_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `order_status` enum('quote','order') DEFAULT NULL,
  `payment_status` enum('Invoiced','Uninvoiced','Paid') DEFAULT NULL,
  `terms_id` int(11) DEFAULT NULL,
  `po_ref` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(500) DEFAULT NULL,
  `non_customer_costs` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `taxing_scheme_id` int(11) DEFAULT NULL,
  `tax1_name` varchar(50) DEFAULT NULL,
  `tax1_per` decimal(10,2) DEFAULT '0.00',
  `tax1_amount` decimal(20,2) DEFAULT '0.00',
  `tax2_name` varchar(50) DEFAULT NULL,
  `tax2_per` decimal(10,2) DEFAULT '0.00',
  `tax2_amount` decimal(20,2) DEFAULT '0.00',
  `pricing_currency_id` int(11) DEFAULT NULL,
  `req_ship_date` date DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT '0.00',
  `freight` decimal(20,2) DEFAULT '0.00',
  `total_amount` decimal(20,2) DEFAULT '0.00',
  `paid_amount` decimal(20,2) DEFAULT '0.00',
  `remarks` text,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tra_sales_quote_order_mas` */

insert  into `tra_sales_quote_order_mas`(`id`,`customer_id`,`customer_address`,`customer_phone`,`order_id`,`order_date`,`sales_rep_id`,`location_id`,`order_status`,`payment_status`,`terms_id`,`po_ref`,`shipping_address`,`non_customer_costs`,`due_date`,`taxing_scheme_id`,`tax1_name`,`tax1_per`,`tax1_amount`,`tax2_name`,`tax2_per`,`tax2_amount`,`pricing_currency_id`,`req_ship_date`,`sub_total`,`freight`,`total_amount`,`paid_amount`,`remarks`,`updated`,`updated_by`,`created`,`created_by`) values (5,2,'108 HSIDC\r\nIndustrial Complex sector 86 faridabad    ',97148996066,'SA-000004','2015-10-02 00:00:00',1,1,'quote',NULL,1,'111','11121',NULL,NULL,1,'Vat','12.00','130.10','2.00','2.00','21.68',1,'1970-01-01','1084.16','0.00','1235.94','0.00','112122','2015-02-27 12:46:14',1,'2015-02-27 10:09:39',1);

/*Table structure for table `user_menu_m` */

DROP TABLE IF EXISTS `user_menu_m`;

CREATE TABLE `user_menu_m` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_type_id` int(11) DEFAULT NULL,
  `screen_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  `home_menu` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `user_menu_m` */

insert  into `user_menu_m`(`id`,`menu_type_id`,`screen_name`,`url`,`Sequence`,`home_menu`) values (1,1,'Manage Card Type','/card-type',1,NULL),(2,1,'Manage Carrier','/carrier',2,NULL),(3,1,'Manage Country','/country',3,NULL),(4,1,'Manage State','/state',4,NULL),(5,1,'Manage City','/city',5,NULL),(6,1,'Manage Currency','/currency',6,NULL),(7,1,'Manage Units','/unit',7,NULL),(8,1,'Manage Inventory Locations','/inventory-location',8,NULL),(9,1,'Manage Payment Methods','/payment-method',9,NULL),(10,1,'Manage Payment Terms','/payment-terms',10,NULL),(11,1,'Manage Pricing Currency','/pricing-currency',11,NULL),(12,1,'Manage Product Category','/product-category',12,NULL),(13,1,'Manage Product Type','/product-type',13,NULL),(14,1,'Manage Receiving Address','/receiving-address',14,NULL),(15,1,'Manage Sales Reps','/sale-reps',15,NULL),(16,1,'Manage Taxing Scheme','/taxing-scheme',16,NULL),(17,2,'Home Page','/',1,NULL),(18,2,'Dashboard','/dashboard',2,NULL),(19,3,'New Product','/product/add',7,1),(20,3,'Product List','/product',6,1),(21,3,'Current Stock','/inventory',1,1),(22,3,'Movement History','/inventory/movement-history',5,NULL),(23,3,'Adjust Stock','/inventory/adjust-stock',2,1),(24,3,'Transfer Stock','/inventory/transfer-stock',4,NULL),(25,3,'Reorder Stock','/inventory/reorder-stock',3,1),(26,4,'New Purchase Order','/purchase-order/add',1,1),(27,4,'Purchase Order List','/purchase-order',2,1),(28,4,'Unreceived Orders','/purchase-order/unrecived-orders',3,NULL),(29,4,'Unpaid Orders','/purchase-order/unpaid-orders',4,NULL),(30,4,'Recent Orders','/purchase-order/recent-orders',5,NULL),(31,5,'New Sales Quote','/sales-quote/add',1,0),(32,5,'Sales Quote List','/sales-quote',2,1),(33,5,'New Sales Order','3',3,NULL),(34,5,'Sales Order List','4',4,1),(35,5,'Unfulfilled Orders','5',5,NULL),(36,5,'Unpaid Orders','6',6,NULL),(37,5,'Uninvoiced Orders','7',7,NULL),(38,5,'Recent Orders','8',8,NULL),(39,7,'Add New User','/users/add',1,NULL),(40,7,'Users List','/users',2,NULL),(41,7,'Manage Roles',NULL,3,NULL),(42,9,'Customers List','/customer',1,1),(43,9,'Add Customer','/customer/add',2,1),(44,8,'Vendor List','/vendor',1,1),(45,8,'Add Vendor','/vendor/add',2,1);

/*Table structure for table `user_menutype_m` */

DROP TABLE IF EXISTS `user_menutype_m`;

CREATE TABLE `user_menutype_m` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `home_menu` tinyint(4) DEFAULT NULL,
  `footer_menu` tinyint(4) DEFAULT NULL,
  `menu_class` varchar(50) DEFAULT NULL,
  `home_sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `user_menutype_m` */

insert  into `user_menutype_m`(`id`,`type`,`sequence`,`home_menu`,`footer_menu`,`menu_class`,`home_sequence`) values (1,'Referances',2,0,1,NULL,NULL),(2,'General',1,0,1,NULL,NULL),(3,'Inventory',3,1,1,'inventory',3),(4,'Purchase',4,1,1,'purchase',2),(5,'Sales',5,1,1,'sales',4),(6,'Reports',6,0,1,NULL,NULL),(7,'Users',7,0,1,NULL,NULL),(8,'Vendor',8,1,0,'vendor',1),(9,'Customer',9,1,0,'customer',5),(10,'Options',10,0,0,NULL,NULL);

/*Table structure for table `user_role_m` */

DROP TABLE IF EXISTS `user_role_m`;

CREATE TABLE `user_role_m` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `user_role_m` */

insert  into `user_role_m`(`id`,`role_name`,`updated`,`updated_by`,`created`,`created_by`) values (1,'Admin','2015-01-27 12:59:02',1,NULL,NULL),(2,'Sub Admin','2015-01-27 12:32:55',1,'2015-01-27 12:32:55',1);

/*Table structure for table `user_roles_rights` */

DROP TABLE IF EXISTS `user_roles_rights`;

CREATE TABLE `user_roles_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `screen_id` int(11) DEFAULT NULL,
  `view` varchar(50) DEFAULT '1',
  `add` varchar(50) DEFAULT '1',
  `edit` varchar(50) DEFAULT '1',
  `delete` varchar(50) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=708 DEFAULT CHARSET=latin1;

/*Data for the table `user_roles_rights` */

insert  into `user_roles_rights`(`id`,`role_id`,`screen_id`,`view`,`add`,`edit`,`delete`) values (622,1,1,'1','1','1','1'),(623,1,2,'1','1','1','1'),(624,1,3,'1','1','1','1'),(625,1,4,'1','1','1','1'),(626,1,5,'1','1','1','1'),(627,1,6,'1','0','0','0'),(628,1,7,'1','0','0','0'),(629,1,8,'1','0','0','0'),(630,1,9,'1','0','0','0'),(631,1,10,'1','0','0','0'),(632,1,11,'1','0','0','0'),(633,1,12,'1','0','0','0'),(634,1,13,'1','0','0','0'),(635,1,14,'1','0','0','0'),(636,1,15,'1','0','0','0'),(637,1,16,'1','0','0','0'),(638,1,17,'1','0','0','0'),(639,1,18,'1','0','0','0'),(640,1,19,'1','0','0','0'),(641,1,20,'1','0','0','0'),(642,1,21,'1','0','0','0'),(643,1,22,'1','0','0','0'),(644,1,23,'1','0','0','0'),(645,1,24,'1','0','0','0'),(646,1,25,'1','0','0','0'),(647,1,26,'1','0','0','0'),(648,1,27,'1','0','0','0'),(649,1,28,'1','0','0','0'),(650,1,29,'1','0','0','0'),(651,1,30,'1','0','0','0'),(652,1,31,'1','0','0','0'),(653,1,32,'1','0','0','0'),(654,1,33,'1','0','0','0'),(655,1,34,'1','0','0','0'),(656,1,35,'1','0','0','0'),(657,1,36,'1','0','0','0'),(658,1,37,'1','0','0','0'),(659,1,38,'1','0','0','0'),(660,1,39,'1','0','0','0'),(661,1,40,'1','0','0','0'),(662,1,41,'1','0','0','0'),(663,2,1,'1','1','0','0'),(664,2,2,'1','1','0','0'),(665,2,3,'1','1','0','0'),(666,2,4,'0','0','0','0'),(667,2,5,'0','0','0','0'),(668,2,6,'0','0','0','0'),(669,2,7,'0','0','0','0'),(670,2,8,'0','0','0','0'),(671,2,9,'0','0','0','0'),(672,2,10,'0','0','0','0'),(673,2,11,'0','0','0','0'),(674,2,12,'0','0','0','0'),(675,2,13,'0','0','0','0'),(676,2,14,'0','0','0','0'),(677,2,15,'0','0','0','0'),(678,2,16,'0','0','0','0'),(679,2,17,'0','0','0','0'),(680,2,18,'0','0','0','0'),(681,2,19,'0','0','0','0'),(682,2,20,'0','0','0','0'),(683,2,21,'0','0','0','0'),(684,2,22,'0','0','0','0'),(685,2,23,'0','0','0','0'),(686,2,24,'0','0','0','0'),(687,2,25,'0','0','0','0'),(688,2,26,'0','0','0','0'),(689,2,27,'0','0','0','0'),(690,2,28,'0','0','0','0'),(691,2,29,'0','0','0','0'),(692,2,30,'0','0','0','0'),(693,2,31,'0','0','0','0'),(694,2,32,'0','0','0','0'),(695,2,33,'0','0','0','0'),(696,2,34,'0','0','0','0'),(697,2,35,'0','0','0','0'),(698,2,36,'0','0','0','0'),(699,2,37,'0','0','0','0'),(700,2,38,'0','0','0','0'),(701,2,39,'0','0','0','0'),(702,2,40,'0','0','0','0'),(703,2,41,'0','0','0','0'),(704,1,42,'1','1','1','1'),(705,1,43,'1','1','1','1'),(706,1,44,'1','1','1','1'),(707,1,45,'1','1','1','1');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=cp866;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`name`,`email`,`phone`,`role_id`,`is_active`,`updated`,`updated_by`,`created`,`created_by`) values (1,'admin','202cb962ac59075b964b07152d234b70','Administrator','admin@bluechip.com','9718996066','1',1,NULL,NULL,NULL,NULL),(28,'Amitk','202cb962ac59075b964b07152d234b70','Amit kumar','amitk@damcogroup.com','9718996066','1',1,'2015-01-27 05:33:54',1,'2015-01-27 05:33:54',1),(29,'Vivekt','202cb962ac59075b964b07152d234b70','Vivek Tiwari','amitk@damcogroup.com','989898989','1',1,'2015-01-27 05:37:23',1,'2015-01-27 05:37:23',1),(34,'Amitk1','202cb962ac59075b964b07152d234b70','Ankit K','amitk@damcogroup.com','222222','1',1,'2015-01-29 11:46:09',1,'2015-01-27 05:58:09',1),(36,'Ramk','202cb962ac59075b964b07152d234b70','Ram kumar Singh','ramk@gg.com','98589585258','1',1,'2015-02-06 05:59:15',1,'2015-01-27 06:11:29',1);

/*Table structure for table `sys_role_mv` */

DROP TABLE IF EXISTS `sys_role_mv`;

/*!50001 DROP VIEW IF EXISTS `sys_role_mv` */;
/*!50001 DROP TABLE IF EXISTS `sys_role_mv` */;

/*!50001 CREATE TABLE  `sys_role_mv`(
 `id` int(11) ,
 `name` varchar(255) 
)*/;

/*View structure for view sys_role_mv */

/*!50001 DROP TABLE IF EXISTS `sys_role_mv` */;
/*!50001 DROP VIEW IF EXISTS `sys_role_mv` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`amit`@`%` SQL SECURITY DEFINER VIEW `sys_role_mv` AS select `t`.`id` AS `id`,`t`.`role_name` AS `name` from `user_role_m` `t` order by `t`.`role_name` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
