/*Table structure for table `company_details` */

DROP TABLE IF EXISTS `company_details`;

CREATE TABLE `company_details` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `logo` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `address` text,
  `city` varchar(255) default NULL,
  `state` varchar(255) default NULL,
  `country` varchar(255) default NULL,
  `zip_code` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `fax` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `website` varchar(255) default NULL,
  `misc_info` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp866;

/*Table structure for table `inventory_work_order` */

DROP TABLE IF EXISTS `inventory_work_order`;

CREATE TABLE `inventory_work_order` (
  `id` int(11) NOT NULL auto_increment,
  `assembled_by` varchar(255) default NULL,
  `inventory_location_id` int(11) NOT NULL,
  `other_cost` decimal(20,2) default NULL,
  `order_date` datetime default NULL,
  `finshed_date` datetime default NULL,
  `status` enum('open','completed') default NULL,
  `inventory_product_id` int(11) NOT NULL,
  `item` varchar(255) default NULL,
  `quantity` int(11) NOT NULL,
  `component_structure` varchar(255) default NULL,
  `remarks` text,
  `pick_item` varchar(255) default NULL,
  `pick_quantity` int(11) NOT NULL,
  `pick_location_id` int(11) NOT NULL,
  `pick_remarks` varchar(255) default NULL,
  `put_away_item` varchar(255) default NULL,
  `put_away_quantity` int(11) default NULL,
  `put_away_location_id` int(11) default NULL,
  `put_away_remarks` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `lookup` */

DROP TABLE IF EXISTS `lookup`;

CREATE TABLE `lookup` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) default NULL,
  `code` varchar(50) default NULL,
  `meaning` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_address_type` */

DROP TABLE IF EXISTS `mas_address_type`;

CREATE TABLE `mas_address_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_card_type` */

DROP TABLE IF EXISTS `mas_card_type`;

CREATE TABLE `mas_card_type` (
  `id` int(11) NOT NULL auto_increment,
  `card_type` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_carriers` */

DROP TABLE IF EXISTS `mas_carriers`;

CREATE TABLE `mas_carriers` (
  `id` int(11) NOT NULL auto_increment,
  `carrier_name` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_cities` */

DROP TABLE IF EXISTS `mas_cities`;

CREATE TABLE `mas_cities` (
  `id` int(11) NOT NULL auto_increment,
  `city_name` varchar(255) default NULL,
  `state_id` int(11) default NULL,
  `country_id` int(11) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_countries` */

DROP TABLE IF EXISTS `mas_countries`;

CREATE TABLE `mas_countries` (
  `id` int(11) NOT NULL auto_increment,
  `country_name` varchar(255) default NULL,
  `country_code` varchar(50) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_currency` */

DROP TABLE IF EXISTS `mas_currency`;

CREATE TABLE `mas_currency` (
  `id` int(11) NOT NULL auto_increment,
  `currency_name` varchar(64) default NULL,
  `currency_code` char(3) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `mas_customers` */

DROP TABLE IF EXISTS `mas_customers`;

CREATE TABLE `mas_customers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `balance` decimal(20,2) default '0.00',
  `credit` decimal(20,2) default '0.00',
  `address_type` varchar(50) default NULL,
  `address` varchar(255) default NULL,
  `city_id` int(11) default NULL,
  `zip_code` varchar(50) default NULL,
  `contact_name` varchar(255) default NULL,
  `contact_phone` varchar(50) default NULL,
  `contact_fax` varchar(50) default NULL,
  `contact_email` varchar(50) default NULL,
  `contact_website` varchar(50) default NULL,
  `pricing_currency_id` int(11) default NULL,
  `discount` decimal(20,2) default NULL,
  `payment_terms` int(11) default NULL,
  `taxing_schemes` int(11) default NULL,
  `tax_exempt` decimal(20,2) default NULL,
  `default_location_id` int(11) default NULL,
  `sales_reps` int(11) default NULL,
  `carrier_id` int(11) default NULL,
  `payment_method` int(11) default NULL,
  `card_type` int(11) default NULL,
  `card_number` bigint(20) default NULL,
  `card_expiry` varchar(50) default NULL,
  `card_security_code` int(11) default NULL,
  `is_active` tinyint(1) default '1',
  `remarks` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_employee` */

DROP TABLE IF EXISTS `mas_employee`;

CREATE TABLE `mas_employee` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_inventory_location` */

DROP TABLE IF EXISTS `mas_inventory_location`;

CREATE TABLE `mas_inventory_location` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_payment_method` */

DROP TABLE IF EXISTS `mas_payment_method`;

CREATE TABLE `mas_payment_method` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_payment_terms` */

DROP TABLE IF EXISTS `mas_payment_terms`;

CREATE TABLE `mas_payment_terms` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `days` int(11) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_pricing_currency` */

DROP TABLE IF EXISTS `mas_pricing_currency`;

CREATE TABLE `mas_pricing_currency` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `currency_name` varchar(255) default NULL,
  `default` tinyint(4) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_product_category` */

DROP TABLE IF EXISTS `mas_product_category`;

CREATE TABLE `mas_product_category` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `parent_id` int(11) default '0',
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `mas_product_type` */

DROP TABLE IF EXISTS `mas_product_type`;

CREATE TABLE `mas_product_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_receiving_addresses` */

DROP TABLE IF EXISTS `mas_receiving_addresses`;

CREATE TABLE `mas_receiving_addresses` (
  `id` int(11) NOT NULL auto_increment,
  `address_name` varchar(255) NOT NULL,
  `street` varchar(512) default NULL,
  `city_id` bigint(20) default NULL,
  `zip_code` varchar(50) default NULL,
  `remarks` text,
  `address_type` varchar(50) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_sales_reps` */

DROP TABLE IF EXISTS `mas_sales_reps`;

CREATE TABLE `mas_sales_reps` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_states` */

DROP TABLE IF EXISTS `mas_states`;

CREATE TABLE `mas_states` (
  `id` int(11) NOT NULL auto_increment,
  `state_name` varchar(255) default NULL,
  `country_id` int(11) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_taxing_schemes` */

DROP TABLE IF EXISTS `mas_taxing_schemes`;

CREATE TABLE `mas_taxing_schemes` (
  `id` int(11) NOT NULL auto_increment,
  `tax_scheme_name` varchar(255) default NULL,
  `tax_name` varchar(255) default NULL,
  `tax_rate` decimal(20,2) default '0.00',
  `tax_on_shipping` tinyint(4) default NULL,
  `show_secondary_tax_rate` tinyint(4) default NULL,
  `secondary_tax_name` varchar(255) default NULL,
  `secondary_tax_rate` decimal(20,2) default '0.00',
  `secondary_tax_on_shipping` tinyint(4) default NULL,
  `compound_secondary` tinyint(4) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_units` */

DROP TABLE IF EXISTS `mas_units`;

CREATE TABLE `mas_units` (
  `id` int(11) NOT NULL auto_increment,
  `unit_name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_vendors` */

DROP TABLE IF EXISTS `mas_vendors`;

CREATE TABLE `mas_vendors` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `balance` decimal(20,2) default '0.00',
  `credit` decimal(20,2) default '0.00',
  `address` text,
  `city_id` int(11) default NULL,
  `zip_code` varchar(50) default NULL,
  `address_type` varchar(255) default NULL,
  `contact_name` varchar(255) default NULL,
  `contact_phone` varchar(50) default NULL,
  `contact_fax` varchar(50) default NULL,
  `contact_email` varchar(50) default NULL,
  `contact_website` varchar(50) default NULL,
  `payment_terms` int(11) default NULL,
  `taxing_schemes` int(11) default NULL,
  `carrier_id` int(11) default NULL,
  `currency_id` int(11) default NULL,
  `is_active` tinyint(1) default '1',
  `remarks` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `mas_vendors_product` */

DROP TABLE IF EXISTS `mas_vendors_product`;

CREATE TABLE `mas_vendors_product` (
  `id` int(11) NOT NULL auto_increment,
  `vendor_id` int(11) default NULL,
  `product_id` int(11) default NULL,
  `vendor_product_code` varchar(50) default NULL,
  `cost` double(20,2) default '0.00',
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `session` */

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `modified` int(11) default NULL,
  `lifetime` int(11) default NULL,
  `data` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tra_emp_timesheet` */

DROP TABLE IF EXISTS `tra_emp_timesheet`;

CREATE TABLE `tra_emp_timesheet` (
  `id` int(11) NOT NULL auto_increment,
  `emp_id` int(11) default NULL,
  `timesheet_date` date default NULL,
  `job_no` int(11) default NULL,
  `hourly_rate` double(20,2) default NULL,
  `total_ord` double(20,2) default NULL,
  `total_1_5` double(20,2) default NULL,
  `total_2_0` double(20,2) default NULL,
  `sub_total` double(20,2) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_adjust_stock` */

DROP TABLE IF EXISTS `tra_inventory_adjust_stock`;

CREATE TABLE `tra_inventory_adjust_stock` (
  `id` int(11) NOT NULL auto_increment,
  `inventory_product_id` int(11) default NULL,
  `new_quantity` int(11) NOT NULL,
  `old_quantity` int(11) NOT NULL,
  `difference` int(11) NOT NULL,
  `inventory_date` date default NULL,
  `status` enum('Unsaved','Saved','Completed','Cancelled') default NULL,
  `inventory_location_id` int(11) default NULL,
  `remarks` text,
  `updated` datetime default NULL,
  `updated_by` int(11) NOT NULL,
  `created` datetime default NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_product` */

DROP TABLE IF EXISTS `tra_inventory_product`;

CREATE TABLE `tra_inventory_product` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(255) default NULL,
  `product_name` varchar(255) default NULL,
  `product_type_id` int(11) default NULL,
  `product_category_id` int(11) default NULL,
  `product_image` varchar(255) default NULL,
  `bar_code` varchar(255) default NULL,
  `reorder_point` varchar(255) default NULL,
  `reorder_quantity` int(11) default NULL,
  `storage_location` int(11) default NULL,
  `last_vendor_id` int(11) default NULL,
  `length` decimal(20,2) default '0.00',
  `width` decimal(20,2) default '0.00',
  `height` decimal(20,2) default '0.00',
  `weight` decimal(20,2) default '0.00',
  `standard_uom_id` int(11) default NULL,
  `sales_uom_id` int(11) default NULL,
  `purchasing_uom_id` int(11) default NULL,
  `remarks` text,
  `is_active` tinyint(4) default '1',
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_product_bill_of_materials` */

DROP TABLE IF EXISTS `tra_inventory_product_bill_of_materials`;

CREATE TABLE `tra_inventory_product_bill_of_materials` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `raw_product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `cost` double(20,2) default '0.00',
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_product_cost` */

DROP TABLE IF EXISTS `tra_inventory_product_cost`;

CREATE TABLE `tra_inventory_product_cost` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `price_currency_id` int(11) default NULL,
  `price` decimal(20,2) default '0.00',
  `markup` decimal(20,2) default '0.00',
  `cost` decimal(20,2) default '0.00',
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_product_quantity` */

DROP TABLE IF EXISTS `tra_inventory_product_quantity`;

CREATE TABLE `tra_inventory_product_quantity` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `location_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `unit_id` int(11) default NULL,
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_product_vendors` */

DROP TABLE IF EXISTS `tra_inventory_product_vendors`;

CREATE TABLE `tra_inventory_product_vendors` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `vendor_id` int(11) default NULL,
  `price` decimal(20,2) default '0.00',
  `vendor_product_code` varchar(50) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_reorder_stock` */

DROP TABLE IF EXISTS `tra_inventory_reorder_stock`;

CREATE TABLE `tra_inventory_reorder_stock` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `quantity_available` int(11) default NULL,
  `reorder_point` int(11) default NULL,
  `reorder_quantity` int(11) default NULL,
  `vendor_id` int(11) default NULL,
  `reorder_status` enum('out of stock','bill of materials','Reordered') default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_inventory_transfer_stock` */

DROP TABLE IF EXISTS `tra_inventory_transfer_stock`;

CREATE TABLE `tra_inventory_transfer_stock` (
  `id` int(11) NOT NULL auto_increment,
  `inventory_product_id` int(11) default NULL,
  `from_location_id` int(11) NOT NULL,
  `to_location_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` enum('Open','In Transit','Completed') default NULL,
  `transfer_date` date default NULL,
  `send_date` date default NULL,
  `received_date` date default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) NOT NULL,
  `created` datetime default NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_job_card` */

DROP TABLE IF EXISTS `tra_job_card`;

CREATE TABLE `tra_job_card` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tra_movement_history` */

DROP TABLE IF EXISTS `tra_movement_history`;

CREATE TABLE `tra_movement_history` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) default NULL,
  `type` varchar(50) default NULL,
  `movement_date` date default NULL,
  `location` int(11) default NULL,
  `order_id` varchar(11) default NULL,
  `qty_before` int(11) default NULL,
  `qty` int(11) default NULL,
  `qty_after` int(11) default NULL,
  `total_amount` double(20,2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_order_payments` */

DROP TABLE IF EXISTS `tra_order_payments`;

CREATE TABLE `tra_order_payments` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) default NULL,
  `order_type` enum('Sales','Purchase') default NULL,
  `payment_date` datetime default NULL,
  `payment_type` varchar(255) default NULL,
  `payment_method_id` int(11) default NULL,
  `payment_ref` varchar(255) default NULL,
  `payment_remarks` varchar(500) default NULL,
  `amount` decimal(20,2) default '0.00',
  `currency_id` int(11) default NULL,
  `sessionid` varchar(50) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_purchase_order_det` */

DROP TABLE IF EXISTS `tra_purchase_order_det`;

CREATE TABLE `tra_purchase_order_det` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) default NULL,
  `item_status` enum('Order','Receive','Return','Unstock') default NULL,
  `product_id` int(11) default NULL,
  `vendor_product_code` varchar(255) default NULL,
  `quantity` int(11) unsigned default NULL,
  `unit_price` int(11) default NULL,
  `discount` decimal(20,2) default NULL,
  `sub_total` decimal(20,2) default NULL,
  `transaction_date` date default NULL,
  `location_id` int(11) default NULL,
  `parant_item_id` int(11) default NULL,
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_purchase_order_mas` */

DROP TABLE IF EXISTS `tra_purchase_order_mas`;

CREATE TABLE `tra_purchase_order_mas` (
  `id` int(11) NOT NULL auto_increment,
  `po_id` varchar(50) default NULL,
  `po_date` date default NULL,
  `order_status` enum('Unfulfilled','Fulfilled','Started','Cancelled') NOT NULL,
  `payment_status` enum('Paid','Unpaid','Owing','Partial') default NULL,
  `vendor_id` int(11) default NULL,
  `vendor_contact` varchar(50) default NULL,
  `vendor_phone` varchar(50) default NULL,
  `vendor_address` text,
  `address_type` varchar(50) default NULL,
  `location_id` int(11) default NULL,
  `req_shipping_date` date default NULL,
  `terms_id` int(11) default NULL,
  `v_order` varchar(255) default NULL,
  `ship_req` tinyint(4) default NULL,
  `ship_to_address` text,
  `carrier_id` int(11) default NULL,
  `due_date` date default NULL,
  `non_vendor_costs` decimal(20,2) default '0.00',
  `currency_id` int(11) default NULL,
  `sub_total` decimal(20,2) default '0.00',
  `freight` decimal(20,2) default '0.00',
  `taxing_scheme_id` int(11) default NULL,
  `tax1_name` varchar(50) default NULL,
  `tax1_per` decimal(10,2) default '0.00',
  `tax1_amount` decimal(20,2) default '0.00',
  `tax_on_shipping` tinyint(4) default NULL,
  `tax2_name` varchar(50) default NULL,
  `tax2_per` decimal(10,2) default '0.00',
  `tax2_amount` decimal(20,2) default '0.00',
  `secondary_tax_on_shipping` tinyint(4) default NULL,
  `compound_secondary` tinyint(4) default NULL,
  `total_amount` decimal(20,2) default '0.00',
  `paid_amount` decimal(20,2) default '0.00',
  `balance` decimal(20,2) default '0.00',
  `remarks` text,
  `receive_remark` text,
  `return_remark` text,
  `unstock_remark` text,
  `ret_subtotal` decimal(20,2) default '0.00',
  `ret_fee` decimal(20,2) default '0.00',
  `ret_total` decimal(20,2) default '0.00',
  `last_pay_date` date default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`,`order_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_sales_order_det` */

DROP TABLE IF EXISTS `tra_sales_order_det`;

CREATE TABLE `tra_sales_order_det` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) default NULL,
  `item_status` enum('Fulfilled','Returned','Discarded','Restocked','Order') default NULL,
  `product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `unit_price` int(11) default NULL,
  `discount` decimal(20,2) default NULL,
  `sub_total` decimal(20,2) default NULL,
  `location_id` int(11) default NULL,
  `box_number` int(20) default NULL,
  `return_date` date default NULL,
  `discarded` tinyint(4) default NULL,
  `restock_date` date default NULL,
  `transaction_date` date default NULL,
  `sessionid` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_sales_order_mas` */

DROP TABLE IF EXISTS `tra_sales_order_mas`;

CREATE TABLE `tra_sales_order_mas` (
  `id` int(11) NOT NULL auto_increment,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(255) default NULL,
  `customer_address` varchar(255) default NULL,
  `customer_phone` bigint(20) default NULL,
  `order_id` varchar(50) default NULL,
  `order_date` datetime default NULL,
  `sales_rep_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `order_status` enum('Unfulfilled','Fulfilled','Started') default NULL,
  `payment_status` enum('Invoiced','Uninvoiced','Paid','Partial') default NULL,
  `terms_id` int(11) default NULL,
  `po_ref` varchar(255) default NULL,
  `ship_req` tinyint(4) default NULL,
  `shipping_address` varchar(500) default NULL,
  `non_customer_costs` varchar(255) default NULL,
  `due_date` date default NULL,
  `taxing_scheme_id` int(11) default NULL,
  `tax1_name` varchar(50) default NULL,
  `tax1_per` decimal(10,2) default '0.00',
  `tax1_amount` decimal(20,2) default '0.00',
  `tax2_name` varchar(50) default NULL,
  `tax2_per` decimal(10,2) default '0.00',
  `tax2_amount` decimal(20,2) default '0.00',
  `pricing_currency_id` int(11) default NULL,
  `req_ship_date` date default NULL,
  `sub_total` decimal(20,2) default '0.00',
  `freight` decimal(20,2) default '0.00',
  `total_amount` decimal(20,2) default '0.00',
  `paid_amount` decimal(20,2) default '0.00',
  `balance` decimal(20,2) default '0.00',
  `last_pay_date` datetime default NULL,
  `remarks` text,
  `fulfilled_remarks` text,
  `return_remarks` text,
  `restock_remarks` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_sales_quote_order_det` */

DROP TABLE IF EXISTS `tra_sales_quote_order_det`;

CREATE TABLE `tra_sales_quote_order_det` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) default NULL,
  `item_status` enum('Quote') default NULL,
  `product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `unit_price` int(11) default NULL,
  `discount` decimal(20,2) default NULL,
  `sub_total` decimal(20,2) default NULL,
  `sessionid` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_sales_quote_order_mas` */

DROP TABLE IF EXISTS `tra_sales_quote_order_mas`;

CREATE TABLE `tra_sales_quote_order_mas` (
  `id` int(11) NOT NULL auto_increment,
  `customer_id` int(11) NOT NULL,
  `customer_address` varchar(500) default NULL,
  `customer_phone` bigint(20) default NULL,
  `order_id` varchar(50) default NULL,
  `order_date` datetime default NULL,
  `sales_rep_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `order_status` enum('quote','order') default NULL,
  `payment_status` enum('Invoiced','Uninvoiced','Paid') default NULL,
  `terms_id` int(11) default NULL,
  `po_ref` varchar(255) default NULL,
  `shipping_address` varchar(500) default NULL,
  `non_customer_costs` varchar(255) default NULL,
  `due_date` date default NULL,
  `taxing_scheme_id` int(11) default NULL,
  `tax1_name` varchar(50) default NULL,
  `tax1_per` decimal(10,2) default '0.00',
  `tax1_amount` decimal(20,2) default '0.00',
  `tax2_name` varchar(50) default NULL,
  `tax2_per` decimal(10,2) default '0.00',
  `tax2_amount` decimal(20,2) default '0.00',
  `pricing_currency_id` int(11) default NULL,
  `req_ship_date` date default NULL,
  `sub_total` decimal(20,2) default '0.00',
  `freight` decimal(20,2) default '0.00',
  `total_amount` decimal(20,2) default '0.00',
  `paid_amount` decimal(20,2) default '0.00',
  `remarks` text,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_menu_m` */

DROP TABLE IF EXISTS `user_menu_m`;

CREATE TABLE `user_menu_m` (
  `id` int(11) NOT NULL auto_increment,
  `menu_type_id` int(11) default NULL,
  `display_name` varchar(255) default NULL,
  `screen_name` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `Sequence` int(11) default NULL,
  `home_menu` tinyint(4) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_menutype_m` */

DROP TABLE IF EXISTS `user_menutype_m`;

CREATE TABLE `user_menutype_m` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) default NULL,
  `sequence` int(11) default NULL,
  `home_menu` tinyint(4) default NULL,
  `footer_menu` tinyint(4) default NULL,
  `menu_class` varchar(50) default NULL,
  `home_sequence` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_role_m` */

DROP TABLE IF EXISTS `user_role_m`;

CREATE TABLE `user_role_m` (
  `id` int(11) NOT NULL auto_increment,
  `role_name` varchar(255) NOT NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_roles_rights` */

DROP TABLE IF EXISTS `user_roles_rights`;

CREATE TABLE `user_roles_rights` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` int(11) default NULL,
  `screen_id` int(11) default NULL,
  `view` varchar(50) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `name` varchar(50) default NULL,
  `email` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `role_id` varchar(255) default NULL,
  `is_active` tinyint(4) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp866;

/*Table structure for table `tra_work_order_assign` */

DROP TABLE IF EXISTS `tra_work_order_assign`;

CREATE TABLE `tra_work_order_assign` (
  `id` int(11) NOT NULL auto_increment,
  `wo_bom_id` int(11) default NULL,
  `assign_to` int(11) default NULL,
  `start_on` datetime default NULL,
  `end_on` datetime default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_work_order_bom` */

DROP TABLE IF EXISTS `tra_work_order_bom`;

CREATE TABLE `tra_work_order_bom` (
  `id` int(11) NOT NULL auto_increment,
  `wo_product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `scheduled_date` date default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_work_order_det` */

DROP TABLE IF EXISTS `tra_work_order_det`;

CREATE TABLE `tra_work_order_det` (
  `id` int(11) NOT NULL auto_increment,
  `work_order_id` int(11) default NULL,
  `product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `description` text,
  `date_scheduled` date default NULL,
  `product_status` enum('Pending','Entered','Started','Fulfilled') default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tra_work_order_mas` */

DROP TABLE IF EXISTS `tra_work_order_mas`;

CREATE TABLE `tra_work_order_mas` (
  `id` int(11) NOT NULL auto_increment,
  `work_order_no` varchar(255) default NULL,
  `scheduled_date` date default NULL,
  `location_id` int(11) default NULL,
  `order_type` varchar(50) default NULL,
  `order_status` enum('Entered','Issued','Started','Fulfilled') default NULL,
  `udpated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/*Table structure for table `tra_work_order_bom` */

DROP TABLE IF EXISTS `tra_work_order_bom`;

CREATE TABLE `tra_work_order_bom` (
  `id` int(11) NOT NULL auto_increment,
  `wo_product_id` int(11) default NULL,
  `bom_product_id` int(11) default NULL,
  `work_status` enum('Entered','Started','Committed','Finished') default NULL,
  `quantity` int(11) default NULL,
  `quantity_used` int(11) default '0',
  `start_on` datetime default NULL,
  `end_on` datetime default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tra_work_order_det` */

DROP TABLE IF EXISTS `tra_work_order_det`;

CREATE TABLE `tra_work_order_det` (
  `id` int(11) NOT NULL auto_increment,
  `work_order_id` int(11) default NULL,
  `product_id` int(11) default NULL,
  `quantity` int(11) default NULL,
  `description` text,
  `date_scheduled` date default NULL,
  `product_status` enum('Pending','Entered','Started','Fulfilled') default NULL,
  `sessionid` varchar(255) default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `tra_work_order_mas` */

DROP TABLE IF EXISTS `tra_work_order_mas`;

CREATE TABLE `tra_work_order_mas` (
  `id` int(11) NOT NULL auto_increment,
  `work_order_no` varchar(255) default NULL,
  `scheduled_date` date default NULL,
  `location_id` int(11) default NULL,
  `order_type` varchar(50) default NULL,
  `order_status` enum('Entered','Issued','Started','Partial','Fulfilled') default NULL,
  `remarks` text,
  `date_issued` date default NULL,
  `date_fulfilled` date default NULL,
  `updated` datetime default NULL,
  `updated_by` int(11) default NULL,
  `created` datetime default NULL,
  `created_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `mas_top_list` */
DROP TABLE IF EXISTS `mas_top_list`;

CREATE TABLE `mas_top_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
